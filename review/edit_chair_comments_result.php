<?php 

/*
 * Copyright (C) 2006, 2007 Thomas Baigneres, Matthieu Finiasz
 *
 * This file is part of iChair.
 *
 * iChair is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 * 
 * iChair is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

?><?php 
$page_title ='Cập Nhật Bình Luận';//Comment Update';
include '../utils/tools.php';
include 'header.php';

if($currentReviewerGroup == Reviewer::$CHAIR_GROUP) {

$submissionNumber = Tools::readPost('submissionNumber');
$versionNumber = Tools::readPost('versionNumber');
$from = Tools::readPost('from');   

$submission = Submission::getBySubmissionNumber($submissionNumber);

if (!is_null($submission)) {
  Log::logEditSubmissionComments($submission,$currentReviewer);
  $versions = $submission->getAllVersions();
  $version = $versions[$versionNumber];
  $version->setChairComments(Tools::readPost('chairComments'));
  print('<div class="OKmessage">');
  print("Cập nhật bình luận thành công."//Your comment was updated successfully."
    );
  print('</div>');

  if ($from != "submissions") {
    print("<form action=\"examine_versions.php\" method=\"post\">\n");
    print("<input type=\"hidden\" name=\"submissionNumber\" value=\"".$submissionNumber."\" />\n");
  } else {
    print("<form action=\"examine.php#nbr".$submissionNumber."\" method=\"post\">\n");
  }
?>

<div class="floatRight">
<input type="submit" class="buttonLink bigButton" value="Đồng ý">
</div>

</form>

<?php }}?>

<?php include('footer.php'); ?>
