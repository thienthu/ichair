<?php 

/*
 * Copyright (C) 2006, 2007 Thomas Baigneres, Matthieu Finiasz
 *
 * This file is part of iChair.
 *
 * iChair is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 * 
 * iChair is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

?><?php $page_title='Phân Công Tự Động';//'Automatic Assignation';
include '../utils/tools.php';
include 'header.php';
if($currentReviewerGroup == Reviewer::$CHAIR_GROUP) {
?>
<div class="OKmessage">
Có muốn ghi đè lên bài viết đã phân công đề xuất đã có trước đó không? 
   <!--Do you really want to overwrite any existing article assignation with those suggested? 
   Note that if you chose to keep previous assignations, they already have been taken 
   into account in the suggestion.-->
</div>
<center>
<table>
  <tr>
    <td>
    <form action="automatic_assignation.php" method="post">
      <input type="submit" class="buttonLink bigButton" value="Trở về"/>
    </form>
    </td>
    <td>
    <form action="automatic_assignation_result.php" method="post">
      <input type="submit" class="buttonLink bigButton" value="Đồng ý"/>
    </form>
    </td>
  </tr>
</table>    
</center>






<?php } ?>

<?php include('footer.php'); ?>
