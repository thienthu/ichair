<?php 

/*
 * Copyright (C) 2006, 2007 Thomas Baigneres, Matthieu Finiasz
 *
 * This file is part of iChair.
 *
 * iChair is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 * 
 * iChair is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

?><?php 
$page_title ="Gỡ Bỏ Bài Viết";//'Remove Article from Database';
include '../utils/tools.php';
include "header.php";

if($currentReviewerGroup == Reviewer::$CHAIR_GROUP) {

$article = Article::getByArticleNumber(Tools::readPost('articleNumber'));

if(!is_null($article)) {

  Assignement::deleteAllArticleAssignements($article->getArticleNumber());
  Meeting::trashAllArticleMessages($article->getArticleNumber());
  $bakFolder = substr($article->getFolder(),0,strlen($article->getFolder()) -1);
  while (file_exists($bakFolder)) {
    $bakFolder.=".bak";
  }
  rename($article->getFolder(), $bakFolder);
  Article::removeFromDB($article->getArticleNumber());
  Log::logDeleteArticle($article, $currentReviewer);
  
?>
<div class="OKmessage">
Bài viết<!--The article--> "<?php Tools::printHTML($article->getTitle()); ?>" đã được gỡ bỏ thành công.<!--has been successfully removed from the database.-->
</div> 
<form action="transfer.php" method="post">
  <div class="floatRight">
    <input type="submit" class="buttonLink bigButton" value="Đồng ý" />
  </div>
</form>

<?php 
}}
include('footer.php'); ?>
