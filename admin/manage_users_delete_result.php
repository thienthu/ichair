<?php 

/*
 * Copyright (C) 2006, 2007 Thomas Baigneres, Matthieu Finiasz
 *
 * This file is part of iChair.
 *
 * iChair is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 * 
 * iChair is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

?><?php 
$page_title ="Xóa Người Dùng";//"Delete User";
include '../utils/tools.php';
include "header.php";

$reviewer = Reviewer::getByReviewerNumber(Tools::readPost('reviewerNumber'));

if(!is_null($reviewer)) {

  /* move all possible reviews to the archive directory */
  $articles = Article::getAllArticles();
  foreach ($articles as $article) {
    $assignement = Assignement::getByNumbers($article->getArticleNumber(), $reviewer->getReviewerNumber());
    if (!is_null($assignement)) { 
      $file = $article->getFolder() . $assignement->getXMLFileName();
    
      if (file_exists($file)) {
        $reviewDocument = new DOMDocument('1.0');
        $reviewDocument->load($file);
      
        $domxPath = new DOMXpath($reviewDocument);
        $domNode = $domxPath->query("/xml/date")->item(0);
        $oldReviewNewFileName = $article->getFolder() . "archive/" . sprintf("review%04d-", $reviewer->getReviewerNumber()) . utf8_decode($domNode->firstChild->nodeValue) .".xml";
        rename($file, $oldReviewNewFileName);
      }
    }
  }
  
  Assignement::deleteAllReviewerAssignements($reviewer->getReviewerNumber());
  Reviewer::removeFromDB(Tools::readPost('reviewerNumber'));
  Article::computeAverages();
  Log::logDeleteUser($reviewer);
  Reviewer::writeHtpasswdFile(); 
  
?>
<div class="OKmessage">
<!--The user-->Xóa bỏ người dùng <?php Tools::printHTML($reviewer->getLogin()); ?> thành công.<!--has been successfully removed from the database.--> 
</div> 
<form action="manage_users.php" method="post">
  <div class="floatRight">
    <input type="submit" class="buttonLink bigButton" value="Đồng ý" />
  </div>
</form>

<?php 
}
?>

</body>
</html>
