<?php 

/*
 * Copyright (C) 2006, 2007 Thomas Baigneres, Matthieu Finiasz
 *
 * This file is part of iChair.
 *
 * iChair is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 * 
 * iChair is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

?><?php $page_title='Hủy Bài Gửi';//Withdraw Submission';
include '../utils/tools.php';
include 'header.php';

if($currentReviewerGroup == Reviewer::$CHAIR_GROUP) {

$submission = Submission::getBySubmissionNumber(Tools::readPost('submissionNumber'));

if(!is_null($submission)) {
  Log::logWithdrawSubmission($submission,$currentReviewer);
  $submission->setWithdrawn();
  print("<div class=\"OKmessage\">Bài gửi số "//Submission number "
	. $submission->getSubmissionNumber() .
	" đã hủy thành công."//was withdrawn successfully.
  ."</div>");
  ?>
  <form action="examine.php#nbr<?php print($submission->getSubmissionNumber()); ?>" method="post">
    <div class="floatRight">
    <input type="submit" class="buttonLink bigButton" value="Đồng ý" />
    </div>
  </form>
  <?php 

}}
?>
</body>
</html>

