<xsl:stylesheet version = '1.0'
     xmlns:xsl='http://www.w3.org/1999/XSL/Transform'>

<xsl:output method="html" />

<xsl:template match="/">
  <form action="configuration.php" method="post">
    <h2>Thiết Lập Thông Tin Hội Thảo<!--Main Conference Settings--></h2>
    <xsl:apply-templates select="xml/conference"/>
    <h2>Thiết Lập Máy Chủ<!--Server Settings--></h2>
    <xsl:apply-templates select="xml/server"/>
    <h2>Thiết Lập Mail<!--Mail Settings--></h2>
    <xsl:apply-templates select="xml/mail"/>
    <h2>Thiết Lập Bài Gửi<!--Submission Settings--></h2>
    <xsl:apply-templates select="xml/submission"/>
    <h2>Thiết Lập Đánh Giá<!--Review Settings--></h2>
    <xsl:apply-templates select="xml/review"/>
    <h2>Thiết Lập Khác<!--Miscellaneous--></h2>
    <xsl:apply-templates select="xml/miscellaneous"/>
    <br />
    <center>
      <input type="submit" class="buttonLink bigButton" value="Cập nhật" />
    </center>
  </form>
</xsl:template>

<xsl:template match="conference">
  <table>
    <tr>
      <td>Tên hội thảo<!--Conference Name-->:</td>
      <td><input type="text" size="30" maxlength="30" name="conference_name"><xsl:attribute name="value"><xsl:value-of select="name" /></xsl:attribute></input><em>(Tên viết tắt, tối đa 30 ký tự)</em></td>
    </tr>  
    <tr>
      <td>Trang web thội thảo<!--Conference Web Site-->:</td>
      <td><input type="text" size="50" name="conference_site"><xsl:attribute name="value"><xsl:value-of select="site" /></xsl:attribute></input></td>
    </tr>  
    <tr>
      <td colspan="2"><h3>Logos hội thảo<!--Conference Logos--></h3></td>
    </tr>
    <xsl:apply-templates select="logos/logo" />
  </table>
</xsl:template>

<xsl:template match="logo">
  <tr>
    <td>Logo hình ảnh <!--Logo--> <xsl:value-of select="number" /><!-- image-->: </td>
    <td><input type="text" size="50">
        <xsl:attribute name="name">conference_logos_<xsl:value-of select="number" />_img</xsl:attribute>
	<xsl:attribute name="value"><xsl:value-of select="img" /></xsl:attribute>
    </input></td>
  </tr>
  <tr>
    <td>Logo link <!--Logo--><xsl:value-of select="number" /><!-- link-->: </td>
    <td><input type="text" size="50" >
        <xsl:attribute name="name">conference_logos_<xsl:value-of select="number" />_link</xsl:attribute>
	<xsl:attribute name="value"><xsl:value-of select="link" /></xsl:attribute>
    </input></td>  
  </tr>
</xsl:template>

<xsl:template match="server">
  <table>
    <tr>
      <td>Địa chỉ máy chủ<!--This Server Location--> (https://www.mysite.com/mypath/iChair):</td>
      <td><input type="text" size="50" name="server_location"><xsl:attribute name="value"><xsl:value-of select="location" /></xsl:attribute></input></td>
    </tr>  
    <tr>
      <td>Đường dẫn gửi bài<!--Submissions Path-->:</td>
      <td><input type="text" size="50" name="server_submissionsPath"><xsl:attribute name="value"><xsl:value-of select="submissionsPath" /></xsl:attribute></input></td>
    </tr>
    <tr>
      <td>Đường dẫn đánh giá bài<!--Reviews Path-->:</td>
      <td><input type="text" size="50" name="server_reviewsPath"><xsl:attribute name="value"><xsl:value-of select="reviewsPath" /></xsl:attribute></input></td>
    </tr>
    <tr>
      <td>Đường dẫn log<!--Log Path-->:</td>
      <td><input type="text" size="50" name="server_logPath"><xsl:attribute name="value"><xsl:value-of select="logPath" /></xsl:attribute></input></td>
    </tr>
    <tr>
      <td>Đường dẫn đến lệnh zip<!--Path to the zip command--> (ví dụ<!--ex-->: /usr/bin/):<br/>(để trống để vô hiệu quá chức năng tự động nén của hệ thống<!--leave blank to disable on the fly zips-->)</td>
      <td><input type="text" size="50" name="server_zipPath"><xsl:attribute name="value"><xsl:value-of select="zipPath" /></xsl:attribute></input></td>
    </tr>
    <tr>
      <td>Đường dẫn đến lệnh pdftk<!--Path to the pdftk command--> (ví dụ<!--ex-->: /usr/local/bin/):<br/>(Để trống để vô hiệu quá thẻ trên tập tin pdf<!--leave blank to disable tags on pdf files-->)</td>
      <td><input type="text" size="50" name="server_pdftkPath"><xsl:attribute name="value"><xsl:value-of select="pdftkPath" /></xsl:attribute></input></td>
    </tr>
    <tr>
      <td>Ngày chính thức hết hạn<!--Official Deadline Date--> (dd/mm/yyyy):</td>
      <td><input type="text" size="10" name="server_deadlineDate"><xsl:attribute name="value"><xsl:value-of select="deadlineDate" /></xsl:attribute></input></td>
    </tr>
    <tr>
      <td>Thời gian chính thức hết hạn<!--Official Deadline Time--> (hh:mm, 24 hours clock format):</td>
      <td><input type="text" size="5" name="server_deadlineTime"><xsl:attribute name="value"><xsl:value-of select="deadlineTime" /></xsl:attribute></input></td>
    </tr>
    <tr>
      <td>Ngày tắt hệ thống<!--Effective Shutdown Date--> (dd/mm/yyyy):</td>
      <td><input type="text" size="10" name="server_shutdownDate"><xsl:attribute name="value"><xsl:value-of select="shutdownDate" /></xsl:attribute></input></td>
    </tr>
    <tr>
      <td>Thời gian tắt hệ thống<!--Effective Shutdown Time--> (hh:mm, 24 hours clock format):</td>
      <td><input type="text" size="5" name="server_shutdownTime"><xsl:attribute name="value"><xsl:value-of select="shutdownTime" /></xsl:attribute></input></td>
    </tr>
    <tr>
      <td>Thông báo máy chủ tắt<!--Server Shutdown Message-->:</td>
      <td><textarea  rows="5" cols="80" name="server_shutdownMessage"><xsl:value-of select="shutdownMessage" /></textarea></td>
    </tr>
    <tr>
      <td>Kích thước tối đa tập tin gửi<!--Max Submission File Size-->:<br />(để trống sẽ sử dụng giá trị mặc định trong<!--Leave blank to use the default value defined in--> php.ini)</td>
      <td>
        <input type="text" size="3" name="server_submissionMaxSize"><xsl:attribute name="value"><xsl:value-of select="submissionMaxSize" /></xsl:attribute></input>MBytes
      </td>
    </tr>    
    <tr>
      <td>Định dạng thời gian hiện thị<!--Time Display Format-->:</td>
      <td>
        <input type="radio" class="noBorder" name="server_timeFormat" id="server_timeFormat12" value="12">
          <xsl:if test="timeFormat='12'">
	    <xsl:attribute name="checked">checked</xsl:attribute>
          </xsl:if>
	</input>
	<label for="server_timeFormat12">12h (am/pm)<!--format (with am/pm indicators)--></label>
        <input type="radio" class="noBorder" name="server_timeFormat" id="server_timeFormat24" value="24">
          <xsl:if test="timeFormat='24'">
	    <xsl:attribute name="checked">checked</xsl:attribute>
          </xsl:if>
	</input>
	<label for="server_timeFormat24">24h <!--format--></label>
      </td>
    </tr>    
  </table>
</xsl:template>

<xsl:template match="mail">
  <table>
    <tr>
      <td>E-mail quản trị<!--Admin e-mail-->:</td>
      <td><input type="text" size="30" name="mail_admin"><xsl:attribute name="value"><xsl:value-of select="admin" /></xsl:attribute></input></td>
    </tr>
    <tr>
      <td>Mật khẩu<!--Admin e-mail-->:</td>
      <td><input type="text" size="30" name="passmail_admin"><xsl:attribute name="value"><xsl:value-of select="passmail_admin" /></xsl:attribute></input></td>
    </tr>
    <tr>
      <td>SMTP Server (smtp.gmail.com)<!--Admin e-mail-->:</td>
      <td><input type="text" size="30" name="smtpmail_admin"><xsl:attribute name="value"><xsl:value-of select="smtpmail_admin" /></xsl:attribute></input></td>
    </tr>
    <tr>
      <td><!--Global BCC-->Tạo bản sao e-mail:</td>
      <td>
        <input type="checkbox" class="noBorder" name="mail_useGlobalBCC" value="yes">
          <xsl:if test="useGlobalBCC='yes'">
            <xsl:attribute name="checked">checked</xsl:attribute>
          </xsl:if>
        </input>
	BCC tất cả e-mails cho:
        <input type="text" size="30" name="mail_globalBCC"><xsl:attribute name="value"><xsl:value-of select="globalBCC" /></xsl:attribute></input></td>
    </tr>
    <tr>
      <td>Địa chỉ email gửi đến tác giả<!--"From" address used in mails to authors-->:
      </td>
      <td><input type="text" size="30" name="mail_authorsFrom"><xsl:attribute name="value"><xsl:value-of select="authorsFrom" /></xsl:attribute></input></td>
    </tr>
    <tr>
      <td>Địa chỉ email gửi đến người đánh giá<!--"From" address used in mails to reviewers-->:</td>
      <td><input type="text" size="30" name="mail_reviewersFrom"><xsl:attribute name="value"><xsl:value-of select="reviewersFrom" /></xsl:attribute></input></td>
    </tr>
    <tr>
      <td colspan="2"><h3>Tự động gửi mail đến tác giả<!--Automatic Mails to Contact Authors--></h3></td>
    </tr>    
    <tr>
      <td>Chủ đề bài viết gửi<!--Submission Subject-->:</td>
      <td>
        <table> 
          <tr>
            <td>
              <input type="text" size="50" name="mail_submissionSubject">
	        <xsl:attribute name="value">
		  <xsl:value-of select="submissionSubject" />
		</xsl:attribute>
	      </input>
            </td>
	    <td>
	      <a class="buttonLink" href="preview_mail.php?mail=submission" target="_blank">Xem truớc mail<!--Preview Mail--></a>
            </td>
          </tr>  
        </table>
      </td>
    </tr>
    <tr>
      <td>Thông báo gửi bài viết<!--Submission Message-->:</td>
      <td><textarea  rows="5" cols="80" name="mail_submissionMessage"><xsl:value-of select="submissionMessage" /></textarea></td>
    </tr>
    <tr>
      <td>Chủ đề bài viết sửa<!--Revision Subject-->:</td>
      <td>
        <table> 
          <tr>
            <td>
              <input type="text" size="50" name="mail_revisionSubject"><xsl:attribute name="value"><xsl:value-of select="revisionSubject" /></xsl:attribute></input>
            </td>
	    <td>
	      <a class="buttonLink" href="preview_mail.php?mail=revision" target="_blank">Xem truớc mail<!--Preview Mail--></a>
            </td>
          </tr>  
        </table>
      </td>
    </tr>
    <tr>
      <td>Thông báo sửa bào viết<!--Revision Message-->:</td>
      <td><textarea rows="5" cols="80" name="mail_revisionMessage"><xsl:value-of select="revisionMessage" /></textarea></td>
    </tr>
    <tr>
      <td>Chủ đề bài viết hủy<!--Withdrawal Subject-->:</td>
      <td>
        <table> 
          <tr>
            <td>
	      <input type="text" size="50" name="mail_withdrawSubject"><xsl:attribute name="value"><xsl:value-of select="withdrawSubject" /></xsl:attribute></input>
            </td>
	    <td>
	      <a class="buttonLink" href="preview_mail.php?mail=withdrawal" target="_blank">Xem truớc mail<!--Preview Mail--></a>
            </td>
          </tr>  
        </table>
      </td>
    </tr>
    <tr>
      <td>Thông báo hủy bài viết<!--Withdrawal Message-->:</td>
      <td><textarea rows="5" cols="80" name="mail_withdrawMessage"><xsl:value-of select="withdrawMessage" /></textarea></td>
    </tr>
    <tr>
      <td colspan="2"><h3>Email gửi đến người đánh giá<!--Password Mails to Reviewers--></h3></td>
    </tr>
    <tr>
      <td>Chủ đề email<!--Password Subject-->:</td>
      <td>
        <table> 
          <tr>
            <td>
	      <input type="text" size="50" name="mail_passwordSubject"><xsl:attribute name="value"><xsl:value-of select="passwordSubject" /></xsl:attribute></input>
            </td>
	    <td>
	      <a class="buttonLink" href="preview_mail.php?mail=password" target="_blank">Xem truớc mail<!--Preview Mail--></a>
            </td>
          </tr>  
        </table>
      </td>
    </tr>
    <tr>
      <td>Nội dung email<!--Password Message-->:</td>
      <td><textarea rows="5" cols="80" name="mail_passwordMessage"><xsl:value-of select="passwordMessage" /></textarea></td>
    </tr>
  </table>
</xsl:template>

<xsl:template match="submission">
  <table>
    <tr>
      <td>Mục có sẳn<!--Available Fields--></td>
      <td>
        <table>
	  <tr>
	    <td>
	      <input type="checkbox" class="noBorder" name="submission_useAffiliations" value="yes">
	        <xsl:if test="useAffiliations='yes'">
	          <xsl:attribute name="checked">checked</xsl:attribute>
		</xsl:if>
	      </input>Tổ chức<!--Affiliations-->
	    </td>
	    <td>
	      <input type="checkbox" class="noBorder" name="submission_useCountry" value="yes">
	        <xsl:if test="useCountry='yes'">
	          <xsl:attribute name="checked">checked</xsl:attribute>
		</xsl:if>
	      </input>Quốc gia<!--Country-->
	    </td>
	    <td>
	      <input type="checkbox" class="noBorder" name="submission_useAbstract" value="yes">
	        <xsl:if test="useAbstract='yes'">
	          <xsl:attribute name="checked">checked</xsl:attribute>
		</xsl:if>
	      </input>Tóm tắc<!--Abstract-->
	    </td>
	  </tr>
	  <tr>
	    <td>
	      <input type="checkbox" class="noBorder" name="submission_useCategory" value="yes">
	        <xsl:if test="useCategory='yes'">
	          <xsl:attribute name="checked">checked</xsl:attribute>
		</xsl:if>
	      </input>Thể loại<!--Category-->
	    </td>
	    <td>
	      <input type="checkbox" class="noBorder" name="submission_useKeywords" value="yes">
	        <xsl:if test="useKeywords='yes'">
	          <xsl:attribute name="checked">checked</xsl:attribute>
		</xsl:if>
	      </input>Từ khóa<!--Keywords-->
	    </td>
	    <td></td>
	  </tr>
        </table>
      </td>
    </tr>
    <tr>
      <td>Thêm tùy chọn checkbox cho gửi bài (student paper...)<!--Additionnal custom checkboxes for sumissions (student paper...)--></td>
      <td>
	<table>
          <tr>
	    <td></td>
	    <td>Tên ngắn gọn<br/>(giao diện người đánh giá)<!--Short name (for the review interface)--></td>
	    <td>Mô tả dài<br/>(chức năng gửi bài)<!--Long description (for the submission form)--></td>
	    <td>Chỉ có ban tổ <br/>chức mới thấy<!--Visible to chair only--></td>
	  </tr>
	  <tr>
            <td>
              <input type="checkbox" class="noBorder" name="submission_useCustomCheck1" value="yes">
                <xsl:if test="useCustomCheck1='yes'">
                  <xsl:attribute name="checked">checked</xsl:attribute>
                </xsl:if>
              </input>
	    </td>
	    <td>
              <input type="text" size="25" name="submission_shortCustomCheck1"><xsl:attribute name="value"><xsl:value-of select="shortCustomCheck1" /></xsl:attribute></input>
	    </td>
	    <td>
	      <textarea rows="3" cols="40" name="submission_longCustomCheck1"><xsl:value-of select="longCustomCheck1" /></textarea>
            </td>
            <td>
              <input type="checkbox" class="noBorder" name="submission_chaironlyCustomCheck1" value="yes">
                <xsl:if test="chaironlyCustomCheck1='yes'">
                  <xsl:attribute name="checked">checked</xsl:attribute>
                </xsl:if>
              </input>
	    </td>
	  </tr>
	  <tr>
            <td>
              <input type="checkbox" class="noBorder" name="submission_useCustomCheck2" value="yes">
                <xsl:if test="useCustomCheck2='yes'">
                  <xsl:attribute name="checked">checked</xsl:attribute>
                </xsl:if>
              </input>
	    </td>
	    <td>
              <input type="text" size="25" name="submission_shortCustomCheck2"><xsl:attribute name="value"><xsl:value-of select="shortCustomCheck2" /></xsl:attribute></input>
	    </td>
	    <td>
	      <textarea  rows="3" cols="40" name="submission_longCustomCheck2"><xsl:value-of select="longCustomCheck2" /></textarea>
	    </td>
            <td>
              <input type="checkbox" class="noBorder" name="submission_chaironlyCustomCheck2" value="yes">
                <xsl:if test="chaironlyCustomCheck2='yes'">
                  <xsl:attribute name="checked">checked</xsl:attribute>
                </xsl:if>
              </input>
	    </td>
	  </tr>
	  <tr>
            <td>
              <input type="checkbox" class="noBorder" name="submission_useCustomCheck3" value="yes">
                <xsl:if test="useCustomCheck3='yes'">
                  <xsl:attribute name="checked">checked</xsl:attribute>
                </xsl:if>
              </input>
	    </td>
	    <td>
              <input type="text" size="25" name="submission_shortCustomCheck3"><xsl:attribute name="value"><xsl:value-of select="shortCustomCheck3" /></xsl:attribute></input>
	    </td>
	    <td>
	      <textarea  rows="3" cols="40" name="submission_longCustomCheck3"><xsl:value-of select="longCustomCheck3" /></textarea>
	    </td>
            <td>
              <input type="checkbox" class="noBorder" name="submission_chaironlyCustomCheck3" value="yes">
                <xsl:if test="chaironlyCustomCheck3='yes'">
                  <xsl:attribute name="checked">checked</xsl:attribute>
                </xsl:if>
              </input>
	    </td>
	  </tr>
	</table>
      </td>
    </tr>
    <tr>
      <td>Xem trước<!---Previews--></td>
      <td>
        <table> 
          <tr>
            <td>
              <input type="checkbox" class="noBorder" name="submission_usePreview" value="yes">
                <xsl:if test="usePreview='yes'">
                  <xsl:attribute name="checked">checked</xsl:attribute>
                </xsl:if>
              </input>Tạo ra một bản xem trước cho mỗi lần gửi/sửa bài viết<!--Create a preview for each submission/revision-->
            </td>
	    <td>
	      <a class="buttonLink" href="showtest.php?format=ps" target="_blank">Kiểm tra PS<!--Test PS Preview--></a>
            </td>
	    <td>
	      <a class="buttonLink" href="showtest.php?format=pdf" target="_blank">Kiểm tra PDF<!--Test PDF Preview--></a>
            </td>
          </tr>  
        </table>
      </td>
    </tr>
    <xsl:apply-templates select="categories" />
  </table>
</xsl:template>

<xsl:template match="categories">
  <tr>
    <td>Thể loại (Sử dụng định dạng XML)<!--Categories (use XML format)-->:</td>
    <td>
      <textarea rows="20" cols="80" name="submission_categories"><![CDATA[]]>
        <xsl:apply-templates select="categoryGroup"/>
	<xsl:apply-templates select="standAloneCategory"/>
	<xsl:apply-templates select="badlyFormatedString"/>
      </textarea>
    </td>
  </tr>
</xsl:template>

<xsl:template match="categoryGroup">
  <categoryGroup><![CDATA[
  ]]>
    <name><xsl:value-of select="name" /></name><![CDATA[
]]>
    <xsl:apply-templates select="category"/>
  </categoryGroup><![CDATA[
]]>
</xsl:template>

<xsl:template match="category">
  <![CDATA[  ]]><category><xsl:attribute name="id"><xsl:value-of select="@id" /></xsl:attribute><xsl:value-of select="." /></category><![CDATA[
]]>
</xsl:template>

<xsl:template match="standAloneCategory">
  <standAloneCategory><xsl:attribute name="id"><xsl:value-of select="@id" /></xsl:attribute><xsl:value-of select="." /></standAloneCategory><![CDATA[
]]>
</xsl:template>

<xsl:template match="badlyFormatedString">
  <xsl:value-of select="." />
</xsl:template>

<xsl:template match="review">
  <table>
    <tr>
      <td>
In quy định mờ trên tất cả các bài viết (Sử dụng thẻ bài viết)
<!--Print the specified watermark on all articles (you can use article tags)--></td>
      <td>
        <input type="checkbox" class="noBorder" name="review_tagArticles" value="yes">
          <xsl:if test="tagArticles='yes'">
            <xsl:attribute name="checked">checked</xsl:attribute>
          </xsl:if>
        </input>
	<input type="text" size="80" name="review_tagArticlesString"><xsl:attribute name="value"><xsl:value-of select="tagArticlesString" /></xsl:attribute></input>
      </td>
    </tr>
    <tr>
      <td>Trong khi <em>Chọn Giai Đoạn</em> người đánh giá có quyền truy cập vào Zip của tất cả các bài viết
        <!--During <em>Election Phase</em> reviewers have access to the zip of all articles--></td>
      <td>
        <input type="checkbox" class="noBorder" name="review_zipForPreferred" value="yes">
          <xsl:if test="zipForPreferred='yes'">
            <xsl:attribute name="checked">checked</xsl:attribute>
          </xsl:if>
        </input>
      </td>
    </tr>
    <tr>
      <td>Số người đánh giá bài viết<!--Default Number of Reviewers per Article-->:</td>
      <td><input type="text" size="2" name="review_numberOfReviewers"><xsl:attribute name="value"><xsl:value-of select="numberOfReviewers" /></xsl:attribute></input></td>
    </tr>
    <tr>
      <td>Số người đánh giá viết trong thành viên ban tổ chức<!--Default Number of Reviewers per Article from a Member of the Program Committee-->:</td>
      <td><input type="text" size="2" name="review_numberOfReviewersCommittee"><xsl:attribute name="value"><xsl:value-of select="numberOfReviewersCommittee" /></xsl:attribute></input></td>
    </tr>
    <tr>
      <td>Ẩn tên tác giả với gười đánh giá (tác giả ẩn danh)<!--Hide Author Names to the Reviewers (i.e., Authors are Anonymous)--></td>
      <td>
        <input type="checkbox" class="noBorder" name="review_anonymousAuthors" value="yes">
          <xsl:if test="anonymousAuthors='yes'">
            <xsl:attribute name="checked">checked</xsl:attribute>
          </xsl:if>
	</input>
      </td>
    </tr>
    <tr>
      <td>Ẩn tên người đánh giá với tác giả (người đánh giá ẩn danh)<!--Hide Reviewer Names to the Authors (i.e., Reviewers are Anonymous)--></td>
      <td>
        <input type="checkbox" class="noBorder" name="review_anonymousReviewers" value="yes">
          <xsl:if test="anonymousReviewers='yes'">
            <xsl:attribute name="checked">checked</xsl:attribute>
          </xsl:if>
	</input>
      </td>
    </tr>
    <tr>
      <td>Sử dụng tùy chọn chấp nhận/từ chối thứ 1<!--Use 1st custom accept/reject status--></td>
      <td>
        <input type="checkbox" class="noBorder" name="review_useCustomAccept1" value="yes">
          <xsl:if test="useCustomAccept1='yes'">
            <xsl:attribute name="checked">checked</xsl:attribute>
          </xsl:if>
        </input>
	<input type="text" size="20" name="review_customAccept1"><xsl:attribute name="value"><xsl:value-of select="customAccept1" /></xsl:attribute></input>
      </td>
    </tr>
    <tr>
      <td>Sử dụng tùy chỉnh trạng thái chấp nhận/từ chối thứ 2<!--Use 2st custom accept/reject status--></td>
      <td>
        <input type="checkbox" class="noBorder" name="review_useCustomAccept2" value="yes">
          <xsl:if test="useCustomAccept2='yes'">
            <xsl:attribute name="checked">checked</xsl:attribute>
          </xsl:if>
        </input>
	<input type="text" size="20" name="review_customAccept2"><xsl:attribute name="value"><xsl:value-of select="customAccept2" /></xsl:attribute></input>
      </td>
    </tr>
    <tr>
      <td>Sử dụng tùy chỉnh trạng thái chấp nhận/từ chối thứ 3<!--Use 3st custom accept/reject status--></td>
      <td>
        <input type="checkbox" class="noBorder" name="review_useCustomAccept3" value="yes">
          <xsl:if test="useCustomAccept3='yes'">
            <xsl:attribute name="checked">checked</xsl:attribute>
          </xsl:if>
        </input>
	<input type="text" size="20" name="review_customAccept3"><xsl:attribute name="value"><xsl:value-of select="customAccept3" /></xsl:attribute></input>
      </td>
    </tr>
    <tr>
      <td>Sử dụng tùy chỉnh trạng thái chấp nhận/từ chối thứ 4<!--Use 4st custom accept/reject status--></td>
      <td>
        <input type="checkbox" class="noBorder" name="review_useCustomAccept4" value="yes">
          <xsl:if test="useCustomAccept4='yes'">
            <xsl:attribute name="checked">checked</xsl:attribute>
          </xsl:if>
        </input>
	<input type="text" size="20" name="review_customAccept4"><xsl:attribute name="value"><xsl:value-of select="customAccept4" /></xsl:attribute></input>
      </td>
    </tr>
    <tr>
      <td>Sử dụng tùy chỉnh trạng thái chấp nhận/từ chối thứ 5<!--Use 5st custom accept/reject status--></td>
      <td>
        <input type="checkbox" class="noBorder" name="review_useCustomAccept5" value="yes">
          <xsl:if test="useCustomAccept5='yes'">
            <xsl:attribute name="checked">checked</xsl:attribute>
          </xsl:if>
        </input>
	<input type="text" size="20" name="review_customAccept5"><xsl:attribute name="value"><xsl:value-of select="customAccept5" /></xsl:attribute></input>
      </td>
    </tr>
    <tr>
      <td>Sử dụng tùy chỉnh trạng thái chấp nhận/từ chối thứ 6<!--Use 6st custom accept/reject status--></td>
      <td>
        <input type="checkbox" class="noBorder" name="review_useCustomAccept6" value="yes">
          <xsl:if test="useCustomAccept6='yes'">
            <xsl:attribute name="checked">checked</xsl:attribute>
          </xsl:if>
        </input>
	<input type="text" size="20" name="review_customAccept6"><xsl:attribute name="value"><xsl:value-of select="customAccept6" /></xsl:attribute></input>
      </td>
    </tr>
    <tr>
      <td>Sử dụng tùy chỉnh trạng thái chấp nhận/từ chối thứ 7<!--Use 7st custom accept/reject status--></td>
      <td>
        <input type="checkbox" class="noBorder" name="review_useCustomAccept7" value="yes">
          <xsl:if test="useCustomAccept7='yes'">
            <xsl:attribute name="checked">checked</xsl:attribute>
          </xsl:if>
        </input>
	<input type="text" size="20" name="review_customAccept7"><xsl:attribute name="value"><xsl:value-of select="customAccept7" /></xsl:attribute></input>
      </td>
    </tr>
    <tr>
      <td>Ngày hết hạn chọn bài viết ưa thích<!--Preferred Articles Deadline Date--> (dd/mm/yyyy):</td>
      <td><input type="text" size="10" name="review_preferredDeadlineDate"><xsl:attribute name="value"><xsl:value-of select="preferredDeadlineDate" /></xsl:attribute></input></td>
    </tr>
    <tr>
      <td>Thời gian hết hạn chọn bài viết ưa thích<!--PPreferred Articles Deadline Time--> (hh:mm, định dạng 24h):</td>
      <td><input type="text" size="5" name="review_preferredDeadlineTime"><xsl:attribute name="value"><xsl:value-of select="preferredDeadlineTime" /></xsl:attribute></input></td>
    </tr>
    <tr>
      <td>Ngày hết hạn đánh giá<!--Review Deadline Date--> (dd/mm/yyyy):</td>
      <td><input type="text" size="10" name="review_deadlineDate"><xsl:attribute name="value"><xsl:value-of select="deadlineDate" /></xsl:attribute></input></td>
    </tr>
    <tr>
      <td>Thời gian hết hạn đánh giá<!--Review Deadline Time--> (hh:mm, định dạng 24h):</td>
      <td><input type="text" size="5" name="review_deadlineTime"><xsl:attribute name="value"><xsl:value-of select="deadlineTime" /></xsl:attribute></input></td>
    </tr>
  </table>
  <h3>Mục đánh giá</h3>
  <table class="configGrades">
    <tr>
      <td></td>
      <td>Tiêu chí đánh giá</td>
      <td>Tối thiểu</td>
      <td>Tối đa</td>
      <td>Thang đánh giá (không dùng &amp;, &lt;, &gt;. Ví dụ: 0 - Bài viết tệ; 4 - Bài viết hay...)</td>
    </tr>
    <tr>
      <td class="firstRow">Đánh giá chung<!--Overall Grade Range-->:</td>
      <td>Bắt buộc<!--Required--></td>
      <td>
        <input type="text" size="3" name="review_overallGradeMin"><xsl:attribute name="value"><xsl:value-of select="overallGradeMin" /></xsl:attribute></input>
      </td>
      <td>
        <input type="text" size="3" name="review_overallGradeMax"><xsl:attribute name="value"><xsl:value-of select="overallGradeMax" /></xsl:attribute></input>
      </td>
      <td>
        <input type="text" size="80" name="review_overallGradeSemantics"><xsl:attribute name="value"><xsl:value-of select="overallGradeSemantics" /></xsl:attribute></input>
      </td>
    </tr>
    <tr>
      <td class="firstRow">Độ tin cậy của đánh giá<!--Confidence Level Range-->:</td>
      <td>
        <input type="checkbox" class="noBorder" name="review_useConfidenceLevel" value="yes">
          <xsl:if test="useConfidenceLevel='yes'">
            <xsl:attribute name="checked">checked</xsl:attribute>
          </xsl:if>
	</input>
      </td>
      <td>
        <input type="text" size="3" name="review_confidenceLevelMin">
	  <xsl:attribute name="value"><xsl:value-of select="confidenceLevelMin" /></xsl:attribute>
        </input>
      </td>
      <td>
        <input type="text" size="3" name="review_confidenceLevelMax">
	  <xsl:attribute name="value"><xsl:value-of select="confidenceLevelMax" /></xsl:attribute>
	</input>
      </td>
      <td>
        <input type="text" size="80" name="review_confidenceLevelSemantics"><xsl:attribute name="value"><xsl:value-of select="confidenceLevelSemantics" /></xsl:attribute></input>
      </td>
    </tr>
    <tr>
      <td class="firstRow">Độ chuyên môn<!--Technical Level Range-->:</td>
      <td>
        <input type="checkbox" class="noBorder" name="review_useTechnicalQuality" value="yes">
          <xsl:if test="useTechnicalQuality='yes'">
            <xsl:attribute name="checked">checked</xsl:attribute>
          </xsl:if>
	</input>
      </td>
      <td>
        <input type="text" size="3" name="review_technicalQualityMin">
	  <xsl:attribute name="value"><xsl:value-of select="technicalQualityMin" /></xsl:attribute>
        </input>
      </td>
      <td>
        <input type="text" size="3" name="review_technicalQualityMax">
	  <xsl:attribute name="value"><xsl:value-of select="technicalQualityMax" /></xsl:attribute>
	</input>
      </td>
      <td>
        <input type="text" size="80" name="review_technicalQualitySemantics"><xsl:attribute name="value"><xsl:value-of select="technicalQualitySemantics" /></xsl:attribute></input>
      </td>
    </tr>
    <tr>
      <td class="firstRow">Chất lượng biên tập<!--Editorial Quality Range-->:</td>
      <td>
        <input type="checkbox" class="noBorder" name="review_useEditorialQuality" value="yes">
          <xsl:if test="useEditorialQuality='yes'">
            <xsl:attribute name="checked">checked</xsl:attribute>
          </xsl:if>
	</input>
      </td>
      <td>
        <input type="text" size="3" name="review_editorialQualityMin">
	  <xsl:attribute name="value"><xsl:value-of select="editorialQualityMin" /></xsl:attribute>
        </input>
      </td>
      <td>
        <input type="text" size="3" name="review_editorialQualityMax">
	  <xsl:attribute name="value"><xsl:value-of select="editorialQualityMax" /></xsl:attribute>
	</input>
      </td>
      <td>
        <input type="text" size="80" name="review_editorialQualitySemantics"><xsl:attribute name="value"><xsl:value-of select="editorialQualitySemantics" /></xsl:attribute></input>
      </td>
    </tr>
    <tr>
      <td class="firstRow">Mức phù hợp<!--Suitability Range-->:</td>
      <td>
        <input type="checkbox" class="noBorder" name="review_useSuitability" value="yes">
          <xsl:if test="useSuitability='yes'">
            <xsl:attribute name="checked">checked</xsl:attribute>
          </xsl:if>
	</input>
      </td>
      <td>
        <input type="text" size="3" name="review_suitabilityMin">
	  <xsl:attribute name="value"><xsl:value-of select="suitabilityMin" /></xsl:attribute>
        </input>
      </td>
      <td>
        <input type="text" size="3" name="review_suitabilityMax">
	  <xsl:attribute name="value"><xsl:value-of select="suitabilityMax" /></xsl:attribute>
	</input>
      </td>
      <td>
        <input type="text" size="80" name="review_suitabilitySemantics"><xsl:attribute name="value"><xsl:value-of select="suitabilitySemantics" /></xsl:attribute></input>
      </td>
    </tr>
    <tr>
      <td class="firstRow">Bài viết tốt nhất<!--Best Paper Range-->:</td>
      <td>
        <input type="checkbox" class="noBorder" name="review_useBestPaper" value="yes">
          <xsl:if test="useBestPaper='yes'">
            <xsl:attribute name="checked">checked</xsl:attribute>
          </xsl:if>
	</input>
      </td>
      <td>
        <input type="text" size="3" name="review_bestPaperMin">
	  <xsl:attribute name="value"><xsl:value-of select="bestPaperMin" /></xsl:attribute>
        </input>
      </td>
      <td>
        <input type="text" size="3" name="review_bestPaperMax">
	  <xsl:attribute name="value"><xsl:value-of select="bestPaperMax" /></xsl:attribute>
	</input>
      </td>
      <td>
        <input type="text" size="80" name="review_bestPaperSemantics"><xsl:attribute name="value"><xsl:value-of select="bestPaperSemantics" /></xsl:attribute></input>
      </td>
    </tr>
    <tr>
      <td class="firstRow">Ý kiến ban tổ chức (tác giả không thể xem)<!--Comments to Program Committee (not seen by Authors)-->:</td>
      <td>
        <input type="checkbox" class="noBorder" name="review_useToProgramCommittee" value="yes">
          <xsl:if test="useToProgramCommittee='yes'">
            <xsl:attribute name="checked">checked</xsl:attribute>
          </xsl:if>
	</input>
      </td>
    </tr>
    <tr>
      <td class="firstRow">Ý kiến tác giả<!--Comments to Authors-->:</td>
      <td>
        <input type="checkbox" class="noBorder" name="review_useToAuthors" value="yes">
          <xsl:if test="useToAuthors='yes'">
            <xsl:attribute name="checked">checked</xsl:attribute>
          </xsl:if>
	</input>
      </td>
    </tr>
  </table>
</xsl:template>

<xsl:template match="miscellaneous">
  <table>
    <tr>
      <td>Ngày thông báo<!--Notification Date--> (dd/mm/yyyy):</td>
      <td>
        <input type="text" size="10" name="miscellaneous_notificationDate"><xsl:attribute name="value"><xsl:value-of select="notificationDate" /></xsl:attribute></input>
      </td>
    </tr>
    <tr>
      <td>Ngày phiên bản cuối<!--Final Version Date--> (dd/mm/yyyy):</td>
      <td>
        <input type="text" size="10" name="miscellaneous_finalVersionDate"><xsl:attribute name="value"><xsl:value-of select="finalVersionDate" /></xsl:attribute></input>
      </td>
    </tr>
    <tr>
      <td>Thời gian chờ<!--New visit timeout--> (phút<!--in minutes-->):</td>
      <td>
        <input type="text" size="5" name="miscellaneous_visitTimeout"><xsl:attribute name="value"><xsl:value-of select="visitTimeout" /></xsl:attribute></input>
      </td>
    </tr> 
    <tr>
      <td>Giao diện trang web<!--Web page skin-->:</td>
      <td>
	<table>
	  <tr>
	    <td>Trang gửi bài<!--Submission pages--></td>
	    <td>Trang đánh giá<!--Review pages--></td>
	    <td>Trang quản trị<!--Administration pages--></td>
          </tr>
          <tr>
            <td>
	      <div class="popUp">
                <input type="radio" class="noBorder" name="miscellaneous_submissionSkin" id="miscellaneous_submissionSkinClassic" value="classic">
                  <xsl:if test="submissionSkin='classic'">
                    <xsl:attribute name="checked">checked</xsl:attribute>
                  </xsl:if>
                </input>   
                <label for="miscellaneous_submissionSkinClassic">Cổ điển<!--Classic skin--></label>
		<div class="hidden"><img src="../images/submissionSkinClassic.png" alt="Classic Submission Skin" /></div>
	      </div>
	      <div class="popUp">
                <input type="radio" class="noBorder" name="miscellaneous_submissionSkin" id="miscellaneous_submissionSkinModern" value="modern">
                  <xsl:if test="submissionSkin='modern'">
                    <xsl:attribute name="checked">checked</xsl:attribute>
                  </xsl:if>
                </input>   
                <label for="miscellaneous_submissionSkinModern">Hiên đại<!--Modern skin--></label>
		<div class="hidden"><img src="../images/submissionSkinModern.png" alt="Modern Submission Skin" /></div>
	      </div>
	    </td>
            <td>
	      <div class="popUp">
                <input type="radio" class="noBorder" name="miscellaneous_reviewSkin" id="miscellaneous_reviewSkinClassic" value="classic">
                  <xsl:if test="reviewSkin='classic'">
                    <xsl:attribute name="checked">checked</xsl:attribute>
                  </xsl:if>
                </input>   
                <label for="miscellaneous_reviewSkinClassic">Cổ điển<!--Classic skin--></label>
		<div class="hidden"><img src="../images/reviewSkinClassic.png" alt="Classic Submission Skin" /></div>
	      </div>
	      <div class="popUp">
                <input type="radio" class="noBorder" name="miscellaneous_reviewSkin" id="miscellaneous_reviewSkinModern" value="modern">
                  <xsl:if test="reviewSkin='modern'">
                    <xsl:attribute name="checked">checked</xsl:attribute>
                  </xsl:if>
                </input>   
                <label for="miscellaneous_reviewSkinModern">Hiện đại<!--Modern skin--></label>
		<div class="hidden"><img src="../images/reviewSkinModern.png" alt="Modern Submission Skin" /></div>
	      </div>
	    </td>
            <td>
	      <div class="popUp">
                <input type="radio" class="noBorder" name="miscellaneous_adminSkin" id="miscellaneous_adminSkinClassic" value="classic">
                  <xsl:if test="adminSkin='classic'">
                    <xsl:attribute name="checked">checked</xsl:attribute>
                  </xsl:if>
                </input>   
                <label for="miscellaneous_adminSkinClassic">Cổ điển<!--Classic skin--></label>
		<div class="hidden"><img src="../images/adminSkinClassic.png" alt="Classic Submission Skin" /></div>
	      </div>
	      <div class="popUp">
                <input type="radio" class="noBorder" name="miscellaneous_adminSkin" id="miscellaneous_adminSkinModern" value="modern">
                  <xsl:if test="adminSkin='modern'">
                    <xsl:attribute name="checked">checked</xsl:attribute>
                  </xsl:if>
                </input>   
                <label for="miscellaneous_adminSkinModern">Hiện đại<!--Modern skin--></label>
		<div class="hidden"><img src="../images/adminSkinModern.png" alt="Modern Submission Skin" /></div>
	      </div>
	    </td>
          </tr>
	</table>
      </td>
    </tr>
  </table>
</xsl:template>



</xsl:stylesheet>
