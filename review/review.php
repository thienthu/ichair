<?php 

/*
 * Copyright (C) 2006, 2007 Thomas Baigneres, Matthieu Finiasz
 *
 * This file is part of iChair.
 *
 * iChair is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 * 
 * iChair is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

?><?php $page_title='Đánh giá Bài Viết';//Review Articles';
include '../utils/tools.php';
include 'header.php';
if((!is_null($currentReviewer)) && ($currentReviewerGroup != Reviewer::$OBSERVER_GROUP) ) {
?>

<?php 

  if(Tools::getCurrentPhase() != Tools::$REVIEW) {
    print('<div class="OKmessage">Chức năng này không thể truy cập trong giai đoạn này.'//This page cannot be accessed during the current phase.
      .'</div>');
  } else {

  /* First, list all the articles assigned to the current reviewer. He must either be a chair or a reviewer */


  if(($currentReviewerGroup == Reviewer::$CHAIR_GROUP) || ($currentReviewerGroup == Reviewer::$REVIEWER_GROUP)) { 


    /* Update the number of articles displayed for the current reviewer */
    
    $numberOfArticlesPerPage = Tools::readPost('numberOfArticlesPerPage');
    if(preg_match("/^[1-9][0-9]*$/", $numberOfArticlesPerPage)) {
      $currentReviewer->setNumberOfArticlesPerPage($numberOfArticlesPerPage);
    }
    $numberOfArticlesPerPage = $currentReviewer->getNumberOfArticlesPerPage();


    /* Display a form allowing to change the number of article displayed on the same page */
    ?>
    <div class="floatRight">
      <form action="review.php" method="post">
       <input type="hidden" name="assignedArticleNumber" value="<?php if (isset($assignedArticleNumber)) {print($assignedArticleNumber);} ?>" />
       <input type="hidden" name="otherArticleNumber" value="<?php if (isset($otherArticleNumber)) {print($otherArticleNumber);} ?>" />
       <table><tr>
        <td><div class="versionAuthors">Số bài viết hiển thị trong mỗi trang:<!--Number of articles displayed per page:-->&nbsp;</div></td>
        <td><input type="text" name="numberOfArticlesPerPage" size="3" class="largeInput" value="<?php print($numberOfArticlesPerPage); ?>" /></td>
	<td><input type="submit" class="buttonLink" value="Đồng ý" /></td>
       </tr></table>
      </form>
    </div>
    <div class="clear"></div>
    <?php 

    /* Update sort options */
    $showAll = !$currentReviewer->sortIsAssignedOnly();
    $showPreferred = $currentReviewer->sortIsAlsoPreferred();
    $sortByReviewStatus = !$currentReviewer->sortIsByArticleNumber();
    $reverse = $currentReviewer->sortIsReversed();
    if(Tools::readPost('updateView')=='Xem') {
      $showAll = (Tools::readPost('showAssignedOnly') == 'all');
      $showPreferred = (Tools::readPost('showAssignedOnly') == 'preferred');
      $sortByReviewStatus = (Tools::readPost('sortKey') != 'articleNumber');
      $reverse = (Tools::readPost('reverseSort') == 'yes');
      $currentReviewer->setSortKeyStringForReviews($showAll, $showPreferred, $sortByReviewStatus, $reverse);
    }
    
    /* Display a banner allowing to choose what the page should display and how */

    ?>
    <div class="paperBoxDetails">
      <form action="review.php" method="post"> 
        <center>
	  <table>
	    <tr>
              <td>
	        <input type="radio" class="noBorder" name="showAssignedOnly" id="assignedViewType" value="assigned" <?php if((!$showAll) && (!$showPreferred)) { ?>checked="checked" <?php } ?>/>
		<label for="assignedViewType">Hiển thị bài viết đã được phân công<!--Show assigned articles only--></label>
              </td>
	      <td>
	        &nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" class="noBorder" name="sortKey" id="sortKeyArticleNumber" value="articleNumber" <?php if(!$sortByReviewStatus) { ?>checked="checked" <?php } ?>/>
		<label for="sortKeyArticleNumber">Sắp xếp theo số bài viết<!--Sort by Article Number--></label>	        
	      </td>
	      <td>&nbsp;</td>
	    </tr>
	    <tr>
              <td>
	        <input type="radio" class="noBorder" name="showAssignedOnly" id="preferredViewType" value="preferred" <?php if($showPreferred) { ?>checked="checked" <?php } ?>/>
		<label for="preferredViewType">Hiển thị bài viết đã được phân công &amp; ưa thích<!--Show assigned &amp; preferred--></label>
    	      </td>
	      <td>
	        &nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" class="noBorder" name="sortKey" id="sortKeyReviewStatus" value="reviewStatus" <?php if($sortByReviewStatus) { ?>checked="checked" <?php } ?>/>
		<label for="sortKeyReviewStatus">Sắp xếp theo trạng thái đánh giá<!--Sort by Review Status--></label>	        
	      </td>
	      <td>
	        &nbsp;&nbsp;&nbsp;<input type="checkbox" class="noBorder" name="reverseSort" id="reverseSort" value="yes" <?php if($reverse) { ?>checked="checked" <?php } ?>/>
		<label for="reverseSort">Sắp xếp theo thứ tự ngược lại<!--Reverse sort order--></label>               
	      </td>
 	    </tr>
            <tr> 
              <td>
                <input type="radio" class="noBorder" name="showAssignedOnly" id="allViewType" value="all" <?php if($showAll) { ?>checked="checked" <?php } ?>/>
                <label for="allViewType">Hiển thị tất cả bài viết<!--Show all articles--></label>
              </td>
              <td></td>
              <td></td>
            </tr>
	  </table>
          <input type="submit" class="buttonLink bigButton" name="updateView" value="Xem" />
	</center>
      </form>
    </div>

    <form action="review_article.php" method="post" enctype="multipart/form-data" style="text-align: right; margin: 0 0 10px auto;" target="_blank">
      Tải tập tin đánh giá XML lên:<!--Upload an XML Review File:-->
      <input type="hidden" name="action" value="any_xml" />
      <input name="file" type="file" size="20" />
      <input type="submit" class="buttonLink" value="Tải XML lên" />
    </form>

    <?php 

  /* Display general messages from the chair */

  $generalMessages = Meeting::getAllNonTrashedGeneralMessages();
    if(count($generalMessages) != 0) {
      print('<br />');
    }

  foreach($generalMessages as $message) { ?>
  <center>
  <table>
    <tr>
      <td>
        <div class="paperBoxTitle" style="margin: 0;">
	  <div class="floatRight">Ngày:<!--Date:--> <?php print(date("d/m/Y h:i", $message->getDate())); ?></div>
	  Thông báo từ ban tổ chức<!--Message from the Chair-->
   	  <br />
	</div>
      </td>
    </tr>
    <tr>
      <td><div class="messageBox"><?php Tools::printHTMLbr($message->getContent()); ?></div></td>
    </tr>
  </table>
  </center>
  <br />
  <?php } 

    /* Get articles according to sort options and number of articles per page */

    $assignements =  Assignement::getSortedNotBlockedAssignementsByReviewerNumber($currentReviewer->getReviewerNumber(), $reverse, $showAll, $showPreferred, $sortByReviewStatus);

    if(count($assignements) == 0) {
      print('<div class="OKmessage">Không có bài viết để hiển thị'//No articles to display.
        .'</div>');
      include('footer.php');
    }


    $articleToDisplay = Tools::readPost('articleToDisplay');
    $pageToDisplay = 0;
    if(preg_match("/^[0-9]+$/",$articleToDisplay)) {
      $i;
      for($i=0; $i<count($assignements); $i++) {
	if($assignements[$i]->getArticleNumber() == $articleToDisplay) {
	  $pageToDisplay = floor($i / $numberOfArticlesPerPage);
	  break;
	}
      }
    }
    
    /* Display the buttons */
										
    for($btNbr=0; $btNbr<ceil(count($assignements) / $numberOfArticlesPerPage); $btNbr++) {
      /* define the button label*/
      $buttonLabel = 'Trang '//Page ' 
      . ($btNbr+1);
      if(!$sortByReviewStatus) {
	$startAssignement = $assignements[$btNbr * $numberOfArticlesPerPage];
	$stopAssignement = $assignements[min(($btNbr+1) * $numberOfArticlesPerPage-1,count($assignements)-1)];
	$startNbr = $startAssignement->getArticleNumber();
	$stopNbr = $stopAssignement->getArticleNumber();
	if($startNbr != $stopNbr) {
	  $buttonLabel = $startNbr . ' - ' . $stopNbr;
	} else {
	  $buttonLabel = $startNbr . '';
	}
      }

      /* Display the buttons */
      $nextArticleToDisplay = $assignements[$btNbr * $numberOfArticlesPerPage];
      $nextArticleToDisplay = $nextArticleToDisplay->getArticleNumber();
      ?>
      <div class="floatLeft">
        <form action="review.php" method="post">
          <input type="hidden" name="articleToDisplay" value="<?php print($nextArticleToDisplay); ?>" />
          <input type="submit" class="buttonLink<?php if($pageToDisplay == $btNbr) { print(' currentButton'); }?>" value="<?php print($buttonLabel); ?>">
        </form>
      </div>
      <?php 
    }
    print('<div class="clear"></div>');

    /* Display the articles */
    print('<center>');

    if (count($assignements) != 0) {
    ?>

    <table class="dottedTable">
      <tr>
        <th class="topRow">Số<br /> bài viết<!--Article<br />Number--></th>
        <th class="topRow">&nbsp;</th>
        <td class="topRow leftAlign">
          <table>
	    <tr>
	      <th class="leftAlign">Tiêu đề<!--Title--></th>
	    </tr><tr>
	      <th class="leftAlign"><em><?php print(Article::getAvailableFieldTitle());?></em></th>
	    </tr>
	  </table>
        </td>
        <th class="topRow" colspan="2">&nbsp;</th>
      </tr>
      <?php 


      $articles = Article::getAllArticles();
      for($i = ($pageToDisplay*$numberOfArticlesPerPage); $i<min(($pageToDisplay+1) * $numberOfArticlesPerPage,count($assignements)); $i++) {
	$assignement = $assignements[$i];
	$article = $articles[$assignement->getArticleNumber()];
	$articleNumber = $article->getArticleNumber();
	?>
	<tr>
	  <?php 
          ?>
 
	  <td><div class="bigNumber<?php 
              switch ($assignement->getReviewStatus()) {
              case Assignement::$COMPLETED :
              print(" completedReview\" title=\"review completed\"");
              break;
              case Assignement::$INPROGRESS :
              print(" inProgressReview\" title=\"review in progress\"");
              break;
              default:
              print(" voidReview\" title=\"no review\"");
              }
              ?>><?php Tools::printHTML($article->getArticleNumber()); ?></div></td>
	  <td>
            <div class="popUp">
	      <form action="article_details.php?articleNumber=<?php print($articleNumber); ?>" target="_blank" method="post"><input type="submit" class="buttonLink" value="Chi tiết"/></form>
	      <div class="hidden"><?php $article->printDetailsBoxPopUp(); ?></div>
	    </div>
	  </td>
	  <td class="leftAlign">
            <table>
	      <tr>
	        <td class="leftAlign"><?php Tools::printHTMLsubstr($article->getTitle(),60); ?></td>
	      </tr><tr>
	        <td class="leftAlign"><?php 
                   if (Article::getAvailableFieldTitle() != ""){
                     print('<em>');
                     Tools::printHTMLsubstr(Article::getAvailableFieldTitle().': '.$article->getAvailableField(),60);
                     print('</em>');
                   }
	           if($assignement->getAssignementStatus() == Assignement::$ASSIGNED) {
	             print('<span class="discussArticle">Đã phân công</span>');
	           } 
                   ?></td>
	      </tr>
	    </table>
	  </td>
	  <td>
	    <?php $article->printDownloadArticleLink(); ?>
	  </td>
	  <td>
            <div class="popUp">
            <form action="review_article.php" method="post" target="_blank">
	      <input type="hidden" name="articleNumber" value="<?php print($article->getArticleNumber()); ?>" />
	      <input type="submit" class="buttonLink" value="Đánh giá bài víêt" />
            </form>
            <div class="hiddenOpposite">
         <?php $reviewReader = new ReviewReader();
            $reviewReader->createFromXMLFile(Assignement::getByNumbers($article->getArticleNumber(), $currentReviewer->getReviewerNumber()));
              $reviewReader->printDetailsBoxPopUpComplete(); ?>
            </div>
            </div>
	  </td>
	</tr>
	<?php 
      }
      ?>
    </table>


    <?php } ?>

    <?php if(Tools::useZipReviews()) {?>
      <form action="download_assigned_zip.php" method="post">
        <input type="submit" class="buttonLink bigButton" value="Tải tất cả ZIP của bài viết đã phân công" />
      </form>
    <?php } ?>
    </center>  


    <?php  if(file_exists(Tools::getConfig('server/reviewsPath').'all_articles.zip')) {?>
        <center><form action="download_all_zip.php" method="post">
          <input type="submit" class="buttonLink bigButton" value="Tải tất cả ZIP của bài viết"/>
        </form></center>
   <?php } ?>
    </center>

<?php } ?>

<div class="clear bottomSpacer"></div>

<?php }} ?>
<?php include('footer.php'); ?>
