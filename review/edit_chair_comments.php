<?php 

/*
 * Copyright (C) 2006, 2007 Thomas Baigneres, Matthieu Finiasz
 *
 * This file is part of iChair.
 *
 * iChair is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 * 
 * iChair is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

?><?php 
$page_title = 'Cập Nhật Bình Luận';//Comment Update';
include '../utils/tools.php';
include 'header.php';

if($currentReviewerGroup == Reviewer::$CHAIR_GROUP) {

$submissionNumber = Tools::readPost('submissionNumber');
$versionNumber = Tools::readPost('versionNumber');
$from = Tools::readPost('from');   

$submission = Submission::getBySubmissionNumber($submissionNumber);

if (!is_null($submission)) {
  $versions = $submission->getAllVersions();
  $chairComments = $versions[$versionNumber]->getChairComments();    

?>
<form action="edit_chair_comments_result.php" method="post">
<center><textarea name="chairComments" cols="80" rows="15"><?php Tools::printHTML($chairComments);?></textarea>
<br />
<input type="hidden" name="submissionNumber" value="<?php print($submissionNumber)?>" />
<input type="hidden" name="versionNumber" value="<?php print($versionNumber)?>" />
<input type="hidden" name="from" value="<?php print($from)?>" />
<input type="submit" class="buttonLink bigButton" value="Cập nhật" />
</center>
</form>

<?php }} ?>

<?php include('footer.php'); ?>
