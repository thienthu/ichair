<?php 

/*
 * Copyright (C) 2006, 2007 Thomas Baigneres, Matthieu Finiasz
 *
 * This file is part of iChair.
 *
 * iChair is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 * 
 * iChair is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

?><?php $page_title="Thống Kê Hội Thảo";//'Conference Statistics';
include '../utils/tools.php';
include 'header.php';
set_time_limit(0);
if($currentReviewerGroup == Reviewer::$CHAIR_GROUP) {
?>

<h2>Bài Viết<!---About the Submissions--></h2>

<?php 
 $submissions = Log::getSubmissionsStats(array(),0,86400);
 $revisions = Log::getRevisionsStats(array(),0,86400);
 $withdrawals = Log::getWithdrawalStats(array(),0,86400);
 print('<div class="paperBoxDetails">');
 Log::printSubmissionStatsLegend();
 print('<div class="versionTitle">Bài Viết Gửi Theo Ngày'//Submissions Day by Day
  .'</div><div class="clear"></div>');
 Log::printSubmissionStatsTable($submissions, $revisions, $withdrawals);
 $oneDayBeforeDeadline = Tools::getUFromDateAndTime(Tools::getConfig("server/deadlineDate"), Tools::getConfig("server/deadlineTime")) - 86400;
 $submissions = Log::getSubmissionsStats(array(),$oneDayBeforeDeadline,3600);
 $revisions = Log::getRevisionsStats(array(),$oneDayBeforeDeadline,3600);
 $withdrawals = Log::getWithdrawalStats(array(),$oneDayBeforeDeadline,3600);
 if ((count($submissions) != 0) || (count($revisions) != 0) || (count($withdrawals) != 0)) {
   print('<div class="versionTitle">Submissions 24h Before the Deadline</div>');
   Log::printSubmissionStatsTableLastDay($submissions, $revisions, $withdrawals);
 }

 print('</div>');
 if ((Tools::getCurrentPhase() == Tools::$REVIEW) || (Tools::getCurrentPhase() == Tools::$DECISION)) {
   $acceptedArticles = Article::getByAcceptance(Article::$ACCEPT);
   $rejectedArticles = Article::getByAcceptance(Article::$REJECT);
   $Asub = array();
   $Arev = array();
   $Rsub = array();
   $Rrev = array();
   foreach ($acceptedArticles as $article) {
     $Asub = Log::getSubmissionsStatsByArticleNumber($Asub, $article->getArticleNumber(),0,86400);
     $Arev = Log::getRevisionsStatsByArticleNumber($Arev, $article->getArticleNumber(),0,86400);
   }
   print('<table style="width: 100%"><tr valign="top"><td><div class="paperBoxDetails"><div class="versionTitle">'//Accepted Articles
    .'</div>');
   ksort($Asub);
   ksort($Arev);
   Log::printSubmissionStatsTable2($Asub, $Arev);
   
   // 24 hour logs for accepted papers
   $Asub = array();
   $Arev = array();
   foreach ($acceptedArticles as $article) {
     $Asub = Log::getSubmissionsStatsByArticleNumber($Asub, $article->getArticleNumber(),$oneDayBeforeDeadline,3600);
     $Arev = Log::getRevisionsStatsByArticleNumber($Arev, $article->getArticleNumber(),$oneDayBeforeDeadline,3600);
   }
   if ((count($Asub) != 0) || (count($Arev) != 0)) {
     print('<div class="versionTitle">Last 24h</div>');
     ksort($Asub);
     ksort($Arev);
     Log::printSubmissionStatsTable2LastDay($Asub, $Arev);
   }
   

   print('</div></td><td><div class="paperBoxDetails"><div class="versionTitle">Bài viết đã bị từ chối'//Rejected Articles
    .'</div>');
   foreach ($rejectedArticles as $article) {
     $Rsub = Log::getSubmissionsStatsByArticleNumber($Rsub, $article->getArticleNumber(),0,86400);
     $Rrev = Log::getRevisionsStatsByArticleNumber($Rrev, $article->getArticleNumber(),0,86400);
   }
   ksort($Rsub);
   ksort($Rrev);
   Log::printSubmissionStatsTable2($Rsub, $Rrev);

   // 24 hour logs for accepted papers
   $Rsub = array();
   $Rrev = array();
   foreach ($rejectedArticles as $article) {
     $Rsub = Log::getSubmissionsStatsByArticleNumber($Rsub, $article->getArticleNumber(),$oneDayBeforeDeadline,3600);
     $Rrev = Log::getRevisionsStatsByArticleNumber($Rrev, $article->getArticleNumber(),$oneDayBeforeDeadline,3600);
   }
   if ((count($Rsub) != 0) || (count($Rrev) != 0)) {
     print('<div class="versionTitle">Last 24h</div>');
     ksort($Rsub);
     ksort($Rrev);
     Log::printSubmissionStatsTable2LastDay($Rsub, $Rrev);
   }
   

   print('</div></td></tr></table>');

 }
 ?>

<h2>Người Đánh Giá<!--About the Reviewers--></h2>
<div class="paperBoxDetails">
<?php Log::printReviewersStats(); ?>
</div>

<?php 

if((Tools::getCurrentPhase() == Tools::$REVIEW) || (Tools::getCurrentPhase() == Tools::$DECISION)) {

?>

<h2>List of Subreviewers</h2>

<?php 

$articles = Article::getAllArticles();
$bozo = "";
foreach($articles as $article) {
  $assignements = $article->getAssignementsByReviewStatus(Assignement::$COMPLETED);
  foreach($assignements as $assignement) {
    $reviewReader = new ReviewReader();
    $reviewReader->createFromXMLFile($assignement);
    if($reviewReader->getSubreviewerName() != "") {
      $bozo .= $reviewReader->getSubreviewerName() . ", ";
    }
  }
}
$bozo = substr($bozo, 0, strlen($bozo)-2);
if($bozo == "") {
  print('<em>Không có.'//None.
  	.'</em>');
} else {
  Tools::printHTML($bozo);
}
?>

<?php }} ?>

<?php include('footer.php'); ?>
