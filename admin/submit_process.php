<?php 

/*
 * Copyright (C) 2006, 2007 Thomas Baigneres, Matthieu Finiasz
 *
 * This file is part of iChair.
 *
 * iChair is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 * 
 * iChair is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

?><?php $page_title='Kết Quả Gủi Bài'//'Submission Result';
include('../utils/tools.php');

$relative_url = "admin/submit_result.php";

/* Create a new object submission */

$submission = new Submission();
$submission->createFromPOST();

/* If the submission is successfull -> redirection to submit_result.php */

if ($submission->status['isSuccessfull']) {
    Log::logAdminSubmission($submission);
    $relative_url .= '?id=' . $submission->getId();
    header('Location: '.Tools::getConfig('server/location') . "/" . $relative_url);
    
} else {

    include 'header.php';
    
    ?>
    Lỗi gửi bài. Chi tiết lỗi gửi bài bên dưới.
    <!--There was an error with your submission. See the error message
    bellow for more details.-->
    <div class="ERRmessage">
        <?php Tools::printHTMLbr($submission->status['message']); ?>
    </div>
    <form action="submit.php" method="post" enctype="multipart/form-data">
      <input name="title" type="hidden" value="<?php Tools::printHTML(Tools::readPost('title')); ?>"  />
      <?php foreach($_POST['authors'] as $i => $val) { ?>
  	<input name="authors[]" type="hidden" value="<?php Tools::printHTML(Tools::readPost('authors', $i)); ?>"  />
	<input name="affiliations[]" type="hidden" value="<?php Tools::printHTML(Tools::readPost('affiliations', $i)); ?>"  />
	<input name="country[]" type="hidden" value="<?php Tools::printHTML(Tools::readPost('country', $i)); ?>"  />
      <?php } ?>
      <input name="contact" type="hidden" value="<?php Tools::printHTML(Tools::readPost('contact')); ?>" />
      <input name="abstract" type="hidden" value="<?php Tools::printHTML(Tools::readPost('abstract')); ?>" />
      <input name="category" type="hidden" value="<?php Tools::printHTML(Tools::readPost('category')); ?>" />
      <input name="keywords" type="hidden" value="<?php Tools::printHTML(Tools::readPost('keywords')); ?>" />
      <input name="disclaimer" type="hidden" value="<?php Tools::printHTML(Tools::readPost('disclaimer')); ?>" />
      <div class="floatRight">
	<input type="submit" class="buttonLink bigButton" value="Trở lại" />
      </div>
    </form>

    <?php 
}
?>

</body>
</html>

