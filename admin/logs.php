<?php 

/*
 * Copyright (C) 2006, 2007 Thomas Baigneres, Matthieu Finiasz
 *
 * This file is part of iChair.
 *
 * iChair is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 * 
 * iChair is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

?><?php $page_title="Trang Chủ";//'Main Page';
include '../utils/tools.php';
include 'header.php';

/* Check if the admin has a password */
Tools::adminHasNoPassword();

/* Check if the admin has properly configured iChair */

$status = Tools::getAdminConfigStatus();
if($status != "") {
  print('<div class="ERRmessage">' . $status . '</div>');
  return null;
}


$number=20;
if (preg_match("/^[0-9]+$/",Tools::readPost('number'))) {
  $number=Tools::readPost('number');
}
?>
<center>
<form action="logs.php" method="post">
<table>
  <tr>
    <td><input type="text" size="5" name="number" value="<?php print($number);?>"/></td>
    <td><input type="submit" class="buttonLink" value="Thiết lập số dòng muốn hiển thị" /></td>
  </tr>
</table>
</form>

<h2><!--Submissions-->Log Gửi Bài</h2>

<?php Log::printCategoryLatestEvents(Log::$SUBMISSION_CATEGORY,$number); ?>

<h2><!--Admin-->Log Quản Trị</h2>

<?php Log::printCategoryLatestEvents(Log::$ADMIN_CATEGORY,$number); ?>

<h2><!--Reviews-->Log Đánh Giá</h2>

<?php Log::printCategoryLatestEvents(Log::$REVIEW_CATEGORY,$number); ?>
</center>


</body>
</html>
