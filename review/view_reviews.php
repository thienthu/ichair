<?php 

/*
 * Copyright (C) 2006, 2007 Thomas Baigneres, Matthieu Finiasz
 *
 * This file is part of iChair.
 *
 * iChair is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 * 
 * iChair is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

?><?php $page_title="Xem Đánh Giá";//'View Reviews';
include '../utils/tools.php';
include 'header_reduced.php';

if($currentReviewerGroup == Reviewer::$CHAIR_GROUP) {

  $reviewerNumber = $_GET['reviewerNumber'];
  $reviewer = Reviewer::getByReviewerNumber($reviewerNumber);
  $reviewReader = new ReviewReader();

  $articleNumber = Tools::ReadPost('articleNumber');  
  if(preg_match("/^[0-9]+$/",$articleNumber)) {
    $assignement = Assignement::getByNumbers($articleNumber, $reviewerNumber);
    if(!is_null($assignement)) {
      $assignement->setReviewStatus(Tools::readPost('newReviewStatus'));
      $article = Article::getByArticleNumber($articleNumber);
      $article->computeAverage();
    }
  }

  if(!is_null($reviewer)) {
?>
    <div class="paperBox">
      <div class="paperBoxDetails">
        <?php $reviewer->printLong(); ?>
      </div>
    </div>
<?php 
    $reviewerNumber = $reviewer->getReviewerNumber();
    $completedAssignedArticles = $reviewer->getAssignedArticlesByStatus(Assignement::$COMPLETED);
    $inProgressAssignedArticles = $reviewer->getAssignedArticlesByStatus(Assignement::$INPROGRESS);
    $notStartedAssignedArticles = $reviewer->getAssignedArticlesByStatus(Assignement::$VOID);
    $completedAuthorizedArticles = $reviewer->getAuthorizedArticlesByStatus(Assignement::$COMPLETED);
    $inProgressAuthorizedArticles = $reviewer->getAuthorizedArticlesByStatus(Assignement::$INPROGRESS);
    
    $total = count($completedAssignedArticles);
    $total += count($inProgressAssignedArticles);
    $total += count($notStartedAssignedArticles);
    $assignedTotal = $total;
    $total += count($completedAuthorizedArticles);
    $total += count($inProgressAuthorizedArticles);
    $authorizedTotal = $total - $assignedTotal;

    if ($total == 0) {
      print('<div class="OKmessage">Không có đánh giá từ '//No review from '
       . htmlentities($reviewer->getFullName(), ENT_COMPAT | ENT_HTML401, 'UTF-8') . ','// sorry!
       .'</div>');
    } else {

      if($assignedTotal != 0) {

	print('<h2>Đánh giá được phân công '//Reviews Assigned to ' 
    . htmlentities($reviewer->getFullName(), ENT_COMPAT | ENT_HTML401, 'UTF-8') . '</h2>');
	
	foreach($completedAssignedArticles as $articleNumber) {
	  $assignement = Assignement::getByNumbers($articleNumber, $reviewerNumber);
	  $reviewReader->createFromXMLFile($assignement);
	  $reviewReader->printDetailsBoxForChair();
	}

	foreach($inProgressAssignedArticles as $articleNumber) {
	  $assignement = Assignement::getByNumbers($articleNumber, $reviewerNumber);
	  $reviewReader->createFromXMLFile($assignement);
	  $reviewReader->printDetailsBoxForChair();
	}

	foreach($notStartedAssignedArticles as $articleNumber) {
	  $assignement = Assignement::getByNumbers($articleNumber, $reviewerNumber);
	  $reviewReader->createFromXMLFile($assignement);
	  $reviewReader->printDetailsBoxForChair();
	}

      }

      if($authorizedTotal != 0) {

	print('<h2>Đánh giá khác bởi '//Other Reviews by ' 
    . htmlentities($reviewer->getFullName(), ENT_COMPAT | ENT_HTML401, 'UTF-8') . '</h2>');
	
	foreach($completedAuthorizedArticles as $articleNumber) {
	  $assignement = Assignement::getByNumbers($articleNumber, $reviewerNumber);
	  $reviewReader->createFromXMLFile($assignement);
	  $reviewReader->printDetailsBoxForChair();
	}

	foreach($inProgressAuthorizedArticles as $articleNumber) {
	  $assignement = Assignement::getByNumbers($articleNumber, $reviewerNumber);
	  $reviewReader->createFromXMLFile($assignement);
	  $reviewReader->printDetailsBoxForChair();
	}

      }


    }
    
  
  ?>

  <center>
    <input type="submit" class="buttonLink bigButton" value="Đóng" onclick="javascript:window.close()" />
  </center>

<?php }} ?>

<?php include('footer.php'); ?>
