<?php 

/*
 * Copyright (C) 2006, 2007 Thomas Baigneres, Matthieu Finiasz
 *
 * This file is part of iChair.
 *
 * iChair is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 * 
 * iChair is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

?><?php $page_title="Gửi Mail";//'Mass Mailing';
include '../utils/tools.php';
include 'header.php';
if($currentReviewerGroup == Reviewer::$CHAIR_GROUP) {
?>

<?php 
  /* Check posted values */
  $currentTemplate = new MailTemplate();

  if(Tools::readPost('init_from_post') == 'yes') {
    $currentTemplate->createFromPOST();
  } else {
    $xmlFileName = false;
    $xmlFilePath = false;
    if(Tools::readPost('template_file')) {
      $xmlFileName = Tools::readPost('template_file');
    } 
    if(Tools::readPost('load_template') == "Tải lên mẫu mặc định") {
      $xmlFilePath = "../utils/mail_templates/";
    } else if(Tools::readPost('load_template') == "Tải lên mail đã gửi trước đó") {
      $xmlFilePath = Tools::getConfig('server/logPath') . 'mailing/';
    }
    if(!file_exists($xmlFilePath . $xmlFileName)) {
      $xmlFilePath = "../utils/mail_templates/";
      $xmlFileName = "void.xtmpl";
    } 
    $currentTemplate->createFromXML($xmlFileName, $xmlFilePath);
  }
  
?>


<h2>Tải mẫu<!--Load a Template--></h2>


<?php 

  /* Get All default mail templates  */
   
  $xmlTemplates = glob('../utils/mail_templates/*.xml');
  $mailTemplates = array();
  for($i = 0; $i < count($xmlTemplates); $i++) {
    $mailTemplates[$i] = new MailTemplate();
    $mailTemplates[$i]->createFromXML(substr($xmlTemplates[$i], strlen('../utils/mail_templates/')), '../utils/mail_templates/');
  }



  /* Display the default template selection list */  ?>
<table style="width: 100%;"><tr><td style="text-align: center;">
<form action="mailing.php" method="post">
  <select name="template_file">
    <optgroup label="Gửi mail đến ủy ban">
    <?php 
      foreach($mailTemplates as $mailTemplate) {
        if($mailTemplate->getCategory() == MailTemplate::$CATEGORY_COMMITTEE) {
	  if($mailTemplate->getXMLFileName() == $currentTemplate->getXMLFileName()) {
	    print('<option value="'. $mailTemplate->getXMLFileName() .'" selected>' . htmlentities($mailTemplate->getTitle(), ENT_COMPAT | ENT_HTML401, 'utf-8') .'</option>');
	  } else {
	    print('<option value="'. $mailTemplate->getXMLFileName() .'">' . htmlentities($mailTemplate->getTitle(), ENT_COMPAT | ENT_HTML401, 'utf-8') .'</option>');
	  }
        }
      }
    ?>
    </optgroup>
    <optgroup label="Gửi mail đến tác giả">
    <?php 
      foreach($mailTemplates as $mailTemplate) {
        if($mailTemplate->getCategory() == MailTemplate::$CATEGORY_AUTHORS) {
	  if($mailTemplate->getXMLFileName() == $currentTemplate->getXMLFileName()) {
	    print('<option value="'. $mailTemplate->getXMLFileName() .'" selected>' . htmlentities($mailTemplate->getTitle(), ENT_COMPAT | ENT_HTML401, 'utf-8') .'</option>');
	  } else {
	    print('<option value="'. $mailTemplate->getXMLFileName() .'">' . htmlentities($mailTemplate->getTitle(), ENT_COMPAT | ENT_HTML401, 'utf-8') .'</option>');
	  }
        }
      }
    ?>
    </optgroup>
  </select>
  <input type="submit" class="buttonLink" value="Tải lên mẫu mặc định" name="load_template" />
</form>
</td>
<?php 
  /* Get All default mail templates  */
   
  $xmlHistory = array_reverse(glob(Tools::getConfig('server/logPath') . 'mailing/*.xml'));
  $mailHistory = array();
  for($i = 0; $i < count($xmlHistory); $i++) {
    $mailHistory[$i] = new MailTemplate();
    $mailHistory[$i]->createFromXML(substr($xmlHistory[$i], strlen(Tools::getConfig('server/logPath').'mailing/')), Tools::getConfig('server/logPath').'mailing/');
  }

 /* Display the history of mailing */
 if(count($mailHistory) != 0) {
   ?>
  <td style="text-align: center; width: 50%;">
  <form action="mailing.php" method="post">
    <select name="template_file">
      <?php 
        foreach($mailHistory as $mailTemplate) {
          print('<option value="'. $mailTemplate->getXMLFileName() .'">' . htmlentities($mailTemplate->getTitle(), ENT_COMPAT | ENT_HTML401, 'utf-8') .'</option>');
        }
      ?>
    </select>
    <input type="submit" class="buttonLink" value="Tải lên mail đã gửi trước đó" name="load_template" />
  </form>
  </td>
<?php } ?>
</tr></table>

<h2>Tùy Chỉnh Mail<!--Customize Mail--></h2>

<center>
  <form action="mailing_confirm.php" method="post">
    <table>
      <tr>
        <td colspan="2">Chủ đề<!--Subject-->:</td>
      </tr>
      <tr>
        <td colspan="2"><input type="text" name="subject" size="50" value="<?php Tools::printHTML($currentTemplate->getSubject()); ?>" /></td>
      </tr>
      <tr>
        <td colspan="2">Nội dung<!--Message-->:</td>
      </tr>
      <tr>
        <td colspan="2"><textarea name="body" rows="25" cols="80"><?php Tools::printHTML($currentTemplate->getBody()); ?></textarea></td>
      </tr>
      <tr>
        <td>Gửi mail đến ban thành viên<!--Send to Committee Members--></td>
        <td>Gửi mail đến tác giả<!--Send to Contact Authors--></td>
      </tr>
      <tr>
        <td><input class="noBorder" type="radio" name="to_committee" value="none" id="committee_none" <?php if($currentTemplate->getToCommittee() == MailTemplate::$TO_NONE) print('checked="checked"'); ?>/><label for="committee_none">Không ai<!--None--></label></td>
        <td><input class="noBorder" type="radio" name="to_authors" value="none" id="authors_none" <?php if($currentTemplate->getToAuthors() == MailTemplate::$TO_NONE) print('checked="checked"'); ?>/><label for="authors_none">Không ai<!--None--></label></td>
      </tr>
      <tr>
        <td><input class="noBorder" type="radio" name="to_committee" value="all" id="committee_all" <?php if($currentTemplate->getToCommittee() == MailTemplate::$TO_ALL) print('checked="checked"'); ?>/><label for="committee_all">Tất cả<!--All--></label></td>
        <td><input class="noBorder" type="radio" name="to_authors" value="all" id="authors_all" <?php if($currentTemplate->getToAuthors() == MailTemplate::$TO_ALL) print('checked="checked"'); ?>/><label for="authors_all">Tất cả<!--All--></label></td>
      </tr>
      <tr valign="top">
        <td>
            <label for="committee_group">
	        <input class="noBorder" type="radio" name="to_committee" value="group" id="committee_group" <?php if($currentTemplate->getToCommittee() == MailTemplate::$TO_GROUP) print('checked="checked"'); ?>/>Nhóm<!--Group--><br />
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input class="noBorder" type="checkbox" name="to_chair" value="yes" <?php if(preg_match("/(^|,)chair(,|$)/", $currentTemplate->getToCommitteeGroup())) print('checked="checked"'); ?> />Ban tổ chức<!--Chair(s)--><br />
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input class="noBorder" type="checkbox" name="to_reviewer" value="yes" <?php if(preg_match("/(^|,)reviewer(,|$)/", $currentTemplate->getToCommitteeGroup())) print('checked="checked"'); ?> />Người đánh giá<!--Reviewers--><br />
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input class="noBorder" type="checkbox" name="to_observer" value="yes" <?php if(preg_match("/(^|,)observer(,|$)/", $currentTemplate->getToCommitteeGroup())) print('checked="checked"'); ?> />Người quan sát<!--Neutral Observer(s)-->
     	     </label>
        </td>
        <td>
	      <label for="authors_group">
	        <input class="noBorder" type="radio" name="to_authors" value="group" id="authors_group" <?php if($currentTemplate->getToAuthors() == MailTemplate::$TO_GROUP) print('checked="checked"'); ?>/>Nhóm<!--Group--><br />
	        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input class="noBorder" type="checkbox" name="to_accept" value="yes" <?php if(preg_match("/(^|,)accept(,|$)/", $currentTemplate->getToAuthorsGroup())) print('checked="checked"'); ?> />Đã chấp nhận<!--Accepted--><br />
	        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input class="noBorder" type="checkbox" name="to_maybe_accept" value="yes" <?php if(preg_match("/(^|,)maybe_accept(,|$)/", $currentTemplate->getToAuthorsGroup())) print('checked="checked"'); ?> />Có thể đã chấp nhận<!--Maybe Accepted--><br />
	        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input class="noBorder" type="checkbox" name="to_discuss" value="yes" <?php if(preg_match("/(^|,)discuss(,|$)/", $currentTemplate->getToAuthorsGroup())) print('checked="checked"'); ?> />Đang trong thảo luận<!--In Discussion--><br />
	        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input class="noBorder" type="checkbox" name="to_maybe_reject" value="yes" <?php if(preg_match("/(^|,)maybe_reject(,|$)/", $currentTemplate->getToAuthorsGroup())) print('checked="checked"'); ?> />Có thể đã tứ chối<!--Maybe Rejected--><br />
	        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input class="noBorder" type="checkbox" name="to_reject" value="yes" <?php if(preg_match("/(^|,)reject(,|$)/", $currentTemplate->getToAuthorsGroup())) print('checked="checked"'); ?> />Đã từ chối<!--Rejected--><br />
	        <?php if (Tools::useCustomAccept1()) { ?>
	          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input class="noBorder" type="checkbox" name="to_custom1" value="yes" <?php if(preg_match("/(^|,)custom1(,|$)/", $currentTemplate->getToAuthorsGroup())) print('checked="checked"'); ?> /><?php print(Tools::getCustomAccept1()); ?><br />
                <?php } ?>
	        <?php if (Tools::useCustomAccept2()) { ?>
	          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input class="noBorder" type="checkbox" name="to_custom2" value="yes" <?php if(preg_match("/(^|,)custom2(,|$)/", $currentTemplate->getToAuthorsGroup())) print('checked="checked"'); ?> /><?php print(Tools::getCustomAccept2()); ?><br />
                <?php } ?>
	        <?php if (Tools::useCustomAccept3()) { ?>
	          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input class="noBorder" type="checkbox" name="to_custom3" value="yes" <?php if(preg_match("/(^|,)custom3(,|$)/", $currentTemplate->getToAuthorsGroup())) print('checked="checked"'); ?> /><?php print(Tools::getCustomAccept3()); ?><br />
                <?php } ?>
	        <?php if (Tools::useCustomAccept4()) { ?>
	          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input class="noBorder" type="checkbox" name="to_custom4" value="yes" <?php if(preg_match("/(^|,)custom4(,|$)/", $currentTemplate->getToAuthorsGroup())) print('checked="checked"'); ?> /><?php print(Tools::getCustomAccept4()); ?><br />
                <?php } ?>
	        <?php if (Tools::useCustomAccept5()) { ?>
	          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input class="noBorder" type="checkbox" name="to_custom5" value="yes" <?php if(preg_match("/(^|,)custom5(,|$)/", $currentTemplate->getToAuthorsGroup())) print('checked="checked"'); ?> /><?php print(Tools::getCustomAccept5()); ?><br />
                <?php } ?>
	        <?php if (Tools::useCustomAccept6()) { ?>
	          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input class="noBorder" type="checkbox" name="to_custom6" value="yes" <?php if(preg_match("/(^|,)custom6(,|$)/", $currentTemplate->getToAuthorsGroup())) print('checked="checked"'); ?> /><?php print(Tools::getCustomAccept6()); ?><br />
                <?php } ?>
	        <?php if (Tools::useCustomAccept7()) { ?>
	          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input class="noBorder" type="checkbox" name="to_custom7" value="yes" <?php if(preg_match("/(^|,)custom7(,|$)/", $currentTemplate->getToAuthorsGroup())) print('checked="checked"'); ?> /><?php print(Tools::getCustomAccept7()); ?><br />
                <?php } ?>
	        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input class="noBorder" type="checkbox" name="to_void" value="yes" <?php if(preg_match("/(^|,)void(,|$)/", $currentTemplate->getToAuthorsGroup())) print('checked="checked"'); ?> />Bỏ trống<!--Void-->
	      </label>
	</td>
      </tr>
      <tr style="vertical-align: top;">
        <?php if($currentTemplate->getToCommitteeList() != "") { ?>
          <td><input class="noBorder" type="radio" name="to_committee" value="list" id="committee_list" <?php if($currentTemplate->getToCommittee() == MailTemplate::$TO_LIST) print('checked="checked"'); ?>/>
	      <label for="committee_list">
	        Predefined List<br />
	        <?php 
	        $list = explode(',',$currentTemplate->getToCommitteeList());
		$reviewers = Reviewer::getAllReviewersSparse();
		foreach($list as $reviewerNumber) {
		  $rev = $reviewers[$reviewerNumber];
		  if(!is_null($rev)) {
		    print('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;');
		    Tools::printHTML($rev->getFullName() . ' (' . $rev->getEmail() . ')');
		    print('<br />');
		  }
		}
	        ?>
	      </label>
	      <input type="hidden" name="to_committee_list" value="<?php print($currentTemplate->getToCommitteeList()); ?>" />
	   </td>
        <?php } else { ?>
          <td></td>
        <?php } ?>
        <?php if($currentTemplate->getToAuthorsList() != "") { ?>
          <td><input class="noBorder" type="radio" name="to_authors" value="list" id="authors_list" <?php if($currentTemplate->getToAuthors() == MailTemplate::$TO_LIST) print('checked="checked"'); ?>/>
	      <label for="authors_list">
	        <?php if(preg_match("/,/", $currentTemplate->getToAuthorsList())) { 
	             print('Tác giả liên lạc của danh sách xác định truớc'//Contact Authors of Predefined List
                .'<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;');
		     print(str_replace(",", ", ", $currentTemplate->getToAuthorsList())); 
		   } else {
		     $article = Article::getByArticleNumber($currentTemplate->getToAuthorsList());
		     if(!is_null($article)) {
		       print('Tác giả của bài viết số'//Contact Authors of Article number ' 
            . $article->getArticleNumber() . '<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;');
		       Tools::printHTML($article->getTitle());
		     } else {
		       print('Bài viết số '//Article number ' 
            . $currentTemplate->getToAuthorsList() . 'không tồn tại.'// does not exist.'
            );
		     }
		   }
	        ?>
	      </label>
	      <input type="hidden" name="to_authors_list" value="<?php print($currentTemplate->getToAuthorsList()); ?>" />
	   </td>
        <?php } else { ?>
          <td></td>
        <?php } ?>
      </tr>
    </table>
    <input class="noBorder" type="checkbox" name="uncheck" value="yes" <?php if($currentTemplate->isUnchecked()) print('checked="checked"'); ?> id="uncheck" /><label for="uncheck">Bỏ chọn tất cả người nhận ban đầu.<!--Initially uncheck all recipients.--></label><br />
    <input type="submit" class="buttonLink bigButton" value="Xem trước mail" />
  </form>
</center>

<h2>Các Thẻ mail<!--Possible Mail Tags--></h2>

<center>
<table>
  <tr><td colspan="2"><b>Các thẻ chung<!--General Tags--></b></td></tr>
  <?php Tools::printMailTags(); ?>
  <tr><td colspan="2" style="padding-top: 10px;"><b>Các thẻ cụ thể cho ủy ban<!--Tags specific to the Committee--></b></td></tr>
  <?php Tools::printMailToCommitteeTags(); ?>
  <tr><td colspan="2" style="padding-top: 10px;"><b>Các thẻ cụ thể cho tác giả<!--Tags specific to Authors--></b></td></tr>
  <?php Tools::printMailToAuthorsTags(); ?>
</table>
</center>

<?php }?>

<?php include('footer.php'); ?>
