<?php 

/*
 * Copyright (C) 2006, 2007 Thomas Baigneres, Matthieu Finiasz
 *
 * This file is part of iChair.
 *
 * iChair is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 * 
 * iChair is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

?><?php 
class ReviewReader {

  private $articleNumber;
  private $reviewerNumber;

  private $articleTitle;

  private $reviewStatus;

  private $reviewerName;
  private $subreviewerName;

  private $overallGrade;
  private $confidenceLevel;
  private $technicalQuality;
  private $editorialQuality;
  private $suitability;
  private $bestPaper;

  private $toProgramCommittee;
  private $toAuthors;
  private $personalNotes;

  private $date;

  private $isInitialized;
  
  function __construct(){}
  
  function createFromXMLFile($assignement) {
    
    if($assignement == null) {
      return null;
    }
    $this->articleNumber = $assignement->getArticleNumber();
    $article = Article::getByArticleNumber($this->articleNumber);
    $this->articleTitle = $article->getTitle();

    $this->reviewStatus = $assignement->getReviewStatus();

    /* get the reviewer name from the assignement, not the xml file */
    $this->reviewerNumber = $assignement->getReviewerNumber();
    $reviewer = Reviewer::getByReviewerNumber($this->reviewerNumber);
    $this->reviewerName = $reviewer->getFullName();

    /* parse the XML file */

    $xmlFileName = $article->getFolder() . $assignement->getXMLFileName();

    if(file_exists($xmlFileName)) {

      $reviewDocument = new DOMDocument('1.0');
      $reviewDocument->load($xmlFileName);
      $domxPath = new DOMXpath($reviewDocument);
      
      $domNode = $domxPath->query("/xml/subreviewer")->item(0);
      if ($domNode->firstChild) {
        $this->subreviewerName = utf8_decode($domNode->firstChild->nodeValue);
      } else {
        $this->subreviewerName = "";
      }
      $domNode = $domxPath->query("/xml/overallGrade")->item(0);
      if ($domNode->firstChild) {
        $this->overallGrade = utf8_decode($domNode->firstChild->nodeValue);
      } else {
        $this->overallGrade = "";
      }
      $domNode = $domxPath->query("/xml/confidenceLevel")->item(0);
      if ($domNode->firstChild) {
        $this->confidenceLevel = utf8_decode($domNode->firstChild->nodeValue);
      } else {
        $this->confidenceLevel = "";
      }
      $domNode = $domxPath->query("/xml/technicalQuality")->item(0);
      if ($domNode->firstChild) {
        $this->technicalQuality = utf8_decode($domNode->firstChild->nodeValue);
      } else {
        $this->technicalQuality = "";
      }
      $domNode = $domxPath->query("/xml/editorialQuality")->item(0);
      if ($domNode->firstChild) {
        $this->editorialQuality = utf8_decode($domNode->firstChild->nodeValue);
      } else {
        $this->editorialQuality = "";
      }
      $domNode = $domxPath->query("/xml/suitability")->item(0);
      if ($domNode->firstChild) {
        $this->suitability = utf8_decode($domNode->firstChild->nodeValue);
      } else {
        $this->suitability = "";
      }
      $domNode = $domxPath->query("/xml/bestPaper")->item(0);
      if ($domNode->firstChild) {
        $this->bestPaper = utf8_decode($domNode->firstChild->nodeValue);
      } else {
        $this->bestPaper = "";
      }
      
      $domNode = $domxPath->query("/xml/toProgramCommittee")->item(0);
      if ($domNode->firstChild) {
        $this->toProgramCommittee = utf8_decode($domNode->firstChild->nodeValue);
      } else {
        $this->toProgramCommittee = "";
      }
      $domNode = $domxPath->query("/xml/toAuthors")->item(0);
      if ($domNode->firstChild) {
        $this->toAuthors = utf8_decode($domNode->firstChild->nodeValue);
      } else {
        $this->toAuthors = "";
      }
      $domNode = $domxPath->query("/xml/personalNotes")->item(0);
      if ($domNode->firstChild) {
        $this->personalNotes = utf8_decode($domNode->firstChild->nodeValue);
      } else {
        $this->personalNotes = "";
      }
      
      $domNode = $domxPath->query("/xml/date")->item(0);
      $this->date = utf8_decode($domNode->firstChild->nodeValue);
      $this->isInitialized = true;

    } else {

      $this->subreviewerName = "";
      $this->overallGrade = "";
      $this->confidenceLevel = "";
      $this->technicalQuality = "";
      $this->editorialQuality = "";
      $this->suitability = "";
      $this->bestPaper = "";
      $this->toProgramCommittee = "";
      $this->toAuthors = "";
      $this->personalNotes = "";
      $this->date = "";
      $this->isInitialized = false;
    }
    
  }

  public function getReviewStatus() {
    return $this->reviewStatus;
  }

  public function getReviewerName() {
    return $this->reviewerName;
  }
  public function getSubreviewerName() {
    return $this->subreviewerName;
  }

  public function getOverallGrade() {
    return $this->overallGrade;
  }
  public function getConfidenceLevel() {
    return $this->confidenceLevel;
  }
  public function getTechnicalQuality() {
    return $this->technicalQuality;
  }
  public function getEditorialQuality() {
    return $this->editorialQuality;
  }
  public function getSuitability() {
    return $this->suitability;
  }
  public function getBestPaper() {
    return $this->bestPaper;
  }

  public function getToProgramCommittee() {
    return $this->toProgramCommittee;
  }
  public function getToAuthors() {
    return $this->toAuthors;
  }
  public function getPersonalNotes() {
    return $this->personalNotes;
  }

  function printDetailsBoxTitle() {
    print('Người hổ trợ: '//Subreviewer: '
      );
    if($this->subreviewerName == "") {
      print('<em>Không có.'//None.
        .'</em><br/>');
    } else {
      print(htmlentities($this->subreviewerName, ENT_COMPAT | ENT_HTML401, 'UTF-8') . '<br/>');
    }
    print('&nbsp;<br/>');
  }

  public function printDetailsBox() {
    print('<div class="paperBox">');
    print('<div class="paperBoxTitle">');
    print('<div class="paperBoxNumber">' . htmlentities($this->reviewerName, ENT_COMPAT | ENT_HTML401, 'UTF-8') . '</div>');
    $this->printDetailsBoxTitle();
    print('</div>');    
    print('<div class="paperBoxDetails">');
    print('<table>');
    print('<tr><td>Đánh giá chung:'//Overall Grade:
      .'</td><td>' . $this->overallGrade . '</td></tr>');
    if(Tools::useConfidenceLevel()) {
      print('<tr><td>Độ tin cậy của nhận xét:'//Confidence Level:
      .'</td><td>' . $this->confidenceLevel . '</td></tr>');
    }
    if(Tools::useTechnicalQuality()) {
      print('<tr><td>Độ chuyên môn:'//Technical Level:
      .'</td><td>' . $this->technicalQuality . '</td></tr>');
    }
    if(Tools::useEditorialQuality()) {
      print('<tr><td>Chất luợng biên tập:'//Editorial Quality:
      .'</td><td>' . $this->editorialQuality . '</td></tr>');
    }
    if(Tools::useSuitability()) {
      print('<tr><td>Mức phù hợp:'//Suitability:
      .'</td><td>' . $this->suitability . '</td></tr>');
    }
    if(Tools::useBestPaper()) {
      print('<tr><td>Bài viết tốt nhất:'//Best Paper:
      .'</td><td>' . $this->bestPaper . '</td></tr>');
    }
    print('</table>');
    if(Tools::useToProgramCommittee()) {
      print('Bình luận từ uỷ ban hội thảo:'//Comments to the Program Committee:'
        );
      print('<div class="versionAbstract">');
      if ($this->toProgramCommittee == ""){
	print('<em>Không có.'//None.
        .'</em>');
      } else {
	Tools::printHTMLbr($this->toProgramCommittee);
      }
      print('</div>');
    }
    if(Tools::useToAuthors()) {
      print('Bình luận từ tác giả:'//Comments to the Authors:'
        );
      print('<div class="versionAbstract">');
      if ($this->toAuthors == ""){
	print('<em>Không có.'//None.
        .'</em>');
      } else {
	Tools::printHTMLbr($this->toAuthors);
      }
      print('</div>');
    }
    print('</div>');
    print('</div>');
  }
   public function printDetailsBoxPopUp() {
     if (Tools::useToProgramCommittee() || Tools::useToAuthors()) {
       print('<div class="paperBox">');
       print('<div class="paperBoxDetails">');
       if(Tools::useToProgramCommittee()) {
	 print('Bình luận từ uỷ ban hội thảo:'//Comments to the Program Committee:'
    );
	 print('<div class="versionAbstract">');
	 if ($this->toProgramCommittee == "") {
	   print('<em>Không có.'//None.
        .'</em>');
	 } else {
	   Tools::printHTMLreport($this->toProgramCommittee,2000);
	 }
	 print('</div>');
       }
       if(Tools::useToAuthors()) {
	 print('Bình luận từ tác giả:'//Comments to the Authors:'
    );
	 print('<div class="versionAbstract">');
	 if ($this->toAuthors == ""){
	   print('<em>Không có.'//None.
        .'</em>');
	 } else {
	   Tools::printHTMLreport($this->toAuthors,2000);
	 }
	 print('</div>');
       }
       print('</div>');
       print('</div>');
     }
   }
  
  public function printDetailsBoxPopUpComplete() {
    if ($this->isInitialized) {
    print('<div class="paperBox">');
    print('<div class="paperBoxDetails">');
      if ($this->subreviewerName != "") {
	print('<div class="versionAuthors">Subreviewer: '.$this->subreviewerName.'</div>');
      }
      print('<table>');
      print('<tr><td class="leftAlign">Đánh giá chung:'//Overall Grade:
      .'</td><td>' . $this->overallGrade . '</td></tr>');
      if(Tools::useConfidenceLevel()) {
	print('<tr><td class="leftAlign">Độ tin cậy của nhận xét:'//Confidence Level:
      .'</td><td>' . $this->confidenceLevel . '</td></tr>');
      }
      if(Tools::useTechnicalQuality()) {
	print('<tr><td class="leftAlign">Độ chuyên môn:'//Technical Level:
      .'</td><td>' . $this->technicalQuality . '</td></tr>');
      }
      if(Tools::useEditorialQuality()) {
	print('<tr><td class="leftAlign">Chất luợng biên tập:'//Editorial Quality:
      .'</td><td>' . $this->editorialQuality . '</td></tr>');
      }
      if(Tools::useSuitability()) {
	print('<tr><td class="leftAlign">Mức phù hợp:'//Suitability:
      .'</td><td>' . $this->suitability . '</td></tr>');
      }
      if(Tools::useBestPaper()) {
	print('<tr><td class="leftAlign">Bài viết tốt nhất:'//Best Paper:
      .'</td><td>' . $this->bestPaper . '</td></tr>');
      }
      print('</table>');
      if(Tools::useToProgramCommittee()) {
	print('Bình luận từ uỷ ban hội thảo:'//Comments to the Program Committee:'
    );
	print('<div class="versionAbstract">');
	if ($this->toProgramCommittee == "") {
	  print('<em>Không có.'//None.
        .'</em>');
	} else {
	  Tools::printHTMLreport($this->toProgramCommittee,1500);
	}
	print('</div>');
      }
      if(Tools::useToAuthors()) {
	print('Bình luận từ tác giả:'//Comments to the Authors:'
    );
	print('<div class="versionAbstract">');
	if ($this->toAuthors == ""){
	  print('<em>Không có.'//None.
        .'</em>');
	} else {
	  Tools::printHTMLreport($this->toAuthors,1500);
	}
	print('</div>');
      }
      if ($this->personalNotes != ""){
	print('Personal Notes:');
	print('<div class="versionAbstract">');
	Tools::printHTMLreport($this->personalNotes,1000);      
	print('</div>');
      }
      print('</div>');
      print('</div>');
    }
  }
  
  function printDetailsBoxTitleForChair() {
    print('Title: ' . htmlentities($this->articleTitle, ENT_COMPAT | ENT_HTML401, 'UTF-8') . '<br/>');
    if($this->subreviewerName == "") {
      print('&nbsp;<br/>');
    } else {
      print('Người hổ trợ: '//Subreviewer: '
        . htmlentities($this->subreviewerName, ENT_COMPAT | ENT_HTML401, 'UTF-8') . '<br/>');
    }
  }

  public function printDetailsBoxForChair() {
    print('<div class="paperBox" id="nbr'. $this->articleNumber .'">');
    print('<div class="paperBoxTitle">');
    print('<div class="paperBoxNumber">Article&nbsp;' . $this->articleNumber . '</div>');
    $this->printDetailsBoxTitleForChair();
    print('</div>');    
    print('<div class="paperBoxDetails">');
    /* Print status of the review*/
    if($this->reviewStatus == Assignement::$COMPLETED) {
      print('<div class="floatRight completedReview">Hoàn thành đánh giá'//Review Completed
        .'</div>');
    } else if($this->reviewStatus == Assignement::$INPROGRESS) {
      print('<div class="floatRight inProgressReview">Trong quá trình đánh giá'//Review in Progress
        .'</div>');
    } else if($this->reviewStatus == Assignement::$VOID) {
      print('<div class="floatRight voidReview">Chưa đánh giá'//No Review Yet
        .'</div>');
    }
    if($this->reviewStatus != Assignement::$VOID) {
      /* print the grades */
      print('<table>');
      print('<tr><td>Đánh giá chung:'//Overall Grade:
      .'</td><td>' . $this->overallGrade . '</td></tr>');
      if(Tools::useConfidenceLevel()) {
	print('<tr><td>Độ tin cậy của nhận xét:'//Confidence Level:
      .'</td><td>' . $this->confidenceLevel . '</td></tr>');
      }
      if(Tools::useTechnicalQuality()) {
	print('<tr><td>Độ chuyên môn:'//Technical Level:
      .'</td><td>' . $this->technicalQuality . '</td></tr>');
      }
      if(Tools::useEditorialQuality()) {
	print('<tr><td>Chất luợng biên tập:'//Editorial Quality:
      .'</td><td>' . $this->editorialQuality . '</td></tr>');
      }
      if(Tools::useSuitability()) {
	print('<tr><td>Mức phù hợp:'//Suitability:
      .'</td><td>' . $this->suitability . '</td></tr>');
      }
      if(Tools::useBestPaper()) {
	print('<tr><td>Bài viết tốt nhất:'//Best Paper:
      .'</td><td>' . $this->bestPaper . '</td></tr>');
      }
      print('</table>');
      if(Tools::useToProgramCommittee()) {
	print('Bình luận từ uỷ ban hội thảo:'//Comments to the Program Committee:'
    );
	print('<div class="versionAbstract">');
	if ($this->toProgramCommittee == ""){
	  print('<em>Không có.'//None.
        .'</em>');
	} else {
	  Tools::printHTMLbr($this->toProgramCommittee);
	}
	print('</div>');
      }
      if(Tools::useToAuthors()) {
	print('Bình luận từ tác giả:'//Comments to the Authors:'
    );
	print('<div class="versionAbstract">');
	if ($this->toAuthors == ""){
	  print('<em>Không có.'//None.
        .'</em>');
	} else {
	  Tools::printHTMLbr($this->toAuthors);
	}
	print('</div>');
      }
      print('</div>');
      /* Add a complete/inProgress button */
      if($this->reviewStatus == Assignement::$INPROGRESS) {
	print('<div class="floatRight">');
	print('<form action="view_reviews.php?reviewerNumber='. $this->reviewerNumber .'#nbr' . $this->articleNumber . '" method="post">');
	print('<input type="submit" class="buttonLink" value="Đánh dấu hoàn thành" />');
	print('<input type="hidden" name="articleNumber" value="'. $this->articleNumber .'" />');
	print('<input type="hidden" name="newReviewStatus" value="'. Assignement::$COMPLETED .'" />');
	print('</form>');
	print('</div>');
      } else if($this->reviewStatus == Assignement::$COMPLETED) {
	print('<div class="floatRight">');
	print('<form action="view_reviews.php?reviewerNumber='. $this->reviewerNumber .'#nbr' . $this->articleNumber . '" method="post">');
	print('<input type="submit" class="buttonLink" value="Đánh dấu trong quá trình" />');
	print('<input type="hidden" name="articleNumber" value="'. $this->articleNumber .'" />');
	print('<input type="hidden" name="newReviewStatus" value="'. Assignement::$INPROGRESS .'" />');
	print('</form>');
	print('</div>');
      }
      print('<div class="clear"></div>');
    } else {
      print('<div class="clear"></div>');
      print('</div>');
    }
    print('</div>');
  }

  public function printDetailsBoxForChairBis() {
    print('<div class="paperBox"  id="nbr'. $this->reviewerNumber .'">');
    print('<div class="paperBoxTitle">');
    print('<div class="paperBoxNumber">' . htmlentities($this->reviewerName, ENT_COMPAT | ENT_HTML401, 'UTF-8') . '</div>');
    $this->printDetailsBoxTitle();
    print('</div>');    
    print('<div class="paperBoxDetails">');
    /* Print status of the review*/
    if($this->reviewStatus == Assignement::$COMPLETED) {
      print('<div class="floatRight completedReview">Hoàn thành đánh giá'//Review Completed
        .'</div>');
    } else if($this->reviewStatus == Assignement::$INPROGRESS) {
      print('<div class="floatRight inProgressReview">Trong quá trình đánh giá'//Review in Progress
        .'</div>');
    } else if($this->reviewStatus == Assignement::$VOID) {
      print('<div class="floatRight voidReview">Chưa đánh giá'//No Review Yet
        .'</div>');
    }
    if($this->reviewStatus != Assignement::$VOID) {
      /* print the grades */
      print('<table>');
      print('<tr><td>Đánh giá chung:'//Overall Grade:
      .'</td><td>' . $this->overallGrade . '</td></tr>');
      if(Tools::useConfidenceLevel()) {
	print('<tr><td>Độ tin cậy của nhận xét:'//Confidence Level:
      .'</td><td>' . $this->confidenceLevel . '</td></tr>');
      }
      if(Tools::useTechnicalQuality()) {
	print('<tr><td>Độ chuyên môn:'//Technical Level:
      .'</td><td>' . $this->technicalQuality . '</td></tr>');
      }
      if(Tools::useEditorialQuality()) {
	print('<tr><td>Chất luợng biên tập:'//Editorial Quality:
      .'</td><td>' . $this->editorialQuality . '</td></tr>');
      }
      if(Tools::useSuitability()) {
	print('<tr><td>Mức phù hợp:'//Suitability:
      .'</td><td>' . $this->suitability . '</td></tr>');
      }
      if(Tools::useBestPaper()) {
	print('<tr><td>Bài viết tốt nhất:'//Best Paper:
      .'</td><td>' . $this->bestPaper . '</td></tr>');
      }
      print('</table>');
      if(Tools::useToProgramCommittee()) {
	print('Bình luận từ uỷ ban hội thảo:'//Comments to the Program Committee:'
    );
	print('<div class="versionAbstract">');
	if ($this->toProgramCommittee == ""){
	  print('<em>Không có.'//None.
      .'</em>');
	} else {
	  Tools::printHTMLbr($this->toProgramCommittee);
	}
	print('</div>');
      }
      if(Tools::useToAuthors()) {
	print('Bình luận từ tác giả:'//Comments to the Authors:'
    );
	print('<div class="versionAbstract">');
	if ($this->toAuthors == ""){
	  print('<em>Không có.'//None.
      .'</em>');
	} else {
	  Tools::printHTMLbr($this->toAuthors);
	}
	print('</div>');
      }
      print('</div>');
      /* Add a complete/inProgress button */
      if($this->reviewStatus == Assignement::$INPROGRESS) {
	print('<div class="floatRight">');
	print('<form action="view_reviews_bis.php?articleNumber='. $this->articleNumber .'#nbr' . $this->reviewerNumber . '" method="post">');
	print('<input type="submit" class="buttonLink" value="Đánh dấu hoàn thành" />');
	print('<input type="hidden" name="reviewerNumber" value="'. $this->reviewerNumber .'" />');
	print('<input type="hidden" name="newReviewStatus" value="'. Assignement::$COMPLETED .'" />');
	print('</form>');
	print('</div>');
      } else if($this->reviewStatus == Assignement::$COMPLETED) {
	print('<div class="floatRight">');
	print('<form action="view_reviews_bis.php?articleNumber='. $this->articleNumber .'#nbr' . $this->reviewerNumber . '" method="post">');
	print('<input type="submit" class="buttonLink" value="Đánh dấu trong quá trình" />');
	print('<input type="hidden" name="reviewerNumber" value="'. $this->reviewerNumber .'" />');
	print('<input type="hidden" name="newReviewStatus" value="'. Assignement::$INPROGRESS .'" />');
	print('</form>');
	print('</div>');
      }
      print('<div class="clear"></div>');
    } else {
      print('<div class="clear"></div>');
      print('</div>');
    }
    print('</div>');
  }

  static function printReviewRowHead() {

    print('<th>&nbsp;</th>');
    print('<th>Nguời hỗ trợ '//Subreviewer
      .'</th>');
    
    print('<th>Overall<br />Grade</th>');
    if(Tools::useConfidenceLevel()) {
      print('<th>Conf.<br />Level</th>');
    }
    if(Tools::useTechnicalQuality()) {
      print('<th>Tech.<br />Level</th>');
    }
    if(Tools::useEditorialQuality()) {
      print('<th>Edit.<br />Quality</th>');
    }
    if(Tools::useSuitability()) {
      print('<th>Suit.</th>');
    }
    if(Tools::useBestPaper()) {
      print('<th>Best<br />Paper</th>');
    }
    
    print('<th>Date</th>');
  }


  static function printReviewSeparator($text) {

    print('<td colspan="2" class="leftAlign"><em>'.$text.'</em></td>');
    if(Tools::useConfidenceLevel()) {
      print('<td>&nbsp;</td>');
    }
    if(Tools::useTechnicalQuality()) {
      print('<td>&nbsp;</td>');
    }
    if(Tools::useEditorialQuality()) {
      print('<td>&nbsp;</td>');
    }
    if(Tools::useSuitability()) {
      print('<td>&nbsp;</td>');
    }
    if(Tools::useBestPaper()) {
      print('<td>&nbsp;</td>');
    }
    print('<td>&nbsp;</td>');
  }

  public function printReviewRow($useColor) {
    if ($useColor) {
      if ($this->reviewStatus == Assignement::$COMPLETED) {
	print('<td class="leftAlign"><div class="completedReview">Hoàn thành'//Completed
    .'</div></td><td>'. htmlentities($this->reviewerName, ENT_COMPAT | ENT_HTML401, 'UTF-8') .'</td>');
      } else if ($this->reviewStatus == Assignement::$INPROGRESS) {
	print('<td class="leftAlign"><div class="inProgressReview">Trong quá trình'//In Progress
    .'</div></td><td>'. htmlentities($this->reviewerName, ENT_COMPAT | ENT_HTML401, 'UTF-8') .'</td>');
      } else {
	print('<td class="leftAlign"><div class="voidReview">Không đánh giá'//No Review
    .'</div></td><td>'. htmlentities($this->reviewerName, ENT_COMPAT | ENT_HTML401, 'UTF-8') .'</td>');
      }
    } else {
      print('<td class="leftAlign">'. htmlentities($this->reviewerName, ENT_COMPAT | ENT_HTML401, 'UTF-8') . '</td>');
    }
    print('<td class="leftAlign">'. htmlentities($this->subreviewerName, ENT_COMPAT | ENT_HTML401, 'UTF-8') . '</td>');

    print('<td>'. $this->overallGrade .'</td>');
    if(Tools::useConfidenceLevel()) {
      print('<td>'. $this->confidenceLevel .'</td>');
    }
    if(Tools::useTechnicalQuality()) {
      print('<td>'. $this->technicalQuality .'</td>');
    }
    if(Tools::useEditorialQuality()) {
      print('<td>'. $this->editorialQuality .'</td>');
    }
    if(Tools::useSuitability()) {
      print('<td>'. $this->suitability .'</td>');
    }
    if(Tools::useBestPaper()) {
      print('<td>'. $this->bestPaper .'</td>');
    }

    /* Print Date */
    if ($this->date != "" ) {
      $year = substr($this->date,0,4);
      $month = substr($this->date,4,2);
      $day = substr($this->date,6,2);
      $hours = substr($this->date,9,2);
      $minutes = substr($this->date,11,2);
      print('<td>' . $day . '/' . $month . '/' . $year . ' - ' . $hours . ':' . $minutes . '</td>');
    } else {
      print('<td>&nbsp;</td>');
    }
  }
}

?>
