<?php 

/*
 * Copyright (C) 2006, 2007 Thomas Baigneres, Matthieu Finiasz
 *
 * This file is part of iChair.
 *
 * iChair is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 * 
 * iChair is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

?><?php $page_title="Xuất Danh Sách Bài Viết";//'Export Article List';
include '../utils/tools.php';

$currentReviewerGroup = Reviewer::getGroupByLogin(); 
$currentReviewer = Reviewer::getReviewerByLogin(); 

if($currentReviewerGroup == Reviewer::$CHAIR_GROUP) {

  $status="";
  $table=array();
  $format = "txt";

  $useNumber = (Tools::readPost('number') != "");
  $useTitle = (Tools::readPost('title') != "");
  $useAuthors = (Tools::readPost('authors') != "");
  $useAffiliations = (Tools::readPost('affiliations') != "");
  $useAbstract = (Tools::readPost('abstract') != "");
  $inclWithdrawn = (Tools::readPost('withdrawn') != "");
  $inclNotWithdrawn = (Tools::readPost('not_withdrawn') != "");
  $inclAccept = (Tools::readPost('accept') != "");
  $inclMaybeAccept = (Tools::readPost('maybe_accept') != "");
  $inclDiscuss = (Tools::readPost('discuss') != "");
  $inclMaybeReject = (Tools::readPost('maybe_reject') != "");
  $inclReject = (Tools::readPost('reject') != "");
  $inclVoid = (Tools::readPost('void') != "");
  $inclCustom1 = (Tools::readPost('custom1') != "");
  $inclCustom2 = (Tools::readPost('custom2') != "");
  $inclCustom3 = (Tools::readPost('custom3') != "");
  $inclCustom4 = (Tools::readPost('custom4') != "");
  $inclCustom5 = (Tools::readPost('custom5') != "");
  $inclCustom6 = (Tools::readPost('custom6') != "");
  $inclCustom7 = (Tools::readPost('custom7') != "");

  $sort = Tools::readPost('sort');
  if(($sort != "number") && ($sort != "title") && ($sort != "authors") && ($sort != "affiliations") && ($sort != "abstract")) {
    $sort = "number";
  }

  if (Tools::readPost('export') != "") {
    if (Tools::readPost('database') == "submissions") {

      $submissions;
      if($inclWithdrawn && $inclNotWithdrawn) {
	$submissions = Submission::getAllSubmissions();
      } else if((!$inclWithdrawn) && $inclNotWithdrawn) {
	$submissions = Submission::getAllNotWithdrawnSubmissions();
      } else if($inclWithdrawn && (!$inclNotWithdrawn)) {
	$submissions = Submission::getAllWithdrawnSubmissions();
      } else {
	$status .= "Vui lòng chọn ít nhất loại bài viết gửi (hủy hoặc không).";//Please choose at least one type of submission (withdrawn or not).";
      }
      if($status == "") {
	if (count($submissions) == 0) {
	  $status .= "Không có bài viết gửi lên trong thời gian này";//There are no valid submissions at the moment.";
	}
	foreach($submissions as $submission) {
	  $version = $submission->getLastVersion();
	  $table[$submission->getSubmissionNumber()] = array("number" => $submission->getSubmissionNumber(), "title" => $version->getTitle(), "authors" => $version->getAuthors(), "affiliations" => $version->getAffiliations(), "abstract" => $version->getAbstract());
	}
      }
    } else {

      $articles = Article::getAllArticles();
      if (count($articles) == 0) {
	$status .= "Không có bài viết trong khoảng thời gian này.";//There is no article at the moment.";
      }
      foreach($articles as $article) {
	if((($article->getAcceptance() == Article::$ACCEPT) && ($inclAccept)) ||
	   (($article->getAcceptance() == Article::$MAYBE_ACCEPT) && ($inclMaybeAccept)) ||
	   (($article->getAcceptance() == Article::$DISCUSS) && ($inclDiscuss)) ||
	   (($article->getAcceptance() == Article::$MAYBE_REJECT) && ($inclMaybeReject)) ||
	   (($article->getAcceptance() == Article::$REJECT) && ($inclReject)) ||
	   (($article->getAcceptance() == Article::$CUSTOM1) && ($inclCustom1)) ||
	   (($article->getAcceptance() == Article::$CUSTOM2) && ($inclCustom2)) ||
	   (($article->getAcceptance() == Article::$CUSTOM3) && ($inclCustom3)) ||
	   (($article->getAcceptance() == Article::$CUSTOM4) && ($inclCustom4)) ||
	   (($article->getAcceptance() == Article::$CUSTOM5) && ($inclCustom5)) ||
	   (($article->getAcceptance() == Article::$CUSTOM6) && ($inclCustom6)) ||
	   (($article->getAcceptance() == Article::$CUSTOM7) && ($inclCustom7)) ||
	   (($article->getAcceptance() == Article::$VOID) && ($inclVoid))) {
	  $table[$article->getArticleNumber()] = array("number" => $article->getArticleNumber(), "title" => $article->getTitle(), "authors" => $article->getAuthors(), "affiliations" => $article->getAffiliations(), "abstract" => $article->getAbstract());
	}
      }
      if (($status == "") && (count($table) == 0)) {
	$status .= "Không có loại bài viết chọn.";//There is no article in the categories you selected.";
      }
    }
    $format = "txt";

    if ((!$useNumber) && (!$useTitle) && (!$useAuthors) && (!$useAffiliations) && (!$useAbstract)) {
      $status .= "Vui lòng chọn ít nhất một mục để xuất";//Please select at least one field to export.";
    }
  } else {
    $useNumber = true;
    $useTitle = true;
    $useAuthors = true;
    $useAffiliations = true;
    $useAbstract = true;
    $inclWithdrawn = false;
    $inclNotWithdrawn = true;
    $inclAccept = true;
    $inclMaybeAccept = true;
    $inclDiscuss = true;
    $inclMaybeReject = true;
    $inclReject = true;
    $inclVoid = true;
    $inclCustom1 = true;
    $inclCustom2 = true;
    $inclCustom3 = true;
    $inclCustom4 = true;
    $inclCustom5 = true;
    $inclCustom6 = true;
    $inclCustom7 = true;
  }

  if ((Tools::readPost('export') != "") && ($status == "")) {
        
    $sortedArray = array();
    
    /* Create an array indexed by the sort key and sort it according to this key */
    foreach($table as $num => $elem) {
      $sortedArray[$num] = $elem[$sort];
    }
    natcasesort($sortedArray);

    header('Content-type: text/plain; charset=UTF-8');
    header('Content-Disposition: attachment; filename="article_list.'.$format.'"');
    foreach($sortedArray as $number => $bozo) {
      /* Output the list in txt format */
      if($useNumber) {
	print("Số: "//Number: " 
    . $table[$number]["number"] . "\n");
      }
      if($useTitle) {
	print("Tiêu đề: "//Title: " 
    . $table[$number]["title"] . "\n");
      }
      if($useAuthors) {
	print("Tác giả: "//Authors: "
   . $table[$number]["authors"] . "\n");
      }
      if($useAffiliations) {
	print("Tổ chức: "//Affiliations: " 
    . $table[$number]["affiliations"] . "\n");
      }
      if($useAbstract) {
	print("Tóm tắc: "//Abstract: "
   . $table[$number]["abstract"] . "\n");
	print("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
      } else {
	print("\n");
      }
    }

  } else {
    include 'header.php';
    if ($status != "" ){
      print('<div class="ERRmessage">'.$status.'</div>');
    }
?>
<form action="export_list.php" method="post">
<div class="paperBoxDetails">
<div class="versionTitle">Mục cần xuất<!--Exported Fields--></div>
<table>
  <tr>
    <td><input type="checkbox" class="noBorder" name="number" id="number" <?php if($useNumber) print('checked="checked"'); ?> /><label for="number">Số&nbsp;&nbsp;<!--Number--></label></td>
    <td><input type="checkbox" class="noBorder" name="title" id="title" <?php if($useTitle) print('checked="checked"'); ?> /><label for="title">Tiêu đề&nbsp;&nbsp;<!--Title--></label></td>
    <td><input type="checkbox" class="noBorder" name="authors" id="authors" <?php if($useAuthors) print('checked="checked"'); ?> /><label for="authors">Tác giả&nbsp;&nbsp;<!--Authors--></label></td>
    <?php if(Tools::useAffiliations()) { ?>
      <td><input type="checkbox" class="noBorder" name="affiliations" id="affiliations" <?php if($useAffiliations) print('checked="checked"'); ?> /><label for="affiliations">Tố chức&nbsp;&nbsp;<!--Affiliations--></label></td>
    <?php } ?>
    <?php if(Tools::useAbstract()) { ?>
      <td><input type="checkbox" class="noBorder" name="abstract" id="abstract" <?php if($useAbstract) print('checked="checked"'); ?> /><label for="abstract">Tóm tắc&nbsp;&nbsp;<!--Abstract--></label></td>
    <?php } ?>
  </tr>
</table>

<div class="versionTitle">Thứ tự sắp xếp<!--Sort Order--></div>
<table>
  <tr>
    <td><input type="radio" class="noBorder" name="sort" id="sort_number" value="number" <?php if($sort == "number") print('checked="checked"'); ?> /><label for="sort_number">Số&nbsp;&nbsp;<!--Number--></label></td>
    <td><input type="radio" class="noBorder" name="sort" id="sort_title" value="title" <?php if($sort == "title") print('checked="checked"'); ?> /><label for="sort_title">Tiêu đề&nbsp;&nbsp;<!--Title--></label></td>
    <td><input type="radio" class="noBorder" name="sort" id="sort_authors" value="authors" <?php if($sort == "authors") print('checked="checked"'); ?> /><label for="sort_authors">Tác giả&nbsp;&nbsp;<!--Authors--></label></td>
    <?php if(Tools::useAffiliations()) {?>
      <td><input type="radio" class="noBorder" name="sort" id="sort_affiliations" value="affiliations" <?php if($sort == "affiliations") print('checked="checked"'); ?> /><label for="sort_affiliations">Tố chức&nbsp;&nbsp;<!--Affiliations--></label></td>
    <?php } ?>
    <?php if(Tools::useAbstract()) { ?>
      <td><input type="radio" class="noBorder" name="sort" id="sort_abstract" value="abstract" <?php if($sort == "abstract") print('checked="checked"'); ?> /><label for="sort_abstract">Tóm tắc&nbsp;&nbsp;<!--Abstract--></label></td>
    <?php } ?>
  </tr>
</table>

<div class="versionTitle">Dữ liệu để đọc<!--Database to Read--></div>
<table>
  <tr>
    <td><input type="radio" class="noBorder" name="database" id="submissions" value="submissions" <?php if(Tools::readPost('database') == 'submissions') print('checked="checked"'); ?> /><label for="submissions">Bài gửi<!--Submissions--></label></td>
    <td>&nbsp;&nbsp;Bao gồm các bài &nbsp;&nbsp;&nbsp;</td>
    <td><input type="checkbox" class="noBorder" name="withdrawn" id="withdrawn" value="yes" <?php if($inclWithdrawn) print('checked="checked"'); ?>/><label for="withdrawn"><!--Withdrawn-->Đã hủy&nbsp;</label></td>
    <td>&nbsp;&nbsp;<input type="checkbox" class="noBorder" name="not_withdrawn" id="not_withdrawn" value="yes" <?php if($inclNotWithdrawn) print('checked="checked"'); ?> /><label for="not_withdrawn">Không hủy&nbsp;<!--Not withdrawn--></label></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td><input type="radio" class="noBorder" name="database" id="articles" value="articles" <?php if(Tools::readPost('database') != 'submissions') print('checked="checked"'); ?> /><label for="articles">Bài viết đã đánh giá<!--Articles to be reviewed--></label></td>
    <td style="text-align:right">&nbsp;&nbsp;Bao gồm các bài&nbsp;&nbsp;&nbsp;</td>
    <td><input type="checkbox" class="noBorder" name="accept" id="accept" value="yes" <?php if($inclAccept) print('checked="checked"'); ?> /><label for="accept">Đã chấp nhận&nbsp;<!--Accepted-->
    </label></td>
    <td>&nbsp;&nbsp;<input type="checkbox" class="noBorder" name="maybe_accept" id="maybe_accept" value="yes" <?php if($inclMaybeAccept) print('checked="checked"'); ?> /><label for="maybe_accept">Có thể chấp nhận&nbsp;<!--Maybe Accepted--></label></td>
    <td>&nbsp;&nbsp;<input type="checkbox" class="noBorder" name="discuss" id="discuss" value="yes" <?php if($inclDiscuss) print('checked="checked"'); ?> /><label for="discuss">Đang xem xét&nbsp;<!--In Discussion--></label></td>
    <td>&nbsp;&nbsp;<input type="checkbox" class="noBorder" name="maybe_reject" id="maybe_reject" value="yes"  <?php if($inclMaybeReject) print('checked="checked"'); ?> /><label for="maybe_reject">Có thể từ chối&nbsp<!--Maybe Rejected--></label></td>
    <td>&nbsp;&nbsp;<input type="checkbox" class="noBorder" name="reject" id="reject" value="yes" <?php if($inclReject) print('checked="checked"'); ?> /><label for="reject">Từ chối&nbsp;<!--Rejected--></label></td>
    <td>&nbsp;&nbsp;<input type="checkbox" class="noBorder" name="void" id="void" value="yes" <?php if($inclVoid) print('checked="checked"'); ?> /><label for="void">Rỗng&nbsp;<!--Void--></label></td>
    <?php if(Tools::useCustomAccept1()) { ?>
      <td><input type="checkbox" class="noBorder" name="custom1" id="custom1" value="yes" <?php if($inclCustom1) print('checked="checked"'); ?> /><label for="custom1"><?php print(Tools::getCustomAccept1()); ?></label></td>
    <?php } ?>
    <?php if(Tools::useCustomAccept2()) { ?>
      <td><input type="checkbox" class="noBorder" name="custom2" id="custom2" value="yes" <?php if($inclCustom2) print('checked="checked"'); ?> /><label for="custom2"><?php print(Tools::getCustomAccept2()); ?></label></td>
    <?php } ?>
    <?php if(Tools::useCustomAccept3()) { ?>
      <td><input type="checkbox" class="noBorder" name="custom3" id="custom3" value="yes" <?php if($inclCustom3) print('checked="checked"'); ?> /><label for="custom3"><?php print(Tools::getCustomAccept3()); ?></label></td>
    <?php } ?>
    <?php if(Tools::useCustomAccept4()) { ?>
      <td><input type="checkbox" class="noBorder" name="custom4" id="custom4" value="yes" <?php if($inclCustom4) print('checked="checked"'); ?> /><label for="custom4"><?php print(Tools::getCustomAccept4()); ?></label></td>
    <?php } ?>
    <?php if(Tools::useCustomAccept5()) { ?>
      <td><input type="checkbox" class="noBorder" name="custom5" id="custom5" value="yes" <?php if($inclCustom5) print('checked="checked"'); ?> /><label for="custom5"><?php print(Tools::getCustomAccept5()); ?></label></td>
    <?php } ?>
    <?php if(Tools::useCustomAccept6()) { ?>
      <td><input type="checkbox" class="noBorder" name="custom6" id="custom6" value="yes" <?php if($inclCustom6) print('checked="checked"'); ?> /><label for="custom6"><?php print(Tools::getCustomAccept6()); ?></label></td>
    <?php } ?>
    <?php if(Tools::useCustomAccept7()) { ?>
      <td><input type="checkbox" class="noBorder" name="custom7" id="custom7" value="yes" <?php if($inclCustom7) print('checked="checked"'); ?> /><label for="custom7"><?php print(Tools::getCustomAccept7()); ?></label></td>
    <?php } ?>
  </tr>
</table>

<div class="versionTitle">Định dạng xuất<!--Export Format--></div>
<table>
  <tr>
    <td><input type="radio" class="noBorder" name="format" id="txt" value="txt" checked="checked" /><label for="txt">Text đơn giản<!--Plain Text--></label></td>
  </tr>
</table>
</div>

<center><input type="submit" class="buttonLink bigButton" name="export" value="Xuất danh sách" /></center>

</form>
<?php 
  }
}
?>
