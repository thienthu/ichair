<?php 

/*
 * Copyright (C) 2006, 2007 Thomas Baigneres, Matthieu Finiasz
 *
 * This file is part of iChair.
 *
 * iChair is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 * 
 * iChair is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

?><?php $page_title='Tải Lên Tập tin Của Hội Thảo';//Upload Conference Specific Files';
include('../utils/tools.php');
include('header.php');
Tools::adminHasNoPassword();

$status = "";

/* Deal with Call for paper HTML version*/

if(Tools::readPost('cfp_html_upload') != "") {
  if($_FILES['file']['name'] == "") {
    $status .= "Vui lòng chọn tập tin!\n";//"Please provide the file!\n";
  } else if($_FILES['file']['error'] != UPLOAD_ERR_OK) {
    if ($_FILES['file']['error'] == UPLOAD_ERR_INI_SIZE) {
      $status .= "Tập tin tải lên vượt quá kích thước "//The file you uploaded is larger than the maximum allowed file size of "
      .ini_get('upload_max_filesize')." cho phép.";
    } else {
      $status .= "Lỗi xảy ra khi nhận tập tin. Có thể do vượt quá kích thước tối đa cho phép.\n";
      //An error occured when receiving your file. Maybe it exceeds the maximum file size allowed.\n";
    }
  } else {
    if(file_exists("../cfp/cfp.html")) {
      rename("../cfp/cfp.html", "../cfp/cfp.html.bak");
    }
    move_uploaded_file($_FILES['file']['tmp_name'], "../cfp/cfp.html");
    print('<div class="OKmessage">Thông báo(call for papers) HTML tải lên thành công.'//HTML call for papers uploaded successfully.
      .'</div>');
  }
}

if(Tools::readPost('cfp_html_delete') != "") {
    if(file_exists("../cfp/cfp.html")) {
      rename("../cfp/cfp.html", "../cfp/cfp.html.bak");
    }
    print('<div class="OKmessage">Xóa (sao lưu) thông báo (call for papers) HTML thành công.'//HTML call for papers deleted (and backuped) successfully.
      .'</div>');
}

if(Tools::readPost('cfp_html_recover') != "") {
    if(file_exists("../cfp/cfp.html")) {
      rename("../cfp/cfp.html", "../cfp/cfp.html.tmp");
    }
    if(file_exists("../cfp/cfp.html.bak")) {
      rename("../cfp/cfp.html.bak", "../cfp/cfp.html");
    }
    if(file_exists("../cfp/cfp.html.tmp")) {
      rename("../cfp/cfp.html.tmp", "../cfp/cfp.html.bak");
    }
    print('<div class="OKmessage">Khôi phục thông báo (call for papers) HTML thành công.'//HTML call for papers recovered successfully.
      .'</div>');
}



/* Deal with Call for papers PDF version*/

if(Tools::readPost('cfp_pdf_upload') != "") {
  if($_FILES['file']['name'] == "") {
    $status .= "Vui lòng chọn tập tin!\n";//"Please provide the file!\n";
  } else if($_FILES['file']['error'] != UPLOAD_ERR_OK) {
    if ($_FILES['file']['error'] == UPLOAD_ERR_INI_SIZE) {
      $status .= "Tập tin tải lên vượt quá kích thước "//The file you uploaded is larger than the maximum allowed file size of "
      .ini_get('upload_max_filesize')." cho phép.";
    } else {
      $status .= "Lỗi xảy ra khi nhận tập tin. Có thể do vượt quá kích thước tối đa cho phép.\n";
      //An error occured when receiving your file. Maybe it exceeds the maximum file size allowed.\n";
    }
  } else {
    if(file_exists("../cfp/cfp.pdf")) {
      rename("../cfp/cfp.pdf", "../cfp/cfp.pdf.bak");
    }
    move_uploaded_file($_FILES['file']['tmp_name'], "../cfp/cfp.pdf");
    print('<div class="OKmessage">Tải tập tin PDF lên thành công.'
//PDF call for papers uploaded successfully
.'</div>');
  }
}

if(Tools::readPost('cfp_pdf_delete') != "") {
    if(file_exists("../cfp/cfp.pdf")) {
      rename("../cfp/cfp.pdf", "../cfp/cfp.pdf.bak");
    }
    print('<div class="OKmessage"> Xóa (sao lưu) tập tin PDF thành công.'
//PDF call for papers deleted (and backuped) successfully
.'</div>');
}

if(Tools::readPost('cfp_pdf_recover') != "") {
    if(file_exists("../cfp/cfp.pdf")) {
      rename("../cfp/cfp.pdf", "../cfp/cfp.pdf.tmp");
    }
    if(file_exists("../cfp/cfp.pdf.bak")) {
      rename("../cfp/cfp.pdf.bak", "../cfp/cfp.pdf");
    }
    if(file_exists("../cfp/cfp.pdf.tmp")) {
      rename("../cfp/cfp.pdf.tmp", "../cfp/cfp.pdf.bak");
    }
    print('<div class="OKmessage">Khôi phục tập tin PDF thành công.'
//PDF call for papers recovered successfully
.'</div>');
}




/* Deal with Call for papers TXT version*/

if(Tools::readPost('cfp_txt_upload') != "") {
  if($_FILES['file']['name'] == "") {
    $status .= "Vui lòng chọn tập tin!\n";//"Please provide the file!\n";
  } else if($_FILES['file']['error'] != UPLOAD_ERR_OK) {
    if ($_FILES['file']['error'] == UPLOAD_ERR_INI_SIZE) {
      $status .= "Tập tin tải lên vượt quá kích thước "//The file you uploaded is larger than the maximum allowed file size of "
      .ini_get('upload_max_filesize')." cho phép.";
    } else {
      $status .= "Lỗi xảy ra khi nhận tập tin. Có thể do vượt quá kích thước tối đa cho phép .\n";
      //An error occured when receiving your file. Maybe it exceeds the maximum file size allowed.\n";
    }
  } else {
    if(file_exists("../cfp/cfp.txt")) {
      rename("../cfp/cfp.txt", "../cfp/cfp.txt.bak");
    }
    move_uploaded_file($_FILES['file']['tmp_name'], "../cfp/cfp.txt");
    print('<div class="OKmessage">Tải thông báo (call for papers) TXT lên thành công.'//TXT call for papers uploaded successfully.
      .'</div>');
  }
}

if(Tools::readPost('cfp_txt_delete') != "") {
    if(file_exists("../cfp/cfp.txt")) {
      rename("../cfp/cfp.txt", "../cfp/cfp.txt.bak");
    }
    print('<div class="OKmessage">Xóa (sao lưu) thông báo (call for papers) TXT thành công.'//TXT call for papers deleted (and backuped) successfully.
      .'</div>');
}

if(Tools::readPost('cfp_txt_recover') != "") {
    if(file_exists("../cfp/cfp.txt")) {
      rename("../cfp/cfp.txt", "../cfp/cfp.txt.tmp");
    }
    if(file_exists("../cfp/cfp.txt.bak")) {
      rename("../cfp/cfp.txt.bak", "../cfp/cfp.txt");
    }
    if(file_exists("../cfp/cfp.txt.tmp")) {
      rename("../cfp/cfp.txt.tmp", "../cfp/cfp.txt.bak");
    }
    print('<div class="OKmessage">Khôi phục thông báo (call for papers) HTML thành công.'//TXT call for papers recovered successfully.
      .'</div>');
}



/* Deal with submission guidelines HTML version*/

if(Tools::readPost('sub_html_upload') != "") {
  if($_FILES['file']['name'] == "") {
    $status .= "Vui lòng chọn tập tin!\n";//"Please provide the file!\n";
  } else if($_FILES['file']['error'] != UPLOAD_ERR_OK) {
    if ($_FILES['file']['error'] == UPLOAD_ERR_INI_SIZE) {
      $status .= "Tập tin tải lên vượt quá kích thước "//The file you uploaded is larger than the maximum allowed file size of "
      .ini_get('upload_max_filesize')." cho phép";
    } else {
      $status .= "Lỗi xảy ra khi nhận tập tin. Có thể do vượt quá kích thước tối đa cho phép .\n";
      //An error occured when receiving your file. Maybe it exceeds the maximum file size allowed.\n";
    }
  } else {
    if(file_exists("../guidelines/guidelines.html")) {
      rename("../guidelines/guidelines.html", "../guidelines/guidelines.html.bak");
    }
    move_uploaded_file($_FILES['file']['tmp_name'], "../guidelines/guidelines.html");
    print('<div class="OKmessage">Tải hướng dẫn gửi bài HTML lên thành công.'//HTML submission guidelines uploaded successfully.
      .'</div>');
  }
}

if(Tools::readPost('sub_html_delete') != "") {
    if(file_exists("../guidelines/guidelines.html")) {
      rename("../guidelines/guidelines.html", "../guidelines/guidelines.html.bak");
    }
    print('<div class="OKmessage">Xóa (sao lưu) hướng dẫn gửi bài HTML thành công.'//HTML submission guidelines deleted (and backuped) successfully.
      .'</div>');
}

if(Tools::readPost('sub_html_recover') != "") {
    if(file_exists("../guidelines/guidelines.html")) {
      rename("../guidelines/guidelines.html", "../guidelines/guidelines.html.tmp");
    }
    if(file_exists("../guidelines/guidelines.html.bak")) {
      rename("../guidelines/guidelines.html.bak", "../guidelines/guidelines.html");
    }
    if(file_exists("../guidelines/guidelines.html.tmp")) {
      rename("../guidelines/guidelines.html.tmp", "../guidelines/guidelines.html.bak");
    }
    print('<div class="OKmessage">Khôi phục hướng dẫn gửi bài HTML thành công.'//HTML submission guidelines recovered successfully.
      .'</div>');
}



/* Deal with submission guidelines PDF version*/

if(Tools::readPost('sub_pdf_upload') != "") {
  if($_FILES['file']['name'] == "") {
    $status .= "Vui lòng chọn tập tin!\n";//"Please provide the file!\n";
  } else if($_FILES['file']['error'] != UPLOAD_ERR_OK) {
    if ($_FILES['file']['error'] == UPLOAD_ERR_INI_SIZE) {
      $status .= "Tập tin tải lên vượt quá kích thước "//The file you uploaded is larger than the maximum allowed file size of "
      .ini_get('upload_max_filesize')." cho phép";
    } else {
      $status .= "Lỗi xảy ra khi nhận tập tin. Có thể do vượt quá kích thước tối đa cho phép .\n";
      //An error occured when receiving your file. Maybe it exceeds the maximum file size allowed.\n";
    }
  } else {
    if(file_exists("../guidelines/guidelines.pdf")) {
      rename("../guidelines/guidelines.pdf", "../guidelines/guidelines.pdf.bak");
    }
    move_uploaded_file($_FILES['file']['tmp_name'], "../guidelines/guidelines.pdf");
    print('<div class="OKmessage">Tải hướng dẫn gửi bài PDF lên thành công.'//PDF submission guidelines uploaded successfully.
      .'</div>');
  }
}

if(Tools::readPost('sub_pdf_delete') != "") {
    if(file_exists("../guidelines/guidelines.pdf")) {
      rename("../guidelines/guidelines.pdf", "../guidelines/guidelines.pdf.bak");
    }
    print('<div class="OKmessage">Xóa (sao lưu) hướng dẫn gửi bài PDF thành công.'//PDF submission guidelines deleted (and backuped) successfully.
      .'</div>');
}

if(Tools::readPost('sub_pdf_recover') != "") {
    if(file_exists("../guidelines/guidelines.pdf")) {
      rename("../guidelines/guidelines.pdf", "../guidelines/guidelines.pdf.tmp");
    }
    if(file_exists("../guidelines/guidelines.pdf.bak")) {
      rename("../guidelines/guidelines.pdf.bak", "../guidelines/guidelines.pdf");
    }
    if(file_exists("../guidelines/guidelines.pdf.tmp")) {
      rename("../guidelines/guidelines.pdf.tmp", "../guidelines/guidelines.pdf.bak");
    }
    print('<div class="OKmessage">Khôi phục hướng dẫn gửi bài PDF thành công.'//PDF submission guidelines recovered successfully.
      .'</div>');
}




/* Deal with submission guidelines TXT version*/

if(Tools::readPost('sub_txt_upload') != "") {
  if($_FILES['file']['name'] == "") {
    $status .= "Vui lòng chọn tập tin!\n";//"Please provide the file!\n";
  } else if($_FILES['file']['error'] != UPLOAD_ERR_OK) {
    if ($_FILES['file']['error'] == UPLOAD_ERR_INI_SIZE) {
      $status .= "Tập tin tải lên vượt quá kích thước "//The file you uploaded is larger than the maximum allowed file size of "
      .ini_get('upload_max_filesize').' cho phép.';
    } else {
      $status .= "Lỗi xảy ra khi nhận tập tin. Có thể do vượt quá kích thước tối đa cho phép .\n";//An error occured when receiving your file. Maybe it exceeds the maximum file size allowed.\n";
    }
  } else {
    if(file_exists("../guidelines/guidelines.txt")) {
      rename("../guidelines/guidelines.txt", "../guidelines/guidelines.txt.bak");
    }
    move_uploaded_file($_FILES['file']['tmp_name'], "../guidelines/guidelines.txt");
    print('<div class="OKmessage">Tải hướng dẫn gửi bài TXT lên thành công.'//TXT submission guidelines uploaded successfully.
      .'</div>');
  }
}

if(Tools::readPost('sub_txt_delete') != "") {
    if(file_exists("../guidelines/guidelines.txt")) {
      rename("../guidelines/guidelines.txt", "../guidelines/guidelines.txt.bak");
    }
    print('<div class="OKmessage">Xóa (sao lưu) hướng dẫn gửi bài TXT thành công.'//TXT submission guidelines deleted (and backuped) successfully.
      .'</div>');
}

if(Tools::readPost('sub_txt_recover') != "") {
    if(file_exists("../guidelines/guidelines.txt")) {
      rename("../guidelines/guidelines.txt", "../guidelines/guidelines.txt.tmp");
    }
    if(file_exists("../guidelines/guidelines.txt.bak")) {
      rename("../guidelines/guidelines.txt.bak", "../guidelines/guidelines.txt");
    }
    if(file_exists("../guidelines/guidelines.txt.tmp")) {
      rename("../guidelines/guidelines.txt.tmp", "../guidelines/guidelines.txt.bak");
    }
    print('<div class="OKmessage">Khôi phục hướng dẫn gửi bài PDF thành công.'//TXT submission guidelines recovered successfully.
      .'</div>');
}




/* Deal with review guidelines HTML version*/

if(Tools::readPost('rev_html_upload') != "") {
  if($_FILES['file']['name'] == "") {
    $status .= "Vui lòng chọn tập tin!\n";//"Please provide the file!\n";
  } else if($_FILES['file']['error'] != UPLOAD_ERR_OK) {
    if ($_FILES['file']['error'] == UPLOAD_ERR_INI_SIZE) {
      $status .= "Tập tin tải lên vượt quá kích thước "//The file you uploaded is larger than the maximum allowed file size of "
      .ini_get('upload_max_filesize').' cho phép.';
    } else {
      $status .= "Lỗi xảy ra khi nhận tập tin. Có thể do vượt quá kích thước tối đa cho phép .\n";//An error occured when receiving your file. Maybe it exceeds the maximum file size allowed.\n";
    }
  } else {
    if(file_exists("../review/guidelines/guidelines.html")) {
      rename("../review/guidelines/guidelines.html", "../review/guidelines/guidelines.html.bak");
    }
    move_uploaded_file($_FILES['file']['tmp_name'], "../review/guidelines/guidelines.html");
    print('<div class="OKmessage">Tải hướng dẫn đánh giá HTML lên thành công.'//HTML review guidelines uploaded successfully.
      .'</div>');
  }
}

if(Tools::readPost('rev_html_delete') != "") {
    if(file_exists("../review/guidelines/guidelines.html")) {
      rename("../review/guidelines/guidelines.html", "../review/guidelines/guidelines.html.bak");
    }
    print('<div class="OKmessage">HTML review guidelines deleted (and backuped) successfully.</div>');
}

if(Tools::readPost('rev_html_recover') != "") {
    if(file_exists("../review/guidelines/guidelines.html")) {
      rename("../review/guidelines/guidelines.html", "../review/guidelines/guidelines.html.tmp");
    }
    if(file_exists("../review/guidelines/guidelines.html.bak")) {
      rename("../review/guidelines/guidelines.html.bak", "../review/guidelines/guidelines.html");
    }
    if(file_exists("../review/guidelines/guidelines.html.tmp")) {
      rename("../review/guidelines/guidelines.html.tmp", "../review/guidelines/guidelines.html.bak");
    }
    print('<div class="OKmessage">HTML review guidelines recovered successfully.</div>');
}



/* Deal with review guidelines PDF version*/

if(Tools::readPost('rev_pdf_upload') != "") {
  if($_FILES['file']['name'] == "") {
    $status .= "Vui lòng chọn tập tin!\n";//"Please provide the file!\n";
  } else if($_FILES['file']['error'] != UPLOAD_ERR_OK) {
    if ($_FILES['file']['error'] == UPLOAD_ERR_INI_SIZE) {
      $status .= "Tập tin tải lên vượt quá kích thước "//The file you uploaded is larger than the maximum allowed file size of "
      .ini_get('upload_max_filesize').' cho phép.';
    } else {
      $status .= "Lỗi xảy ra khi nhận tập tin. Có thể do vượt quá kích thước tối đa cho phép .\n";//An error occured when receiving your file. Maybe it exceeds the maximum file size allowed.\n";
    }
  } else {
    if(file_exists("../review/guidelines/guidelines.pdf")) {
      rename("../review/guidelines/guidelines.pdf", "../review/guidelines/guidelines.pdf.bak");
    }
    move_uploaded_file($_FILES['file']['tmp_name'], "../review/guidelines/guidelines.pdf");
    print('<div class="OKmessage">Tải hướng dẫn đánh giá PDF lên thành công.'//PDF review guidelines uploaded successfully.
      .'</div>');
  }
}

if(Tools::readPost('rev_pdf_delete') != "") {
    if(file_exists("../review/guidelines/guidelines.pdf")) {
      rename("../review/guidelines/guidelines.pdf", "../review/guidelines/guidelines.pdf.bak");
    }
    print('<div class="OKmessage">Xóa (sao lưu) hướng dẫn đánh giá PDF lên thành công.'//PDF review guidelines deleted (and backuped) successfully.
      .'</div>');
}

if(Tools::readPost('rev_pdf_recover') != "") {
    if(file_exists("../review/guidelines/guidelines.pdf")) {
      rename("../review/guidelines/guidelines.pdf", "../review/guidelines/guidelines.pdf.tmp");
    }
    if(file_exists("../review/guidelines/guidelines.pdf.bak")) {
      rename("../review/guidelines/guidelines.pdf.bak", "../review/guidelines/guidelines.pdf");
    }
    if(file_exists("../review/guidelines/guidelines.pdf.tmp")) {
      rename("../review/guidelines/guidelines.pdf.tmp", "../review/guidelines/guidelines.pdf.bak");
    }
    print('<div class="OKmessage">Khôi phục hướng dẫn đánh giá PDF lên thành công.'//PDF review guidelines recovered successfully.
      .'</div>');
}




/* Deal with review guidelines TXT version*/

if(Tools::readPost('rev_txt_upload') != "") {
  if($_FILES['file']['name'] == "") {
    $status .="Vui lòng chọn tập tin!\n";//"Please provide the file!\n";
  } else if($_FILES['file']['error'] != UPLOAD_ERR_OK) {
    if ($_FILES['file']['error'] == UPLOAD_ERR_INI_SIZE) {
      $status .= "Tập tin tải lên vượt quá kích thước "//The file you uploaded is larger than the maximum allowed file size of "
      .ini_get('upload_max_filesize').' cho phép.';
    } else {
      $status .= "Lỗi xảy ra khi nhận tập tin. Có thể do vượt quá kích thước tối đa cho phép .\n";//An error occured when receiving your file. Maybe it exceeds the maximum file size allowed.\n";
    }
  } else {
    if(file_exists("../review/guidelines/guidelines.txt")) {
      rename("../review/guidelines/guidelines.txt", "../review/guidelines/guidelines.txt.bak");
    }
    move_uploaded_file($_FILES['file']['tmp_name'], "../review/guidelines/guidelines.txt");
    print('<div class="OKmessage">Tải hướng dẫn đánh giá TXT lên thành công.'//TXT review guidelines uploaded successfully.
      .'</div>');
  }
}

if(Tools::readPost('rev_txt_delete') != "") {
    if(file_exists("../review/guidelines/guidelines.txt")) {
      rename("../review/guidelines/guidelines.txt", "../review/guidelines/guidelines.txt.bak");
    }
    print('<div class="OKmessage">Xóa (sao lưu) hướng dẫn đánh giá TXT lên thành công.'//TXT review guidelines deleted (and backuped) successfully.
      .'</div>');
}

if(Tools::readPost('rev_txt_recover') != "") {
    if(file_exists("../review/guidelines/guidelines.txt")) {
      rename("../review/guidelines/guidelines.txt", "../review/guidelines/guidelines.txt.tmp");
    }
    if(file_exists("../review/guidelines/guidelines.txt.bak")) {
      rename("../review/guidelines/guidelines.txt.bak", "../review/guidelines/guidelines.txt");
    }
    if(file_exists("../review/guidelines/guidelines.txt.tmp")) {
      rename("../review/guidelines/guidelines.txt.tmp", "../review/guidelines/guidelines.txt.bak");
    }
    print('<div class="OKmessage">Khôi phục hướng dẫn đánh giá PDF lên thành công.'//TXT review guidelines recovered successfully.
      .'</div>');
}

if($status != "") {
  print('<div class="ERRmessage">'. $status .'</div>');
}


?>

<h2>Thông Báo (Call for Papers)</h2>


<table class="dottedTable">
  <tr>
    <td class="topRow"><div class="bigNumber">HTML</div></td>
    <td class="topRow">
      <form action="file_uploads.php" method="post" enctype="multipart/form-data">
        <input type="file" name="file" size="50" /><input name="cfp_html_upload" type="submit" class="buttonLink" value="Tải tập tin HTML lên"/>
      </form>
    </td>
    <?php if(Tools::hasCallForPaperHTML("..")) {?>
    <td class="topRow">
      <form action="file_uploads.php" method="post">
        <input type="submit" class="buttonLink" name="cfp_html_delete"  value="Xóa tập tin HTML"/></td>
      </form>
    <?php } else { ?>
    <td class="topRow"><em>Không có tập tin nào được tải lên<!--No file uploaded yet--></em></td>
    <?php } ?>
    <?php if(Tools::hasCallForPaperHTMLbak("..")) {?>
    <td class="topRow">
      <form action="file_uploads.php" method="post">
        <input type="submit" class="buttonLink" name="cfp_html_recover" value="Phục hồi tập tin HTML"/></td>
      </form>
    <?php } else { ?>
    <td class="topRow"><em>Không sao lưu tập tin có sẳn<!--No backup available--></em></td>
    <?php } ?>
  </tr>
  <tr>
    <td><div class="bigNumber">PDF</div></td>
    <td>
      <form action="file_uploads.php" method="post" enctype="multipart/form-data">
        <input type="file" name="file" size="50" /><input type="submit" name="cfp_pdf_upload" class="buttonLink" value="Tải tập tin PDF lên"/>
      </form>
    </td>
    <?php if(Tools::hasCallForPaperPDF("..")) {?>
    <td>
      <form action="file_uploads.php" method="post">
        <input type="submit" class="buttonLink" name="cfp_pdf_delete" value="Xóa tập tin P"/></td>
      </form>
    <?php } else { ?>
    <td><em>Không có tập tin nào được tải lên<!--No file uploaded yet--></em></td>
    <?php } ?>
    <?php if(Tools::hasCallForPaperPDFbak("..")) {?>
    <td>
      <form action="file_uploads.php" method="post">
        <input type="submit" class="buttonLink" name="cfp_pdf_recover" value="Phục hồi tập tin PDF"/></td>
      </form>
    <?php } else { ?>
    <td><em>Không sao lưu tập tin có sẳn<!--No backup available--></em></td>
    <?php } ?>
  </tr>
  <tr>
    <td><div class="bigNumber">TXT</div></td>
    <td>
      <form action="file_uploads.php" method="post" enctype="multipart/form-data">
        <input type="file" name="file" size="50" /><input type="submit" name="cfp_txt_upload" class="buttonLink" value="Tải tập tin TXT lên"/>
      </form>
    </td>
    <?php if(Tools::hasCallForPaperTXT("..")) {?>
    <td>
      <form action="file_uploads.php" method="post">
        <input type="submit" class="buttonLink" name="cfp_txt_delete" value="Xóa tập tin TXT"/></td>
      </form>
    <?php } else { ?>
    <td><em>Không có tập tin nào được tải lên<!--No file uploaded yet--></em></td>
    <?php } ?>
    <?php if(Tools::hasCallForPaperTXTbak("..")) {?>
    <td>
      <form action="file_uploads.php" method="post">
        <input type="submit" class="buttonLink" name="cfp_txt_recover" value="Phục hồi tập tin TXT"/></td>
      </form>
    <?php } else { ?>
    <td><em>Không sao lưu tập tin có sẳn<!--No backup available--></em></td>
    <?php } ?>
  </tr>
</table>

<h2>Hướng Dẫn Gửi Bài<!--Submission Guidelines--></h2>


<table class="dottedTable">
  <tr>
    <td class="topRow"><div class="bigNumber">HTML</div></td>
    <td class="topRow">
      <form action="file_uploads.php" method="post" enctype="multipart/form-data">
        <input type="file" name="file" size="50" /><input type="submit" name="sub_html_upload" class="buttonLink" value="Tải tập tin HTML lên"/>
      </form>
    </td>
    <?php if(Tools::hasSubmissionGuidelinesHTML("..")) {?>
    <td class="topRow">
      <form action="file_uploads.php" method="post">
        <input type="submit" class="buttonLink" name="sub_html_delete"  value="Xóa tập tin HTML"/></td>
      </form>
    <?php } else { ?>
    <td class="topRow"><em>Không có tập tin nào được tải lên<!--No file uploaded yet--></em></td>
    <?php } ?>
    <?php if(Tools::hasSubmissionGuidelinesHTMLbak("..")) {?>
    <td class="topRow">
      <form action="file_uploads.php" method="post">
        <input type="submit" class="buttonLink" name="sub_html_recover" value="Phục hồi tập tin HTML"/></td>
      </form>
    <?php } else { ?>
    <td class="topRow"><em>Không sao lưu tập tin có sẳn<!--No backup available--></em></td>
    <?php } ?>
  </tr>
  <tr>
    <td><div class="bigNumber">PDF</div></td>
    <td>
      <form action="file_uploads.php" method="post" enctype="multipart/form-data">
        <input type="file" name="file" size="50" /><input type="submit" name="sub_pdf_upload" class="buttonLink" value="Tải tập tin PDF lên"/>
      </form>
    </td>
    <?php if(Tools::hasSubmissionGuidelinesPDF("..")) {?>
    <td>
      <form action="file_uploads.php" method="post">
        <input type="submit" class="buttonLink" name="sub_pdf_delete" value="Xóa tập tin P"/></td>
      </form>
    <?php } else { ?>
    <td><em>Không có tập tin nào được tải lên<!--No file uploaded yet--></em></td>
    <?php } ?>
    <?php if(Tools::hasSubmissionGuidelinesPDFbak("..")) {?>
    <td>
      <form action="file_uploads.php" method="post">
        <input type="submit" class="buttonLink" name="sub_pdf_recover" value="Phục hồi tập tin PDF"/></td>
      </form>
    <?php } else { ?>
    <td><em>Không sao lưu tập tin có sẳn<!--No backup available--></em></td>
    <?php } ?>
  </tr>
  <tr>
    <td><div class="bigNumber">TXT</div></td>
    <td>
      <form action="file_uploads.php" method="post" enctype="multipart/form-data">
        <input type="file" name="file" size="50" /><input type="submit" name="sub_txt_upload" class="buttonLink" value="Tải tập tin TXT lên"/>
      </form>
    </td>
    <?php if(Tools::hasSubmissionGuidelinesTXT("..")) {?>
    <td>
      <form action="file_uploads.php" method="post">
        <input type="submit" class="buttonLink" name="sub_txt_delete" value="Xóa tập tin TXT"/></td>
      </form>
    <?php } else { ?>
    <td><em>Không có tập tin nào được tải lên<!--No file uploaded yet--></em></td>
    <?php } ?>
    <?php if(Tools::hasSubmissionGuidelinesTXTbak("..")) {?>
    <td>
      <form action="file_uploads.php" method="post">
        <input type="submit" class="buttonLink" name="sub_txt_recover" value="Phục hồi tập tin TXT"/></td>
      </form>
    <?php } else { ?>
    <td><em>Không sao lưu tập tin có sẳn<!--No backup available--></em></td>
    <?php } ?>
  </tr>
</table>

<h2>Hướng Dẫn Đánh Giá<!--Review Guidelines--></h2>


<table class="dottedTable">
  <tr>
    <td class="topRow"><div class="bigNumber">HTML</div></td>
    <td class="topRow">
      <form action="file_uploads.php" method="post" enctype="multipart/form-data">
        <input type="file" name="file" size="50" /><input type="submit" name="rev_html_upload" class="buttonLink" value="Tải tập tin HTML lên"/>
      </form>
    </td>
    <?php if(Tools::hasReviewGuidelinesHTML("..")) {?>
    <td class="topRow">
      <form action="file_uploads.php" method="post">
        <input type="submit" class="buttonLink" name="rev_html_delete"  value="Xóa tập tin HTML"/></td>
      </form>
    <?php } else { ?>
    <td class="topRow"><em>Không có tập tin nào được tải lên<!--No file uploaded yet--></em></td>
    <?php } ?>
    <?php if(Tools::hasReviewGuidelinesHTMLbak("..")) {?>
    <td class="topRow">
      <form action="file_uploads.php" method="post">
        <input type="submit" class="buttonLink" name="rev_html_recover" value="Phục hồi tập tin HTML"/></td>
      </form>
    <?php } else { ?>
    <td class="topRow"><em>Không sao lưu tập tin có sẳn<!--No backup available--></em></td>
    <?php } ?>
  </tr>
  <tr>
    <td><div class="bigNumber">PDF</div></td>
    <td>
      <form action="file_uploads.php" method="post" enctype="multipart/form-data">
        <input type="file" name="file" size="50" /><input type="submit" name="rev_pdf_upload" class="buttonLink" value="Tải tập tin PDF l"/>
      </form>
    </td>
    <?php if(Tools::hasReviewGuidelinesPDF("..")) {?>
    <td>
      <form action="file_uploads.php" method="post">
        <input type="submit" class="buttonLink" name="rev_pdf_delete" value="Xóa tập tin PDF"/></td>
      </form>
    <?php } else { ?>
    <td><em>Không có tập tin nào được tải lên<!--No file uploaded yet--></em></td>
    <?php } ?>
    <?php if(Tools::hasReviewGuidelinesPDFbak("..")) {?>
    <td>
      <form action="file_uploads.php" method="post">
        <input type="submit" class="buttonLink" name="rev_pdf_recover" value="Phục hồi tập tin PDF"/></td>
      </form>
    <?php } else { ?>
    <td><em>Không sao lưu tập tin có sẳn<!--No backup available--></em></td>
    <?php } ?>
  </tr>
  <tr>
    <td><div class="bigNumber">TXT</div></td>
    <td>
      <form action="file_uploads.php" method="post" enctype="multipart/form-data">
        <input type="file" name="file" size="50" /><input type="submit" name="rev_txt_upload" class="buttonLink" value="Tải tập tin TXT lên">
      </form>
    </td>
    <?php if(Tools::hasReviewGuidelinesTXT("..")) {?>
    <td>
      <form action="file_uploads.php" method="post">
        <input type="submit" class="buttonLink" name="rev_txt_delete" value="Xóa tập tin TXT"/></td>
      </form>
    <?php } else { ?>
    <td><em>Không có tập tin nào được tải lên<!--No file uploaded yet--></em></td>
    <?php } ?>
    <?php if(Tools::hasReviewGuidelinesTXTbak("..")) {?>
    <td>
      <form action="file_uploads.php" method="post">
        <input type="submit" class="buttonLink" name="rev_txt_recover" value="Phục hồi tập tin TXT"/></td>
      </form>
    <?php } else { ?>
    <td><em>Không sao lưu tập tin có sẳn<!--No backup available--></em></td>
    <?php } ?>
  </tr>
</table>


</body>
</html>
