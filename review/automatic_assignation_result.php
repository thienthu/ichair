<?php 

/*
 * Copyright (C) 2006, 2007 Thomas Baigneres, Matthieu Finiasz
 *
 * This file is part of iChair.
 *
 * iChair is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 * 
 * iChair is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

?><?php $page_title='Phân Công Tự Động';//'Automatic Assignation';
include '../utils/tools.php';
include 'header.php';
set_time_limit(0);
if($currentReviewerGroup == Reviewer::$CHAIR_GROUP) {

  Assignement::transferTempAssignements();
  Log::logAutomaticAssignement($currentReviewer);
    ?>
    <div class="OKmessage">
    Cập nhật phân công thành công.<!--Assignations have been updated successfully.-->
    </div>
    <div class="floatRight">
      <form action="index_chair.php" method="post">
        <input type="submit" class="buttonLink bigButton" value="Đóng"/>
      </form>
    </div>
    <?php 
} 

include('footer.php'); 

?>
