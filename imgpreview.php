<?php 

/*
 * Copyright (C) 2006, 2007 Thomas Baigneres, Matthieu Finiasz
 *
 * This file is part of iChair.
 *
 * iChair is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 * 
 * iChair is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

?><?php 
include 'utils/tools.php';


$id = $_GET['id'];
$bool = true;
if (Tools::isAnId($id)) {
  $submission = Submission::getByID($id);
  if (!is_null($submission)){
    $versions = $submission->getAllVersions();
    if (!is_null($versions[$_GET['version']])) {
      $preview_file = $versions[$_GET['version']]->getPreviewFile();
      if (file_exists($preview_file)) {
	header("Content-type: image/jpeg");
	@readfile($preview_file);
	$bool = false;
      } 
    }
  }
} 
if ($bool) {
  header("Content-type: image/gif");
  @readfile("images/nopreview.gif");
}
?>
