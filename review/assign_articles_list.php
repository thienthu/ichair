<?php 

/*
 * Copyright (C) 2006, 2007 Thomas Baigneres, Matthieu Finiasz
 *
 * This file is part of iChair.
 *
 * iChair is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 * 
 * iChair is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

?><?php $page_title='Phân Công Bài Viết Cho Người Đánh Giá';//'Assign Articles To Reviewers';
include '../utils/tools.php';
include 'header.php';

if($currentReviewerGroup == Reviewer::$CHAIR_GROUP) {

  $allArticles = Article::getAllArticles();

  /* check if a max number of articles has been updated */
  if (Tools::readPost('update') == "Đồng ý") {
    $reviewerNumber = Tools::readPost('reviewerNumber');
    $reviewer = Reviewer::getByReviewerNumber($reviewerNumber);
    $numberOfArticles = Tools::readPost('maxArticles');
    if ((!is_null($reviewer)) && preg_match("/^[0-9]*$/", $numberOfArticles)) {
      $reviewer->setNumberOfArticles($numberOfArticles);
    }
  }
  
  /* If there is no article in the database, print a message */

  $articles = Article::getAllArticles();
  if(count($articles) == 0) {
    print('<div class="OKmessage">There is no article to assign yet, you may wish to go ' .
	  'to the <a href="transfer.php">Load Submissions for Review</a> section to create a ' .
	  'first list of articles to review or to the ' . 
	  '<a href="index_chair.php">Chair Main Page</a> for more instructions.</div>');
  }

  $allcategories;
  if(Tools::useCategory()) {
    $allcategories = Tools::getAllCategories();
  }

  /* Display a list of all reviewers */

  foreach(Reviewer::getAllActiveReviewers() as $reviewer) {

    $fullName = $reviewer->getFullName();
    $reviewerNumber = $reviewer->getReviewerNumber();
    ?>

    <div class="paperBox" id="nbr<?php print($reviewerNumber); ?>">

      <div class="paperBoxTitle">

        <div class="paperBoxNumber"><?php print($fullName); ?></div>
  
        <?php $reviewer->printInfo(); ?>

      </div>

      <div class="paperBoxDetails">
      
        <?php /* Print the ratio of assigned article for this reviewer */ 
	  $reviewer->printAssignedRatio();

          /* Print two lists, one of assigned articles, the other of blocked articles */
          $reviewer->printArticlesByStatus(ASSIGNEMENT::$ASSIGNED, $allArticles);
          $reviewer->printArticlesByStatus(ASSIGNEMENT::$BLOCKED, $allArticles);

	  /* Print the preferred categories of each reviewer */
	  if(Tools::useCategory()) {
	    print('Thể loại ưa thích: '//Preferred Categories:
        );
	    $preferredCat = '';
            if($reviewer->getPreferedCategory() != '') {
              foreach(explode(";",$reviewer->getPreferedCategory()) as $catid) {
                $preferredCat .= '; ' . $allcategories[$catid];
              }
            }
	    if($preferredCat != '') {
	      Tools::printHTML(substr($preferredCat, 2));
	    } else {
	      print('<em>Không có'//none
          .'</em>');
	    }
	  }

        ?>
        </div>

        <div class="floatRight">
	  <form action="assign_articles.php" method="post">
            <input type="hidden" name="reviewerNumber" value="<?php print($reviewerNumber); ?>" />
            <input type="submit" class="buttonLink" value="Phân công bài viết" />
          </form>
        </div>
  
        <div class="clear"></div>


    </div>

  <?php } ?>

  <br />

  <?php 

  foreach(Reviewer::getAllByGroup(Reviewer::$OBSERVER_GROUP) as $reviewer) {

    $fullName = $reviewer->getFullName();
    $reviewerNumber = $reviewer->getReviewerNumber();
    ?>

    <div class="paperBox" id="nbr<?php print($reviewerNumber); ?>">

      <div class="paperBoxTitle">

        <div class="paperBoxNumber"><?php print($fullName); ?></div>
  
        <?php $reviewer->printInfo(); ?>

      </div>

      <div class="paperBoxDetails">
            
        <?php 
          /* Print two lists, one of assigned articles, the other of blocked articles */
          $reviewer->printArticlesByStatus(ASSIGNEMENT::$BLOCKED, $allArticles);
        ?>
  
      </div>
	<div class="floatRight">
	  <form action="assign_articles_observers.php" method="post">
            <input type="hidden" name="reviewerNumber" value="<?php print($reviewerNumber); ?>" />
            <input type="submit" class="buttonLink" value="Cho phép xem bài viết" />
          </form>
        </div>

      <div class="clear"></div>

    </div>

  <?php } ?>
     
  <?php } ?>

<?php include('footer.php'); ?>
