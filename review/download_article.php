<?php 

/*
 * Copyright (C) 2006, 2007 Thomas Baigneres, Matthieu Finiasz
 *
 * This file is part of iChair.
 *
 * iChair is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 * 
 * iChair is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

?><?php 
include '../utils/tools.php';

$articleNumber = Tools::readPost('articleNumber');
$currentReviewer = Reviewer::getReviewerByLogin(); 
$reviewerNumber = $currentReviewer->getReviewerNumber();

$assignement = Assignement::getByNumbers($articleNumber, $reviewerNumber);

if(($assignement->getAssignementStatus() == Assignement::$AUTHORIZED) || ($assignement->getAssignementStatus() == Assignement::$ASSIGNED)) {

  $article = Article::getByArticleNumber($articleNumber);
  if(!is_null($article)) {
    Log::logDownloadArticle($article,$currentReviewer);
    $file = $article->getFile();

    if(preg_match("/\.[pP][dD][fF]$/", $file)) {
      header('Content-type: application/pdf');
    } else if(preg_match("/\.[pP][sS]$/", $file)) {
      header('Content-type: application/postscript');
    } else {
      header('Content-type: application/binary');
    }

    header('Content-Disposition: attachment; filename="' . $file . '"');
    header('Content-Length: '.filesize($article->getFolder() . $file));
    @readfile($article->getFolder() . $file);

  }

}

?>
