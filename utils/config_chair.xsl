<xsl:stylesheet version = '1.0'
     xmlns:xsl='http://www.w3.org/1999/XSL/Transform'>

<xsl:template match="/">
  <form action="configuration_result.php" method="post">
    <h2>Chọn Giai Đoạn Hiện Tại<!--Current Phase Selection--></h2>
    <xsl:apply-templates select="xml/phase"/>
    <h2>Menu Ban Tổ Chức<!--Chair Menu--></h2>
    <xsl:apply-templates select="xml/restrictedChairMenu"/>
    <br />
    <center>
      <input type="submit" class="buttonLink bigButton" value="Cập nhật cấu hình"/>
    </center>
  </form>
</xsl:template>

<xsl:template match="phase">
  <table>

    <tr>
      <td>
        <input type="radio" class="noBorder" name="phase" value="submission" id="submissionPhase">
	  <xsl:if test="current='submission'">
	    <xsl:attribute name="checked">checked</xsl:attribute>
	  </xsl:if>
	</input>
      </td>
      <td>
        <label for="submissionPhase"><em>Gửi Bài/Tải Bài/Phân Quyền<!--Submission/Transfer/Authorizations--></em></label>
      </td>
      </tr>
      <tr>
        <td></td>
        <td>
          Giai đoạn này nên được hoạt kích trong quá trình gửi bài và 
trong thời gian  ban tổ chức tải bài gửi lên cho đánh giá (ví dụ, 
tạo ra các bài viết) sử dụng chức năng <a 
href = "transfer.php"> Bài Bài Gửi Lên Cho Đánh Giá </a>. Nó cũng là
giai đoạn này để ban tổ chức chọn bài viết đánh giá cho người đánh giá (tức là chặn người đánh giá đánh giá 
bài viết mà họ được phân công), bằng cách sử dụng <a 
href = "assign_reviewers_list.php"> Phân Công Người Đánh Giá Bài viết </a> 
hoặc <a href="assign_articles_list.php"> Phân Công Bài Viết Cho Người Đánh Giá </a>. Trong giai đoạn này, người đánh giá chỉ có thể truy cập 
<a href="profile.php"> Hồ Sơ Cá Nhân</a>.
          <!--
	  This phase should be active during the submission process and
	  during the time when the chair transfers the submissions (i.e.,
	  creates the articles database) using <a
	  href="transfer.php">Load Submissions for Review</a>. It is also in
	  this phase that the chair should select which articles will be
	  accessible to which reviewer (i.e. block a reviewer from reviewing
	  his own article), using either <a
	  href="assign_reviewers_list.php">Assign Reviewers to Articles</a>
	  or <a href="assign_articles_list.php"> Assign Articles to
	  Reviewer</a>. During this phase, the reviewers only have access to
	  their <a href="profile.php">Personal Profile</a>.-->
        </td>
      </tr>

      <tr>
      <td>
        <input type="radio" class="noBorder" name="phase" value="election" id="electionPhase">
	  <xsl:if test="current='election'">
	    <xsl:attribute name="checked">checked</xsl:attribute>
	  </xsl:if>
	</input>
      </td>
      <td>
        <label for="electionPhase"><em>Bầu chọn bài viết ưa thích<!--Preferred Articles Election--></em></label>
      </td>
      </tr>
      <tr>
      <td></td>
      <td>
        Giai đoạn này sẽ được hoạt động trong thời gian khi người đánh giá chọn bài viết ưa thích mà họ muốn đánh giá, không muốn đánh giá và bài viết không có thể/không muốn đánh giá. Sử dụng chức năng<a href="affinity_list.php"> Bài Viết Ưa Thích</a> để làm điều đó. Đồng thời trong thời giai đoạn này bắt đầu, ban tổ chức có thể thông báo 
tất cả người đánh giá bằng cách sử dụng <a href="mass_mailing.php"> Gửi Mail </a>. Khi tất cả các nhận xét đã chọn của họ 
bài viết ưa thích (hoặc khi ban tổ chức cho rằng họ có đủ thời gian để làm như vậy), giai đoạn tiếp theo có thể bắt đầu.
       <!-- This phase should be active during the time when the reviewers choose which articles they would prefer to review, 
	which articles they would rather not review, and which articles they just cannot/don't want to review. This is 
	done using <a href="affinity_list.php">Preferred Articles</a>. At the time this phase starts, the chair can notify 
	all the reviewers by using <a href="mass_mailing.php">Mass Mailing</a>. When all the reviewers have chosen their 
	preferred articles (or when the chair considers that they had enough time to do so), the next phase can start.-->
      </td>
      </tr>
      <tr>
      <td>
        <input type="radio" class="noBorder" name="phase" value="assignation" id="assignationPhase">
	  <xsl:if test="current='assignation'">
	    <xsl:attribute name="checked">checked</xsl:attribute>
	  </xsl:if>
	</input>
      </td>
      <td>
        <label for="assignationPhase"><em>Phân công đánh giá<!--Assignations--></em></label>
      </td>
      </tr>
      <tr>
      <td></td>
      <td>
        Giai đoạn này sẽ được hoạt động trong thời gian khi ban tổ chức phân công bài viết cho từng người đánh giá. Điều này được thực hiện bằng cách sử dụng một trong hai chức năng
<a href="assign_reviewers_list.php"> Phân Công Người Đánh giá Bài Viết</a>, <a href="assign_articles_list.php"> Phân Công Bài Viết Cho Người Đánh Giá</a> hoặc <a href="automatic_assignation.php">Tự Động Phân Công</a>. Trong thời gian này, người đánh giá chỉ có thể truy cập <a href="profile.php"> Hồ Sơ Cá Nhân</a>. Trong một lần mỗi bài viết đựơc đủ người đánh giá, 
giai đoạn tiếp theo có thể bắt đầu.
        <!--
        This phase should be active during the time when the chair assigns articles to each reviewer. This can be done by using either 
	<a href="assign_reviewers_list.php">Assign Reviewers to Articles</a>, <a href="assign_articles_list.php">
	Assign Articles to Reviewers</a>, or <a href="automatic_assignation.php">Automatic Assignation</a>. During this time, 
	the reviewers only have access to their <a href="profile.php">Personal Profile</a>. Once each articles has been assigned enough reviewers, 
	the next phase can start.-->
      </td>
      </tr>

      <tr>
      <td>
        <input type="radio" class="noBorder" name="phase" value="review" id="reviewPhase">
	  <xsl:if test="current='review'">
	    <xsl:attribute name="checked">checked</xsl:attribute>
	  </xsl:if>
	</input>
      </td>
      <td>
        <label for="reviewPhase"><em>Đánh Giá/Thảo Luận/Quyết Định<!--Review/Discussion/Decision--></em></label>
      </td>
      </tr>
      <tr>
      <td></td>
      <td>
      Giai đoạn này sẽ được hoạt động trong thời gian khi người đánh giá 
đánh giá bài viết được phân công, sử dụng <a 
href = "review.php"> Đánh Giá </a> và thảo luận về những đánh giá, 
sử dụng <a href="discuss.php"> Đánh Giá &amp; Thảo Luận </a>. Bắt đầu của giai đoạn này, không có người nhận xét có thể truy cập đánh giá của người đánh khác, cũng không thể thảo luận. Ban tổ chức có thể cấp quyền truy cập bằng cách sử dụng menu <a 
href = "supervise_reviewers.php"> Quyền Người Đánh Giá</a>. Kết thúc giai đoạn này, bằng cách sử dụng <a href="accept_and_reject.php"> Chấp Nhận 
&amp; Từ Chối </a>, ban tổ chức quyết định mà bài ​​viết là đã được 
chấp nhận của hội thảo. <br />
        <!--
        This phase should be active during the time when the reviewers
	review the articles assigned to them, using <a
	href="review.php">Your Reviews</a> and discuss about these reviews,
	using <a href="discuss.php">Reviews &amp; Discussions</a>. At the
	beginning of this phase, none of the reviewers have access to the
	reviews of other reviewers, nor to the discussion section. The chair
	can grant access to these using the <a
	href="supervise_reviewers.php">Reviewers Access Privileges</a> menu. At the
	end of this phase, using the <a href="accept_and_reject.php">Accept
	&amp; Reject</a> page, the chair shall decide which articles are
	accepted to the conference.<br />-->
        <input type="checkbox" class="noBorder" name="hideTime" value="yes" id="hideTime">
          <xsl:if test="hideTime='yes'">
            <xsl:attribute name="checked">checked</xsl:attribute>
          </xsl:if>
        </input>
	<label for="hideTime">Ẩn hộp thoại <em>thời gian hết hạn đánh giá</em><!--Hide the <em>Review Deadline</em> time box-->.</label>
      </td>
    </tr>  

      <tr>
      <td>
        <input type="radio" class="noBorder" name="phase" value="decision" id="decisionPhase">
          <xsl:if test="current='decision'">
            <xsl:attribute name="checked">checked</xsl:attribute>
          </xsl:if>
        </input>
      </td>
      <td>
        <label for="decisionPhase"><em>Quyết Định<!--Decision only--></em></label>
      </td>
      </tr>
      <tr>
      <td></td> 
      <td> 
        Trong giai đoạn cuối cùng này người đánh giá không còn có thể sửa đổi các đánh giá của mình ​hoặc tiếp tục thảo luận về bài viết. Ban tổ sể quyết định số phận cuối cùng của bài viết. Bằng cách sử dụng chức năng <a
        href="accept_and_reject.php">Chấp Nhận &amp; Từ Chối<!--Accept &amp; Reject--></a> là đủ rồi.
	<!--During this final phase the reviewers can no longer modify their
        reviews or continue discussing on the articles. The chair is left
        alone to decide the final fate of the articles. Using the <a
        href="accept_and_reject.php">Accept &amp; Reject</a> page should be
        enough then.-->
     </td>
    </tr>  
  </table>
</xsl:template>

<xsl:template match="restrictedChairMenu">
  <table>
    <tr>
      <td>
        <label for="restrictMenu">Hạn chế khả năng hiển thị các mục trong <em>Menu Ban Tổ Chức</em> để sử dụng hiệu quả trong giai đoạn hiện tại. 
          <!--Restrict the items visibility in the <em>Chair Menu</em> to the ones useful in the current phase-->    </label>
      </td>
      <td>
        <input type="checkbox" class="noBorder" name="restrictedChairMenu" value="yes" id="restrictMenu">
	  <xsl:if test=".='yes'">
	    <xsl:attribute name="checked">checked</xsl:attribute>
	  </xsl:if>
	</input>
      </td>
    </tr>
  </table>  
</xsl:template>

</xsl:stylesheet>

