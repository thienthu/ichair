<?php 

/*
 * Copyright (C) 2006, 2007 Thomas Baigneres, Matthieu Finiasz
 *
 * This file is part of iChair.
 *
 * iChair is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 * 
 * iChair is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

?><?php 

class Article {

   public $mailer;

  public static $ACCEPT='01accept';
  public static $MAYBE_ACCEPT='02maybe_accept';
  public static $DISCUSS='03discuss';
  public static $MAYBE_REJECT='04maybe_reject';
  public static $REJECT='05reject';
  public static $VOID='06void';
  public static $CUSTOM1='07custom1';
  public static $CUSTOM2='08custom2';
  public static $CUSTOM3='09custom3';
  public static $CUSTOM4='10custom4';
  public static $CUSTOM5='11custom5';
  public static $CUSTOM6='12custom6';
  public static $CUSTOM7='13custom7';

  private $articleNumber;         /* Unique article number -> corresponds to the submission */
  private $folder;                /* Relative Folder containing the article and its reviews */
  private $file;

  private $numberOfReviewers;     /* The chair decides how many reviewers will have to review this article */

  /* The following variable are imported from the submission database */
  private $id;                    /* unique id of the article, equal to id of the corresponding submission */
  private $versionNumber;         /* Number of the transfered Version corresponding to this article */
  private $title;
  private $authors;
  private $isCommitteeMember;
  private $isCustomCheck1;
  private $isCustomCheck2;
  private $isCustomCheck3;
  private $affiliations;
  private $country;
  private $abstract;
  private $category;
  private $keywords;
  private $contact;
  private $averageOverallGrade;
  private $averageOverallGradeSnapshot;
  private $lastModificationDate;
  private $acceptance;            /* Can either be "accepted", "rejected", .... */
  private $chairComments;

 function __construct(){
    $this->mailer= new PHPMailer;

    $this->mailer->isSMTP();                                      // Set mailer to use SMTP
    $this->mailer->Host = 'smtp.gmail.com;';  // Specify main and backup SMTP servers
    $this->mailer->SMTPAuth = true;                               // Enable SMTP authentication

    // 
    $this->mailer->Username =  Tools::getConfig("mail/admin");     // SMTP username
    $this->mailer->Password =   Tools::getConfig("mail/passmail_admin");               // SMTP password
    $this->mailer->SMTPSecure =      "tls";      // Enable encryption, 'ssl' also accepted

    //$this->mail->From = 'admin@cit.ctu.edu.vn';
    //$this->mail->FromName = 'Nguyen Gia Hung';
    //$this->mail->addAddress('giahung24@gmail.com', 'Joe User');     // Add a recipient
    //$this->mail->addReplyTo('jean.deanny@gmail.com', 'Information - Please reply to this email');
    //$this->$mail->addCC('cc@example.com');
    //$this->$mail->addBCC('bcc@example.com');
    $this->mailer->CharSet = 'UTF-8';
    $this->mailer->WordWrap = 50;                
  }

  /* During the transfer from a submission database to the article database, we need
   * to create an article object from a submission object */

  function createFromSubmission($submission) {
    /* create a DB entry for this article */
    $version = $submission->getLastVersion();
    $this->articleNumber = $submission->getSubmissionNumber();
    $this->folder = sprintf("%04d", $this->articleNumber) . "/";
    $this->file =   sprintf("%04d", $this->articleNumber) . "." . $submission->getLastVersionFileType();
    if ($submission->getIsCommitteeMember()) {
      $this->numberOfReviewers = Tools::getConfig('review/numberOfReviewersCommittee');
    } else {
      $this->numberOfReviewers = Tools::getConfig('review/numberOfReviewers');
    }
    $this->id = $submission->getId();
    $this->versionNumber = $submission->getLastVersionNumber();
    $this->title = $version->getTitle();
    $this->authors = implode('¦', $version->getAuthorsArray());
    if ($submission->getIsCommitteeMember()) {
      $this->isCommitteeMember = "True";
    } else {
      $this->isCommitteeMember = "False";
    }
    if ($submission->getIsCustomCheck1()) {
      $this->isCustomCheck1 = "True";
    } else {
      $this->isCustomCheck1 = "False";
    }
    if ($submission->getIsCustomCheck2()) {
      $this->isCustomCheck2 = "True";
    } else {
      $this->isCustomCheck2 = "False";
    }
    if ($submission->getIsCustomCheck3()) {
      $this->isCustomCheck3 = "True";
    } else {
      $this->isCustomCheck3 = "False";
    }
    $this->affiliations  = implode('¦', $version->getAffiliationsArray());
    $this->country  = implode('¦', $version->getCountryArray());
    $this->abstract = $version->getAbstract();
    $this->category = $version->getCategory();
    $this->keywords = $version->getKeywords();
    $this->contact = $submission->getContact();
    $this->averageOverallGrade = "-1";
    $this->averageOverallGradeSnapshot = "-1";
    $this->lastModificationDate = date("U");
    $this->acceptance = Article::$VOID;
    $this->chairComments = $submission->getAllChairComments();
    if(is_null(Article::getByArticleNumber($this->getArticleNumber()))) {
      $this->insertInDB();
      if (!file_exists(Tools::getConfig("server/reviewsPath") . $this->folder)) {
	mkdir(Tools::getConfig("server/reviewsPath") . $this->folder);
      }
      if (!file_exists(Tools::getConfig("server/reviewsPath") . $this->folder . "archive/")) {
	mkdir(Tools::getConfig("server/reviewsPath") . $this->folder . "archive/");
      }
    } else {
      $this->updateInDB();
      Log::logRetransfer($this->articleNumber);
    }
    /* copy the file to the correct directory */
    if(Tools::useTagArticles()) {
      Tools::tagAndCopy($submission->getFolder() . $submission->getLastVersion()->getFile(), Tools::getConfig("server/reviewsPath") . $this->folder . $this->file, $this);
    } else {
      copy($submission->getFolder() . $submission->getLastVersion()->getFile(), Tools::getConfig("server/reviewsPath") . $this->folder . $this->file);
    }
    /* create all assignements for this article, be default, all the reviewers are blocked for this article */
    $this->createAllAssignements();
  }

  function makeNewArticleFolder() {
    $folder = sprintf("%04d",$this->articleNumber)."article/";
    mkdir(Tools::getConfig("server/reviewsPath").$folder);    
    return $folder;
  }

  static function createDB() {
    $db = new SQLite3(Tools::getConfig("server/reviewsPath")."articles.db");
    $db->exec("CREATE TABLE articles(articleNumber INTEGER PRIMARY KEY, " . 
	       "folder, file, " .
	       "numberOfReviewers, id, versionNumber, title, authors, isCommitteeMember, isCustomCheck1, isCustomCheck2, isCustomCheck3, affiliations, country, " .
	       "abstract, category, keywords, contact, averageOverallGrade, averageOverallGradeSnapshot, lastModificationDate, acceptance, chairComments)");
  }

  static function openDB() {
    $dbfile = Tools::getConfig("server/reviewsPath")."articles.db";
    if (!file_exists($dbfile)){
      Article::createDB();
    }
    return new SQLite3($dbfile);
  }

  function insertInDB() {
    $db = Article::openDB();
    $db->exec("INSERT INTO articles VALUES (\"" 
	       . $this->articleNumber . "\", \""
	       . base64_encode($this->folder) . "\", \""
	       . base64_encode($this->file) . "\", \""
	       . base64_encode($this->numberOfReviewers) . "\", \""
	       . base64_encode($this->id) . "\", \""
	       . base64_encode($this->versionNumber) . "\", \""
	       . base64_encode($this->title) . "\", \""
	       . base64_encode($this->authors) . "\", \""
	       . base64_encode($this->isCommitteeMember) . "\", \""
	       . base64_encode($this->isCustomCheck1) . "\", \""
	       . base64_encode($this->isCustomCheck2) . "\", \""
	       . base64_encode($this->isCustomCheck3) . "\", \""
	       . base64_encode($this->affiliations) . "\", \""
	       . base64_encode($this->country) . "\", \""
	       . base64_encode($this->abstract) . "\", \""
	       . base64_encode($this->category) . "\", \""
	       . base64_encode($this->keywords) . "\", \""
	       . base64_encode($this->contact) . "\", \""
	       . $this->averageOverallGrade . "\", \""
	       . $this->averageOverallGradeSnapshot . "\", \""
	       . $this->lastModificationDate . "\", \""
	       . $this->acceptance . "\", \""
	       . base64_encode($this->chairComments) . "\")");
  }

  static function removeFromDB($articleNumber) {
    $db = Article::openDB();
    $db->exec("DELETE FROM articles WHERE articleNumber=\"" . $articleNumber . "\"");
  }

  function updateInDB() {
    $db = Article::openDB();
    $db->exec("UPDATE articles SET " 
	       . "file=\"" . base64_encode($this->file) . "\", "
	       . "numberOfReviewers=\"" . base64_encode($this->numberOfReviewers) . "\", "
	       . "versionNumber=\"" . base64_encode($this->versionNumber) . "\", "
	       . "title=\"" . base64_encode($this->title) . "\", "
	       . "authors=\"" . base64_encode($this->authors) . "\", "
	       . "isCommitteeMember=\"" . base64_encode($this->isCommitteeMember) . "\", "
	       . "isCustomCheck1=\"" . base64_encode($this->isCustomCheck1) . "\", "
	       . "isCustomCheck2=\"" . base64_encode($this->isCustomCheck2) . "\", "
	       . "isCustomCheck3=\"" . base64_encode($this->isCustomCheck3) . "\", "
	       . "affiliations=\"" . base64_encode($this->affiliations) . "\", "
	       . "country=\"" . base64_encode($this->country) . "\", "
	       . "abstract=\"" . base64_encode($this->abstract) . "\", "
	       . "category=\"" . base64_encode($this->category) . "\", "
	       . "keywords=\"" . base64_encode($this->keywords) . "\", "
	       . "contact=\"" . base64_encode($this->contact) . "\", "
	       . "averageOverallGrade=\"" . $this->averageOverallGrade . "\", "
	       . "averageOverallGradeSnapshot=\"" . $this->averageOverallGradeSnapshot . "\", "
	       . "lastModificationDate=\"" . $this->lastModificationDate . "\", "
	       . "acceptance=\"" . $this->acceptance . "\", "
	       . "chairComments=\"" . base64_encode($this->chairComments) . "\" "
	       . "WHERE articleNumber=\"" . $this->articleNumber . "\"");
  }

  function createFromDB($db_row) {
    $this->articleNumber = $db_row['articleNumber'];
    $this->folder = base64_decode($db_row['folder']);
    $this->file = base64_decode($db_row['file']);
    $this->numberOfReviewers = base64_decode($db_row['numberOfReviewers']);
    $this->id = base64_decode($db_row['id']);
    $this->versionNumber = base64_decode($db_row['versionNumber']);
    $this->title = base64_decode($db_row['title']);
    $this->authors = base64_decode($db_row['authors']);
    $this->isCommitteeMember = base64_decode($db_row['isCommitteeMember']);
    $this->isCustomCheck1 = base64_decode($db_row['isCustomCheck1']);
    $this->isCustomCheck2 = base64_decode($db_row['isCustomCheck2']);
    $this->isCustomCheck3 = base64_decode($db_row['isCustomCheck3']);
    $this->affiliations = base64_decode($db_row['affiliations']);
    $this->country = base64_decode($db_row['country']);
    $this->abstract = base64_decode($db_row['abstract']);
    $this->category = base64_decode($db_row['category']);
    $this->keywords = base64_decode($db_row['keywords']);
    $this->contact = base64_decode($db_row['contact']);
    $this->averageOverallGrade = $db_row['averageOverallGrade'];
    $this->averageOverallGradeSnapshot = $db_row['averageOverallGradeSnapshot'];
    $this->lastModificationDate = $db_row['lastModificationDate'];
    $this->acceptance = $db_row['acceptance'];
    $this->chairComments = base64_decode($db_row['chairComments']);
  }

  function createDummy() {
    $this->articleNumber = (rand() % 100) +1;
    $this->folder = "";
    $this->file = "dummyArticle.pdf";
    $this->numberOfReviewers = Tools::getConfig('review/numberOfReviewers');
    $this->id = Tools::generateIdString(Tools::$ID_LENGTH);
    $this->versionNumber = (rand() % 10) +1;
    $this->title = "Dummy Title";
    $this->authors = "Matthieu and Thomas";
    $this->isCommitteeMember = "False";
    $this->isCustomCheck1 = "False";
    $this->isCustomCheck2 = "False";
    $this->isCustomCheck3 = "False";
    $this->affiliations = "Dummy Affiliation";
    $this->country = "DM_CT";
    $this->abstract = "Dummy Abstract";
    $this->category = "Dummy Category";
    $this->keywords = "Dummy Keywords";
    $this->contact = "dummy@article.com";
    $this->averageOverallGrade = Tools::getConfig('review/overallGradeMin');
    $this->averageOverallGradeSnapshot = Tools::getConfig('review/overallGradeMax');
    $this->lastModificationDate = "0";
    $this->acceptance = Article::$VOID;
    $this->chairComments = "";
  }


  function createAllAssignements() {
    $reviewers = Reviewer::getAllReviewers();
    foreach($reviewers as $reviewer) {
      if(!is_null($reviewer)) {
	/* create an assignement for this article with the current reviewer if there no such assignement yet */
	$assignement = Assignement::getByNumbers($this->getArticleNumber(), $reviewer->getReviewerNumber());
	if(is_null($assignement)) {
	  $assignement = new Assignement();
	  $assignement->createFromScratch($this->getArticleNumber(), $reviewer->getReviewerNumber());
	}
      }
    }
  }

  /* Returns a article instance, given its number */

  static function getByArticleNumber($articleNumber) {
    if(!preg_match("/[0-9]*/",$articleNumber)) {
      return;
    }
    $db = Article::openDB();
    $result = $db->querySingle('SELECT * FROM articles WHERE articleNumber = "' . $articleNumber . '"', true);
    if (empty($result)) {
      return null;
    } else {
      $bozo = new Article();
      $bozo->createFromDB($result);
      return $bozo;
    }    
  }

  function getFolder() {
    return Tools::getConfig("server/reviewsPath").$this->folder;
  }

  function getFile() {
    return $this->file;
  }

  function printInfo() {
    print("Title: ".htmlentities($this->title, ENT_COMPAT | ENT_HTML401, 'UTF-8')."<br />");
    print("Authors: ".htmlentities($this->getAuthors(), ENT_COMPAT | ENT_HTML401, 'UTF-8')."<br />");
  }

  /* This function is used in assign_list.php to increment or decrement the total number of reviewers per article */

  function printAssignedRatio() {
    $currentNumberOfReviewers = count($this->getAssignedReviewers());
    print("<div class=\"floatRightClear\">");
    print("<form action=\"assign_reviewers_list.php#nbr" . $this->getArticleNumber() . "\" method=\"post\">\n");
    print("<input type=\"hidden\" name=\"articleNumber\" value=\"" . $this->getArticleNumber() . "\" />");
    print("<table>\n<tr>\n<td rowspan=\"2\">\n");
    if($currentNumberOfReviewers < $this->getNumberOfReviewers()) {
      print("<div class=\"badRatio\">");
    } else {
      print("<div class=\"goodRatio\">");
    }
    print($currentNumberOfReviewers . "/" . $this->getNumberOfReviewers() . "</div>");
    print("</td>\n");
    print("<td><input type=\"submit\" name=\"increment\" class=\"incrButton\" value=\"+\" /></td>");
    print("</tr><tr>");
    print("<td><input type=\"submit\" name=\"increment\" class=\"decrButton\" value=\"-\" /></td>");
    print("</tr>");
    print("</table>\n");
    print("</form>");
    print("</div>");
  }

  /* Returns a list of all the number of the reviewers that are assigned to this article */
  function getAssignedReviewers() {
    $assignedReviewers = array();
    $i = 0;
    $db = Assignement::openDB();
    $result = $db->query("SELECT reviewerNumber FROM assignements WHERE " . 
			 "articleNumber = \"" . $this->articleNumber . "\" AND " . 
			 "assignementStatus = \"" . Assignement::$ASSIGNED . "\"");
    while($db_row = $result->fetchArray()) {
      $assignedReviewers[$i] = $db_row['reviewerNumber'];
      $i++;
    }
    return $assignedReviewers;
  }

  /* Returns a list of all the number of the reviewers that are assigned to this article depending on their stauts */
  function getAssignedReviewersByStatus($reviewStatus) {
    $assignedReviewers = array();
    $i = 0;
    $db = Assignement::openDB();
    $result = $db->query('SELECT reviewerNumber FROM assignements WHERE ' . 
			 'articleNumber = "' . $this->articleNumber . '" AND ' . 
			 'assignementStatus = "' . Assignement::$ASSIGNED . '" AND ' .
			 'reviewStatus="'.$reviewStatus.'"');
    while($db_row = $result->fetchArray()) {
      $assignedReviewers[$i] = $db_row['reviewerNumber'];
      $i++;
    }
    return $assignedReviewers;
  }

  /* Returns a list of all the number of the reviewers that are authorized to review this article */
  function getAuthorizedReviewers() {
    $authorizedReviewers = array();
    $i = 0;
    $db = Assignement::openDB();
    $result = $db->query("SELECT reviewerNumber FROM assignements WHERE " .
                         "articleNumber = \"" . $this->articleNumber . "\" AND " .
                         "assignementStatus = \"" . Assignement::$AUTHORIZED . "\"");
    while($db_row = $result->fetchArray()) {
      $authorizedReviewers[$i] = $db_row['reviewerNumber'];
      $i++;
    }
    return $authorizedReviewers;
  }

  /* Returns a list of all the number of the reviewers that are assigned to this article depending on their stauts */
  function getAuthorizedReviewersByStatus($reviewStatus) {
    $assignedReviewers = array();
    $i = 0;
    $db = Assignement::openDB();
    $result = $db->query('SELECT reviewerNumber FROM assignements WHERE ' . 
			 'articleNumber = "' . $this->articleNumber . '" AND ' . 
			 'assignementStatus = "' . Assignement::$AUTHORIZED . '" AND ' .
			 'reviewStatus="'.$reviewStatus.'"');
    while($db_row = $result->fetchArray()) {
      $assignedReviewers[$i] = $db_row['reviewerNumber'];
      $i++;
    }
    return $assignedReviewers;
  }


  function getBlockedReviewers() {
    $assignedReviewers = $this->getAssignedReviewers();
    $authorizedReviewers = $this->getAuthorizedReviewers();
    /* create an array of blocked reviewers*/
    $blockedReviewers = array();
    $i=0;
    $allReviewers = Reviewer::getAllReviewers();
    foreach($allReviewers as $reviewer) {
      if(!in_array($reviewer->getReviewerNumber(), $authorizedReviewers) && !in_array($reviewer->getReviewerNumber(), $assignedReviewers) ) {
	$blockedReviewers[$i]= $reviewer->getReviewerNumber();
	$i++;
      }
    }
    return $blockedReviewers;
  }

  /* print a string with all the full names of the reviewers assigned to this article */
  function printAssignedReviewers() {
    $listOfAssignedReviewers = $this->getAssignedReviewers();
    $names = "";
    foreach($listOfAssignedReviewers as $reviewerNumber) {
      $reviewer = Reviewer::getByReviewerNumber($reviewerNumber);
      if(!is_null($reviewer)) {
	$names .= ", " . htmlentities($reviewer->getFullName(), ENT_COMPAT | ENT_HTML401, 'UTF-8');
      }
    }
    if($names != "") {
      $names = substr($names,2);
    } else {
      $names = "<em>Không có."//None.
      ."</em>";
    }
    print("Người đánh giá phân công"//Assigned Reviewers:
      ."<br /><div class=\"nameList\">" . $names . "</div>");
  }


  /* print a string with all the full names of the reviewers that should not have access to this article */
  function printBlockedReviewers() {
    $listOfBlockedReviewers = $this->getBlockedReviewers();
    $names = "";
    foreach($listOfBlockedReviewers as $reviewerNumber) {
      $reviewer = Reviewer::getByReviewerNumber($reviewerNumber);
      if(!is_null($reviewer)) {
	$names .= ", " . htmlentities($reviewer->getFullName(), ENT_COMPAT | ENT_HTML401, 'UTF-8');
      }
    }
    if($names != "") {
      $names = substr($names,2);
    } else {
        $names = "<em>Không có."//None.
      ."</em>";
    }
    print("Người đánh giá bị chặn:"//Blocked Reviewers:
      ."<br /><div class=\"nameList\">" . $names . "</div>");
  }

  function getNumberOfReviewers() {
    return $this->numberOfReviewers;
  }

  /* for the accept & reject page */
  function printCompletedReviewsRatio() {
    $db = Assignement::openDB();
    $completedAssigned = $db->querySingle('SELECT COUNT(*) FROM assignements WHERE assignementStatus="'. Assignement::$ASSIGNED .'" AND reviewStatus="'.Assignement::$COMPLETED .'" AND articleNumber="'.$this->articleNumber.'"');
    $totalAssigned = $db->querySingle('SELECT COUNT(*) FROM assignements WHERE assignementStatus="'. Assignement::$ASSIGNED .'" AND articleNumber="'.$this->articleNumber.'"');
    $completedOthers = $db->querySingle('SELECT COUNT(*) FROM assignements WHERE assignementStatus<>"'. Assignement::$ASSIGNED .'" AND reviewStatus="'.Assignement::$COMPLETED .'" AND articleNumber="'.$this->articleNumber.'"');
    if ($completedAssigned == $totalAssigned) {
      print('<span class="bigNumber completedReview">');
    } else {
      print('<span class="bigNumber voidReview">');
    }
    print($completedAssigned.'/'.$totalAssigned.'</span>');
    if ($completedOthers != 0) {
      print('<span class="bigNumber inProgressReview">+'.$completedOthers.'</span>');
    }
  }


  /* Return an array containing all the articles: the position of an article in this array */
  /* corresponds to the submission Number -> there may be holes in this table...           */

  static function getAllArticles() {
    $articles=array();
    $db = Article::openDB();
    $result = $db->query("SELECT * FROM articles");
    while ($db_row = $result->fetchArray()){
      $na = new Article();
      $na->createFromDB($db_row);
      $articles[$na->getArticleNumber()] = $na;
    }
    return $articles;
  }

  /* Return an array containing the articles in a given range */

  static function getRangeOfArticles($articleNumber, $nbrOfArticles) {
    $articles=array();
    $db = Article::openDB();
    $result = $db->querySingle('SELECT COUNT(*) FROM articles WHERE '.
			 'articleNumber < ' . $articleNumber);
    $offset = floor($result / $nbrOfArticles) * $nbrOfArticles;
    $result = $db->query('SELECT * FROM articles'.
			 ' ORDER BY articleNumber LIMIT '. $nbrOfArticles . ' OFFSET ' . $offset);
    while ($db_row = $result->fetchArray()){
      $na = new Article();
      $na->createFromDB($db_row);
      $articles[$na->getArticleNumber()] = $na;
    }
    return $articles;
  }

  /* Return an array containing all the articles having the corresponding acceptance: the position of an article in this array */
  /* corresponds to the submission Number -> there may be holes in this table...           */

  static function getByAcceptance($acceptance) {
    $articles=array();
    $db = Article::openDB();
    $result = $db->query('SELECT * FROM articles WHERE acceptance="' . $acceptance .'"');
    while ($db_row = $result->fetchArray()){
      $na = new Article();
      $na->createFromDB($db_row);
      $articles[$na->getArticleNumber()] = $na;
    }
    return $articles;
  }

  /* Return an array containing all the article numbers */

  static function getAllArticleNumbers() {
    $articleNumbers=array();
    $i=0;
    $db = Article::openDB();
    $result = $db->query('SELECT articleNumber FROM articles');
    while ($db_row = $result->fetchArray()){
      $articleNumbers[$i] = $db_row['articleNumber'];
      $i++;
    }
    return $articleNumbers;
  }

  function getNextArticleNumber() {
    $db = Article::openDB();
    $result = $db->query('SELECT articleNumber FROM articles WHERE articleNumber > "' . $this->getArticleNumber() . '"');
    if ($db_row = $result->fetchArray()){
      return $db_row['articleNumber'];
    }
    return null;
  }

  function getId() {
    return $this->id;
  }

  function getContact() {
    return $this->contact;
  }

  function setContact($email) {
    $this->contact = $email;
    $db = Article::openDB();
    $db->exec("UPDATE articles SET contact=\"" . base64_encode($this->contact) . "\" WHERE articleNumber=\"" . $this->articleNumber . "\"");
  }

  function getVersionNumber() {
    return $this->versionNumber;
  }

  function getCategory() {
    return $this->category;
  }

  function getAbstract() {
    return $this->abstract;
  }

  function getAffiliations(){
    return implode(' và ', explode('¦', $this->affiliations));
  }

  function getAffiliationsArray(){
    return explode('¦', $this->affiliations);
  }

  function getCountry(){
    $countries = explode('¦', $this->country);
    $ret = "";
    foreach($countries as $country) {
      $ret .= ' và ' . Tools::$COUNTRY_LIST[$country];
    }
    return substr($ret, 5);
  }

  function getCountryArray(){
    return explode('¦', $this->country);
  }

  function getIsCommitteeMember() {
    if($this->isCommitteeMember != "False") {
      return true;
    }
    return false;
  }

  function getIsCustomCheck1() {
    if($this->isCustomCheck1 != "False") {
      return true;
    }
    return false;
  }

  function getIsCustomCheck2() {
    if($this->isCustomCheck2 != "False") {
      return true;
    }
    return false;
  }

  function getIsCustomCheck3() {
    if($this->isCustomCheck3 != "False") {
      return true;
    }
    return false;
  }

  function setCommittee() {
    $this->isCommitteeMember = "True";
    $this->updateInDB();
  }

  function setNotCommittee() {
    $this->isCommitteeMember = "False";
    $this->updateInDB();
  }

  function setNumberOfReviewers($numberOfReviewers) {
    $this->numberOfReviewers = $numberOfReviewers;
    $this->updateInDB();
  }

  function setAcceptance($acceptance) {
    $this->acceptance = $acceptance;
    $this->updateInDB();
  }

  function setLastModificationDate() {
    $this->lastModificationDate = date("U");
    $this->updateInDB();
  }

  function getLastModificationDate() {
    return $this->lastModificationDate;
  }

  function printLong() {
    print("<div class=\"versionTitle\">Tiều đề: "//Title: "
      .htmlentities($this->title, ENT_COMPAT | ENT_HTML401, 'UTF-8')."</div>\n");
    print("<div class=\"versionAuthors\">Tác giả: "//Authors: "
      .htmlentities($this->getAuthors(), ENT_COMPAT | ENT_HTML401, 'UTF-8')."</div>\n");
    if(Tools::useAffiliations()) {
      print("Tổ chức: "//Affiliations: "
        . htmlentities($this->getAffiliations(), ENT_COMPAT | ENT_HTML401, 'UTF-8')."<br />\n");
    }
    if(Tools::useCountry()) {
      print("Quốc gia: "//Countries: " 
        . htmlentities($this->getCountry(), ENT_COMPAT | ENT_HTML401, 'UTF-8') ."<br />\n");
    }
    if(Tools::useAbstract()) {
      print("Tóm tắc: "//Abstract:"
        ."<br />\n<div class=\"versionAbstract\">".nl2br(htmlentities($this->abstract, ENT_COMPAT | ENT_HTML401, 'UTF-8'))."</div>\n");
    }
    if(Tools::useCategory()) {
      print("Thể loại: "//Category: "
        .htmlentities(Tools::getCategoryNameById($this->category), ENT_COMPAT | ENT_HTML401, 'UTF-8')."<br />\n");
    }
    if(Tools::useKeywords()) {
      print("Từ khoá (Keywords): "//Keywords: "
        .htmlentities($this->keywords, ENT_COMPAT | ENT_HTML401, 'UTF-8')."<br />\n");
    }
  }

  function printCustomCheckboxes($forchair = false, $print = true) {
    $out = '<div class="customchecks">';
    if (Tools::useCustomCheck1() && ($forchair || !Tools::chaironlyCustomCheck1()) && $this->getIsCustomCheck1()) {
      $out .= '<div>' . htmlentities(Tools::getCustomCheck1(), ENT_COMPAT | ENT_HTML401, 'UTF-8') . '</div>';
    }
    if (Tools::useCustomCheck2() && ($forchair || !Tools::chaironlyCustomCheck2()) && $this->getIsCustomCheck2()) {
      $out .= '<div>' . htmlentities(Tools::getCustomCheck2(), ENT_COMPAT | ENT_HTML401, 'UTF-8') . '</div>';
    }
    if (Tools::useCustomCheck3() && ($forchair || !Tools::chaironlyCustomCheck3()) && $this->getIsCustomCheck3()) {
      $out .= '<div>' . htmlentities(Tools::getCustomCheck3(), ENT_COMPAT | ENT_HTML401, 'UTF-8') . '</div>';
    }
    $out .= '</div>';
    if ($print) {
      print($out);
    } else {
      return $out;
    }
  }

  function getAvailableField() {
    if(!Tools::authorsAreAnonymous()) {
      return $this->getAuthors();
    } else if(Tools::useCategory()) { 
      return Tools::getCategoryNameById($this->getCategory());
    } else if(Tools::useKeywords()) { 
      return $this->getKeywords();
    } else {
      return "";
    }
  }

  static function getAvailableFieldTitle() {
    if(!Tools::authorsAreAnonymous()) {
      return "Tác giả";//"Authors";
    } else if(Tools::useCategory()) { 
      return "Thể loại";//"Category";
    } else if(Tools::useKeywords()) { 
      return "Từ khoá (Keywords)";//"Keywords";
    } else {
      return "";
    }
  }

  function printTitleAndOther() {
    print('Tiêu đề: '//'Title: ' 
      . htmlentities($this->title, ENT_COMPAT | ENT_HTML401, 'UTF-8') . '<br/>');
    if (Article::getAvailableFieldTitle() != ""){
      print(Article::getAvailableFieldTitle().': '.htmlentities($this->getAvailableField(), ENT_COMPAT | ENT_HTML401, 'UTF-8'));
    }
  }

  function printDetailsBoxTitle() {
    $count=2;
    if(Tools::useCategory()) { 
      print('Thể loại: '//'Category: ' 
        . htmlentities(Tools::getCategoryNameById($this->getCategory()), ENT_COMPAT | ENT_HTML401, 'UTF-8') . '<br/>');
      $count--;
    }
    if(Tools::useKeywords()) { 
      print('Từ khoá (Keywords): '//'Keywords: ' 
        . htmlentities($this->getKeywords(), ENT_COMPAT | ENT_HTML401, 'UTF-8') . '<br/>');
      $count--;
    }
    for (;$count>0; $count--){
      print('&nbsp;<br/>');
    }
  }

  function printDetailsBoxPopUp() {
    print('<div class="paperBox">');
    print('<div class="paperBoxDetails">');
    if(!Tools::authorsAreAnonymous() && Tools::useCountry()) {
      Tools::printContinentDivs($this->getCountryArray());
    } else {
      print('<div><div><div>');
    }
    $this->printCustomCheckboxes();
    print("<div class=\"versionTitle\"> Tiều đề: "//Title: "
      . htmlentities($this->title, ENT_COMPAT | ENT_HTML401, 'UTF-8') ."</div>\n");
    if(!Tools::authorsAreAnonymous()) {
      print("<div class=\"versionAuthors\">Tác giả: "//Authors: "
        .htmlentities($this->getAuthors(), ENT_COMPAT | ENT_HTML401, 'UTF-8')."</div>\n");
      if(Tools::useAffiliations()) {
	print("Tổ chức:  "//Affiliations: " 
    . htmlentities($this->getAffiliations(), ENT_COMPAT | ENT_HTML401, 'UTF-8') ."<br />\n");
      }
      if(Tools::useCountry()) {
        print("Quốc gia: "//Countries: " 
          . htmlentities($this->getCountry(), ENT_COMPAT | ENT_HTML401, 'UTF-8') ."<br />\n");
      }
    }
    if(Tools::useAbstract()) {
      print('<div class="versionAbstract">Tóm tắc:  '//Abstract: ' 
        . htmlentities($this->abstract, ENT_COMPAT | ENT_HTML401, 'UTF-8') . '</div>');
    }
    if(Tools::useCategory()) {
      print("Thể loại: "//Category: "
        .htmlentities(Tools::getCategoryNameById($this->category), ENT_COMPAT | ENT_HTML401, 'UTF-8')."<br />\n");
    }
    if(Tools::useKeywords()) {
      print("Từ khoá (Keywords):"//Keywords: "
        .htmlentities($this->keywords, ENT_COMPAT | ENT_HTML401, 'UTF-8')."<br />\n");
    }
    print('</div></div></div></div></div>');

  }

  function printDetailsBoxForChairPopUp() {
    print('<div class="paperBox">');
    if ($this->getIsCommitteeMember()){
      print('<div class="paperBoxDetailsCommittee">');
    } else {
      print('<div class="paperBoxDetails">');
    }
    if(Tools::useCountry()) {
      Tools::printContinentDivs($this->getCountryArray());
    } else {
      print('<div><div><div>');
    }
    $this->printCustomCheckboxes(true);
    print("<div class=\"versionTitle\">Tiêu đề: "//Title: "
      . htmlentities($this->title, ENT_COMPAT | ENT_HTML401, 'UTF-8') ."</div>\n");
    print("<div class=\"versionAuthors\">Tác giả: "//Authors: "
      .htmlentities($this->getAuthors(), ENT_COMPAT | ENT_HTML401, 'UTF-8')."</div>\n");
    if(Tools::useAffiliations()) {
      print("Tổ chức: "//Affiliations: " 
        . htmlentities($this->getAffiliations(), ENT_COMPAT | ENT_HTML401, 'UTF-8') ."<br />\n");
    }
    if(Tools::useCountry()) {
      print("Quôc gia: "//Countries: " 
        . htmlentities($this->getCountry(), ENT_COMPAT | ENT_HTML401, 'UTF-8') ."<br />\n");
    }
    if(Tools::useAbstract()) {
      print('<div class="versionAbstract">Tóm tắc: '//Abstract: ' 
        . htmlentities($this->getAbstract(), ENT_COMPAT | ENT_HTML401, 'UTF-8') . '</div>');
    } 
    if(Tools::useCategory()) {
      print("Thể loại: "//Category: "
        .htmlentities(Tools::getCategoryNameById($this->category), ENT_COMPAT | ENT_HTML401, 'UTF-8')."<br />\n");
    }
    if(Tools::useKeywords()) {
      print("Từ khoá (Keywords): "//Keywords: "
        .htmlentities($this->keywords, ENT_COMPAT | ENT_HTML401, 'UTF-8')."<br />\n");
    }
    $chairComments = $this->getChairComments();
    if ($chairComments != "Không có."//None."
      ) {
      print('Chair comments:<br />');
      print("<div class=\"chairComments\">\n");
      Tools::printHTMLbr($chairComments);
      print("\n</div>\n");
    }
    if ($this->acceptance == Article::$ACCEPT) {
      print('Trạng thái: <span class="acceptArticle">Chấp nhận'//Accept
       .' </span>');
    } else if ($this->acceptance == Article::$MAYBE_ACCEPT) {
      print('Trạng thái: <span class="maybeAcceptArticle">Có thể chấp nhận'//Maybe accept
        .'</span>');
    } else if ($this->acceptance == Article::$DISCUSS) {
      print('Trạng thái: <span class="discussArticle">Thảo luận'//Discuss
        .'</span>');
    } else if ($this->acceptance == Article::$MAYBE_REJECT) {
      print('Trạng thái: <span class="maybeRejectArticle">Có thể từ chối'//Maybe reject
        .'</span>');
    } else if ($this->acceptance == Article::$REJECT) {
      print('Trạng thái: <span class="rejectArticle">Từ chối'//Reject
        .'</span>');
    } else if ($this->acceptance == Article::$CUSTOM1) {
      print('Trạng thái: <span class="customAccept1">' . htmlentities(Tools::getCustomAccept1(), ENT_COMPAT | ENT_HTML401, 'UTF-8') . '</span>');
    } else if ($this->acceptance == Article::$CUSTOM2) {
      print('Trạng thái: <span class="customAccept2">' . htmlentities(Tools::getCustomAccept2(), ENT_COMPAT | ENT_HTML401, 'UTF-8') . '</span>');
    } else if ($this->acceptance == Article::$CUSTOM3) {
      print('Trạng thái: <span class="customAccept3">' . htmlentities(Tools::getCustomAccept3(), ENT_COMPAT | ENT_HTML401, 'UTF-8') . '</span>');
    } else if ($this->acceptance == Article::$CUSTOM4) {
      print('Trạng thái: <span class="customAccept4">' . htmlentities(Tools::getCustomAccept4(), ENT_COMPAT | ENT_HTML401, 'UTF-8') . '</span>');
    } else if ($this->acceptance == Article::$CUSTOM5) {
      print('Trạng thái: <span class="customAccept5">' . htmlentities(Tools::getCustomAccept5(), ENT_COMPAT | ENT_HTML401, 'UTF-8') . '</span>');
    } else if ($this->acceptance == Article::$CUSTOM6) {
      print('Trạng thái: <span class="customAccept6">' . htmlentities(Tools::getCustomAccept6(), ENT_COMPAT | ENT_HTML401, 'UTF-8') . '</span>');
    } else if ($this->acceptance == Article::$CUSTOM7) {
      print('Trạng thái: <span class="customAccept7">' . htmlentities(Tools::getCustomAccept7(), ENT_COMPAT | ENT_HTML401, 'UTF-8') . '</span>');
    }
    print('</div></div></div></div></div>');

  }


  function printDetailsBox() {
    print('<div class="paperBox">');
    print('<div class="paperBoxTitle">');
    print('<div class="paperBoxNumber">Bài viết&nbsp;'//Article&nbsp;' 
      . $this->articleNumber . '</div>');
    $this->printDetailsBoxTitle();
    print('</div>');    
    print('<div class="paperBoxDetails">');
    if(!Tools::authorsAreAnonymous() && Tools::useCountry()) {
      Tools::printContinentDivs($this->getCountryArray());
    } else {
      print('<div><div><div>');
    }
    $this->printCustomCheckboxes();
    print("<div class=\"versionTitle\">Tiều đề: "//Title: "
      . htmlentities($this->title, ENT_COMPAT | ENT_HTML401, 'UTF-8') ."</div>\n");
    if(!Tools::authorsAreAnonymous()) {
      print("<div class=\"versionAuthors\">Tác giả: "//Authors: "
        .htmlentities($this->getAuthors(), ENT_COMPAT | ENT_HTML401, 'UTF-8')."</div>\n");
      if(Tools::useAffiliations()) {
	print("Tổ chức: "//Affiliations: " 
    . htmlentities($this->getAffiliations(), ENT_COMPAT | ENT_HTML401, 'UTF-8') ."<br />\n");
      }
      if(Tools::useCountry()) {
        print("Quôc gia: "//Countries: "
         . htmlentities($this->getCountry(), ENT_COMPAT | ENT_HTML401, 'UTF-8') ."<br />\n");
      }
    }
    if(Tools::useAbstract()) {
      print('Tóm tắc: '//Abstract:
        .'<br/><div class="versionAbstract">');
      Tools::printHTMLbr($this->getAbstract());
      print('</div>');
    } 
    print('</div></div></div></div></div>');
  }

  function printDetailsBoxWithDownload($currentReviewer) {
    print('<div class="paperBox">');
    print('<div class="paperBoxTitle">');
    print('<div class="paperBoxNumber">Bài viết '//Article&nbsp;' 
      . $this->articleNumber . '</div>');
    $this->printDetailsBoxTitle();
    print('</div>');    
    print('<div class="paperBoxDetails">');
    if(!Tools::authorsAreAnonymous() && Tools::useCountry()) {
      Tools::printContinentDivs($this->getCountryArray());
    } else {
      print('<div><div><div>');
    }
    $this->printCustomCheckboxes();
    print("<div class=\"versionTitle\">Tiêu đề: "//Title: "
      . htmlentities($this->title, ENT_COMPAT | ENT_HTML401, 'UTF-8') ."</div>\n");
    if(!Tools::authorsAreAnonymous()) {
      print("<div class=\"versionAuthors\">Tác giả: "//Authors: "
        .htmlentities($this->getAuthors(), ENT_COMPAT | ENT_HTML401, 'UTF-8')."</div>\n");
      if(Tools::useAffiliations()) {
	print("Tổ chức: "//Affiliations: " 
    . htmlentities($this->getAffiliations(), ENT_COMPAT | ENT_HTML401, 'UTF-8') ."<br />\n");
      }
      if(Tools::useCountry()) {
        print("Quốc gia: "//Countries: " 
          . htmlentities($this->getCountry(), ENT_COMPAT | ENT_HTML401, 'UTF-8') ."<br />\n");
      }
    }
    if(Tools::useAbstract()) {
      print('<div class="versionAbstract">Tóm tắc: '//Abstract: ' 
        . htmlentities($this->getAbstract(), ENT_COMPAT | ENT_HTML401, 'UTF-8') . '</div>');
    } 
    print('</div></div></div></div>');
    print('<div class="floatRight">');
    $this->printDownloadArticleLink();
    print('</div>');
    if ($currentReviewer->isAllowedToViewOtherReviews()) {
      print('<div class="floatRight">');
      print('<form action="discuss_article.php?articleNumber=' . $this->getArticleNumber() . '#firstNotSeen" method="post">');
      print('<input type="submit" class="buttonLink" value="Reviews &amp; Discussion" />');
      print('</form></div>');
    }
    print('<div class="clear"></div>');
    print('</div>');
  }

  function printDetailsBoxForChair() {
    print('<div class="paperBox">');
    print('<div class="paperBoxTitle">');
    print('<div class="paperBoxNumber">Bài viết '//Article&nbsp;'
     . $this->articleNumber . '</div>');
    $this->printDetailsBoxTitle();
    print('</div>');    
    if ($this->getIsCommitteeMember()){
      print('<div class="paperBoxDetailsCommittee">');
    } else {
      print('<div class="paperBoxDetails">');
    }
    if(Tools::useCountry()) {
      Tools::printContinentDivs($this->getCountryArray());
    } else {
      print('<div><div><div>');
    }
    $this->printCustomCheckboxes(true);
    print("<div class=\"versionTitle\">Tiêu đề: "//Title: "
      . htmlentities($this->title, ENT_COMPAT | ENT_HTML401, 'UTF-8') ."</div>\n");
    print("<div class=\"versionAuthors\">Tác giả: "//Authors: "
      .htmlentities($this->getAuthors(), ENT_COMPAT | ENT_HTML401, 'UTF-8')."</div>\n");
    if(Tools::useAffiliations()) {
      print("Tổ chức: "//Affiliations: " 
        . htmlentities($this->getAffiliations(), ENT_COMPAT | ENT_HTML401, 'UTF-8') ."<br />\n");
    }
    if(Tools::useCountry()) {
      print("Quốc gia: "//Countries: " 
        . htmlentities($this->getCountry(), ENT_COMPAT | ENT_HTML401, 'UTF-8') ."<br />\n");
    }
    if(Tools::useAbstract()) {
      print('Tóm tắc: '//Abstract:
        .'<br /><div class="versionAbstract">');
      Tools::printHTMLbr($this->getAbstract());
      print('</div>');
    } 
    $chairComments = $this->getChairComments();
    if ($chairComments != "Không có."//None."
      ) {
      print('Chair comments:<br />');
      print("<div class=\"chairComments\">\n");
      Tools::printHTMLbr($chairComments);
      print("\n</div>\n");
    }
    print('</div></div></div></div></div>');

  }

  function sendMail($subject, $message) {
    $headers = "Từ: "//"From: " 
    . Tools::getConfig("mail/authorsFrom") . "\r\n" .
      "Đến: "//Reply-To: " 
      . Tools::getConfig("mail/authorsFrom") . "\r\n";
    if(Tools::useGlobalBCC()) {
      $headers .= "Bcc: " . Tools::getConfig("mail/globalBCC") . "\r\n";
    }
    $headers .= 'X-Mailer: PHP/' . phpversion() . "\r\n";
    
    /* If there is a tag [attach_article_checked] in the $message, modify the header an include the article in the mail */
    if(preg_match('/\[attach_article_checked\]/', $message)) {
      /* remove the tag */
      $message = preg_replace('/\[attach_article_checked\]/', '', $message);
      /* modify header */
      $headers .= 'MIME-Version: 1.0' . "\r\n";
      $mime_boundary = 'Boundary_(ID_'. md5(time().$message)  .')';
      $headers .= 'Content-type: multipart/mixed; boundary="'.$mime_boundary.'"' . "\r\n";
      /* modify message */
      $mime_message = "This is a multi-part message in MIME format.\r\n\r\n";
      $mime_message .= '--'.$mime_boundary."\r\n";
      $mime_message .= "Content-type: text/plain; charset=UTF-8\r\n";
      $mime_message .= "Content-transfer-encoding: 8BIT\r\n\r\n";
      $mime_message .= $message."\r\n\r\n";
      $mime_message .= '--'.$mime_boundary."\r\n";
      if(preg_match('/\.ps$/', $this->getFile())) { 
	$mime_message .= 'Content-type: application/postscript; name="your_submission.ps"'."\r\n";
	$mime_message .= "Content-transfer-encoding: base64\r\n";
	$mime_message .= 'Content-Disposition: attachment; filename="your_submission.ps"'."\r\n\r\n";
      } else {
	$mime_message .= 'Content-type: application/pdf; name="your_submission.pdf"'."\r\n";
	$mime_message .= "Content-transfer-encoding: base64\r\n";
	$mime_message .= 'Content-Disposition: attachment; filename="your_submission.pdf"'."\r\n\r\n";
      }
      $mime_message .= chunk_split(base64_encode(file_get_contents($this->getFolder() . $this->getFile())));
      $mime_message .= "\r\n\r\n--" . $mime_boundary . "--\r\n\r\n";
      $message = $mime_message;
    }


    $this->mailer->From = Tools::getConfig("mail/authorsFrom");
    $this->mailer->FromName =  Tools::getConfig("conference/name");
    $this->mailer->addReplyTo(Tools::getConfig("mail/authorsFrom") );
    $this->mailer->addBCC(Tools::getConfig("mail/admin"));
    if(Tools::useGlobalBCC()) {
      $this->mailer->addBCC(Tools::getConfig("mail/globalBCC"));
    } 

    $this->mailer->Subject = $subject;
    $this->mailer->Body    = $message;

    // split contact
    $contact_arr = explode(",", $this->getContact());
    foreach ($contact_arr as $contact_email) {
      $this->mailer->addAddress($contact_email);  
    }
    
    if(!$this->mailer->send()) {
      Log::logMailError($this->getContact(), $subject, $message, $headers);
      return false;
    }
    return true;
  }

  function getArticleNumber() {
    return $this->articleNumber;
  }

  function getKeywords() {
    return $this->keywords;
  }

  function getTitle() {
    return $this->title;
  }

  function getAuthors(){
    return implode(' và ', explode('¦', $this->authors));
  }

  function getAuthorsArray(){
    return explode('¦', $this->authors);
  }

  function getAcceptance() {
    return $this->acceptance;
  }

  /*
   * Displays the link to the file downloader page
   * This is used in the review pages only
   */

  function printDownloadArticleLink() {
    print('<form action="download_article.php" method="post">');
    print('<input type="hidden" name="articleNumber" value="'. $this->getArticleNumber() . '" />');
    print('<input type="submit" class="buttonLink" value="Tải bài viết" />');
    print('</form>');
  }
   
  function printUnwithdrawLink() {
    print("<div class=\"floatRight\">");
    print("<form action=\"unwithdraw_article.php\" method=\"post\">\n");
    print("<input type=\"hidden\" name=\"articleNumber\" value=\"". $this->getArticleNumber() . "\" />\n");
    print("<input type=\"submit\" class=\"buttonLink\" value=\"Unwithdraw Article\" />\n");
    print("</form>\n");
    print("</div>\n");
  }

  /* This static function returns the total number of articles in the database */
  static function getNumberOfArticles() {
    $db = Article::openDB();
    $result = $db->querySingle('SELECT COUNT("articleNumber") FROM articles');
    if(!is_null($result)){
      return $result;
    }
    return 0;
  }

  /* This static function returns the total number of needed reviews for the conference */
  /* Note that this is not the number of articles times the number of reviews per article defined by default , */
  /* as each paper can have its own number of needed reviews */

  static function getTotalNumberOfNeededReviews() {
    $total = 0;
    $db = Article::openDB();
    $result = $db->query('SELECT numberOfReviewers FROM articles');
    while($res = $result->fetchArray()){
      $total += base64_decode($res['numberOfReviewers']);
    }
    return $total;
  }

  function getAssignementsByReviewStatus($reviewStatus) {
    return Assignement::getByArticleNumberAndReviewStatus($this->articleNumber, $reviewStatus);
  }

  /* This function returns a concatenation of all the reporst made on this article, including the reviewer if not anonymous */

  function getAllCompletedReports() {

    $assignements = $this->getAssignementsByReviewStatus(Assignement::$COMPLETED);
    $reports = "";
    $ii=0;
    foreach($assignements as $assignement) {
      $ii++;
      $fileName = $this->getFolder() . $assignement->getXMLFileName();
      $reviewDocument = new DOMDocument('1.0');
      $reviewDocument->load($fileName);
      $domxPath = new DOMXpath($reviewDocument);
      $domNode = $domxPath->query("/xml/toAuthors")->item(0);
      $reviewContent = utf8_decode($domNode->firstChild->nodeValue);
      if($reviewContent != "") {
	if(!Tools::reviewersAreAnonymous()) {
	  $reviewerNumber = $assignement->getReviewerNumber();
	  $reviewer = Reviewer::getByReviewerNumber($reviewerNumber);
	  $reports .= "\n\n---- Review by ";
	  $reports .= $reviewer->getFullName();
	  $reports .= " -----------------------------------";
	  $reviewReader = new ReviewReader();
	  $reviewReader->createFromXMLFile($assignement);
	  if($reviewReader->getSubreviewerName() != "") {
	    $reports .= "\n--- Subreviewer: ";
	    $reports .= $reviewReader->getSubreviewerName();
	    $reports .= " ---------------------------------\n\n";	  
	  } else {
	    $reports .= "\n\n";
	  }
	} else {
	  $reports .= "\n\n----------------------------------------------------------\n\nComments #".$ii.": \n";
	}
	$reports .= $reviewContent;
      }
    }
    $reports .= "\n\n-----------------------------------------------------------\n\n";
    return $reports;
  }

  function printAcceptanceSelect() {
    print('<select name="newAcceptance'. $this->articleNumber .'" size="1">');
    print('<option>Rỗng'//Void
      .'</option>');
    if($this->acceptance == Article::$ACCEPT) {
      print('<option selected>Chấp nhận'//Accept
        .'</option>');
    } else {
      print('<option>Chấp nhận'//Accept
        .'</option>');
    }
    if($this->acceptance == Article::$MAYBE_ACCEPT) {
      print('<option selected>Có thể chấp nhận'//Maybe Accept
        .'</option>');
    } else {
      print('<option>Có thể chấp nhận'//Maybe Accept
        .'</option>');
    }
    if($this->acceptance == Article::$DISCUSS) {
      print('<option selected>Thảo luận'//Discuss
        .'</option>');
    } else {
      print('<option>Thảo luận'//Discuss
        .'</option>');
    }
    if($this->acceptance == Article::$MAYBE_REJECT) {
      print('<option selected>Có thể từ chối'//Maybe Reject
        .'</option>');
    } else {
      print('<option>Có thể từ chối'//Maybe Reject
        .'</option>');
    }
    if($this->acceptance == Article::$REJECT) {
      print('<option selected>Từ chối'//Reject
        .'</option>');
    } else {
      print('<option>Từ chối'//Reject
        .'</option>');
    }      
    if (Tools::useCustomAccept1()) {
      if($this->acceptance == Article::$CUSTOM1) {
        print('<option selected>' . Tools::getCustomAccept1() . '</option>');
      } else {
        print('<option>' . Tools::getCustomAccept1() . '</option>');
      }      
    }
    if (Tools::useCustomAccept2()) {
      if($this->acceptance == Article::$CUSTOM2) {
        print('<option selected>' . Tools::getCustomAccept2() . '</option>');
      } else {
        print('<option>' . Tools::getCustomAccept2() . '</option>');
      }      
    }
    if (Tools::useCustomAccept3()) {
      if($this->acceptance == Article::$CUSTOM3) {
        print('<option selected>' . Tools::getCustomAccept3() . '</option>');
      } else {
        print('<option>' . Tools::getCustomAccept3() . '</option>');
      }      
    }
    if (Tools::useCustomAccept4()) {
      if($this->acceptance == Article::$CUSTOM4) {
        print('<option selected>' . Tools::getCustomAccept4() . '</option>');
      } else {
        print('<option>' . Tools::getCustomAccept4() . '</option>');
      }      
    }
    if (Tools::useCustomAccept5()) {
      if($this->acceptance == Article::$CUSTOM5) {
        print('<option selected>' . Tools::getCustomAccept5() . '</option>');
      } else {
        print('<option>' . Tools::getCustomAccept5() . '</option>');
      }      
    }
    if (Tools::useCustomAccept6()) {
      if($this->acceptance == Article::$CUSTOM6) {
        print('<option selected>' . Tools::getCustomAccept6() . '</option>');
      } else {
        print('<option>' . Tools::getCustomAccept6() . '</option>');
      }      
    }
    if (Tools::useCustomAccept7()) {
      if($this->acceptance == Article::$CUSTOM7) {
        print('<option selected>' . Tools::getCustomAccept7() . '</option>');
      } else {
        print('<option>' . Tools::getCustomAccept7() . '</option>');
      }      
    }
    print('</select>');
  }

  static function printSelectArticle() {
    print('<select name="article" size="1">');
    print('<option selected>Không có'//None
      .'</option>');
    $articles = Article::getAllArticles();
    foreach($articles as $article) {
      print('<option>' . $article->getArticleNumber() . ' - '. Tools::HTMLsubstr($article->getTitle(),45) .'</option>');
    }
    print('</select>');
  }

  function printAcceptance() {
    if($this->acceptance == Article::$ACCEPT) {
      print('Accept');
    } else if($this->acceptance == Article::$MAYBE_ACCEPT) {
      print('Maybe Accept');
    } else if($this->acceptance == Article::$DISCUSS) {
      print('Discuss');
    } else if($this->acceptance == Article::$MAYBE_REJECT) {
      print('Maybe Reject');
    } else if($this->acceptance == Article::$REJECT) {
      print('Reject');
    } else {
      print('-');
    }
  }

  function roundAverageOverallGrade($digits) {
    $roundedGrade = "";
    if($this->averageOverallGrade != "-1") {
      $roundedGrade = sprintf("%01." . $digits ."f", $this->averageOverallGrade);
    } else {
      $roundedGrade = "-";
    }
    return $roundedGrade;
  }

  function roundAverageOverallGradeSnapshot($digits) {
    $roundedGrade = "";
    if($this->averageOverallGradeSnapshot != "-1") {
      $roundedGrade = sprintf("%01." . $digits ."f", $this->averageOverallGradeSnapshot);
    } else {
      $roundedGrade = "-";
    }
    return $roundedGrade;
  }

  static function printAcceptanceSummaryTable() {

    $articles = Article::getAllArticles();
    $totals = array();
    $totals[Article::$ACCEPT] = 0;
    $totals[Article::$MAYBE_ACCEPT] = 0;
    $totals[Article::$REJECT] = 0;
    $totals[Article::$MAYBE_REJECT] = 0;
    $totals[Article::$DISCUSS] = 0;
    $totals[Article::$VOID] = 0;
    $totals[Article::$CUSTOM1] = 0;
    $totals[Article::$CUSTOM2] = 0;
    $totals[Article::$CUSTOM3] = 0;
    $totals[Article::$CUSTOM4] = 0;
    $totals[Article::$CUSTOM5] = 0;
    $totals[Article::$CUSTOM6] = 0;
    $totals[Article::$CUSTOM7] = 0;
    foreach($articles as $article) {
      $totals[$article->getAcceptance()]++;
    }
    print('<table class="dottedTable"><tr>');
    print('<th class="topRow" colspan="' . (6+(Tools::useCustomAccept1()?1:0)+(Tools::useCustomAccept2()?1:0)+(Tools::useCustomAccept3()?1:0)+(Tools::useCustomAccept4()?1:0)+(Tools::useCustomAccept5()?1:0)+(Tools::useCustomAccept6()?1:0)+(Tools::useCustomAccept7()?1:0)) . '">
      Tóm tắc của bài viết chấp nhận &amp; từ chối'//Summary of the number of accepted &amp; rejected articles
      .'</th>');
    print('</tr><tr>');
    print('<th class="center">Rỗng'//Void
      .'</th>');
    print('<th class="center">Từ chối'//Reject
      .'</th>');
    print('<th class="center">Có thể từ chối'//Maybe Reject
      .'</th>');
    print('<th class="center">Thảo luận'//Discuss
      .'</th>');
    print('<th class="center">Có thể chấp nhận'//Maybe Accept
      .'</th>');
    print('<th class="center">Chấp nhận'//Accept
      .'</th>');
    if (Tools::useCustomAccept1()) {
      print('<th class="center">' . Tools::getCustomAccept1() . '</th>');
    }
    if (Tools::useCustomAccept2()) {
      print('<th class="center">' . Tools::getCustomAccept2() . '</th>');
    }
    if (Tools::useCustomAccept3()) {
      print('<th class="center">' . Tools::getCustomAccept3() . '</th>');
    }
    if (Tools::useCustomAccept4()) {
      print('<th class="center">' . Tools::getCustomAccept4() . '</th>');
    }
    if (Tools::useCustomAccept5()) {
      print('<th class="center">' . Tools::getCustomAccept5() . '</th>');
    }
    if (Tools::useCustomAccept6()) {
      print('<th class="center">' . Tools::getCustomAccept6() . '</th>');
    }
    if (Tools::useCustomAccept7()) {
      print('<th class="center">' . Tools::getCustomAccept7() . '</th>');
    }
    print('</tr><tr>');
    print('<td class="bigNumber center voidArticle">' . $totals[Article::$VOID] . '</td>');
    print('<td class="bigNumber center rejectArticle">' . $totals[Article::$REJECT] . '</td>');
    print('<td class="bigNumber center maybeRejectArticle">' . $totals[Article::$MAYBE_REJECT] . '</td>');
    print('<td class="bigNumber center discussArticle">' . $totals[Article::$DISCUSS] . '</td>');
    print('<td class="bigNumber center maybeAcceptArticle">' . $totals[Article::$MAYBE_ACCEPT] . '</td>');
    print('<td class="bigNumber center acceptArticle">' . $totals[Article::$ACCEPT] . '</td>');
    if (Tools::useCustomAccept1()) {
      print('<td class="bigNumber center customAccept1">' . $totals[Article::$CUSTOM1] . '</td>');
    }    
    if (Tools::useCustomAccept2()) {
      print('<td class="bigNumber center customAccept2">' . $totals[Article::$CUSTOM2] . '</td>');
    }    
    if (Tools::useCustomAccept3()) {
      print('<td class="bigNumber center customAccept3">' . $totals[Article::$CUSTOM3] . '</td>');
    }    
    if (Tools::useCustomAccept4()) {
      print('<td class="bigNumber center customAccept4">' . $totals[Article::$CUSTOM4] . '</td>');
    }    
    if (Tools::useCustomAccept5()) {
      print('<td class="bigNumber center customAccept5">' . $totals[Article::$CUSTOM5] . '</td>');
    }    
    if (Tools::useCustomAccept6()) {
      print('<td class="bigNumber center customAccept6">' . $totals[Article::$CUSTOM6] . '</td>');
    }    
    if (Tools::useCustomAccept7()) {
      print('<td class="bigNumber center customAccept7">' . $totals[Article::$CUSTOM7] . '</td>');
    }    
    print('</tr></table>');
  }

  static function printAuthorizedAcceptanceSummaryTable($reviewer) {

    $articles = Article::getAllArticles();
    $assignements = Assignement::getByReviewerNumber($reviewer->getReviewerNumber());
    $totals = array();
    $totals[Article::$ACCEPT] = 0;
    $totals[Article::$MAYBE_ACCEPT] = 0;
    $totals[Article::$REJECT] = 0;
    $totals[Article::$MAYBE_REJECT] = 0;
    $totals[Article::$DISCUSS] = 0;
    $totals[Article::$VOID] = 0;
    $totals[Article::$CUSTOM1] = 0;
    $totals[Article::$CUSTOM2] = 0;
    $totals[Article::$CUSTOM3] = 0;
    $totals[Article::$CUSTOM4] = 0;
    $totals[Article::$CUSTOM5] = 0;
    $totals[Article::$CUSTOM6] = 0;
    $totals[Article::$CUSTOM7] = 0;
    foreach($articles as $article) {
      $assignement = $assignements[$article->getArticleNumber()];
      if ($assignement->getAssignementStatus() != Assignement::$BLOCKED) {
	$totals[$article->getAcceptance()]++;
      }
    }
    print('<table class="dottedTable"><tr>');
    print('<th class="topRow" colspan="' . (6+(Tools::useCustomAccept1()?1:0)+(Tools::useCustomAccept2()?1:0)+(Tools::useCustomAccept3()?1:0)+(Tools::useCustomAccept4()?1:0)+(Tools::useCustomAccept5()?1:0)+(Tools::useCustomAccept6()?1:0)+(Tools::useCustomAccept7()?1:0)) . '">Summary of the number of accepted &amp; rejected articles (that you are authorized to see)</th>');
    print('</tr><tr>');
    print('<th class="center">Rỗng'//Void
      .'</th>');
    print('<th class="center">Từ chối'//Reject
      .'</th>');
    print('<th class="center">Có thể từ chối'//Maybe Reject
      .'</th>');
    print('<th class="center">Thảo luận'//Discuss
      .'</th>');
    print('<th class="center">Có thể chấp nhận'//Maybe Accept
      .'</th>');
    print('<th class="center">Chấp nhận'//Accept
      .'</th>');
    if (Tools::useCustomAccept1()) {
      print('<th class="center">' . Tools::getCustomAccept1() . '</th>');
    }
    if (Tools::useCustomAccept2()) {
      print('<th class="center">' . Tools::getCustomAccept2() . '</th>');
    }
    if (Tools::useCustomAccept3()) {
      print('<th class="center">' . Tools::getCustomAccept3() . '</th>');
    }
    if (Tools::useCustomAccept4()) {
      print('<th class="center">' . Tools::getCustomAccept4() . '</th>');
    }
    if (Tools::useCustomAccept5()) {
      print('<th class="center">' . Tools::getCustomAccept5() . '</th>');
    }
    if (Tools::useCustomAccept6()) {
      print('<th class="center">' . Tools::getCustomAccept6() . '</th>');
    }
    if (Tools::useCustomAccept7()) {
      print('<th class="center">' . Tools::getCustomAccept7() . '</th>');
    }
    print('</tr><tr>');
    print('<td class="bigNumber center voidArticle">' . $totals[Article::$VOID] . '</td>');
    print('<td class="bigNumber center rejectArticle">' . $totals[Article::$REJECT] . '</td>');
    print('<td class="bigNumber center maybeRejectArticle">' . $totals[Article::$MAYBE_REJECT] . '</td>');
    print('<td class="bigNumber center discussArticle">' . $totals[Article::$DISCUSS] . '</td>');
    print('<td class="bigNumber center maybeAcceptArticle">' . $totals[Article::$MAYBE_ACCEPT] . '</td>');
    print('<td class="bigNumber center acceptArticle">' . $totals[Article::$ACCEPT] . '</td>');
    if (Tools::useCustomAccept1()) {
      print('<td class="bigNumber center customAccept1">' . $totals[Article::$CUSTOM1] . '</td>');
    }    
    if (Tools::useCustomAccept2()) {
      print('<td class="bigNumber center customAccept2">' . $totals[Article::$CUSTOM2] . '</td>');
    }    
    if (Tools::useCustomAccept3()) {
      print('<td class="bigNumber center customAccept3">' . $totals[Article::$CUSTOM3] . '</td>');
    }    
    if (Tools::useCustomAccept4()) {
      print('<td class="bigNumber center customAccept4">' . $totals[Article::$CUSTOM4] . '</td>');
    }    
    if (Tools::useCustomAccept5()) {
      print('<td class="bigNumber center customAccept5">' . $totals[Article::$CUSTOM5] . '</td>');
    }    
    if (Tools::useCustomAccept6()) {
      print('<td class="bigNumber center customAccept6">' . $totals[Article::$CUSTOM6] . '</td>');
    }    
    if (Tools::useCustomAccept7()) {
      print('<td class="bigNumber center customAccept7">' . $totals[Article::$CUSTOM7] . '</td>');
    }    
    print('</tr></table>');
  }

  static function snapshotAverages() {

    $articles = Article::getAllArticles();
    foreach($articles as $article) {
      $article->averageOverallGradeSnapshot = $article->averageOverallGrade;
      $article->updateInDB();
    }

  }

  static function computeAverages() {
    $articles = Article::getAllArticles();
    foreach($articles as $article) {
      $article->computeAverage();
      $article->updateInDB();
    }
    
  }

  function computeAverage() {

    $assignements = $this->getAssignementsByReviewStatus(Assignement::$COMPLETED);
    $barryWhite = 0; 
    $nbrOfGrades = 0;
    $this->averageOverallGrade = 0;
    $notWeightedAverageOverallGrade = 0;
    $reviewReader = new ReviewReader();
    foreach($assignements as $assignement) {
      $reviewReader->createFromXMLFile($assignement);
      if($reviewReader->getOverallGrade() != "") {
	$nbrOfGrades++;
	$notWeightedAverageOverallGrade += $reviewReader->getOverallGrade();
	if(Tools::useConfidenceLevel()) {
	  if($reviewReader->getConfidenceLevel() != "") {
	    $this->averageOverallGrade += $reviewReader->getConfidenceLevel() * $reviewReader->getOverallGrade();
	    $barryWhite += $reviewReader->getConfidenceLevel();
	  } else {
	    $this->averageOverallGrade += $reviewReader->getOverallGrade();
	    $barryWhite++;
	  }
	}
      }
    }

    if($nbrOfGrades == 0) {

      $this->averageOverallGrade = "-1";

    } else {
    
      if((Tools::useConfidenceLevel()) && ($barryWhite != 0)) {
	$this->averageOverallGrade /= $barryWhite;
      } else {
	$this->averageOverallGrade = $notWeightedAverageOverallGrade / $nbrOfGrades;
      }

    }

    $this->updateInDB();
  }

      

  static function getNotBlockedSortedArticleNumbers($currentReviewer, $sortKey, $reverseOrder, $restrict, $recent, $allArticles, $discussAllowance) {
    
    $reviewerNumber = $currentReviewer->getReviewerNumber();
    $result;
    $articleNumbers=array();
    $db = Article::openDB();
    switch($sortKey) {
    case(Reviewer::$SORT_BY_OVERALL_GRADE):
      if($reverseOrder) {
	$result = $db->query("SELECT articleNumber FROM articles ORDER BY averageOverallGrade, articleNumber DESC");
      } else {
	$result = $db->query("SELECT articleNumber FROM articles ORDER BY averageOverallGrade DESC, articleNumber");
      }
      break;
    case(Reviewer::$SORT_BY_ACCEPTANCE_STATUS):
      if($reverseOrder) {
	$result = $db->query("SELECT articleNumber FROM articles ORDER BY acceptance DESC, articleNumber DESC");  
      } else {
	$result = $db->query("SELECT articleNumber FROM articles ORDER BY acceptance, articleNumber");  
      }
      break;
    case(Reviewer::$SORT_BY_ACCEPTANCE_STATUS_AND_GRADE):
      if($reverseOrder) {
	$result = $db->query("SELECT articleNumber FROM articles ORDER BY acceptance DESC, averageOverallGrade, articleNumber DESC");  
      } else {
	$result = $db->query("SELECT articleNumber FROM articles ORDER BY acceptance, averageOverallGrade DESC, articleNumber");  
      }
      break;
    case(Reviewer::$SORT_BY_LAST_MODIFICATION):
      if($reverseOrder) {
	$result = $db->query("SELECT articleNumber FROM articles ORDER BY lastModificationDate , articleNumber DESC");  
      } else {
	$result = $db->query("SELECT articleNumber FROM articles ORDER BY lastModificationDate DESC, articleNumber");  
      }
      break;
    default:
      if($reverseOrder) {
	$result = $db->query("SELECT articleNumber FROM articles ORDER BY articleNumber DESC");  
      } else {
	$result = $db->query("SELECT articleNumber FROM articles ORDER BY articleNumber");  
      }

    }
    $allAssignements = Assignement::getByReviewerNumber($reviewerNumber);
    $i = 0;
    while ($db_row = $result->fetchArray()){
      $articleNumber = $db_row['articleNumber'];
      $assignement = $allAssignements[$articleNumber];
      $assStat = $assignement->getAssignementStatus();
      if(($assStat == Assignement::$AUTHORIZED) || ($assStat == Assignement::$ASSIGNED)) {
	if((!$restrict) || ($assignement->isSomeWorkDone())) {
	  if ((!$recent) || ($currentReviewer->getLastVisitDate() <= $allArticles[$articleNumber]->getLastModificationDate())) {
	    if (($discussAllowance == Reviewer::$ALLOWED_ALL) ||
	        ($assStat == Assignement::$AUTHORIZED) ||
	        (($assignement->getReviewStatus() == Assignement::$COMPLETED)) && ($discussAllowance == Reviewer::$ALLOWED_FINISHED)) {
	      $articleNumbers[$i] = $articleNumber;
	      $i++;
            }
	  }
	}
      }
    }
    return $articleNumbers;
  }

  static function getSortedArticleNumbersForChair($currentReviewer, $sortKey, $reverseOrder, $useSnapshot, $recent, $allArticles) {
    
    $result;
    $articleNumbers=array();
    $db = Article::openDB();
    $gradeUsed = "averageOverallGrade";
    if($useSnapshot) {
      $gradeUsed = "averageOverallGradeSnapshot";
    } 
    switch($sortKey) {
    case(Reviewer::$SORT_BY_OVERALL_GRADE):
      if($reverseOrder) {
	$result = $db->query("SELECT articleNumber FROM articles ORDER BY ". $gradeUsed .", articleNumber DESC");
      } else {
	$result = $db->query("SELECT articleNumber FROM articles ORDER BY ". $gradeUsed ." DESC, articleNumber");
      }
      break;
    case(Reviewer::$SORT_BY_ACCEPTANCE_STATUS):
      if($reverseOrder) {
	$result = $db->query("SELECT articleNumber FROM articles ORDER BY acceptance DESC, articleNumber DESC");  
      } else {
	$result = $db->query("SELECT articleNumber FROM articles ORDER BY acceptance, articleNumber");  
      }
      break;
    case(Reviewer::$SORT_BY_ACCEPTANCE_STATUS_AND_GRADE):
      if($reverseOrder) {
	$result = $db->query("SELECT articleNumber FROM articles ORDER BY acceptance DESC, ". $gradeUsed .", articleNumber DESC");  
      } else {
	$result = $db->query("SELECT articleNumber FROM articles ORDER BY acceptance, ". $gradeUsed ." DESC, articleNumber");  
      }
      break;
    case(Reviewer::$SORT_BY_LAST_MODIFICATION):
      if($reverseOrder) {
	$result = $db->query("SELECT articleNumber FROM articles ORDER BY lastModificationDate, articleNumber DESC");  
      } else {
	$result = $db->query("SELECT articleNumber FROM articles ORDER BY lastModificationDate DESC, articleNumber");  
      }
      break;
    default:
      if($reverseOrder) {
	$result = $db->query("SELECT articleNumber FROM articles ORDER BY articleNumber DESC");  
      } else {
	$result = $db->query("SELECT articleNumber FROM articles ORDER BY articleNumber");  
      }

    }
    $i = 0;
    while ($db_row = $result->fetchArray()){
      if ((!$recent) || ($currentReviewer->getLastVisitDate() <= $allArticles[$db_row['articleNumber']]->getLastModificationDate())) {
	$articleNumbers[$i] = $db_row['articleNumber'];
	$i++;
      }
    }
    return $articleNumbers;

  }

  function printLastModifications($numberOfLines) {
    $result = Log::getLastModificationsByArticleNumber($this->getArticleNumber(), $numberOfLines);
    if ($result->fetchArray()) {
      $result->reset();
      print('<table>');
      while ($db_row = $result->fetchArray()) {
	$login = base64_decode($db_row['performer']);
	if ($login == "") {
	  if(Tools::use12HourFormat()) {
	    print('<tr><td>'.date("D, j M Y h:i a", $db_row['date']).'</td><td></td><td></td><td></td><td>'. Tools::HTMLsubstr(base64_decode($db_row['description']),60) .'</td></tr>');
	  } else {
	    print('<tr><td>'.date("D, j M Y H:i", $db_row['date']).'</td><td></td><td></td><td></td><td>'. Tools::HTMLsubstr(base64_decode($db_row['description']),60) .'</td></tr>');
	  }
	} else {
	  $reviewer = Reviewer::getByLogin($login);
	  $fullName = $login;
	  if (!is_null($reviewer)) {
	    $fullName = $reviewer->getFullName();
	  }
	  if(Tools::use12HourFormat()) {
	    print('<tr><td>'.date("D, j M Y h:i a", $db_row['date']).'</td><td>&nbsp;by&nbsp;</td><td>'. htmlentities($fullName, ENT_COMPAT | ENT_HTML401, 'UTF-8') .'</td><td>&nbsp;--&nbsp;</td><td>'. Tools::HTMLsubstr(base64_decode($db_row['description']),60) .'</td></tr>');
	  } else {
	    print('<tr><td>'.date("D, j M Y H:i", $db_row['date']).'</td><td>&nbsp;by&nbsp;</td><td>'. htmlentities($fullName, ENT_COMPAT | ENT_HTML401, 'UTF-8') .'</td><td>&nbsp;--&nbsp;</td><td>'. Tools::HTMLsubstr(base64_decode($db_row['description']),60) .'</td></tr>');
	  }
	}
      }
      print('</table>');
    } else {
      print('<div class="versionAbstract"><em>Không có.'//None.''
        .'</em></div>');
    }
  }

  function printLastComments($numberOfComments) {
    $db = Discussion::openDB();
    $totalNumberOfComments = $db->querySingle('SELECT COUNT(*) FROM discussions WHERE articleNumber="'. $this->articleNumber .'"');
    if($totalNumberOfComments == 0) {
      print('<em>Không thảo luận'//No discussion yet.
        .'</em>');
    } else {
      $result = $db->query('SELECT * FROM discussions WHERE articleNumber="'. $this->articleNumber .'" ORDER BY date');
      print('<table><tr><th>Date</th><th>Reviewer</th><th>&nbsp;&nbsp;&nbsp;</th><th class="leftAlign">Bình luận'//Comment
        .'</th></tr>');
      if ($totalNumberOfComments > $numberOfComments) {
	print('<tr><td>...</td><td>...</td><td></td><td>...</td></tr>');
	$result->seek($totalNumberOfComments - $numberOfComments);
      }
      while($db_row = $result->fetchArray()) {
	if(Tools::use12HourFormat()) {
	  print('<tr class="topAlign"><td>'.date("j/m/Y h:i a", $db_row['date']).'</td><td>'.base64_decode($db_row['reviewerName']).' </td><td></td><td class="leftAlign"> '.htmlentities(base64_decode($db_row['comment']), ENT_COMPAT | ENT_HTML401, 'UTF-8').'</td></tr>');
	} else {
	  print('<tr class="topAlign"><td>'.date("j/m/Y H:i", $db_row['date']).'</td><td>'.base64_decode($db_row['reviewerName']).' </td><td></td><td class="leftAlign"> '.htmlentities(base64_decode($db_row['comment']), ENT_COMPAT | ENT_HTML401, 'UTF-8').'</td></tr>');
	}
      }
      print('</table>');
    }
  }

  function printLastModificationDateAndNumberOfComments($currentReviewer) {
    if ($this->lastModificationDate == 0) {
      print('<br/>');
      return;
    }
    $num = Log::getNumberOfComments($this->getArticleNumber());
    if ($num == 1) {
      print($num. ' comment');
    } else {
      print($num. ' comments');
    }
    print(' - Last update: ');
    if ($this->lastModificationDate > $currentReviewer->getLastVisitDate()) {
      print('<span class="maybeRejectArticle">');
      if(Tools::use12HourFormat()) {
	Tools::printHTML(date("D, j M Y h:i a", $this->lastModificationDate));
      } else {
	Tools::printHTML(date("D, j M Y H:i", $this->lastModificationDate));
      }
      print('</span>');
    } else {
      if(Tools::use12HourFormat()) {
	Tools::printHTML(date("D, j M Y h:i a", $this->lastModificationDate));
      } else {
	Tools::printHTML(date("D, j M Y H:i", $this->lastModificationDate));
      }
    }

  }

  function printLastModificationDateOnly($currentReviewer) {
    if ($this->lastModificationDate == 0) {
      print('<br/>');
      return;
    }
    print('Cập nhật cuối cùng:'//Last update: '
      );
    if ($this->lastModificationDate > $currentReviewer->getLastVisitDate()) {
      print('<span class="maybeRejectArticle">');
      if(Tools::use12HourFormat()) {
	Tools::printHTML(date("D, j M Y h:i a", $this->lastModificationDate));
      } else {
	Tools::printHTML(date("D, j M Y H:i", $this->lastModificationDate));
      }
      print('</span>');
    } else {
      if(Tools::use12HourFormat()) {
	Tools::printHTML(date("D, j M Y h:i a", $this->lastModificationDate));
      } else {
	Tools::printHTML(date("D, j M Y H:i", $this->lastModificationDate));
      }
    }

  }

  function getChairComments() {
    return $this->chairComments;
  }

  function printNotCommitteeLink() {
    print("<div class=\"floatRight\">");
    print("<form action=\"uncommittee_article.php\" method=\"post\">\n");
    print("<input type=\"hidden\" name=\"articleNumber\" value=\"". $this->getArticleNumber() . "\" />\n");
    print("<input type=\"submit\" class=\"buttonLink\" value=\"Không thuộc ủy ban\" />\n");
    print("</form>\n");
    print("</div>\n");
  }
  
  function printCommitteeLink() {
    print('<div class="floatRight">');
    print('<form action="committee_article.php" method="post">');
    print('<input type="hidden" name="articleNumber" value="'. $this->getArticleNumber() . '" />');
    print('<input type="submit" class="buttonLink" value="Thuộc ủy ban" />');
    print('</form>');
    print('</div>');
  }

  static function printAcceptanceFinalResults() {
    print('<center>');
    print('<table class="dottedTable"><tr>');
    print('<th class="topRow" colspan="3"><div class="versionTitle">Summary of the accepted &amp; rejected articles</div>(subject to modifications by the chair)</th>');
    
    print('</tr><tr><th class="topRow">Article<br/>Number</th><th class="topRow leftAlign">Title</th><th class="topRow">Status</th></tr>');
    $allArticles = Article::getAllArticles();
    $articleNumbers = Article::getSortedArticleNumbersForChair(null, Reviewer::$SORT_BY_ACCEPTANCE_STATUS, false, false, false, $allArticles);
    foreach ($articleNumbers as $articleNumber) {
      $article = $allArticles[$articleNumber];
      print('<tr><td><div class="bigNumber">'.$article->getArticleNumber().'</div></td><td class="leftAlign">');
      Tools::printHTML($article->getTitle());
      print('</td>');
      $acceptance = $article->getAcceptance();
      if($acceptance == Article::$ACCEPT) {
	print('<td><div class="acceptArticle">Accept</div></td></tr>');
      } else if($acceptance == Article::$MAYBE_ACCEPT) {
	print('<td><div class="maybeAcceptArticle">Maybe Accept</div></td></tr>');
      } else if($acceptance == Article::$DISCUSS) {
	print('<td><div class="discussArticle">Discuss</div></td></tr>');
      } else if($acceptance == Article::$MAYBE_REJECT) {
	print('<td><div class="maybeRejectArticle">Maybe Reject</div></td></tr>');
      } else if($acceptance == Article::$REJECT) {
	print('<td><div class="rejectArticle">Reject</div></td></tr>');
      } else if($acceptance == Article::$CUSTOM1) {
	print('<td><div class="customAccept1">' . Tools::getCustomAccept1() . '</div></td></tr>');
      } else if($acceptance == Article::$CUSTOM2) {
	print('<td><div class="customAccept2">' . Tools::getCustomAccept2() . '</div></td></tr>');
      } else if($acceptance == Article::$CUSTOM3) {
	print('<td><div class="customAccept3">' . Tools::getCustomAccept3() . '</div></td></tr>');
      } else if($acceptance == Article::$CUSTOM4) {
	print('<td><div class="customAccept4">' . Tools::getCustomAccept4() . '</div></td></tr>');
      } else if($acceptance == Article::$CUSTOM5) {
	print('<td><div class="customAccept5">' . Tools::getCustomAccept5() . '</div></td></tr>');
      } else if($acceptance == Article::$CUSTOM6) {
	print('<td><div class="customAccept6">' . Tools::getCustomAccept6() . '</div></td></tr>');
      } else if($acceptance == Article::$CUSTOM7) {
	print('<td><div class="customAccept7">' . Tools::getCustomAccept7() . '</div></td></tr>');
      } else {
	print('<td><div class="voidArticle">-</div></td></tr>');
      }
    }
    print('</table></center>');
  }

}

?>
