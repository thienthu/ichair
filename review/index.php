<?php 

/*
 * Copyright (C) 2006, 2007 Thomas Baigneres, Matthieu Finiasz
 *
 * This file is part of iChair.
 *
 * iChair is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 * 
 * iChair is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

?><?php $page_title='Trang Chủ';//'Main Page';
include('../utils/tools.php');
include('header.php');
if(is_null($currentReviewer)) {
print('<div class="ERRmessage">Tài khoản chưa được thiết lập cấu hình. Vui long liên hệ với quản trị hệ thống để được thiết lập cấu hình.'.
//It seems that your account has not been configured yet. Please contact the iChair admin and ask him to do so.
'</div>');
}

if(!is_null($currentReviewer)) {

  Log::logAccessIndex($currentReviewer);

?>

<?php if(Tools::getCurrentPhase() != Tools::$DECISION) { ?>

<?php if($revHTML) { ?>

  Người dùng nên đọc<!--It is important that you read the--> <a href="guidelines/guidelines.html" target="_blank">Hướng Dẫn Đánh Giá<!--Review Guidelines--></a>

  <?php if($revPDF && $revTXT) { ?>
    (also available in <a href="guidelines/guidelines.pdf" target="_blank">[pdf]</a> and <a href="guidelines/guidelines.txt" target="_blank">[text]</a>)
  <?php } else if($revPDF) { ?>
    (also available in <a href="guidelines/guidelines.pdf" target="_blank">[pdf]</a>)
  <?php } else if($revTXT) { ?>
    (also available in <a href="guidelines/guidelines.txt" target="_blank">[text]</a>)
  <?php } ?>
trước khi bắt đầu đánh giá.
  <!--before starting your reviews.-->

<?php } else if($revPDF) { ?>

  Người dùng nên đọc<!--It is important that you read the--> <a href="guidelines/guidelines.pdf" target="_blank">Hướng Dẫn Đánh Giá<!--Review Guidelines--></a>

  <?php if($revTXT) { ?>
    (also available in <a href="guidelines/guidelines.txt" target="_blank">[text]</a>)
  <?php } ?>

  trước khi bắt đầu đánh giá.
  <!--before starting your reviews.-->

<?php } else if($revTXT) { ?>

  Người dùng nên đọc<!--It is important that you read the--> <a href="guidelines/guidelines.txt" target="_blank">Hướng Dẫn Đánh Giá<!--Review Guidelines--></a>
  trước khi bắt đầu đánh giá.
  <!--before starting your reviews.-->

<?php } ?>
<?php } ?>


</p>


<?php 

  if($currentReviewerGroup != Reviewer::$OBSERVER_GROUP) {

    if(Tools::getCurrentPhase() == Tools::$SUBMISSION) {

      print('<p>Hiện tại hệ thống đang trong giai đoạn <em>Gửi Bài/Tải Bài/Cấp Quyền</em>. Trong thời gian này người dùng có thể cập nhật hồ sơ cá nhân.'.
      	//We currently are in the '<em>Submission/Transfer/Authorizations</em> phase. During this time, you can update your personal profile.
      	'</p>');
      if (Tools::useCategory()) {
	print('<p>Trong thời gian chờ bài viết để đánh giá, người dùng có thể cập nhật hồ sơ cá nhân và chọn loại bài viết ưa thích đánh giá.'
	 //While we are waiting for submissions, you can update your personal profile and choose which categories of articles you would prefer to review. Note that this choice will however have less influence than the individual article selection you will have the chance to make later on.
	 .'</p>');
      } else {
	print('<p>Trong thời gian chờ bài viết để đánh giá, người dùng có thể cập nhật hồ sơ cá nhân.' 
		//While we are waiting for submissions, you can update your personal profile.
		.'</p>');
      }

    } else if(Tools::getCurrentPhase() == Tools::$ELECTION) {
      print('<p>Hiện tại hệ thống đang trong giai đoạn <em>Bầu Chọn Bài Viết Ưa Thích</em>.'.
      //We currently are in the <em>Preferred Articles Election</em> phase.
      	'</p>');
      if(Tools::useAbstract()) {
	print('<div class="floatRight"><form action="download_all_abstracts.php" method="post" />');
	print('<input type="submit" class="buttonLink" value="Tải tất cả tóm tác bài viết" />');
	print('</form></div>');
      }
      if(Tools::getConfig('review/zipForPreferred') && file_exists(Tools::getConfig('server/reviewsPath').'all_articles.zip')) {?>
        <div class="floatRight"><form action="download_all_zip.php" method="post">
           <input type="submit" class="buttonLink" value="Tải Zip của tất cả bài viết"/>
        </form></div>
      <?php }  
      print('<div class="clear"></div><center>');
      $currentReviewer->printAffinityTotalsTable();
      print('</center>');

    } else if(Tools::getCurrentPhase() == Tools::$ASSIGNATION) {
      if(file_exists(Tools::getConfig('server/reviewsPath').'all_articles.zip')) {?>
        <div class="floatRight"><form action="download_all_zip.php" method="post">
          <input type="submit" class="buttonLink" value="Tải Zip của tất cả bài viết"/>
        </form></div>
   <?php }
      print('<p>Hiện tại hệ thống đang trong giai đoạn <em>phân công đánh giá</em>. Trong thời gian này ban tổ chức sẽ phân công người đánh giá bài viết.'
      	//We currently are in the <em>Assignations</em> phase. For the moment the program chair is deciding which articles will be assigned to which reviewer.
      	.'</p>');
      print('<div class="clear"></div>');
      $currentReviewer->printAffinityTotalsTable();

    } else if(Tools::getCurrentPhase() == Tools::$REVIEW) {
      if(file_exists(Tools::getConfig('server/reviewsPath').'all_articles.zip')) {?>
        <div class="floatRight"><form action="download_all_zip.php" method="post">
          <input type="submit" class="buttonLink" value="Tải Zip của tất cả bài viết"/>
        </form></div>
   <?php }
      if(!$currentReviewer->isAllowedToViewOtherReviews()) {
	print('<p>Hiện tại hệ thống đang trong giai đoạn <em>Đánh Giá</em>.'
		//You currently are in the <em>Review</em> phase.
		.'</p>');
      } else {
	print('<p>Hiện tại hệ thống đang trong giai đoạn <em>Đánh Giá/Thảo Luận/Quyết Định</em>.'
		//You currently are in the <em>Review/Discussion/Decision</em> phase.
		.'</p>');
      }
      print('<div class="clear"></div>');
      print('<center>');
      $currentReviewer->printReviewTotalsTable();
      if($currentReviewer->isAllowedToViewOtherReviews()) {
        Article::printAuthorizedAcceptanceSummaryTable($currentReviewer);
      }
      print('</center>');
    } else if(Tools::getCurrentPhase() == Tools::$DECISION) {

      print('<p>Hiện tại hệ thống đang trong giai đoạn <em>Quyết Định</em>. '
      	//We currently are in the <em>Decision</em> phase. During this time, the program chair is alone to choose the fate of the articles. You can no longer modify your reviews or comment on the articles.
      	.'</p>');
      Article::printAcceptanceFinalResults();
    }

  } else {   

   if(Tools::getCurrentPhase() == Tools::$SUBMISSION) {
      print('<p>Hiện tại hệ thống đang trong giai đoạn <em>Gửi Bài/Tải Bài/Cấp Quyền</em>. Trong giai đoạn chờ gửi bài, người dùng có thể cập nhật hồ sơ cá nhận.'
        //We currently are in the <em>Submission/Transfer/Authorizations</em> phase. While we are waiting for submissions, you can update your personal profile.
        .'</p>');

    } else if(Tools::getCurrentPhase() == Tools::$ELECTION) {
      print('<p>Hiện tại hệ thống đang trong giai đoạn <em>Chọn Bài Viết Ưa Thích</em>.'//We currently are in the <em>Preferred Articles Election</em> phase.
        .'</p>');

    } else if(Tools::getCurrentPhase() == Tools::$ASSIGNATION) {
      if(file_exists(Tools::getConfig('server/reviewsPath').'all_articles.zip')) {?>
        <div class="floatRight"><form action="download_all_zip.php" method="post">
          <input type="submit" class="buttonLink" value="Tải Zip của tất cả bài viết"/>
        </form></div>
   <?php }
      print('<p>Hiện tại hệ thống đang trong giai đoạn <em>Phân Công</em>. Trong khoảng thời gian này ban tổ chức sẽ quyết định phân công bài viết cho người đánh giá.'
      //We currently are in the <em>Assignations</em> phase. For the moment the program chair is deciding which articles will be assigned to which reviewer.
        .'</p>');
      print('<div class="clear"></div>');

    } else if(Tools::getCurrentPhase() == Tools::$REVIEW) {
      if(file_exists(Tools::getConfig('server/reviewsPath').'all_articles.zip')) {?>
        <div class="floatRight"><form action="download_all_zip.php" method="post">
          <input type="submit" class="buttonLink" value="Tải Zip của tất cả bài viết"/>
        </form></div>
   <?php }
      print('<p>Hiện tại hệ thống đang trong giai đoạn <em>Đánh Giá/Thảo Luận/Quyết Định</em>.'//You currently are in the <em>Review/Discussion/Decision</em> phase.
        .'</p>');
      print('<div class="clear"></div>');
    } else if(Tools::getCurrentPhase() == Tools::$DECISION) {

      print('<p>Hiện tại hệ thống đang trong giai đoạn <em>Quyết Định</em>. Trong thời gian này, ban tổ chức lựa chọn bài viết. Người dùng không có thể sửa đổi các đánh giá ​​hoặc bình luận về các bài viết. '
      //We currently are in the <em>Decision</em> phase. During this time, the program chair is alone to choose the fate of the articles. You can no longer modify your reviews or comment on the articles.
        .'</p>');
      Article::printAcceptanceFinalResults();
    }
   }

?>

<?php if(Tools::getCurrentPhase() != Tools::$DECISION) { ?>

<h2><a href="meeting_show.php">Thông Báo Từ Ban Tổ Chức<!--Messages from Chair--></a></h2>

<p>Kiểm tra<!--Check  the --> <a href="meeting_show.php">Thông Báo Từ Ban Tổ Chức<!--Messages from Chair--></a>  
thường xuyên để tham khảo ý kiến, ​​bình luận của ban tổ chức.
<!--page regularly to consult comments published by the program chair either of general order or specific to an article.-->
</p>

<h2><a href="profile.php">Hồ Sơ Cá Nhân<!--Personal Profile--></a></h2>

<p>Sử dụng chức năng <!--Use--> <a href="profile.php">Hồ Sơ Cá Nhân<!--Personal Profile--></a> 
để chỉnh sửa họ tên, địa chỉ email hoặc thay đổi mật khẩu.<!--to make modification
to your full name, e-mail address, or to change your password.-->
</p>

<?php } ?>
<?php if(Tools::getCurrentPhase() == Tools::$ELECTION) { ?>

<h2><a href="affinity_list.php">Bài Viết Ưa Thích<!--Preferred Articles--></a></h2>

<?php if ($currentReviewerGroup != Reviewer::$OBSERVER_GROUP) { ?>

<p>Sử dụng chức năng<!--Use-->  <a href="affinity_list.php">Bài Viết Ưa Thích<!--Preferred Articles--></a> cho ban tổ chức chọn loại bài ưa thích đánh giá, không muốn đánh giá, và không có thể đánh giá. Đối với mỗi 
bài viết, nhấn vào <em> chi tiết </em> để xem tất cả chi tiết về bài gửi cụ thể và tải về các tập tin tương ứng.
<!--to indicate
to the chair the paper you would like to review, those you would
rather not have to review, and those you just cannot review. For each
article, click on <em>Details</em> to get all the details about a
specific submissions and to download the corresponding file. -->
</p>

<p>
<!--Note that no change is taken into account until you click on the-->
Lưu ý không thể thay đổi tài  khoản cho đến khi nhấp vào nút
<em>Cập Nhật Ưa Thích<!--Update Preferences--></em><!--button-->.
</p>

<?php } else { ?>

<p>Active reviewers can use the  <a href="affinity_list.php">Bài Viết Ưa Thích<!--Preferred Articles--></a>cho ban tổ chức chọn loại bài ưa thích đánh giá. Như người quan sát trung lập  không có khả năng để lưu lại
sự lựa chọn của họ.
<!--page to indicate to the chair the paper they would like to review.
As a &quot;neutral observer&quot;, you do not have the possibility to save
your choices.--></p>

<?php } ?>

<?php } ?>

<?php if((Tools::getCurrentPhase() == Tools::$REVIEW) && ($currentReviewerGroup != Reviewer::$OBSERVER_GROUP)) { ?>

  <h2><a href="review.php">Đánh Giá<!--Your Reviews--></a></h2>

<p> Sử dụng chức năng <a href="review.php"> Đánh Giá<!--Your Reviéw--> </a> để xem phân công đánh giá và gửi đánh giá. Bấm vào <em> Chi Tiết <!--Detáils--></em> để xem thêm thông tin bài viết cụ thể. Trình nhận xét của bạn, bấm vào <em> Đánh Giá Bài Viết </em> và điền vào các mẫu tương ứng. không quên lưu các thay đổi bằng cách nhấp vào nút <em> Cập Nhật Đánh Giá<!--Update Review--></em>. Đánh giá có thể vẫn còn invisble bằng cách kiểm tra một báo cáo như <em> Trong Tiến Trình<!--In Progress<--> </em>. 
   </ p>
  <!--<p> Use <a href="review.php">Your Reviews</a> to see your assigned reviews
    and to submit reviews. Click on <em>Details</em> to obtain some
    additional information about a specific article. To submit your review,
    click on <em>Review Article</em> and fill the corresponding form. Don't
    forget to save your changes by clicking on the <em>Update Review</em>
    button. Reviews can remain invisble by checking a report as <em>In Progress</em>.
  </p>-->

  <p>Nếu không đánh giá bài viết trực tuyến thì cũng có thể tải một tập tin XML tương ứng với bài viết đó, điền đánh giá vào tập tin và sau đó tải nó lên để xác nhận đánh giá.<!--If you do not want to fill the review form online, you also have
  the possibility to download an XML file corresponding to a given
  article, fill it in offline and then upload it later to validate
  your review.-->
  </p>

<?php } ?>

<?php if((Tools::getCurrentPhase() == Tools::$REVIEW) && ($currentReviewer->isAllowedToViewOtherReviews()) ) { ?>

  <h2><a href="discuss.php">Đánh Giá &amp; Thảo Luận<!--Reviews &amp; Discussions--></a></h2>

  <p> Sử dụng chức năng<!--Use--> <a href="discuss.php">Đánh Giá &amp; Thảo Luận<!--Reviews &amp; Discussions--></a> 
để tham khảo đánh giá và thảo luận được thực hiện bởi tất cả người đánh giá. Với mỗi bài viết có thế nhấn vào <em>chi tiết</em> để biết thêm thông tin về đánh giá cụ thể. Chức năng <em>Đánh giá &amp; Bình Luận </em>cho phép truy cập vào các cuộc thảo luận về bài viết và một danh sách đầy đủ đánh giá ​​về bài viết này.<!--to consult the
    reviews and discussions made by all reviewers. For each article, you can
    click on <em>iếtDetails</em> to get more information about a specific
    review. <em>Reviews &amp; Comments</em> give access to the discussion
    about the article and to a complete list of reviews about this article.-->
  </p>

<?php } ?>

<?php } ?>
<?php include('w3c.php'); ?>
<?php include('footer.php'); ?>
