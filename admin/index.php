<?php 

/*
 * Copyright (C) 2006, 2007 Thomas Baigneres, Matthieu Finiasz
 *
 * This file is part of iChair.
 *
 * iChair is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 * 
 * iChair is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

?><?php $page_title='Trang Chủ';//'Main Page';
include('../utils/tools.php');
include('header.php');
Tools::adminHasNoPassword();
?>

<p>Chào mừng đến với trang quản trị iChair<!--Welcome to the iChair administration page-->. </p>

<h2><a href="configuration.php">Cấu Hình<!--Configuration--></a></h2>

<p>Trang<!--The--> <a href="configuration.php">Cấu hình<!--Configuration--></a> là trang quan trong nhất trong iChair<!-- page is the most important
page in iChair-->:
 đây là nơi có thể cấu hình lại tất cả thông tin từ tên hội thảo  đến hướng.
<!--this is where you can configure everything from the conference name
to the directories where the reviews should be stored.-->
Hệ thống chỉ hoạt đông tốt khi tất cả mọi thứ điều được cấu hình.
<!--The software will only function
correctly once everything is configured as it should.--> 
Phần mô tả chi tiết có thể xem trong cuốn hướng dẫn của iChair.
<!--A detailed description of this
page can be found in the manual coming with iChair.--></p>

<h2><a href="admin_password.php">Mật Khẩu<!--Admin Password--></a></h2>

<p> Sử dụng chức năng <!--Use the>--> <a href="admin_password.php">Mật Khẩu<!--Admin Password--></a> 
 để thiết lập/sửa mật khẩu
<!--page to set/modify the admin password-->.
 Thiết lập mật khẩu là điều đầu tiên cần làm khi cài đặt iChair.
<!--Setting an admin password is the first thing to do after installing iChair.-->
Lưu ý tên đăng nhập luôn là 
<!--Remember that the login for the administration pages is always--> <em>admin</em>, 
để thay đồi vào tập tin .htpasswd sửa thủ công.
<!--to change it you will have to edit the .htpasswd file by hand--></p>

<h2><a href="manage_users.php">Quản Lý Người Dùng<!--Manage Users--></a></h2>

<p><!--The --><a href="manage_users.php">Quản lý người dùng<!--Manage Users--></a> 
là chức năng phân quyền cho phép người dùng nào có thể truy cập vào trang đánh giá bài viết.
<!--page will let you edit the list of users having access to the review pages.-->
Người dùng có 3 nhóm:
<!--You have the possibility to create three types of users:--> 
ban tổ chức hội thảo (phân quyền cao nhất), người phản biện bài viết và người quan sát (xem tất cả mọi thứ nhưng không thể can thiệp vào quá trình đánh gía)
<!--chairs (having many access privileges), reviewers and observers (those can
see everything, but cannot interfere with the review process)-->
. 
 Người dùng muốn vận hành hệ thống ổn định thì cần có ít nhất một tài khoản ban tổ chức hội thảo.
<!--You always need to configure at least one account as chair, otherwise it will not be possible to operate the software correctly--></p>

<p>
Khi tạo tài khoản cần ghi nhớ mật khẩu để có đăng nhập.
<!--Once you have created some users do not forget to generate their passwords
otherwise they will never have the possibility to log on the server. Also be sure
to generate these passwords before the program chair starts sending emails to
the reviewers telling them to log on--></p>


<h2><a href="submit.php">Gửi Bài Khẩn Cấp<!--Emergency Submission--></a> &amp; Sửa Bài Khẩn Cấp<a href="revise.php"><!--Emergency Revision--></a></h2>

<p>
Khi hết thời gian gửi bài và máy chủ tắt, quản trị vẫn có thể gửi hoặc sửa bài với chức năng 
<!--When the submission deadline is passed and the server is shutdown, the administrator
still has the possibility to submit or revise a paper using the-->
<a href="submit.php">Gửi bài khẩn cấp<!--Emergency Submission--></a> và <!-- and the-->
<a href="revise.php">Sửa bài khẩn cấp<!--Emergency Revision--></a><!--pages-->.</p>


<h2><a href="logs.php">Logs</a></h2>

Chức năng<!--The--> <a href="logs.php">Logs</a> giúp cho quản trị có thể theo dõi quá trính hoạt động của máy chủ.
<!--page can be used by the administrator to monitor the activity on the server.--> 
Mọi hoạt động của quản trị, tác giả hoặc người đánh gía khi đăng nhập.
<!--Every action performed by him, an author or a reviewer is logged.-->
Lưu ý chức năng này không để thay thế apache logs, nó chỉ hỗ trợ thêm chức năng, đặc biệt la dùng mục đích thống kê. 
 <!--Note that this is not intended to replace the apache logs, but only comes as a complement, especially for statistical purpose.-->


<h2><a href="rescue_emails.php">E-mails Cứu Hộ<!--Rescue E-mails--></a></h2>

<!--The--><a href="rescue_emails.php">E-mails cứu hộ<!--Rescue E-mails--></a>
thường không được sử dụng
<!-- page should normally not have tobe used-->. 
Tuy nhiên, chức năng này sẽ được sử dụng khi quá trình sendmail không thực hiện được:
<!--However, it can happen that sendmail sometimes is not able to fulfil an email request:-->
nó sẽ giúp khôi phục lại nội dung của mail với chứ năng
 <!--when this is the case, you should be able to recover the content of this mail
with the-->
 <a href="rescue_emails.php">E-mails cứu hộ<!--Rescue E-mails--></a><!-- page--->.
Lưu ý email vẫn có thể được lưu trữ trong hàng đợi mail của sendmail: 
<!--Note that the email might still be stored in the mail queue of sendmail-->: 
nếu lỗi là do máy chủ SMTP cấu hình lạ thì phải loại bỏ nó bằng tay.
<!--if the error was due to a strangely configured SMTP server you might have to remove it manually-->.

<?php include('w3c.php'); ?>
</body>
</html>
