<?php 

/*
 * Copyright (C) 2006, 2007 Thomas Baigneres, Matthieu Finiasz
 *
 * This file is part of iChair.
 *
 * iChair is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 * 
 * iChair is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

?><?php 
$page_title ='Cấu Hình';//'Configuration';
include '../utils/tools.php';
date_default_timezone_set('asia/saigon');
/******************************/
/* Update the config.xml file */
/******************************/

if(count($_POST) != 0) {

  $status = "";
  $warning = "";

  $newConfig = new DOMDocument();
  $newConfig->formatOutput = true;
  $newConfig->encoding = "utf-8";

  $xml = $newConfig->createElement('xml'); 
  $xml = $newConfig->appendChild($xml);

  $conference = $newConfig->createElement('conference');
  $conference = $xml->appendChild($conference);

  $conference_name = $newConfig->createElement('name');
  $conference_name = $conference->appendChild($conference_name);

  if(trim(Tools::readPost('conference_name')) == "") {
    $status .= "Tên hội thảo không để bỏ trống."//The conference name cannot be left empty.
    ."\n";
  } 
  if(strlen(trim(Tools::readPost('conference_name')))> 30) {
    $status .= "Tên hội thảo tối đa 30 ký tự."//The conference name cannot be left empty.
    ."\n";
  } 
  $value = $newConfig->createTextNode(trim(Tools::UTF8readPost('conference_name')));
  $conference_name->appendChild($value);

  $conference_site = $newConfig->createElement('site');
  $conference_site = $conference->appendChild($conference_site);

  $value = $newConfig->createTextNode(trim(Tools::UTF8readPost('conference_site')));
  $conference_site->appendChild($value);

  $conference_logos = $newConfig->createElement('logos');
  $conference_logos = $conference->appendChild($conference_logos);
  
  $i = 1;
  while(array_key_exists('conference_logos_' . $i . '_img', $_POST)) {
    $logo = $newConfig->createElement('logo');
    $logo = $conference_logos->appendChild($logo);
    
    $number = $newConfig->createElement('number');
    $number = $logo->appendChild($number);

    $img = $newConfig->createElement('img');
    $img = $logo->appendChild($img);

    $link = $newConfig->createElement('link');
    $link = $logo->appendChild($link);

    $value = $newConfig->createTextNode($i);
    $number->appendChild($value);

    $value = $newConfig->createTextNode(trim(Tools::UTF8readPost('conference_logos_' . $i . "_img")));
    $img->appendChild($value);

    $value = $newConfig->createTextNode(trim(Tools::UTF8readPost('conference_logos_' . $i . "_link")));
    $link->appendChild($value);

    $i++;
    
  }

  /* server section */

  $server = $newConfig->createElement('server');
  $server = $xml->appendChild($server);

  $server_location = $newConfig->createElement('location');
  $server_location = $server->appendChild($server_location);

  /* Any trailing slash is deleted from the server location */
  $value = $newConfig->createTextNode(preg_replace("/\/$/", "", trim(Tools::UTF8readPost('server_location'))));
  $server_location->appendChild($value);

  $server_submissionsPath = $newConfig->createElement('submissionsPath');
  $server_submissionsPath = $server->appendChild($server_submissionsPath);

  $path = trim(Tools::UTF8readPost('server_submissionsPath'));
  if(!file_exists($path)) {
    $status .= "Đường dẫn bài viết gửi không tồn tại. Vui lòng nhập đường dẫn lại."//The submission path you entered does not exist. Please enter a valid one.
    ."\n";
  } else if(is_file($path)) {
    $status .= "Đường dẫn bài viết gửi không hợp lệ. Vui lòng nhập đường dẫn hợp lệ."//The submission path you entered is a not a valid directory. Please enter a valid one.
    ."\n";
  } else if(!is_writable($path)) {
    $warning .= "Đường dẫn bài viết gửi không truy cập được."//The submission path you entered does not have write access.
    ."\n";
  }
  if(!preg_match("/\/$/", $path)) {
    $path .= "/";
  }
  $value = $newConfig->createTextNode($path);
  $server_submissionsPath->appendChild($value);

  $server_reviewsPath = $newConfig->createElement('reviewsPath');
  $server_reviewsPath = $server->appendChild($server_reviewsPath);

  $path = trim(Tools::UTF8readPost('server_reviewsPath'));
  if(!file_exists($path)) {
    $status .= "Đường dẫn bài viết đánh giá không tồn tại. Vui lòng nhập đường dẫn lại."//The review path you entered does not exist. Please enter a valid one.
    ."\n";
  } else if(is_file($path)) {
    $status .= "Đường dẫn bài viết đánh giá không hợp lệ. Vui lòng nhập đường dẫn hợp lệ."//The review path you entered is a not a valid directory. Please enter a valid one.
    ."\n";
  } else if(!is_writable($path)) {
    $warning .= "Đường dẫn bài viết đánh giá không truy cập được."//The review path you entered does not have write access.
    ."\n";
  } 
  if(!preg_match("/\/$/", $path)) {
    $path .= "/";
  }
  $value = $newConfig->createTextNode($path);
  $server_reviewsPath->appendChild($value);

  $server_logPath = $newConfig->createElement('logPath');
  $server_logPath = $server->appendChild($server_logPath);

  $path = trim(Tools::UTF8readPost('server_logPath'));
  if(!file_exists($path)) {
    $status .= "Đường dẫn log không tồn tại. Vui lòng nhập đường dẫn lại."//The log path you entered does not exist. Please enter a valid one.
    ."\n";
  } else if(is_file($path)) {
    $status .= "Đường dẫn log không hợp lệ. Vui lòng nhập đường dẫn hợp lệ."//The log path you entered is a not a valid directory. Please enter a valid one.
    ."\n";
  } else if(!is_writable($path)) {
    $warning .= "Đường dẫn log không truy cập được."//The log path you entered does not have write access.
    ."\n";
  } 
  if(!preg_match("/\/$/", $path)) {
    $path .= "/";
  }
  $value = $newConfig->createTextNode($path);
  $server_logPath->appendChild($value);

  $server_zipPath = $newConfig->createElement('zipPath');
  $server_zipPath = $server->appendChild($server_zipPath);

  $path = trim(Tools::UTF8readPost('server_zipPath'));
  if($path != "") {

    if(!preg_match("/\/$/", $path)) {
      $path .= "/";
    }
    if (!file_exists($path)) {
      $status .= "Đường dẫn zip không tồn tại. Vui lòng nhập đường dẫn lại hoặc để trống để vô hiệu quá tính năng zip."
      //The zip path you entered does not exist. Please enter a valid one, or leave the field blank to disable the zip feature.
      ."\n";
    } else if(!is_dir($path)) {
      $status .= "Đường dẫn zip không hợp lệ. Vui lòng nhập đường hợp lệ hoặc để trống để vô hiệu quá tính năng zip."
      //The zip path you entered is not a valid directory. Please enter a valid one, or leave the field blank to disable the zip feature.
      ."\n";
    } else if(!file_exists($path . "zip")) {
      $status .= "Đường dẫn zip không chứa lệnh 'zip'. Vui lòng nhập đường hợp lệ hoặc để trống để vô hiệu quá tính năng zip."
      //The zip path you entered does not contain the \"zip\" command. Please enter a valid path, or leave the field blank to disable the zip feature.
      ."\n";
    } else if(!is_executable($path . "zip")) {
      $status .= $path . "zip không thực thi được. Vui lòng nhập đường hợp lệ hoặc để trống để vô hiệu quá tính năng zip."
      //zip is not an executable! Please enter a valid path, or leave the field blank to disable the zip feature.
      ."\n";
    }

  }
  if((!preg_match("/\/$/", $path)) && ($path != "")) {
    $path .= "/";
  }
  $value = $newConfig->createTextNode($path);
  $server_zipPath->appendChild($value);

  $server_pdftkPath = $newConfig->createElement('pdftkPath');
  $server_pdftkPath = $server->appendChild($server_pdftkPath);

  $path = trim(Tools::UTF8readPost('server_pdftkPath'));
  if($path != "") {

    if(!preg_match("/\/$/", $path)) {
      $path .= "/";
    }
    if (!file_exists($path)) {
      $status .= "Đường dẫn pdftk không tồn tại. Vui lòng nhập đường dẫn lại hoặc để trống để vô hiệu quá tính năng pdftk."
      //The pdftk path you entered does not exist. Please enter a valid one, or leave the field blank to disable the pdftk feature.
      ."\n";
    } else if(!is_dir($path)) {
      $status .= "Đường dẫn pdftk  không hợp lệ. Vui lòng nhập đường hợp lệ hoặc để trống để vô hiệu quá tính năng pdftk."
      //The pdftk path you entered is not a valid directory. Please enter a valid one, or leave the field blank to disable the pdftk feature.
      ."\n";
    } else if(!file_exists($path . "pdftk")) {
      $status .= "Đường dẫn pdftk không chứa lệnh 'pdftk'. Vui lòng nhập đường hợp lệ hoặc để trống để vô hiệu quá tính năng pdftk."
      //The pdftk path you entered does not contain the \"pdftk\" command. Please enter a valid path, or leave the field blank to disable the pdftk feature.
      ."\n";
    } else if(!is_executable($path . "pdftk")) {
      $status .= $path . "pdftk  không thực thi được. Vui lòng nhập đường hợp lệ hoặc để trống để vô hiệu quá tính năng pdftk ."
      //pdftk is not an executable! Please enter a valid path, or leave the field blank to disable the pdftk feature.
      ."\n";
    }

  }
  if((!preg_match("/\/$/", $path)) && ($path != "")) {
    $path .= "/";
  }
  $value = $newConfig->createTextNode($path);
  $server_pdftkPath->appendChild($value);

  
  $server_deadlineDate = $newConfig->createElement('deadlineDate');
  $server_deadlineDate = $server->appendChild($server_deadlineDate);

  if(!preg_match("/^[0-3][0-9]\/[0-1][0-9]\/[0-9][0-9][0-9][0-9]$/",trim(Tools::readPost('server_deadlineDate')))) {
    $status .= trim(Tools::readPost('server_deadlineDate')) . " is not a valid deadline date. Please enter a valid one (dd/mm/yyyy).\n";
  } 
  $value = $newConfig->createTextNode(trim(Tools::UTF8readPost('server_deadlineDate')));
  $server_deadlineDate->appendChild($value);

  $server_deadlineTime = $newConfig->createElement('deadlineTime');
  $server_deadlineTime = $server->appendChild($server_deadlineTime);

  if(!preg_match("/^[0-2][0-9]:[0-5][0-9]$/",trim(Tools::readPost('server_deadlineTime')))) {
    $status .= "Please enter a valid deadline time (hh:mm).\n";
  } 
  $value = $newConfig->createTextNode(trim(Tools::UTF8readPost('server_deadlineTime')));
  $server_deadlineTime->appendChild($value);

  $server_shutdownDate = $newConfig->createElement('shutdownDate');
  $server_shutdownDate = $server->appendChild($server_shutdownDate);

  if(!preg_match("/^[0-3][0-9]\/[0-1][0-9]\/[0-9][0-9][0-9][0-9]$/",trim(Tools::readPost('server_shutdownDate')))) {
    $status .= "Please enter a valid shutdown date (dd/mm/yyyy).\n";
  } 
  $value = $newConfig->createTextNode(trim(Tools::UTF8readPost('server_shutdownDate')));
  $server_shutdownDate->appendChild($value);

  $server_shutdownTime = $newConfig->createElement('shutdownTime');
  $server_shutdownTime = $server->appendChild($server_shutdownTime);

  if(!preg_match("/^[0-2][0-9]:[0-5][0-9]$/",trim(Tools::readPost('server_shutdownTime')))) {
    $status .= "Please enter a valid shutdown time (hh:mm).\n";
  } 
  $value = $newConfig->createTextNode(trim(Tools::UTF8readPost('server_shutdownTime')));
  $server_shutdownTime->appendChild($value);

  $server_shutdownMessage = $newConfig->createElement('shutdownMessage');
  $server_shutdownMessage = $server->appendChild($server_shutdownMessage);

  $value = $newConfig->createTextNode(trim(Tools::UTF8readPost('server_shutdownMessage')));
  $server_shutdownMessage->appendChild($value);

  $server_submissionMaxSize = $newConfig->createElement('submissionMaxSize');
  $server_submissionMaxSize = $server->appendChild($server_submissionMaxSize);

  if((!preg_match("/^\d*$/",trim(Tools::readPost('server_submissionMaxSize')))) || (trim(Tools::readPost('server_submissionMaxSize')) == "0")) {
    $status .= "The max submission file size you entered is not a positive integer.\n";
  } 
  $value = $newConfig->createTextNode(trim(Tools::UTF8readPost('server_submissionMaxSize')));
  $server_submissionMaxSize->appendChild($value);

  $server_timeFormat = $newConfig->createElement('timeFormat');
  $server_timeFormat = $server->appendChild($server_timeFormat);

  $value = $newConfig->createTextNode(trim(Tools::UTF8readPost('server_timeFormat')));
  $server_timeFormat->appendChild($value);

  /* mail section */

  $mail = $newConfig->createElement('mail');
  $mail = $xml->appendChild($mail);

  $mail_admin = $newConfig->createElement('admin');
  $mail_admin = $mail->appendChild($mail_admin);

  if(!Tools::isValidEmail(trim(Tools::readPost('mail_admin')))) {
    $status .= trim(Tools::readPost('mail_admin')) . " không hợp lệ. Vui lòng nhập email hợp lệ!\n";
  }
  $value = $newConfig->createTextNode(trim(Tools::UTF8readPost('mail_admin')));
  $mail_admin->appendChild($value);

/// pass email
  $passmail_admin = $newConfig->createElement('passmail_admin');
  $passmail_admin = $mail->appendChild($passmail_admin);

  if(Tools::readPost('passmail_admin') == "") {
    $status .= "Vui lòng nhập mật khẩu cho email admin\n";
  }
  $value = $newConfig->createTextNode(trim(Tools::UTF8readPost('passmail_admin')));
  $passmail_admin->appendChild($value);
//smtp
  $smtpmail_admin = $newConfig->createElement('smtpmail_admin');
  $smtpmail_admin = $mail->appendChild($smtpmail_admin);

  if(trim(Tools::readPost('smtpmail_admin')) == "") {
    $status .= "Vui lòng nhập địa chỉ server STMP cho email admin\n";
  }
  $value = $newConfig->createTextNode(trim(Tools::UTF8readPost('smtpmail_admin')));
  $smtpmail_admin->appendChild($value);


  $mail_useGlobalBCC = $newConfig->createElement('useGlobalBCC');
  $mail_useGlobalBCC = $mail->appendChild($mail_useGlobalBCC);

  $value = $newConfig->createTextNode(trim(Tools::readPost('mail_useGlobalBCC')));
  $mail_useGlobalBCC->appendChild($value);

  $mail_globalBCC = $newConfig->createElement('globalBCC');
  $mail_globalBCC = $mail->appendChild($mail_globalBCC);

  if((!Tools::isValidEmail(trim(Tools::readPost('mail_globalBCC')))) && (trim(Tools::readPost('mail_useGlobalBCC')) == "yes")) {
    $status .= trim(Tools::readPost('mail_globalBCC')) . " is not a valid global BCC email address. Please enter a valid one.\n";
  }
  $value = $newConfig->createTextNode(trim(Tools::readPost('mail_globalBCC')));
  $mail_globalBCC->appendChild($value);

  $mail_authorsFrom = $newConfig->createElement('authorsFrom');
  $mail_authorsFrom = $mail->appendChild($mail_authorsFrom);

  if(!Tools::isValidEmail(trim(Tools::readPost('mail_authorsFrom')))) {
    $status .= trim(Tools::readPost('mail_authorsFrom')) . " is not a valid email address. Please enter a valid one.\n";
  }
  $value = $newConfig->createTextNode(trim(Tools::UTF8readPost('mail_authorsFrom')));
  $mail_authorsFrom->appendChild($value);

  $mail_reviewersFrom = $newConfig->createElement('reviewersFrom');
  $mail_reviewersFrom = $mail->appendChild($mail_reviewersFrom);

  if(!Tools::isValidEmail(trim(Tools::readPost('mail_reviewersFrom')))) {
    $status .= trim(Tools::readPost('mail_reviewersFrom')) . " is not a valid email address. Please enter a valid one.\n";
  }
  $value = $newConfig->createTextNode(trim(Tools::UTF8readPost('mail_reviewersFrom')));
  $mail_reviewersFrom->appendChild($value);

  $mail_submissionSubject = $newConfig->createElement('submissionSubject');
  $mail_submissionSubject = $mail->appendChild($mail_submissionSubject);

  $value = $newConfig->createTextNode(trim(Tools::UTF8readPost('mail_submissionSubject')));
  $mail_submissionSubject->appendChild($value);

  $mail_revisionSubject = $newConfig->createElement('revisionSubject');
  $mail_revisionSubject = $mail->appendChild($mail_revisionSubject);

  $value = $newConfig->createTextNode(trim(Tools::UTF8readPost('mail_revisionSubject')));
  $mail_revisionSubject->appendChild($value);
  
  $mail_withdrawSubject = $newConfig->createElement('withdrawSubject');
  $mail_withdrawSubject = $mail->appendChild($mail_withdrawSubject);

  $value = $newConfig->createTextNode(trim(Tools::UTF8readPost('mail_withdrawSubject')));
  $mail_withdrawSubject->appendChild($value);

  $mail_passwordSubject = $newConfig->createElement('passwordSubject');
  $mail_passwordSubject = $mail->appendChild($mail_passwordSubject);

  $value = $newConfig->createTextNode(trim(Tools::UTF8readPost('mail_passwordSubject')));
  $mail_passwordSubject->appendChild($value);

  $mail_submissionMessage = $newConfig->createElement('submissionMessage');
  $mail_submissionMessage = $mail->appendChild($mail_submissionMessage);

  $value = $newConfig->createTextNode(trim(Tools::UTF8readPost('mail_submissionMessage')));
  $mail_submissionMessage->appendChild($value);

  $mail_revisionMessage = $newConfig->createElement('revisionMessage');
  $mail_revisionMessage = $mail->appendChild($mail_revisionMessage);

  $value = $newConfig->createTextNode(trim(Tools::UTF8readPost('mail_revisionMessage')));
  $mail_revisionMessage->appendChild($value);
  
  $mail_withdrawMessage = $newConfig->createElement('withdrawMessage');
  $mail_withdrawMessage = $mail->appendChild($mail_withdrawMessage);

  $value = $newConfig->createTextNode(trim(Tools::UTF8readPost('mail_withdrawMessage')));
  $mail_withdrawMessage->appendChild($value);

  $mail_passwordMessage = $newConfig->createElement('passwordMessage');
  $mail_passwordMessage = $mail->appendChild($mail_passwordMessage);

  $value = $newConfig->createTextNode(trim(Tools::UTF8readPost('mail_passwordMessage')));
  $mail_passwordMessage->appendChild($value);

  /* Submission Section */

  $submission = $newConfig->createElement('submission');
  $submission = $xml->appendChild($submission);

  $submission_useAffiliations = $newConfig->createElement('useAffiliations');
  $submission_useAffiliations = $submission->appendChild($submission_useAffiliations);

  $value = $newConfig->createTextNode(trim(Tools::UTF8readPost('submission_useAffiliations')));
  $submission_useAffiliations->appendChild($value);

  $submission_useCustomCheck1 = $newConfig->createElement('useCustomCheck1');
  $submission_useCustomCheck1 = $submission->appendChild($submission_useCustomCheck1);

  $value = $newConfig->createTextNode(Tools::UTF8readPost('submission_useCustomCheck1'));
  $submission_useCustomCheck1->appendChild($value);

  $submission_shortCustomCheck1 = $newConfig->createElement('shortCustomCheck1');
  $submission_shortCustomCheck1 = $submission->appendChild($submission_shortCustomCheck1);

  $value = $newConfig->createTextNode(Tools::UTF8readPost('submission_shortCustomCheck1'));
  $submission_shortCustomCheck1->appendChild($value);

  $submission_longCustomCheck1 = $newConfig->createElement('longCustomCheck1');
  $submission_longCustomCheck1 = $submission->appendChild($submission_longCustomCheck1);

  $value = $newConfig->createTextNode(Tools::UTF8readPost('submission_longCustomCheck1'));
  $submission_longCustomCheck1->appendChild($value);

  $submission_chaironlyCustomCheck1 = $newConfig->createElement('chaironlyCustomCheck1');
  $submission_chaironlyCustomCheck1 = $submission->appendChild($submission_chaironlyCustomCheck1);

  $value = $newConfig->createTextNode(Tools::UTF8readPost('submission_chaironlyCustomCheck1'));
  $submission_chaironlyCustomCheck1->appendChild($value);

  $submission_useCustomCheck2 = $newConfig->createElement('useCustomCheck2');
  $submission_useCustomCheck2 = $submission->appendChild($submission_useCustomCheck2);

  $value = $newConfig->createTextNode(Tools::UTF8readPost('submission_useCustomCheck2'));
  $submission_useCustomCheck2->appendChild($value);

  $submission_shortCustomCheck2 = $newConfig->createElement('shortCustomCheck2');
  $submission_shortCustomCheck2 = $submission->appendChild($submission_shortCustomCheck2);

  $value = $newConfig->createTextNode(Tools::UTF8readPost('submission_shortCustomCheck2'));
  $submission_shortCustomCheck2->appendChild($value);

  $submission_longCustomCheck2 = $newConfig->createElement('longCustomCheck2');
  $submission_longCustomCheck2 = $submission->appendChild($submission_longCustomCheck2);

  $value = $newConfig->createTextNode(Tools::UTF8readPost('submission_longCustomCheck2'));
  $submission_longCustomCheck2->appendChild($value);

  $submission_chaironlyCustomCheck2 = $newConfig->createElement('chaironlyCustomCheck2');
  $submission_chaironlyCustomCheck2 = $submission->appendChild($submission_chaironlyCustomCheck2);

  $value = $newConfig->createTextNode(Tools::UTF8readPost('submission_chaironlyCustomCheck2'));
  $submission_chaironlyCustomCheck2->appendChild($value);

  $submission_useCustomCheck3 = $newConfig->createElement('useCustomCheck3');
  $submission_useCustomCheck3 = $submission->appendChild($submission_useCustomCheck3);

  $value = $newConfig->createTextNode(Tools::UTF8readPost('submission_useCustomCheck3'));
  $submission_useCustomCheck3->appendChild($value);

  $submission_shortCustomCheck3 = $newConfig->createElement('shortCustomCheck3');
  $submission_shortCustomCheck3 = $submission->appendChild($submission_shortCustomCheck3);

  $value = $newConfig->createTextNode(Tools::UTF8readPost('submission_shortCustomCheck3'));
  $submission_shortCustomCheck3->appendChild($value);

  $submission_longCustomCheck3 = $newConfig->createElement('longCustomCheck3');
  $submission_longCustomCheck3 = $submission->appendChild($submission_longCustomCheck3);

  $value = $newConfig->createTextNode(Tools::UTF8readPost('submission_longCustomCheck3'));
  $submission_longCustomCheck3->appendChild($value);

  $submission_chaironlyCustomCheck3 = $newConfig->createElement('chaironlyCustomCheck3');
  $submission_chaironlyCustomCheck3 = $submission->appendChild($submission_chaironlyCustomCheck3);

  $value = $newConfig->createTextNode(Tools::UTF8readPost('submission_chaironlyCustomCheck3'));
  $submission_chaironlyCustomCheck3->appendChild($value);



  $submission_useAbstract = $newConfig->createElement('useAbstract');
  $submission_useAbstract = $submission->appendChild($submission_useAbstract);

  $value = $newConfig->createTextNode(Tools::UTF8readPost('submission_useAbstract'));
  $submission_useAbstract->appendChild($value);

  $submission_useCategory = $newConfig->createElement('useCategory');
  $submission_useCategory = $submission->appendChild($submission_useCategory);

  $value = $newConfig->createTextNode(Tools::UTF8readPost('submission_useCategory'));
  $submission_useCategory->appendChild($value);

  $submission_useKeywords = $newConfig->createElement('useKeywords');
  $submission_useKeywords = $submission->appendChild($submission_useKeywords);

  $value = $newConfig->createTextNode(Tools::UTF8readPost('submission_useKeywords'));
  $submission_useKeywords->appendChild($value);

  $submission_useCountry = $newConfig->createElement('useCountry');
  $submission_useCountry = $submission->appendChild($submission_useCountry);

  $value = $newConfig->createTextNode(Tools::UTF8readPost('submission_useCountry'));
  $submission_useCountry->appendChild($value);

  $submission_usePreview = $newConfig->createElement('usePreview');
  $submission_usePreview = $submission->appendChild($submission_usePreview);

  $value = $newConfig->createTextNode(Tools::UTF8readPost('submission_usePreview'));
  $submission_usePreview->appendChild($value);

  $categoriesXMLString = Tools::readPost('submission_categories');
  $newCategories = new DOMDocument();
  /* If there is nothing in the POST */
  if(!@$newCategories->loadXML("<categories>" . $categoriesXMLString . "</categories>")) {
    /* The XML data for the categories is badly formated, we need to build a CDATA field */
    $categories = $newConfig->createElement('categories');
    $categories = $submission->appendChild($categories);
    $bfs = $newConfig->createElement('badlyFormatedString');
    $bfs = $categories->appendChild($bfs);
    $value = $newConfig->createCDATASection($categoriesXMLString);
    $bfs->appendChild($value);
    $status .= "Không phải loại định dạng XML. Phải chắc rằng đóng tất cả các thẻ đúng cách"
    //The categories are not in a valid XML format. Please make sure you closed every tag properly.
    ."\n";
  } else {
    $xpath = new DOMXPath($newCategories);
    $newCategoriesNode = $xpath->query('/categories')->item(0);
    $newCategoriesNode = $newConfig->importNode($newCategoriesNode,true);
    $submission->appendChild($newCategoriesNode);
  }

  /* Review Section */

  $review = $newConfig->createElement('review');
  $review = $xml->appendChild($review);

  $review_tagArticles = $newConfig->createElement('tagArticles');
  $review_tagArticles = $review->appendChild($review_tagArticles);

  $value = $newConfig->createTextNode(Tools::UTF8readPost('review_tagArticles'));
  $review_tagArticles->appendChild($value);

  $review_tagArticlesString = $newConfig->createElement('tagArticlesString');
  $review_tagArticlesString = $review->appendChild($review_tagArticlesString);

  $value = $newConfig->createTextNode(trim(Tools::UTF8readPost('review_tagArticlesString')));
  $review_tagArticlesString->appendChild($value);


  $review_useCustomAccept1 = $newConfig->createElement('useCustomAccept1');
  $review_useCustomAccept1 = $review->appendChild($review_useCustomAccept1);

  $value = $newConfig->createTextNode(Tools::UTF8readPost('review_useCustomAccept1'));
  $review_useCustomAccept1->appendChild($value);

  $review_customAccept1 = $newConfig->createElement('customAccept1');
  $review_customAccept1 = $review->appendChild($review_customAccept1);

  $value = $newConfig->createTextNode(trim(Tools::UTF8readPost('review_customAccept1')));
  $review_customAccept1->appendChild($value);

  $review_useCustomAccept2 = $newConfig->createElement('useCustomAccept2');
  $review_useCustomAccept2 = $review->appendChild($review_useCustomAccept2);

  $value = $newConfig->createTextNode(Tools::UTF8readPost('review_useCustomAccept2'));
  $review_useCustomAccept2->appendChild($value);

  $review_customAccept2 = $newConfig->createElement('customAccept2');
  $review_customAccept2 = $review->appendChild($review_customAccept2);

  $value = $newConfig->createTextNode(trim(Tools::UTF8readPost('review_customAccept2')));
  $review_customAccept2->appendChild($value);

  $review_useCustomAccept3 = $newConfig->createElement('useCustomAccept3');
  $review_useCustomAccept3 = $review->appendChild($review_useCustomAccept3);

  $value = $newConfig->createTextNode(Tools::UTF8readPost('review_useCustomAccept3'));
  $review_useCustomAccept3->appendChild($value);

  $review_customAccept3 = $newConfig->createElement('customAccept3');
  $review_customAccept3 = $review->appendChild($review_customAccept3);

  $value = $newConfig->createTextNode(trim(Tools::UTF8readPost('review_customAccept3')));
  $review_customAccept3->appendChild($value);

  $review_useCustomAccept4 = $newConfig->createElement('useCustomAccept4');
  $review_useCustomAccept4 = $review->appendChild($review_useCustomAccept4);

  $value = $newConfig->createTextNode(Tools::UTF8readPost('review_useCustomAccept4'));
  $review_useCustomAccept4->appendChild($value);

  $review_customAccept4 = $newConfig->createElement('customAccept4');
  $review_customAccept4 = $review->appendChild($review_customAccept4);

  $value = $newConfig->createTextNode(trim(Tools::UTF8readPost('review_customAccept4')));
  $review_customAccept4->appendChild($value);

  $review_useCustomAccept5 = $newConfig->createElement('useCustomAccept5');
  $review_useCustomAccept5 = $review->appendChild($review_useCustomAccept5);

  $value = $newConfig->createTextNode(Tools::UTF8readPost('review_useCustomAccept5'));
  $review_useCustomAccept5->appendChild($value);

  $review_customAccept5 = $newConfig->createElement('customAccept5');
  $review_customAccept5 = $review->appendChild($review_customAccept5);

  $value = $newConfig->createTextNode(trim(Tools::UTF8readPost('review_customAccept5')));
  $review_customAccept5->appendChild($value);

  $review_useCustomAccept6 = $newConfig->createElement('useCustomAccept6');
  $review_useCustomAccept6 = $review->appendChild($review_useCustomAccept6);

  $value = $newConfig->createTextNode(Tools::UTF8readPost('review_useCustomAccept6'));
  $review_useCustomAccept6->appendChild($value);

  $review_customAccept6 = $newConfig->createElement('customAccept6');
  $review_customAccept6 = $review->appendChild($review_customAccept6);

  $value = $newConfig->createTextNode(trim(Tools::UTF8readPost('review_customAccept6')));
  $review_customAccept6->appendChild($value);

  $review_useCustomAccept7 = $newConfig->createElement('useCustomAccept7');
  $review_useCustomAccept7 = $review->appendChild($review_useCustomAccept7);

  $value = $newConfig->createTextNode(Tools::UTF8readPost('review_useCustomAccept7'));
  $review_useCustomAccept7->appendChild($value);

  $review_customAccept7 = $newConfig->createElement('customAccept7');
  $review_customAccept7 = $review->appendChild($review_customAccept7);

  $value = $newConfig->createTextNode(trim(Tools::UTF8readPost('review_customAccept7')));
  $review_customAccept7->appendChild($value);




  $review_zipForPreferred = $newConfig->createElement('zipForPreferred');
  $review_zipForPreferred = $review->appendChild($review_zipForPreferred);

  $value = $newConfig->createTextNode(trim(Tools::UTF8readPost('review_zipForPreferred')));
  $review_zipForPreferred->appendChild($value);

  if(!preg_match("/^[0-9]+$/",trim(Tools::readPost('review_numberOfReviewers')))) {
    $status .= "Số mặc định người đánh giá nhập vào phải là một số nguyên không âm."
    //The default number of reviewers you entered is not a non-negative integer.
    ."\n";
  }
  $review_numberOfReviewers = $newConfig->createElement('numberOfReviewers');
  $review_numberOfReviewers = $review->appendChild($review_numberOfReviewers);

  $value = $newConfig->createTextNode(trim(Tools::UTF8readPost('review_numberOfReviewers')));
  $review_numberOfReviewers->appendChild($value);

  if(!preg_match("/^[0-9]+$/",trim(Tools::readPost('review_numberOfReviewersCommittee')))) {
    $status .= "Số mặc định người đánh giá của ban tổ chức nhập vào phải là một số nguyên không âm"
    //The default number of reviewers for program committee papers you entered is not a non-negative integer.
    ."\n";
  }
  $review_numberOfReviewersCommittee = $newConfig->createElement('numberOfReviewersCommittee');
  $review_numberOfReviewersCommittee = $review->appendChild($review_numberOfReviewersCommittee);

  $value = $newConfig->createTextNode(trim(Tools::UTF8readPost('review_numberOfReviewersCommittee')));
  $review_numberOfReviewersCommittee->appendChild($value);

  $review_anonymousAuthors = $newConfig->createElement('anonymousAuthors');
  $review_anonymousAuthors = $review->appendChild($review_anonymousAuthors);

  $value = $newConfig->createTextNode(Tools::UTF8readPost('review_anonymousAuthors'));
  $review_anonymousAuthors->appendChild($value);

  $review_anonymousReviewers = $newConfig->createElement('anonymousReviewers');
  $review_anonymousReviewers = $review->appendChild($review_anonymousReviewers);

  $value = $newConfig->createTextNode(Tools::UTF8readPost('review_anonymousReviewers'));
  $review_anonymousReviewers->appendChild($value);

  $review_preferredDeadlineDate = $newConfig->createElement('preferredDeadlineDate');
  $review_preferredDeadlineDate = $review->appendChild($review_preferredDeadlineDate);
    
  if(!preg_match("/^[0-3][0-9]\/[0-1][0-9]\/[0-9][0-9][0-9][0-9]$/",trim(Tools::readPost('review_preferredDeadlineDate')))) {
    $status .= trim(Tools::readPost('review_preferredDeadlineDate')) . " là ngày hết hạn chọn bài viết ưa thích không hợp lệ. Vui lòng nhập ngày hợp lệ (dd/mm/yyy)."
    // is not a valid preferred articles deadline date. Please enter a valid one (dd/mm/yyyy).
    ."\n";
  }
  $value = $newConfig->createTextNode(trim(Tools::UTF8readPost('review_preferredDeadlineDate')));
  $review_preferredDeadlineDate->appendChild($value);
                
  $review_preferredDeadlineTime = $newConfig->createElement('preferredDeadlineTime');
  $review_preferredDeadlineTime = $review->appendChild($review_preferredDeadlineTime);
                    
  if(!preg_match("/^[0-2][0-9]:[0-5][0-9]$/",trim(Tools::readPost('review_preferredDeadlineTime')))) {
    $status .= "Please enter a valid preferred articles deadline time (hh:mm).\n";
  }
  $value = $newConfig->createTextNode(trim(Tools::UTF8readPost('review_preferredDeadlineTime')));
  $review_preferredDeadlineTime->appendChild($value);
                                                             

  $review_deadlineDate = $newConfig->createElement('deadlineDate');
  $review_deadlineDate = $review->appendChild($review_deadlineDate);

  if(!preg_match("/^[0-3][0-9]\/[0-1][0-9]\/[0-9][0-9][0-9][0-9]$/",trim(Tools::readPost('review_deadlineDate')))) {
    $status .= trim(Tools::readPost('review_deadlineDate')) . " là ngày hết hạn đánh giá bài viết không hợp lệ. Vui lòng nhập ngày hợp lệ (dd/mm/yyy)."
    //is not a valid review deadline date. Please enter a valid one (dd/mm/yyyy).
    ."\n";
  } 
  $value = $newConfig->createTextNode(trim(Tools::UTF8readPost('review_deadlineDate')));
  $review_deadlineDate->appendChild($value);

  $review_deadlineTime = $newConfig->createElement('deadlineTime');
  $review_deadlineTime = $review->appendChild($review_deadlineTime);

  if(!preg_match("/^[0-2][0-9]:[0-5][0-9]$/",trim(Tools::readPost('review_deadlineTime')))) {
    $status .= "Vui lòng nhập thời gian hết hạn đánh giá (hh:mm)"
    //Please enter a valid review deadline time (hh:mm).
    ."\n";
  } 
  $value = $newConfig->createTextNode(trim(Tools::UTF8readPost('review_deadlineTime')));
  $review_deadlineTime->appendChild($value);

  $review_overallGradeMax = $newConfig->createElement('overallGradeMax');
  $review_overallGradeMax = $review->appendChild($review_overallGradeMax);

  $value = $newConfig->createTextNode(trim(Tools::UTF8readPost('review_overallGradeMax')));
  $review_overallGradeMax->appendChild($value);

  $review_overallGradeMin = $newConfig->createElement('overallGradeMin');
  $review_overallGradeMin = $review->appendChild($review_overallGradeMin);

  $value = $newConfig->createTextNode(trim(Tools::UTF8readPost('review_overallGradeMin')));
  $review_overallGradeMin->appendChild($value);

  $status .= Tools::checkMinMaxGrades(trim(Tools::readPost('review_overallGradeMin')), trim(Tools::readPost('review_overallGradeMax')), "Overall Grade");

  $review_overallGradeSemantics = $newConfig->createElement('overallGradeSemantics');
  $review_overallGradeSemantics = $review->appendChild($review_overallGradeSemantics);

  $value = $newConfig->createTextNode(trim(Tools::UTF8readPost('review_overallGradeSemantics')));
  $review_overallGradeSemantics->appendChild($value);

  $status .= Tools::checkGradeSematics(trim(Tools::readPost('review_overallGradeSemantics')), "Overall Grade");

  $review_confidenceLevelMax = $newConfig->createElement('confidenceLevelMax');
  $review_confidenceLevelMax = $review->appendChild($review_confidenceLevelMax);

  $value = $newConfig->createTextNode(trim(Tools::UTF8readPost('review_confidenceLevelMax')));
  $review_confidenceLevelMax->appendChild($value);

  $review_confidenceLevelMin = $newConfig->createElement('confidenceLevelMin');
  $review_confidenceLevelMin = $review->appendChild($review_confidenceLevelMin);

  $value = $newConfig->createTextNode(trim(Tools::UTF8readPost('review_confidenceLevelMin')));
  $review_confidenceLevelMin->appendChild($value);

  $review_useConfidenceLevel = $newConfig->createElement('useConfidenceLevel');
  $review_useConfidenceLevel = $review->appendChild($review_useConfidenceLevel);

  $value = $newConfig->createTextNode(Tools::UTF8readPost('review_useConfidenceLevel'));
  $review_useConfidenceLevel->appendChild($value);

  $status .= Tools::checkMinMaxGrades(trim(Tools::readPost('review_confidenceLevelMin')), trim(Tools::readPost('review_confidenceLevelMax')), "Confidence Level");

  $review_confidenceLevelSemantics = $newConfig->createElement('confidenceLevelSemantics');
  $review_confidenceLevelSemantics = $review->appendChild($review_confidenceLevelSemantics);

  $value = $newConfig->createTextNode(trim(Tools::UTF8readPost('review_confidenceLevelSemantics')));
  $review_confidenceLevelSemantics->appendChild($value);

  $status .= Tools::checkGradeSematics(trim(Tools::readPost('review_confidenceLevelSemantics')), "Confidence Level");

  $review_technicalQualityMax = $newConfig->createElement('technicalQualityMax');
  $review_technicalQualityMax = $review->appendChild($review_technicalQualityMax);

  $value = $newConfig->createTextNode(trim(Tools::UTF8readPost('review_technicalQualityMax')));
  $review_technicalQualityMax->appendChild($value);

  $review_technicalQualityMin = $newConfig->createElement('technicalQualityMin');
  $review_technicalQualityMin = $review->appendChild($review_technicalQualityMin);

  $value = $newConfig->createTextNode(trim(Tools::UTF8readPost('review_technicalQualityMin')));
  $review_technicalQualityMin->appendChild($value);

  $review_useTechnicalQuality = $newConfig->createElement('useTechnicalQuality');
  $review_useTechnicalQuality = $review->appendChild($review_useTechnicalQuality);

  $value = $newConfig->createTextNode(Tools::UTF8readPost('review_useTechnicalQuality'));
  $review_useTechnicalQuality->appendChild($value);

  $status .= Tools::checkMinMaxGrades(trim(Tools::readPost('review_technicalQualityMin')), trim(Tools::readPost('review_technicalQualityMax')), "Technical Level");

  $review_technicalQualitySemantics = $newConfig->createElement('technicalQualitySemantics');
  $review_technicalQualitySemantics = $review->appendChild($review_technicalQualitySemantics);

  $value = $newConfig->createTextNode(trim(Tools::UTF8readPost('review_technicalQualitySemantics')));
  $review_technicalQualitySemantics->appendChild($value);

  $status .= Tools::checkGradeSematics(trim(Tools::readPost('review_technicalQualitySemantics')), "Technical Level");

  $review_editorialQualityMax = $newConfig->createElement('editorialQualityMax');
  $review_editorialQualityMax = $review->appendChild($review_editorialQualityMax);

  $value = $newConfig->createTextNode(trim(Tools::UTF8readPost('review_editorialQualityMax')));
  $review_editorialQualityMax->appendChild($value);

  $review_editorialQualityMin = $newConfig->createElement('editorialQualityMin');
  $review_editorialQualityMin = $review->appendChild($review_editorialQualityMin);

  $value = $newConfig->createTextNode(trim(Tools::UTF8readPost('review_editorialQualityMin')));
  $review_editorialQualityMin->appendChild($value);

  $review_useEditorialQuality = $newConfig->createElement('useEditorialQuality');
  $review_useEditorialQuality = $review->appendChild($review_useEditorialQuality);

  $value = $newConfig->createTextNode(Tools::UTF8readPost('review_useEditorialQuality'));
  $review_useEditorialQuality->appendChild($value);

  $status .= Tools::checkMinMaxGrades(trim(Tools::readPost('review_editorialQualityMin')), trim(Tools::readPost('review_editorialQualityMax')), "Editorial Quality");

  $review_editorialQualitySemantics = $newConfig->createElement('editorialQualitySemantics');
  $review_editorialQualitySemantics = $review->appendChild($review_editorialQualitySemantics);

  $value = $newConfig->createTextNode(trim(Tools::UTF8readPost('review_editorialQualitySemantics')));
  $review_editorialQualitySemantics->appendChild($value);

  $status .= Tools::checkGradeSematics(trim(Tools::readPost('review_editorialQualitySemantics')), "Editorial Quality");

  $review_suitabilityMax = $newConfig->createElement('suitabilityMax');
  $review_suitabilityMax = $review->appendChild($review_suitabilityMax);

  $value = $newConfig->createTextNode(trim(Tools::UTF8readPost('review_suitabilityMax')));
  $review_suitabilityMax->appendChild($value);

  $review_suitabilityMin = $newConfig->createElement('suitabilityMin');
  $review_suitabilityMin = $review->appendChild($review_suitabilityMin);

  $value = $newConfig->createTextNode(trim(Tools::UTF8readPost('review_suitabilityMin')));
  $review_suitabilityMin->appendChild($value);

  $review_useSuitability = $newConfig->createElement('useSuitability');
  $review_useSuitability = $review->appendChild($review_useSuitability);

  $value = $newConfig->createTextNode(Tools::UTF8readPost('review_useSuitability'));
  $review_useSuitability->appendChild($value);
  
  $status .= Tools::checkMinMaxGrades(trim(Tools::readPost('review_suitabilityMin')), trim(Tools::readPost('review_suitabilityMax')), "Suitability");

  $review_suitabilitySemantics = $newConfig->createElement('suitabilitySemantics');
  $review_suitabilitySemantics = $review->appendChild($review_suitabilitySemantics);

  $value = $newConfig->createTextNode(trim(Tools::UTF8readPost('review_suitabilitySemantics')));
  $review_suitabilitySemantics->appendChild($value);

  $status .= Tools::checkGradeSematics(trim(Tools::readPost('review_suitabilitySemantics')), "Suitability");

  $review_bestPaperMax = $newConfig->createElement('bestPaperMax');
  $review_bestPaperMax = $review->appendChild($review_bestPaperMax);

  $value = $newConfig->createTextNode(trim(Tools::UTF8readPost('review_bestPaperMax')));
  $review_bestPaperMax->appendChild($value);

  $review_bestPaperMin = $newConfig->createElement('bestPaperMin');
  $review_bestPaperMin = $review->appendChild($review_bestPaperMin);

  $value = $newConfig->createTextNode(trim(Tools::UTF8readPost('review_bestPaperMin')));
  $review_bestPaperMin->appendChild($value);

  $review_useBestPaper = $newConfig->createElement('useBestPaper');
  $review_useBestPaper = $review->appendChild($review_useBestPaper);

  $value = $newConfig->createTextNode(Tools::UTF8readPost('review_useBestPaper'));
  $review_useBestPaper->appendChild($value);
  
  $status .= Tools::checkMinMaxGrades(trim(Tools::readPost('review_bestPaperMin')), trim(Tools::readPost('review_bestPaperMax')), "Best Paper");

  $review_bestPaperSemantics = $newConfig->createElement('bestPaperSemantics');
  $review_bestPaperSemantics = $review->appendChild($review_bestPaperSemantics);

  $value = $newConfig->createTextNode(trim(Tools::UTF8readPost('review_bestPaperSemantics')));
  $review_bestPaperSemantics->appendChild($value);

  $status .= Tools::checkGradeSematics(trim(Tools::readPost('review_bestPaperSemantics')), "Best Paper");

  $review_useToProgramCommittee = $newConfig->createElement('useToProgramCommittee');
  $review_useToProgramCommittee = $review->appendChild($review_useToProgramCommittee);

  $value = $newConfig->createTextNode(Tools::UTF8readPost('review_useToProgramCommittee'));
  $review_useToProgramCommittee->appendChild($value);

  $review_useToAuthors = $newConfig->createElement('useToAuthors');
  $review_useToAuthors = $review->appendChild($review_useToAuthors);

  $value = $newConfig->createTextNode(Tools::UTF8readPost('review_useToAuthors'));
  $review_useToAuthors->appendChild($value);

  /* miscellaneous section */

  $miscellaneous = $newConfig->createElement('miscellaneous');
  $miscellaneous = $xml->appendChild($miscellaneous);
  
  $miscellaneous_notificationDate = $newConfig->createElement('notificationDate');
  $miscellaneous_notificationDate = $miscellaneous->appendChild($miscellaneous_notificationDate);

  if(!preg_match("/^[0-3][0-9]\/[0-1][0-9]\/[0-9][0-9][0-9][0-9]$/",trim(Tools::readPost('miscellaneous_notificationDate')))) {
    $status .= trim(Tools::readPost('miscellaneous_notificationDate')) . " là ngày thông báo không hợp lệ. Vui lòng nhập ngày hợp lệ (dd/mm/yyy)."
    //is not a valid notification date. Please enter a valid one (dd/mm/yyyy).
    ."\n";
  } 
  $value = $newConfig->createTextNode(trim(Tools::UTF8readPost('miscellaneous_notificationDate')));
  $miscellaneous_notificationDate->appendChild($value);

  $miscellaneous_finalVersionDate = $newConfig->createElement('finalVersionDate');
  $miscellaneous_finalVersionDate = $miscellaneous->appendChild($miscellaneous_finalVersionDate);

  if(!preg_match("/^[0-3][0-9]\/[0-1][0-9]\/[0-9][0-9][0-9][0-9]$/",trim(Tools::readPost('miscellaneous_finalVersionDate')))) {
    $status .= trim(Tools::readPost('miscellaneous_finalVersionDate')) . " là ngày phiên bản cuối cùng không hợp lệ. Vui lòng nhập ngày hợp lệ (dd/mm/yyy)."
    //is not a valid final version date. Please enter a valid one (dd/mm/yyyy).
    ."\n";
  } 
  $value = $newConfig->createTextNode(trim(Tools::UTF8readPost('miscellaneous_finalVersionDate')));
  $miscellaneous_finalVersionDate->appendChild($value);

  $miscellaneous_visitTimeout = $newConfig->createElement('visitTimeout');
  $miscellaneous_visitTimeout = $miscellaneous->appendChild($miscellaneous_visitTimeout);

  if(!preg_match("/^[0-9]+$/",trim(Tools::readPost('miscellaneous_visitTimeout')))) {
    $status .= trim(Tools::readPost('miscellaneous_visitTimeout')) . " là ngày hết hạn chọn bài viết ưa thích không hợp lệ. Vui lòng nhập ngày hợp lệ (dd/mm/yyy)."
    //is not a valid visit timeout. Please enter a valid one (any number).
    ."\n";
  }
  $value = $newConfig->createTextNode(trim(Tools::UTF8readPost('miscellaneous_visitTimeout')));
  $miscellaneous_visitTimeout->appendChild($value);

  $miscellaneous_submissionSkin = $newConfig->createElement('submissionSkin');
  $miscellaneous_submissionSkin = $miscellaneous->appendChild($miscellaneous_submissionSkin);   

  $value = $newConfig->createTextNode(trim(Tools::UTF8readPost('miscellaneous_submissionSkin')));
  $miscellaneous_submissionSkin->appendChild($value);

  $miscellaneous_reviewSkin = $newConfig->createElement('reviewSkin');
  $miscellaneous_reviewSkin = $miscellaneous->appendChild($miscellaneous_reviewSkin);   

  $value = $newConfig->createTextNode(trim(Tools::UTF8readPost('miscellaneous_reviewSkin')));
  $miscellaneous_reviewSkin->appendChild($value);

  $miscellaneous_adminSkin = $newConfig->createElement('adminSkin');
  $miscellaneous_adminSkin = $miscellaneous->appendChild($miscellaneous_adminSkin);   

  $value = $newConfig->createTextNode(trim(Tools::UTF8readPost('miscellaneous_adminSkin')));
  $miscellaneous_adminSkin->appendChild($value);




  /* Testing the status -> if empty everything is alright */

  if($status == "") {
    if(file_exists("../utils/config.xml")) {
      rename("../utils/config.xml", "../utils/config.xml.bak");
    }
    if($newConfig->save("../utils/config.xml")) {
      Tools::updateConfig();
      Log::logConfiguration();

      /* update .htaccess file with the submission max size*/
      $size = Tools::getConfig('server/submissionMaxSize');
      if(!file_exists("../.htaccess")) {
	if($size != "") {
	  $string = "php_value upload_max_filesize " . $size . "M\n";
	  $string .= "php_value post_max_size " . ($size+3) . "M\n";
	  file_put_contents("../.htaccess", $string);
	}
      } else {
	$string = file_get_contents("../.htaccess");
	if(preg_match("/upload_max_filesize/", $string)) {
	  $string = preg_replace("/upload_max_filesize.*/", "upload_max_filesize " . $size . "M", $string);
	} else {
	  $string .= "\nphp_value upload_max_filesize " . $size . "M";
	}
	if(preg_match("/post_max_size/", $string)) {
	  $string = preg_replace("/post_max_size.*/", "post_max_size " . ($size+3) . "M", $string);
	} else {
	  $string .= "\nphp_value post_max_size " . ($size+3) . "M";
	}
	file_put_contents("../.htaccess", $string);
      }


      include 'header.php';
      print('<div class="OKmessage">'
.'Cập nhật cấu hình thành công.'.
//The configuration file was updated successfully.
'</div>');
      if($warning != "") {
	print('<div class="ERRmessage">Cảnh báo:'
    //Warning:
    .'<br />');
	Tools::printHTMLbr($warning);
	print('</div>');
      }
    } else {
      include 'header.php';
      if(file_exists("../utils/config.xml.bak")) {
        rename("../utils/config.xml.bak", "../utils/config.xml");
      }
      print('<div class="ERRmessage"> Lỗi viết cấu hình.'
        //There was an error writing the configuration file.
        .'</div>');
      $styleSheet = new DOMDocument();
      $styleSheet->load("../utils/config.xsl");
      
      $xsl = new XSLTProcessor();
      $xsl->importStyleSheet($styleSheet);
      
      echo $xsl->transformToXML($newConfig);
      return;
    }
  } else {
    include 'header.php';
    print('<div class="ERRmessage">');
    Tools::printHTMLbr($status . "\nCấu hình không cập nhật được."
      //The configuration file was NOT updated.
      ."\n");
    print("</div>");
    $styleSheet = new DOMDocument();
    $styleSheet->load("../utils/config.xsl");
    
    $xsl = new XSLTProcessor();
    $xsl->importStyleSheet($styleSheet);
  
    echo $xsl->transformToXML($newConfig);
    return;
  }
} else {
  include 'header.php';
}


/***************************************************************************/
/* Display the config.xml file in an html format using the config.xsl file */
/***************************************************************************/

$config = new DOMDocument();
/* The .xml is always supposed to be valid, as, at each update, we make sure that the */
/* updated .xml file is valid.                                                        */
/* If no valid config.xml exists, load defaults and display a message */
if(file_exists("../utils/config.xml")) {
    $config->load("../utils/config.xml");
} else {
    $config->load("../utils/default_config.xml");
    print('<div class="OKmessage">It seems that this is the first time you configure iChair '.
	  'for your conference. Please note that no change will actually be saved until you '.
	  'click on the <em>Update Configuration File</em> at the bottom of this page. Also '.
	  'note that all your changes (paths, times &amp; dates, etc.) must be valid.</div>');    
}
$config->encoding = "utf-8";

$styleSheet = new DOMDocument();
$styleSheet->load("../utils/config.xsl");

$xsl = new XSLTProcessor();
$xsl->importStyleSheet($styleSheet);

print($xsl->transformToXML($config));

?>
</body>
</html>
