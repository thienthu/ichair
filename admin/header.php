<?php 

/*
 * Copyright (C) 2006, 2007 Thomas Baigneres, Matthieu Finiasz
 *
 * This file is part of iChair.
 *
 * iChair is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 * 
 * iChair is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

?><?php 
header("Content-type: text/html; charset=utf-8");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
  <title><?php Tools::printHTML(Tools::getConfig("conference/name")." Quản Trị - ".$page_title); ?></title>
  <link href="../css/admin_<?php print(Tools::getConfig("miscellaneous/adminSkin"));?>.css" rel="stylesheet" type="text/css" media="screen" />
  <link href="../css/print_<?php print(Tools::getConfig("miscellaneous/adminSkin"));?>.css" rel="stylesheet" type="text/css" media="print" />
  <link rel="SHORTCUT ICON" type="image/png" href="../images/icon_admin.png" />
  <script type="text/javascript" src="../js/jquery.min.js"></script>
  <script type="text/javascript" src="../js/authors.js"></script>  
</head>

<body>

<div class="navigate">

  <div class="navigateTitle"><a href="../index.php"><?php Tools::printHTML(Tools::getConfig("conference/name"));?></a></div>
    <div class="navigateLinks">
      <p><a href="index.php">Trang chủ<!--Main Page--></a></p>
      <p><a href="admin_password.php">Mật khẩu<!--Admin Password--></a></p>
      <p><a href="configuration.php">Cấu hình<!--Configuration--></a></p>
      <p><a href="file_uploads.php">Tải tập tin lên<!--Upload Files--></a></p>
      <p><a href="manage_users.php">Quản lý người dùng<!--Manage Users--></a></p>
      <p><a href="logs.php">Logs</a></p>
      <p><a href="rescue_emails.php">E-mails đã gửi<!--Rescue E-mails--></a></p>
      <p class="topLine"><a href="submit.php">Gửi bài khẩn cấp<!--Emergency Submission--></a></p>
      <p><a href="revise.php">Sửa bài khẩn cấp<!--Emergency Revision--></a></p>
      <p><a href="change_contact_email.php">Thay đổi<!--Change Contact--> e-mail</a></p>
      <p class="topLine"><a href="about.php">Giới thiệu<!--About--> iChair</a></p>
    </div>

    <div class="navigateTitle">
      <a href="http://www.time.gov/timezone.cgi?UTC/s/0/java" target="_new">Thời gian hiện tại<!--Current Time--></a>
    </div>
    <div class="navigateTimes">
      <div class="timeTitleTop">Thời gian GMT+7<!--UTC Time--></div>
      <?php if(Tools::use12HourFormat()) { 
       date_default_timezone_set('asia/saigon');?>
        <div class="time"><?php print(date("j M y - h:i&#160;a")); ?></div>
      <?php } else {?>
        <div class="time"><?php print(date("j M y - H:i")); ?></div>      
      <?php } ?>
    </div>

    <div class="navigateTitle">
      Thời gian tắt hệ thống<!--Server Shutdown-->
    </div>
    <div class="navigateTimes">
      <div class="timeTitleTop">Thời gian GMT+7<!--UTC Time--></div>
      <div class="time">
        <?php 
          $U = Tools::getUFromDateAndTime(Tools::getConfig("server/shutdownDate"), Tools::getConfig("server/shutdownTime"));
	  if(Tools::use12HourFormat()) {
	    print(date("j M y - h:i&#160;a", $U)); 
	  } else {
	    print(date("j M y - H:i", $U)); 
	  }
        ?>
      </div>
      <div class="timeTitle">Thời gian còn lại<!--Time left--></div>
      <div class="time">
      <?php 
      $timeLeft = $U - date("U");
      if($timeLeft > 0) {
        print(Tools::stringTimeLeft($timeLeft));
      } else {
        print("<div class=\"timeUp\">Thời gian đã hết!"//Time is up!
          ."</div>");
      }
      ?>
      </div>
    </div>

  </div>

<h1><?php Tools::printHTML(Tools::getConfig("conference/name")." Quản Trị - "//Admin - "
.$page_title); ?></h1>
