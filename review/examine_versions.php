<?php 

/*
 * Copyright (C) 2006, 2007 Thomas Baigneres, Matthieu Finiasz
 *
 * This file is part of iChair.
 *
 * iChair is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 * 
 * iChair is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

?><?php $page_title='Xem Phiên Bản';//Examine Versions';
include '../utils/tools.php';
include 'header.php';

if($currentReviewerGroup == Reviewer::$CHAIR_GROUP) {

$submissionNumber = Tools::readPost('submissionNumber');
$submission = Submission::getBySubmissionNumber($submissionNumber);
$versions = $submission->getAllVersions();

print('<div class="paperBox"><div class="paperBoxDetails">');

/* Display informations common to all versions */
print("<div class=\"versionTitle\">Phiên Bản Của Bài Gửi "//Versions of Submission 
  ."$submissionNumber</div>\n");
$submission->printInfo();
print("</div></div>");

/* Display each versions' information */

for ($i = count($versions); $i>0; $i--){
  print("<div class=\"paperBox\"><div class=\"paperBoxTitle\">");
  if ($versions[$i]->getDate() >= $currentReviewer->getLastVisitDate()) {
    print('<div class="paperBoxNumber"><div class="maybeRejectArticle">Phiên bản '//Version '
      .$i.'</div></div>');
  } else {
    print('<div class="paperBoxNumber">Phiên bản ' .  $i . '</div>');
  }

  $versions[$i]->printDownloadLink();
  $versions[$i]->printEditChairCommentsLink('versions');

  $versions[$i]->printShort();
  print("</div>\n");
  if ($submission->getIsWithdrawn())
    print("<div class='paperBoxDetailsWithdrawn'>");
  else
    print("<div class='paperBoxDetails'>");
  if (Tools::useCountry()) {
    Tools::printContinentDivs($versions[$i]->getCountryArray());
  } else {
    print('<div><div><div>');
  }  
  $versions[$i]->printLongBr();
  print("Ban tổ chức bình luận:"//Chair comments:
    ."<br />\n");
  $versions[$i]->printChairComments();
  print("</div></div></div></div></div>\n");
}

?>
<form action="examine.php#<?php print("nbr" . $submissionNumber);?>" method="post">
<div class="floatRight">
<input type="submit" class="buttonLink bigButton" value="Quay lại bài gửi" />
</div>
</form>

<?php 
}
?>

<?php include('footer.php'); ?>
