<?php 

/*
 * Copyright (C) 2006, 2007 Thomas Baigneres, Matthieu Finiasz
 *
 * This file is part of iChair.
 *
 * iChair is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 * 
 * iChair is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

?><?php 

class Log {

  static $SUBMISSION_CATEGORY = 0;
  static $ADMIN_CATEGORY = 1;
  static $REVIEW_CATEGORY = 2;
  
  
  static $SUBMISSION_EVENT = 0;
  static $REVISION_EVENT = 1;
  static $WITHDRAWAL_EVENT = 2;


  static $CONFIGURATION_EVENT = 100;
  static $ADMIN_PASSWORD_EVENT = 101;

  static $ADD_USER_EVENT = 103;
  static $EDIT_USER_EVENT = 104;
  static $DELETE_USER_EVENT = 105;
  static $GENERATE_PASSWORD_EVENT = 106;

  static $ADMIN_SUBMISSION_EVENT = 108;
  static $ADMIN_REVISION_EVENT = 109;
  static $ADMIN_CONTACT_EVENT = 110;

  static $MAIL_ERROR_EVENT = 150;
  
  static $DELETE_SUBMISSION_EVENT = 200;
  static $WITHDRAW_SUBMISSION_EVENT = 201;
  static $UNWITHDRAW_SUBMISSION_EVENT = 202;
  static $EDIT_SUBMISSION_COMMENTS_EVENT = 203;
  static $DOWNLOAD_SUBMISSION_EVENT = 204;
  static $SUBMISSION_TO_ARTICLE_EVENT = 205;
  static $DELETE_ARTICLE_EVENT = 206;

  static $ASSIGNEMENT_UPDATE_EVENT = 210;
  static $AUTOMATIC_SUGGESTION_EVENT = 211;
  static $AUTOMATIC_ASSIGNEMENT_EVENT = 212;
  static $AUTOMATIC_DELETION_EVENT = 213;

  static $MASS_MAIL_EVENT = 220;

  static $GENERAL_MESSAGE_EVENT = 230;
  static $SPECIFIC_MESSAGE_EVENT = 231;

  static $ACCESS_INDEX_EVENT = 250;
  static $EDIT_PROFILE_EVENT = 251;
  static $PASSWORD_EVENT = 252;
  static $AFFINITY_UPDATE_EVENT = 253;
  
  static $DOWNLOAD_ARTICLE_EVENT = 255;
  static $DOWNLOAD_XML_EVENT = 256;
  static $UPLOAD_XML_EVENT = 257;
  static $REVIEW_EVENT = 258;
  static $DISCUSS_EVENT = 260;
  static $ACCEPTANCE_EVENT = 261;
  static $RETRANSFER_EVENT = 262;
  

  function __construct() {}

  static function openDB() {
    $dbFile = Tools::getConfig('server/logPath') . "ichair_logs.db";
    if (!file_exists($dbFile)) {
      Log::createDB();
    }
    return new SQLite3($dbFile);
  }

  static function createDB() {
    $db = new SQLite3(Tools::getConfig('server/logPath') . "ichair_logs.db");
    $db->exec("CREATE TABLE logs( category, event, description," .
	                          "articleNumber, reviewerNumber, performer," .
                                  "date, IP)");
  }

  private static function logEvent($event, $description, $articleNumber, $reviewerNumber, $performer){
    $category = Log::$REVIEW_CATEGORY;
    if ($event < 100) {
      $category = Log::$SUBMISSION_CATEGORY;
    } else if ($event < 200) {
      $category = LOG::$ADMIN_CATEGORY;
    }
    
    $date = date("U");
    $IP = $_SERVER['REMOTE_ADDR'];

    $db = Log::openDB();
    $db->exec("INSERT INTO logs VALUES (\""
	       . $category . "\", \""
	       . $event . "\", \""
	       . base64_encode($description) . "\", \""
	       . $articleNumber . "\", \""
	       . $reviewerNumber . "\", \""
	       . base64_encode($performer) . "\", \""
	       . $date . "\", \""
	       . $IP . "\")");
  }

  
  static function logSubmission($submission) {
    Log::logEvent(Log::$SUBMISSION_EVENT, "New submission: \"" . $submission->getLastVersion()->getTitle() . "\"", $submission->getSubmissionNumber(), "", "");
  }
  
  static function logRevision($submission) {
    Log::logEvent(Log::$REVISION_EVENT, "Revision for: \"" . $submission->getLastVersion()->getTitle() . "\"", $submission->getSubmissionNumber(), "", "");
  }

  static function logWithdrawal($submission) {
    Log::logEvent(Log::$WITHDRAWAL_EVENT, "Withdrawal of: \"" . $submission->getLastVersion()->getTitle() . "\"", $submission->getSubmissionNumber(), "", "");
  }
  static function logConfiguration() {
    Log::logEvent(Log::$CONFIGURATION_EVENT, "Cấu hinh máy chủ đã được cập nhât"//"The server configuration was updated"
, "", "", "admin");
  }

  static function logAdminPassword() {
    Log::logEvent(Log::$ADMIN_PASSWORD_EVENT, "Admin password updated", "", "", "admin");
  }

  static function logAddUser($reviewer) {
    Log::logEvent(Log::$ADD_USER_EVENT, "Reviewer \"" . $reviewer->getLogin() . "\" added", "", $reviewer->getReviewerNumber(), "admin");
  }

  static function logEditUser($reviewer) {
    Log::logEvent(Log::$EDIT_USER_EVENT, "Reviewer \"" . $reviewer->getLogin() . "\" updated", "", $reviewer->getReviewerNumber(), "admin");
  }

  static function logDeleteUser($reviewer) {
    Log::logEvent(Log::$DELETE_USER_EVENT, "Reviewer \"" . $reviewer->getLogin() . "\" deleted", "", $reviewer->getReviewerNumber(), "admin");
  }

  static function logGeneratePassword($reviewer) {
    Log::logEvent(Log::$GENERATE_PASSWORD_EVENT, "New password emailed to \"" . $reviewer->getLogin() . "\"", "", $reviewer->getReviewerNumber(), "admin");
  }

  static function logAdminSubmission($submission) {
    Log::logEvent(Log::$ADMIN_SUBMISSION_EVENT, "New submission: \"" . $submission->getLastVersion()->getTitle() . "\"", $submission->getSubmissionNumber(), "", "admin");
  }

  static function logAdminRevision($submission) {
    Log::logEvent(Log::$ADMIN_REVISION_EVENT, "Revision for: \"" . $submission->getLastVersion()->getTitle() . "\"", $submission->getSubmissionNumber(), "", "admin");
  }

  static function logAdminContact($submission, $oldEmail) {
    Log::logEvent(Log::$ADMIN_CONTACT_EVENT, "E-mail changed from \"" . $oldEmail . "\" to \"" . $submission->getContact() . "\"", $submission->getSubmissionNumber(), "", "admin");
  }

  static function logMailError($contact, $subject, $content, $header) {
    Log::logEvent(Log::$MAIL_ERROR_EVENT, "Mail-error: ".base64_encode($subject)." ".base64_encode($content)." ".base64_encode($header), "", "", $contact);
  }

  static function logDeleteSubmission($submission,$title,$currentReviewer) {
    Log::logEvent(Log::$DELETE_SUBMISSION_EVENT, "Deleted: \"" . $title . "\"", $submission->getSubmissionNumber(), $currentReviewer->getReviewerNumber(), $currentReviewer->getLogin());
  }

  static function logWithdrawSubmission($submission,$currentReviewer) {
    Log::logEvent(Log::$WITHDRAW_SUBMISSION_EVENT, "Withdrawn: \"" . $submission->getLastVersion()->getTitle() . "\"", $submission->getSubmissionNumber(), $currentReviewer->getReviewerNumber(), $currentReviewer->getLogin());
  }

  static function logUnwithdrawSubmission($submission,$currentReviewer) {
    Log::logEvent(Log::$UNWITHDRAW_SUBMISSION_EVENT, "Unwithdrawn: \"" . $submission->getLastVersion()->getTitle() . "\"", $submission->getSubmissionNumber(), $currentReviewer->getReviewerNumber(), $currentReviewer->getLogin());
  }

  static function logEditSubmissionComments($submission,$currentReviewer) {
    Log::logEvent(Log::$EDIT_SUBMISSION_COMMENTS_EVENT, "Chair comments edited", $submission->getSubmissionNumber(), $currentReviewer->getReviewerNumber(), $currentReviewer->getLogin());
  }

  static function logDownloadSubmission($submission,$currentReviewer) {
    Log::logEvent(Log::$DOWNLOAD_SUBMISSION_EVENT, "Downloaded submission: \"" . $submission->getLastVersion()->getTitle() . "\"", $submission->getSubmissionNumber(), $currentReviewer->getReviewerNumber(), $currentReviewer->getLogin());
  }
  
  static function logSubmissionToArticle($article,$currentReviewer) {
    Log::logEvent(Log::$SUBMISSION_TO_ARTICLE_EVENT, "Submission transfered: \"" . $article->getTitle() . "\"", $article->getArticleNumber(), $currentReviewer->getReviewerNumber(), $currentReviewer->getLogin());
  }

  static function logDeleteArticle($article, $currentReviewer) {
    Log::logEvent(Log::$DELETE_ARTICLE_EVENT, "Article \"" . $article->getTitle() . "\" deleted", $article->getArticleNumber(), "", $currentReviewer->getLogin());
  }
  
  static function logAssignementReviewer($reviewer, $currentReviewer) {
    Log::logEvent(Log::$ASSIGNEMENT_UPDATE_EVENT, "Reviewer assignements updated" , "", $reviewer->getReviewerNumber(), $currentReviewer->getLogin());
  }

  static function logAssignementArticle($article,$currentReviewer) {
    Log::logEvent(Log::$ASSIGNEMENT_UPDATE_EVENT, "Article assignements updated" , $article->getArticleNumber(), "", $currentReviewer->getLogin());
  }

  static function logAutomaticSuggestion($currentReviewer) {
    Log::logEvent(Log::$AUTOMATIC_SUGGESTION_EVENT, "Automatic suggestion generated" , "", "", $currentReviewer->getLogin());
  }

  static function logAutomaticDeletion($currentReviewer) {
    Log::logEvent(Log::$AUTOMATIC_DELETION_EVENT, "Automatic suggestion deleted" , "", "", $currentReviewer->getLogin());
  }

  static function logAutomaticAssignement($currentReviewer) {
    Log::logEvent(Log::$AUTOMATIC_ASSIGNEMENT_EVENT, "Automatic assignements transfered" , "", "", $currentReviewer->getLogin());
  }

  static function logMassMail($currentReviewer) {
    Log::logEvent(Log::$MASS_MAIL_EVENT, "Mass mail sent", "", "", $currentReviewer->getLogin());
  }

  static function logGeneralMessage($currentReviewer) {
    Log::logEvent(Log::$GENERAL_MESSAGE_EVENT, "New General Message from the Chair", "", "", $currentReviewer->getLogin());
  }

  static function logSpecificMessage($currentReviewer, $articleNumber) {
    Log::logEvent(Log::$SPECIFIC_MESSAGE_EVENT, "New Message from the Chair", $articleNumber, "", $currentReviewer->getLogin());
  }

  static function logAccessIndex($currentReviewer) {
    Log::logEvent(Log::$ACCESS_INDEX_EVENT, "Access to index.php", "", $currentReviewer->getReviewerNumber(), $currentReviewer->getLogin());
  }

  static function logEditProfile($currentReviewer) {
    Log::logEvent(Log::$EDIT_PROFILE_EVENT, "Profile updated", "", $currentReviewer->getReviewerNumber(), $currentReviewer->getLogin());
  }

  static function logPassword($currentReviewer){
    Log::logEvent(Log::$PASSWORD_EVENT, "Password updated", "", $currentReviewer->getReviewerNumber(), $currentReviewer->getLogin());
  }

  static function logAffinityUpdate($currentReviewer) {
    Log::logEvent(Log::$AFFINITY_UPDATE_EVENT, "Affinities updated", "", $currentReviewer->getReviewerNumber(), $currentReviewer->getLogin());
  }

  static function logDownloadArticle($article,$currentReviewer) {
    Log::logEvent(Log::$DOWNLOAD_ARTICLE_EVENT, "Downloaded article: \"" . $article->getTitle() . "\"", $article->getArticleNumber(), $currentReviewer->getReviewerNumber(), $currentReviewer->getLogin());
  }

  static function logDownloadXML($assignement, $currentReviewer) {
    Log::logEvent(Log::$DOWNLOAD_XML_EVENT, "Downloaded XML review", $assignement->getArticleNumber(), $assignement->getReviewerNumber(), $currentReviewer->getLogin());
  }

  static function logUploadXML($assignement, $currentReviewer) {
    Log::logEvent(Log::$DOWNLOAD_XML_EVENT, "Uploaded XML review", $assignement->getArticleNumber(), $assignement->getReviewerNumber(), $currentReviewer->getLogin());
  }

  static function logReview($assignement, $currentReviewer, $logDiff) {
    $bozo = "";
    if($logDiff != "") {
      $bozo .= ": " . $logDiff;
    }
    Log::logEvent(Log::$REVIEW_EVENT, "Review updated" . $bozo, 
		  $assignement->getArticleNumber(), 
		  $assignement->getReviewerNumber(), 
		  $currentReviewer->getLogin());
  }

  static function logDiscussion($articleNumber, $currentReviewer) {
    Log::logEvent(Log::$DISCUSS_EVENT, "New discussion comment", $articleNumber, $currentReviewer->getReviewerNumber(), $currentReviewer->getLogin());
  }

  static function logAcceptanceChange($articleNumber, $acceptance, $currentReviewer) {
    $stat = "nothing";
    if($acceptance == Article::$ACCEPT) {
      $stat = "accept";
    } else if($acceptance == Article::$MAYBE_ACCEPT) {
      $stat = "maybe accept";
    } else if($acceptance == Article::$DISCUSS) {
      $stat = "discuss";
    } else if($acceptance == Article::$MAYBE_REJECT) {
      $stat = "maybe reject";
    } else if($acceptance == Article::$REJECT) {
      $stat = "reject";
    } else if($acceptance == Article::$CUSTOM1) {
      $stat = Tools::getCustomAccept1();
    } else if($acceptance == Article::$CUSTOM2) {
      $stat = Tools::getCustomAccept2();
    } else if($acceptance == Article::$CUSTOM3) {
      $stat = Tools::getCustomAccept3();
    } else if($acceptance == Article::$CUSTOM4) {
      $stat = Tools::getCustomAccept4();
    } else if($acceptance == Article::$CUSTOM5) {
      $stat = Tools::getCustomAccept5();
    } else if($acceptance == Article::$CUSTOM6) {
      $stat = Tools::getCustomAccept6();
    } else if($acceptance == Article::$CUSTOM7) {
      $stat = Tools::getCustomAccept7();
    }
    Log::logEvent(Log::$ACCEPTANCE_EVENT, "Status changed to: ".$stat, $articleNumber, "", $currentReviewer->getLogin());
  }

  static function logRetransfer($articleNumber) {
    Log::logEvent(Log::$RETRANSFER_EVENT, "New version of the article received.", $articleNumber, "", "");
  }




  static function printCategoryLatestEvents($category,$number) {
    $db = Log::openDB();
    $result = $db->query("SELECT * FROM logs WHERE category=\"" . $category . "\" ORDER BY date DESC LIMIT ".$number);
    if ($result->fetchArray()) {
      $result->reset();
      print('<table class="usersTable">'.
'<tr>
    <td>Thời gian</td>
    <td>Loại sự kiện</td>
    <td>Mô tả</td>
    <td>Bài viết</td>
    <td>Người đánh giá</td>
    <td>Người dùng</td>
    <td>Địa chỉ IP</td>
  </tr>'
  /*<tr>
    <td>Time</td>
    <td>Event Type</td>
    <td>Description</td>
    <td>Article</td>
    <td>Reviewer</td>
    <td>Performer</td>
    <td>Originating IP</td>
  </tr>*/);
      while ($db_row = $result->fetchArray()){
        Log::printDBrow($db_row);
      }
      print('</table>');
    }
  }

  static function printDBrow($db_row) {
    print('
  <tr>
    <td>'. date("H:i:s - d/m/Y", $db_row['date']).'</td>
    <td>'. $db_row['event'].'</td>
    <td class="leftAlign">'. Tools::HTMLsubstr(base64_decode($db_row['description']),50) .'</td>
    <td>'. $db_row['articleNumber'].'</td>
    <td>'. $db_row['reviewerNumber'].'</td>
    <td>'. htmlentities(base64_decode($db_row['performer']), ENT_COMPAT | ENT_HTML401, 'UTF-8').'</td>
    <td>'. $db_row['IP'].'</td>
  </tr>
');
  }

  static function printMassMailDBrow($db_row) {
    $login = base64_decode($db_row['performer']);
    $reviewer = Reviewer::getByLogin($login);
    $fullName = $login;
    if (!is_null($reviewer)) {
      $fullName = $reviewer->getFullName();
    }
    if(Tools::use12HourFormat()) {
      print('<tr><td>'. date("h:i:s - d/m/Y", $db_row['date']).'</td><td>&nbsp; by &nbsp;</td><td>'. htmlentities($fullName, ENT_COMPAT | ENT_HTML401, 'UTF-8') .'</td></tr>');
    } else {
      print('<tr><td>'. date("H:i:s - d/m/Y", $db_row['date']).'</td><td>&nbsp; by &nbsp;</td><td>'. htmlentities($fullName, ENT_COMPAT | ENT_HTML401, 'UTF-8') .'</td></tr>');
    }
  }

  static function getLastModificationsByArticleNumber($articleNumber, $numberOfLines) {
    $db = Log::openDB();
    $result = $db->query('SELECT * FROM logs WHERE articleNumber="'. $articleNumber .'" AND (event="'. Log::$DISCUSS_EVENT .'" OR event="'. Log::$REVIEW_EVENT .'" OR event="'. Log::$ACCEPTANCE_EVENT .'" OR event="'. Log::$SPECIFIC_MESSAGE_EVENT .'" OR event="'. Log::$RETRANSFER_EVENT .'") ORDER BY date DESC LIMIT '. $numberOfLines .'');
    return $result;
  }

  static function getNumberOfComments($articleNumber) {
    $db = Log::openDB();
    $result = $db->querySingle('SELECT COUNT(*) FROM logs WHERE articleNumber="'. $articleNumber .'" AND event="'. Log::$DISCUSS_EVENT .'"');
    return $result;
  }

  static function getAllBadEmails() {
    $db = Log::openDB();
    $result = $db->query('SELECT * FROM logs WHERE event="'.Log::$MAIL_ERROR_EVENT.'" ORDER BY date DESC');
    return $result;
  }

  static function getSubmissionsStats($submissions, $offset, $step){
    $db = Log::openDB();
    $result = $db->query('SELECT date FROM logs WHERE event="'.Log::$SUBMISSION_EVENT.'" AND date >= "'.$offset .'" ORDER BY date');
    while ($db_row = $result->fetchArray()) {
      $date = $db_row['date'] - $offset;
      if (!array_key_exists($date - ($date % $step), $submissions)) {
        $submissions[$date - ($date % $step)] = 0;
      }
      $submissions[$date - ($date % $step)]++;
    }
    return $submissions;
  }

  static function getSubmissionsStatsByArticleNumber($submissions, $articleNumber, $offset, $step) {
    $db = Log::openDB();
    $result = $db->query('SELECT date FROM logs WHERE event="'.Log::$SUBMISSION_EVENT.'" AND articleNumber="'.$articleNumber.'"  AND date >= "'.$offset .'" ORDER BY date');
    while ($db_row = $result->fetchArray()) {
      $date = $db_row['date'] - $offset;
      if (!array_key_exists($date - ($date % $step), $submissions)) {
        $submissions[$date - ($date % $step)] = 0;
      }
      $submissions[$date - ($date % $step)]++;
    }
    return $submissions;
  }

  static function getRevisionsStats($revisions, $offset, $step){
    $db = Log::openDB();
    $result = $db->query('SELECT date FROM logs WHERE event="'.Log::$REVISION_EVENT.'"  AND date >= "'.$offset .'" ORDER BY date');
    while ($db_row = $result->fetchArray()) {
      $date = $db_row['date'] - $offset;
      if (!array_key_exists($date - ($date % $step), $revisions)) {
        $revisions[$date - ($date % $step)] = 0;
      }
      $revisions[$date - ($date % $step)]++;
    }
    return $revisions;
  }

  static function getRevisionsStatsByArticleNumber($revisions, $articleNumber, $offset, $step) {
    $db = Log::openDB();
    $result = $db->query('SELECT date FROM logs WHERE event="'.Log::$REVISION_EVENT.'" AND articleNumber="'.$articleNumber.'"  AND date >= "'.$offset .'" ORDER BY date');
    while ($db_row = $result->fetchArray()) {
      $date = $db_row['date'] - $offset;
      if (!array_key_exists($date - ($date % $step), $revisions)) {
        $revisions[$date - ($date % $step)] = 0;
      }
      $revisions[$date - ($date % $step)]++;
    }
    return $revisions;
  }

  static function getWithdrawalStats($withdrawals, $offset, $step){
    $db = Log::openDB();
    $result = $db->query('SELECT date FROM logs WHERE event="'.Log::$WITHDRAWAL_EVENT.'"  AND date >= "'.$offset .'" ORDER BY date');
    while ($db_row = $result->fetchArray()) {
      $date = $db_row['date'] - $offset;
      if (!array_key_exists($date - ($date % $step), $withdrawals)) {
        $withdrawals[$date - ($date % $step)] = 0;
      }
      $withdrawals[$date - ($date % $step)]++;
    }
    return $withdrawals;
  }
  
  static function printSubmissionStatsLegend() {
    print('<div class="floatRight"><table><tr><td style="font-size: 12px">Bài viết đã gửi'//Submissions
      .'</td><td style="background: green; width: 30px;">&nbsp;</td></tr>');
    print('<tr><td style="font-size: 12px">Bài viết đã sửa'//Revisions
      .'</td><td style="background: orange; ">&nbsp;</td></tr>');
    print('<tr><td style="font-size: 12px">Bài viết đã hủy'//Withdrawals
      .'</td><td style="background: red;">&nbsp;</td></tr></table></div>');
  }
  
  static function printSubmissionStatsTable($submissions, $revisions, $withdrawals) {
    if (count($submissions) == 0) {
      print('<em>No submissions yet.</em>');
      return null;
    }
    if (count($revisions) == 0) {
      $revisions[key($submissions)] = 0;
    }
    if (count($withdrawals) == 0) {
      $withdrawals[key($submissions)] = 0;
    }
    $beg = min(key($submissions), key($revisions), key($withdrawals));
    end($submissions);
    end($revisions);
    end($withdrawals);
    $end = max(key($submissions), key($revisions), key($withdrawals));
    $max = 0;
    for ($i = $beg; $i <= $end; $i += 86400){
      if (!array_key_exists($i, $submissions)) { $submissions[$i] = 0; }
      if (!array_key_exists($i, $revisions)) { $revisions[$i] = 0; }
      if (!array_key_exists($i, $withdrawals)) { $withdrawals[$i] = 0; }
      $max = max($max, $submissions[$i], $revisions[$i], $withdrawals[$i]);
    }

    print('<table style="font-size: 0;border-collapse: separate;"><tr><td></td>');
    for ($j = 0; $j<$max ; $j++) {
      print('<td style="width:15px">&nbsp;</td>');
    }
    print('</tr>');
    for ($i = $beg; $i <= $end; $i += 86400){
      if ($submissions[$i] == "") {
      print('<tr title="0 submission"><td rowspan="3" style="font-size: 12px">'.str_replace(" ", "&nbsp;", date("d/m/Y",$i)).'</td>');
	print('<td colspan="'.$max.'" style="padding: 3px 0;">&nbsp;</td></tr>');
      } else if ($submissions[$i] == $max) {
	print('<tr title="'.$submissions[$i].' submission'.(($submissions[$i] != 1)?'s':'').'"><td rowspan="3" style="font-size: 12px">'.str_replace(" ", "&nbsp;", date("d/m/Y",$i)).'</td>');
	print('<td colspan="'.$max.'" style="background: green; padding: 3px 0;">&nbsp;</td></tr>');
      } else {
	print('<tr title="'.$submissions[$i].' submission'.(($submissions[$i] != 1)?'s':'').'"><td rowspan="3" style="font-size: 12px">'.str_replace(" ", "&nbsp;", date("d/m/Y",$i)).'</td>');
	print('<td colspan="'.$submissions[$i].'" style="background: green; padding: 3px 0;">&nbsp;</td>');
	print('<td colspan="'.($max - $submissions[$i]).'" style="padding: 3px 0;">&nbsp;</td></tr>');
      }
      if ($revisions[$i] == "") {
	print('<tr title="0 revision"><td colspan="'.$max.'" style="padding: 3px 0;">&nbsp;</td></tr>');
      } else if ($revisions[$i] == $max) {
	print('<tr title="'.$revisions[$i].' revision'.(($revisions[$i] != 1)?'s':'').'"><td colspan="'.$max.'" style="background: orange; padding: 3px 0;">&nbsp;</td></tr>');
      } else {
	print('<tr title="'.$revisions[$i].' revision'.(($revisions[$i] != 1)?'s':'').'"><td colspan="'.$revisions[$i].'" style="background: orange; padding: 3px 0;">&nbsp;</td>');
	print('<td colspan="'.($max - $revisions[$i]).'" style="padding: 3px 0;">&nbsp;</td></tr>');
      }
      if ($withdrawals[$i] == "") {
	print('<tr title="0 withdrawal"><td colspan="'.$max.'" style="padding: 3px 0">&nbsp;</td></tr>');
      } else if ($withdrawals[$i] == $max) {
	print('<tr title="'.$withdrawals[$i].' withdrawal'.(($withdrawals[$i] != 1)?'s':'').'"><td colspan="'.$max.'" style="background: red; padding: 3px 0;">&nbsp;</td></tr>');
      } else {
	print('<tr title="'.$withdrawals[$i].' withdrawal'.(($withdrawals[$i] != 1)?'s':'').'"><td colspan="'.$withdrawals[$i].'" style="background: red; padding: 3px 0;">&nbsp;</td>');
	print('<td colspan="'.($max - $withdrawals[$i]).'" style="padding: 3px 0;">&nbsp;</td></tr>');
      }
      print('<tr><td colspan="'.$max.'"></td></tr>');
    }
    print('</table>'); 
  }

  static function printSubmissionStatsTableLastDay($submissions, $revisions, $withdrawals) {
    if ((count($submissions) == 0) && (count($revisions) == 0) && (count($withdrawals) == 0)) {
      return;
    }
    if (count($submissions) == 0) {
      $submissions[0] = 0;
    }
    if (count($revisions) == 0) {
      $revisions[0] = 0;
    }
    if (count($withdrawals) == 0) {
      $withdrawals[0] = 0;
    }
    end($submissions);
    end($revisions);
    end($withdrawals);
    $end = max(key($submissions), key($revisions), key($withdrawals));
    $max = 0;
    for ($i = 0; $i <= $end; $i += 3600){
      if (!array_key_exists($i, $submissions)) { $submissions[$i] = 0; }
      if (!array_key_exists($i, $revisions)) { $revisions[$i] = 0; }
      if (!array_key_exists($i, $withdrawals)) { $withdrawals[$i] = 0; }
      $max = max($max, $submissions[$i], $revisions[$i], $withdrawals[$i]);
    }

    print('<table style="font-size: 0;border-collapse: separate;"><tr><td></td>');
    for ($j = 0; $j<$max ; $j++) {
      print('<td style="width:15px">&nbsp;</td>');
    }
    print('</tr>');
    for ($i = 0; $i <= $end; $i += 3600){
      if ($submissions[$i] == "") {
	print('<tr title="0 submission"><td rowspan="3" style="font-size: 12px; padding: 0 5px; border: 1px dashed #006; text-align: center;">H'. (((-23 + ($i/3600)) > 0) ? '+'.(-23 + ($i/3600)) : (-24 + ($i/3600))).'</td>');
	print('<td colspan="'.$max.'" style="padding: 3px 0;">&nbsp;</td></tr>');
      } else if ($submissions[$i] == $max) {
	print('<tr title="'.$submissions[$i].' submission'.(($submissions[$i] != 1)?'s':'').'"><td rowspan="3" style="font-size: 12px; padding: 0 5px; border: 1px dashed #006; text-align: center;">H'. (((-23 + ($i/3600)) > 0) ? '+'.(-23 + ($i/3600)) : (-24 + ($i/3600))).'</td>');
	print('<td colspan="'.$max.'" style="background: green; padding: 3px 0;">&nbsp;</td></tr>');
      } else {
	print('<tr title="'.$submissions[$i].' submission'.(($submissions[$i] != 1)?'s':'').'"><td rowspan="3" style="font-size: 12px; padding: 0 5px; border: 1px dashed #006; text-align: center;">H'. (((-23 + ($i/3600)) > 0) ? '+'.(-23 + ($i/3600)) : (-24 + ($i/3600))).'</td>');
	print('<td colspan="'.$submissions[$i].'" style="background: green; padding: 3px 0;">&nbsp;</td>');
	print('<td colspan="'.($max - $submissions[$i]).'" style="padding: 3px 0;">&nbsp;</td></tr>');
      }
      if ($revisions[$i] == "") {
	print('<tr title="0 revision">');
	print('<td colspan="'.$max.'" style="padding: 3px 0;">&nbsp;</td></tr>');
      } else if ($revisions[$i] == $max) {
	print('<tr title="'.$revisions[$i].' revision'.(($revisions[$i] != 1)?'s':'').'">');
	print('<td colspan="'.$max.'" style="background: orange; padding: 3px 0;">&nbsp;</td></tr>');
      } else {
	print('<tr title="'.$revisions[$i].' revision'.(($revisions[$i] != 1)?'s':'').'">');
	print('<td colspan="'.$revisions[$i].'" style="background: orange; padding: 3px 0;">&nbsp;</td>');
	print('<td colspan="'.($max - $revisions[$i]).'" style="padding: 3px 0;">&nbsp;</td></tr>');
      }
      if ($withdrawals[$i] == "") {
	print('<tr title="0 withdrawal">');
	print('<td colspan="'.$max.'" style="padding: 3px 0">&nbsp;</td></tr>');
      } else if ($withdrawals[$i] == $max) {
	print('<tr title="'.$withdrawals[$i].' withdrawal'.(($withdrawals[$i] != 1)?'s':'').'">');
	print('<td colspan="'.$max.'" style="background: red; padding: 3px 0;">&nbsp;</td></tr>');
      } else {
	print('<tr title="'.$withdrawals[$i].' withdrawal'.(($withdrawals[$i] != 1)?'s':'').'">');
	print('<td colspan="'.$withdrawals[$i].'" style="background: red; padding: 3px 0;">&nbsp;</td>');
	print('<td colspan="'.($max - $withdrawals[$i]).'" style="padding: 3px 0;">&nbsp;</td></tr>');
      }
      print('<tr><td colspan="'.$max.'"></td></tr>');
    }
    print('</table>'); 
  }

  static function printSubmissionStatsTable2LastDay($submissions, $revisions) {
    if ((count($submissions) == 0) && (count($revisions) == 0)) {
      return;
    }
    if (count($submissions) == 0) {
      $submissions[0] = 0;
    }
    if (count($revisions) == 0) {
      $revisions[0] = 0;
    }
    end($submissions);
    end($revisions);
    $end = max(key($submissions), key($revisions));
    $max = 0;
    for ($i = 0; $i <= $end; $i += 3600){
      if (!array_key_exists($i, $submissions)) { $submissions[$i] = 0; }
      if (!array_key_exists($i, $revisions)) { $revisions[$i] = 0; }
      $max = max($max, $submissions[$i], $revisions[$i]);
    }

    print('<table style="font-size: 0;border-collapse: separate"><tr><td></td>');
    for ($j = 0; $j<$max ; $j++) {
      print('<td style="width:15px">&nbsp;</td>');
    }
    print('</tr>');
    for ($i = 0; $i <= $end; $i += 3600){
      if ($submissions[$i] == "") {
	print('<tr title="0 submission"><td rowspan="2" style="font-size: 12px; padding: 0 5px; border: 1px dashed #006; text-align: center;">H'. (((-23 + ($i/3600)) > 0) ? '+'.(-23 + ($i/3600)) : (-24 + ($i/3600))).'</td>');
	print('<td colspan="'.$max.'" style="padding: 3px 0;">&nbsp;</td></tr>');
      } else if ($submissions[$i] == $max) {
	print('<tr title="'.$submissions[$i].' submission'.(($submissions[$i] != 1)?'s':'').'"><td rowspan="2" style="font-size: 12px; padding: 0 5px; border: 1px dashed #006; text-align: center;">H'. (((-23 + ($i/3600)) > 0) ? '+'.(-23 + ($i/3600)) : (-24 + ($i/3600))).'</td>');
	print('<td colspan="'.$max.'" style="background: green; padding: 3px 0;">&nbsp;</td></tr>');
      } else {
	print('<tr title="'.$submissions[$i].' submission'.(($submissions[$i] != 1)?'s':'').'"><td rowspan="2" style="font-size: 12px; padding: 0 5px; border: 1px dashed #006; text-align: center;">H'. (((-23 + ($i/3600)) > 0) ? '+'.(-23 + ($i/3600)) : (-24 + ($i/3600))).'</td>');
	print('<td colspan="'.$submissions[$i].'" style="background: green; padding: 3px 0;">&nbsp;</td>');
	print('<td colspan="'.($max - $submissions[$i]).'" style="padding: 3px 0;">&nbsp;</td></tr>');
      }
      if ($revisions[$i] == "") {
	print('<tr title="0 revision">');
	print('<td colspan="'.$max.'" style="padding: 3px 0;">&nbsp;</td></tr>');
      } else if ($revisions[$i] == $max) {
	print('<tr title="'.$revisions[$i].' revision'.(($revisions[$i] != 1)?'s':'').'">');
	print('<td colspan="'.$max.'" style="background: orange; padding: 3px 0;">&nbsp;</td></tr>');
      } else {
	print('<tr title="'.$revisions[$i].' revision'.(($revisions[$i] != 1)?'s':'').'">');
	print('<td colspan="'.$revisions[$i].'" style="background: orange; padding: 3px 0;">&nbsp;</td>');
	print('<td colspan="'.($max - $revisions[$i]).'" style="padding: 3px 0;">&nbsp;</td></tr>');
      }
      print('<tr><td colspan="'.$max.'"></td></tr>');
    }
    print('</table>'); 
  }
  

  static function printSubmissionStatsTable2($submissions, $revisions) {
    if (count($submissions) == 0) {
      print('<em>Không có.'//None yet.
        .'</em>');
      return null;
    }
    if (count($revisions) == 0) {
      $revisions[key($submissions)] = 0;
    }
    $beg = min(key($submissions), key($revisions));
    end($submissions);
    end($revisions);
    $end = max(key($submissions), key($revisions));
    $max = 0;
    for ($i = $beg; $i <= $end; $i += 86400){
      if (!array_key_exists($i, $submissions)) { $submissions[$i] = 0; }
      if (!array_key_exists($i, $revisions)) { $revisions[$i] = 0; }
      $max = max($max, $submissions[$i], $revisions[$i]);
    }

    print('<table style="font-size: 0;border-collapse: separate"><tr><td></td>');
    for ($j = 0; $j<$max ; $j++) {
      print('<td style="width:15px">&nbsp;</td>');
    }
    print('</tr>');
    for ($i = $beg; $i <= $end; $i += 86400){
      if ($submissions[$i] == "") {
      print('<tr title="0 submission"><td rowspan="2" style="font-size: 12px">'.str_replace(" ", "&nbsp;", date("d/m/Y",$i)).'</td>');
	print('<td colspan="'.$max.'" style="padding: 3px 0;">&nbsp;</td></tr>');
      } else if ($submissions[$i] == $max) {
	print('<tr title="'.$submissions[$i].' submission'.(($submissions[$i] != 1)?'s':'').'"><td rowspan="2" style="font-size: 12px">'.str_replace(" ", "&nbsp;", date("d/m/Y",$i)).'</td>');
	print('<td colspan="'.$max.'" style="background: green; padding: 3px 0;">&nbsp;</td></tr>');
      } else {
	print('<tr title="'.$submissions[$i].' submission'.(($submissions[$i] != 1)?'s':'').'"><td rowspan="2" style="font-size: 12px">'.str_replace(" ", "&nbsp;", date("d/m/Y",$i)).'</td>');
	print('<td colspan="'.$submissions[$i].'" style="background: green; padding: 3px 0;">&nbsp;</td>');
	print('<td colspan="'.($max - $submissions[$i]).'" style="padding: 3px 0;">&nbsp;</td></tr>');
      }
      if ($revisions[$i] == "") {
	print('<tr title="0 revision"><td colspan="'.$max.'" style="padding: 3px 0;">&nbsp;</td></tr>');
      } else if ($revisions[$i] == $max) {
	print('<tr title="'.$revisions[$i].' revision'.(($revisions[$i] != 1)?'s':'').'"><td colspan="'.$max.'" style="background: orange; padding: 3px 0;">&nbsp;</td></tr>');
      } else {
	print('<tr title="'.$revisions[$i].' revision'.(($revisions[$i] != 1)?'s':'').'"><td colspan="'.$revisions[$i].'" style="background: orange; padding: 3px 0;">&nbsp;</td>');
	print('<td colspan="'.($max - $revisions[$i]).'" style="padding: 3px 0;">&nbsp;</td></tr>');
      }
      print('<tr><td colspan="'.$max.'"></td></tr>');
    }
    print('</table>'); 
  }
  
  static function printReviewersStats() {
    $db = Log::openDB();
    $reviewers = Reviewer::getAllReviewers();
    print('<table class="dottedTable"><tr><th class="topRow">Người đánh giá'//Reviewer
      .'</th><th class="topRow">Lần đầu'//First
      .'<br/>kết nối'//Connection
      .'</th><th class="topRow">Lần cuối'//Last
      .'<br/>kết nối'//Connection
      .'</th>');
    if ((Tools::getCurrentPhase() == Tools::$ELECTION) || (Tools::getCurrentPhase() == Tools::$ASSIGNATION)) {
      print('<th class="topRow">Bài viết ưa thích'//Preferred Articles
        .'<br/>đã chọn'//Selected
        .'</th>');
    }
    if ((Tools::getCurrentPhase() == Tools::$REVIEW) || (Tools::getCurrentPhase() == Tools::$DECISION)) {
      print('<th class="topRow">Discussion<br/>Bình luận'//Comments
        .'</th>');
    }
    print('</tr>');
    foreach ($reviewers as $reviewer) {
      $reviewerNumber = $reviewer ->getReviewerNumber();
      print('<tr><td>'.htmlentities($reviewer->getFullName(), ENT_COMPAT | ENT_HTML401, 'UTF-8').'</td>');
      $result = $db->querySingle('SELECT date FROM logs WHERE reviewerNumber="'.$reviewerNumber.'" AND event="'.Log::$ACCESS_INDEX_EVENT.'" ORDER BY date LIMIT 1');
      if (!is_null($result)) {
	if(Tools::use12HourFormat()) {
	  print('<td>'.date("h:i:s - d/m/Y", $result).'</td>');
	} else {
	  print('<td>'.date("H:i:s - d/m/Y", $result).'</td>');
	}
	$result = $db->querySingle('SELECT date FROM logs WHERE reviewerNumber="'.$reviewerNumber.'" AND event="'.Log::$ACCESS_INDEX_EVENT.'" ORDER BY date DESC LIMIT 1');
	if(Tools::use12HourFormat()) {
	  print('<td>'.date("h:i:s - d/m/Y", $result).'</td>');
	} else {
	  print('<td>'.date("H:i:s - d/m/Y", $result).'</td>');
	}
      } else {
	print('<td><em>Chưa từng kết nối.'//Never connected.
    .'</em></td><td>-</td>');
      }
      if ((Tools::getCurrentPhase() == Tools::$ELECTION) || (Tools::getCurrentPhase() == Tools::$ASSIGNATION)) {
        if ($reviewer->getGroup() == Reviewer::$OBSERVER_GROUP) {
          print('<td>-</td></tr>');
        } else {
          $result = $db->query('SELECT date FROM logs WHERE reviewerNumber="'.$reviewerNumber.'" AND event="'.Log::$AFFINITY_UPDATE_EVENT.'"');
          if ($result->fetchArray()) {
	    print('<td><div class="completedReview">Có'//Yes
        .'</div></td>');
          } else {
	    print('<td><div class="voidReview">Không'//No
        .'</div></td>');
          }
        }
      }
      if ((Tools::getCurrentPhase() == Tools::$REVIEW) || (Tools::getCurrentPhase() == Tools::$DECISION)) {
        if ($reviewer->getGroup() == Reviewer::$OBSERVER_GROUP) {
	  print('<td>-</td></tr>');
        } else {
	  $result = $db->querySingle('SELECT COUNT(*) FROM logs WHERE reviewerNumber="'.$reviewerNumber.'" AND event="'.Log::$DISCUSS_EVENT.'"');
	  print('<td>'.$result.'</td></tr>');
        }
      }
    }
    print('</table>');
  }

}

?>
