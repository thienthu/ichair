<?php 

/*
 * Copyright (C) 2006, 2007 Thomas Baigneres, Matthieu Finiasz
 *
 * This file is part of iChair.
 *
 * iChair is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 * 
 * iChair is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

?><?php $page_title="Gửi Mail";//'Mass Mailing';
include '../utils/tools.php';
include 'header.php';
if($currentReviewerGroup == Reviewer::$CHAIR_GROUP) {
?>

<?php 
  /* Check posted values */

  $reviewerRecipients = array();
  $articleRecipients = array();
  $allReviewers = Reviewer::getAllReviewersSparse();
  $allArticles = Article::getAllArticles();
  $warning = "";
  $uncheck = "";
  if(Tools::readPost('uncheck') != 'yes') {
    $uncheck = ' checked="checked"';
  }

  /* Prepare the list of committee recipients */
  switch(Tools::readPost('to_committee')) {
  case 'all':
    $reviewerRecipients = $allReviewers;
    break;
  case 'list':
    $list = explode(",", Tools::readPost('to_committee_list'));
    foreach($list as $reviewerNumber) {
      if(!is_null($allReviewers[$reviewerNumber])) {
	$reviewerRecipients[$reviewerNumber] = $allReviewers[$reviewerNumber];
      }
    }
    break;
  case 'group':
    if(Tools::readPost('to_chair') == 'yes') {
      foreach($allReviewers as $reviewer) {
	if($reviewer->getGroup() == Reviewer::$CHAIR_GROUP) {
	  $reviewerRecipients[$reviewer->getReviewerNumber()] = $reviewer;
	}
      }
    }
    if(Tools::readPost('to_reviewer') == 'yes') {
      foreach($allReviewers as $reviewer) {
	if($reviewer->getGroup() == Reviewer::$REVIEWER_GROUP) {
	  $reviewerRecipients[$reviewer->getReviewerNumber()] = $reviewer;
	}
      }
    }
    if(Tools::readPost('to_observer') == 'yes') {
      foreach($allReviewers as $reviewer) {
	if($reviewer->getGroup() == Reviewer::$OBSERVER_GROUP) {
	  $reviewerRecipients[$reviewer->getReviewerNumber()] = $reviewer;
	}
      }
    }
    break;
  default:
  }

  /* Prepare the list of author recipients */
  switch(Tools::readPost('to_authors')) {
  case 'all':
    $articleRecipients = $allArticles;
    break;
  case 'list':
    $list = explode(",", Tools::readPost('to_authors_list'));
    foreach($list as $articleNumber) {
      if(!is_null($allArticles[$articleNumber])) {
	$articleRecipients[$articleNumber] = $allArticles[$articleNumber];
      }
    }
    break;
  case 'group':
    if(Tools::readPost('to_accept') == 'yes') {
      foreach($allArticles as $article) {
	if($article->getAcceptance() == Article::$ACCEPT) {
	  $articleRecipients[$article->getArticleNumber()] = $article;
	}
      }
    }
    if(Tools::readPost('to_maybe_accept') == 'yes') {
      foreach($allArticles as $article) {
	if($article->getAcceptance() == Article::$MAYBE_ACCEPT) {
	  $articleRecipients[$article->getArticleNumber()] = $article;
	}
      }
    }
    if(Tools::readPost('to_discuss') == 'yes') {
      foreach($allArticles as $article) {
	if($article->getAcceptance() == Article::$DISCUSS) {
	  $articleRecipients[$article->getArticleNumber()] = $article;
	}
      }
    }
    if(Tools::readPost('to_maybe_reject') == 'yes') {
      foreach($allArticles as $article) {
	if($article->getAcceptance() == Article::$MAYBE_REJECT) {
	  $articleRecipients[$article->getArticleNumber()] = $article;
	}
      }
    }
    if(Tools::readPost('to_reject') == 'yes') {
      foreach($allArticles as $article) {
	if($article->getAcceptance() == Article::$REJECT) {
	  $articleRecipients[$article->getArticleNumber()] = $article;
	}
      }
    }
    if(Tools::readPost('to_void') == 'yes') {
      foreach($allArticles as $article) {
	if($article->getAcceptance() == Article::$VOID) {
	  $articleRecipients[$article->getArticleNumber()] = $article;
	}
      }
    }
    if(Tools::readPost('to_custom1') == 'yes') {
      foreach($allArticles as $article) {
	if($article->getAcceptance() == Article::$CUSTOM1) {
	  $articleRecipients[$article->getArticleNumber()] = $article;
	}
      }
    }
    if(Tools::readPost('to_custom2') == 'yes') {
      foreach($allArticles as $article) {
	if($article->getAcceptance() == Article::$CUSTOM2) {
	  $articleRecipients[$article->getArticleNumber()] = $article;
	}
      }
    }
    if(Tools::readPost('to_custom3') == 'yes') {
      foreach($allArticles as $article) {
	if($article->getAcceptance() == Article::$CUSTOM3) {
	  $articleRecipients[$article->getArticleNumber()] = $article;
	}
      }
    }
    if(Tools::readPost('to_custom4') == 'yes') {
      foreach($allArticles as $article) {
	if($article->getAcceptance() == Article::$CUSTOM4) {
	  $articleRecipients[$article->getArticleNumber()] = $article;
	}
      }
    }
    if(Tools::readPost('to_custom5') == 'yes') {
      foreach($allArticles as $article) {
	if($article->getAcceptance() == Article::$CUSTOM5) {
	  $articleRecipients[$article->getArticleNumber()] = $article;
	}
      }
    }
    if(Tools::readPost('to_custommai6') == 'yes') {
      foreach($allArticles as $article) {
	if($article->getAcceptance() == Article::$CUSTOM6) {
	  $articleRecipients[$article->getArticleNumber()] = $article;
	}
      }
    }
    if(Tools::readPost('to_custom7') == 'yes') {
      foreach($allArticles as $article) {
	if($article->getAcceptance() == Article::$CUSTOM7) {
	  $articleRecipients[$article->getArticleNumber()] = $article;
	}
      }
    }
    break;
  default:
  }
  
  /* If there is no recipient at all -> Go Back */
  if(count($articleRecipients) + count($reviewerRecipients) == 0) {
    print('<div class="ERRmessage">Địa chỉ mail người nhận không hợp lệ.'//There is no valid recipient for your e-mail.
      .'</div>');
    ?>
      <form action="mailing.php" method="post">
        <input type="hidden" name="subject" value="<?php Tools::printHTML(Tools::readPost('subject')); ?>" />
        <input type="hidden" name="body" value="<?php Tools::printHTML(Tools::readPost('body')); ?>" />
        <input type="hidden" name="to_committee" value="<?php Tools::printHTML(Tools::readPost('to_committee')); ?>" />
        <input type="hidden" name="to_authors" value="<?php Tools::printHTML(Tools::readPost('to_authors')); ?>" />
        <input type="hidden" name="to_chair" value="<?php Tools::printHTML(Tools::readPost('to_chair')); ?>" />
        <input type="hidden" name="to_reviewer" value="<?php Tools::printHTML(Tools::readPost('to_reviewer')); ?>" />
        <input type="hidden" name="to_observer" value="<?php Tools::printHTML(Tools::readPost('to_observer')); ?>" />
        <input type="hidden" name="to_committee_list" value="<?php Tools::printHTML(Tools::readPost('to_committee_list')); ?>" />
        <input type="hidden" name="to_authors_list" value="<?php Tools::printHTML(Tools::readPost('to_authors_list')); ?>" />
        <input type="hidden" name="to_accept" value="<?php Tools::printHTML(Tools::readPost('to_accept')); ?>" />
        <input type="hidden" name="to_maybe_accept" value="<?php Tools::printHTML(Tools::readPost('to_maybe_accept')); ?>" />
        <input type="hidden" name="to_discuss" value="<?php Tools::printHTML(Tools::readPost('to_discuss')); ?>" />
        <input type="hidden" name="to_maybe_reject" value="<?php Tools::printHTML(Tools::readPost('to_maybe_reject')); ?>" />
        <input type="hidden" name="to_reject" value="<?php Tools::printHTML(Tools::readPost('to_reject')); ?>" />
        <input type="hidden" name="to_void" value="<?php Tools::printHTML(Tools::readPost('to_void')); ?>" />
        <input type="hidden" name="to_custom1" value="<?php Tools::printHTML(Tools::readPost('to_custom1')); ?>" />
        <input type="hidden" name="to_custom2" value="<?php Tools::printHTML(Tools::readPost('to_custom2')); ?>" />
        <input type="hidden" name="to_custom3" value="<?php Tools::printHTML(Tools::readPost('to_custom3')); ?>" />
        <input type="hidden" name="to_custom4" value="<?php Tools::printHTML(Tools::readPost('to_custom4')); ?>" />
        <input type="hidden" name="to_custom5" value="<?php Tools::printHTML(Tools::readPost('to_custom5')); ?>" />
        <input type="hidden" name="to_custom6" value="<?php Tools::printHTML(Tools::readPost('to_custom6')); ?>" />
        <input type="hidden" name="to_custom7" value="<?php Tools::printHTML(Tools::readPost('to_custom7')); ?>" />
        <input type="hidden" name="uncheck" value="<?php Tools::printHTML(Tools::readPost('uncheck')); ?>" />
        <input type="hidden" name="init_from_post" value="yes" />
	<div class="floatRight">
          <input type="submit" class="buttonLink bigButton" value="Trở về/Sửa mail" />
        </div>
      </form>
    <?php 
  } else {

    /* Check if there is somebody in the list of reciepients */
    if((count($articleRecipients) != 0) && (count($reviewerRecipients) != 0)) {
      print('<div class="ERRmessage">Trong danh sách người nhận có tác giả và ban thành viên.'
        //There are contact authors AND committee members in the list of recipients of your mail.
        .'</div>');
      $warning .= "Trong danh sách người nhận có tác giả và ban thành viên."
      //There are contact authors AND committee members in the list of recipients of your mail.
      ."\n";
    }

    /* Tell who receives the mail in the committee */
    switch(Tools::readPost('to_committee')) {
    case 'all':
      print('<div class="OKmessage">Gửi mail đến tất cả thành viên.'
        //You are sending a mail to all the committee members.
        .'</div>');
      break;
    case 'list':
      print('<div class="OKmessage">Gửi mail đến danh sách tùy chọn ban thành viên.'
        //You are sending a mail to a custom list of committee members.
        .'</div>');
      break;
    case 'group':
      $i = 0;
      $committeeGroup = array();
      if(Tools::readPost('to_chair') == 'yes') {
	$committeeGroup[$i] = 'ban tổ chức';//'chairs';
	$i++;
      }
      if(Tools::readPost('to_reviewer') == 'yes') {
	$committeeGroup[$i] = 'người đánh giá';//'reviewers';
	$i++;
      }
      if(Tools::readPost('to_observer') == 'yes') {
	$committeeGroup[$i] = 'observers';
	$i++;
      }
      switch($i) {
      case 1:
	print('<div class="OKmessage">Gửi mail đến tất cả '
    //You are sending a mail to all the committee '.
    .$committeeGroup[0].'.</div>');
	break;
      case 2:
	print('<div class="OKmessage">Gửi mail đến tất cả '//You are sending a mail to all the ' 
    . $committeeGroup[0] . ' và ' . $committeeGroup[1] . '.</div>');
	break;
      case 3:
	print('<div class="OKmessage">Gửi mail đến tất cả ban thành viên'
    //You are sending a mail to all the committee members.
    .'</div>');
      default:
      }
    }

    /* Tell who receives the mail among the authors */
    switch(Tools::readPost('to_authors')) {
    case 'all':
      print('<div class="OKmessage">Gửi mail đến tất cả tác giả.'//You are sending a mail to all the contact authors.
        .'</div>');
      break;
    case 'list':
      print('<div class="OKmessage">Gửi mail đến danh sách tùy chọn tác giả.'
        //You are sending a mail to a custom list of contact authors.
        .'</div>');
      break;
    case 'group':
      $i = 0;
      $committeeGroup = array();
      if(Tools::readPost('to_accept') == 'yes') {
	$committeeGroup[$i] = 'đã chấp nhận';//accepted;
	$i++;
      }
      if(Tools::readPost('to_maybe_accept') == 'yes') {
	$committeeGroup[$i] = 'có thể đã chấp nhận';//'maybe accepted';
	$i++;
      }
      if(Tools::readPost('to_discuss') == 'yes') {
	$committeeGroup[$i] = 'trong thảo luận';//in discussion';
	$i++;
      }
      if(Tools::readPost('to_maybe_reject') == 'yes') {
	$committeeGroup[$i] = 'có thể đã từ chối';//'maybe rejected';
	$i++;
      }
      if(Tools::readPost('to_reject') == 'yes') {
	$committeeGroup[$i] = 'đã từ chối';//rejected';
	$i++;
      }
      if(Tools::readPost('to_void') == 'yes') {
	$committeeGroup[$i] = 'rỗng';//'void';
	$i++;
      }
      if(Tools::readPost('to_custom1') == 'yes') {
	$committeeGroup[$i] = Tools::getCustomAccept1();
	$i++;
      }
      if(Tools::readPost('to_custom2') == 'yes') {
	$committeeGroup[$i] = Tools::getCustomAccept2();
	$i++;
      }
      if(Tools::readPost('to_custom3') == 'yes') {
	$committeeGroup[$i] = Tools::getCustomAccept3();
	$i++;
      }
      if(Tools::readPost('to_custom4') == 'yes') {
	$committeeGroup[$i] = Tools::getCustomAccept4();
	$i++;
      }
      if(Tools::readPost('to_custom5') == 'yes') {
	$committeeGroup[$i] = Tools::getCustomAccept5();
	$i++;
      }
      if(Tools::readPost('to_custom6') == 'yes') {
	$committeeGroup[$i] = Tools::getCustomAccept6();
	$i++;
      }
      if(Tools::readPost('to_custom7') == 'yes') {
	$committeeGroup[$i] = Tools::getCustomAccept7();
	$i++;
      }
      $customAcceptsNumber = 0;
      if (Tools::useCustomAccept1()) { $customAcceptsNumber++; }
      if (Tools::useCustomAccept2()) { $customAcceptsNumber++; }
      if (Tools::useCustomAccept3()) { $customAcceptsNumber++; }
      if (Tools::useCustomAccept4()) { $customAcceptsNumber++; }
      if (Tools::useCustomAccept5()) { $customAcceptsNumber++; }
      if (Tools::useCustomAccept6()) { $customAcceptsNumber++; }
      if (Tools::useCustomAccept7()) { $customAcceptsNumber++; }
      if ($i == (6+$customAcceptsNumber)) {
        print('<div class="OKmessage">Gửi mail đến tất cả tác giả.'//You are sending a mail to all the contact authors.
          .'</div>');
      } else {
	print('<div class="OKmessage">Gửi mail đến tác giả của bài viết '//You are sending a mail to the coGửi mail đến tất cả tác giả.ntact authors of '
    .implode($committeeGroup, ', ')//' articles.
    .'</div>');
      }
    }

///Phần gửi mail**********************************************************************************************************************************************
    /* Display an example */
    $exampleSubject;
    $exampleBody;
    if((count($reviewerRecipients) != 0) && (count($articleRecipients) == 0)) {
      $exampleSubject = Tools::parseMailToCommittee(Tools::readPost('subject'), reset($reviewerRecipients));
      $exampleBody = Tools::parseMailToCommittee(Tools::readPost('body'), reset($reviewerRecipients));
    } else if((count($reviewerRecipients) == 0) && (count($articleRecipients) != 0)) {
      $exampleSubject = Tools::parseMailToAuthors(Tools::readPost('subject'), reset($articleRecipients));
      $exampleBody = Tools::parseMailToAuthors(Tools::readPost('body'), reset($articleRecipients));
    } else {
      $exampleSubject = Tools::parseMail(Tools::readPost('subject'));
      $exampleBody = Tools::parseMail(Tools::readPost('body'));      
    }
    $tags = Tools::getAllNonReplacedTags($exampleSubject);
    if(count($tags) != 0) {
      $tagString = "";
      foreach($tags as $tag) {
	$tagString .= ', ' . $tag;
      }
      print('<div class="ERRmessage">Các thẻ sau đây không thể thay thế trong chủ để:'//The following tags could not be replaced in the subject:'
       . substr($tagString, 1) . '.</div>');
      $warning .= 'Các thẻ sau đây không thể thay thế trong chủ để:'//The following tags could not be replaced in the subject:' 
      . substr($tagString, 1) . ".\n";
    }
    $tags = Tools::getAllNonReplacedTags($exampleBody);
    if(count($tags) != 0) {
      $tagString = "";
      foreach($tags as $tag) {
	$tagString .= ', ' . $tag;
      }
      print('<div class="ERRmessage">Các thẻ sau đây không thể thay thế trong thông báo:'//The following tags could not be replaced in the message:'
       . substr($tagString, 1) . '.</div>');
      $warning .= 'Các thẻ sau đây không thể thay thế trong thông báo:'
      //The following tags could not be replaced in the message:' 
      . substr($tagString, 1) . ".\n";
    }


    ?>

    
    <center>
    <table>
      <tr>
        <td colspan="2">Chủ đề:<!--Sample Subject:--></td>
      </tr>
      <tr>
        <td colspan="2"><input type="text" name="subject" size="50" value="<?php Tools::printHTML($exampleSubject); ?>" readonly="readonly" /></td>
      </tr>
      <tr>
        <td colspan="2">Nội dung:<!--Sample Message:--></td>
      </tr>
      <tr>
        <td colspan="2"><textarea name="body" rows="25" cols="80" readonly="readonly"><?php Tools::printHTML($exampleBody); ?></textarea></td>
      </tr>
      <tr>
        <td style="text-align: right; width: 50%;">
	  <form action="mailing.php" method="post">
	    <input type="hidden" name="subject" value="<?php Tools::printHTML(Tools::readPost('subject')); ?>" />
	    <input type="hidden" name="body" value="<?php Tools::printHTML(Tools::readPost('body')); ?>" />
	    <input type="hidden" name="to_committee" value="<?php Tools::printHTML(Tools::readPost('to_committee')); ?>" />
	    <input type="hidden" name="to_authors" value="<?php Tools::printHTML(Tools::readPost('to_authors')); ?>" />
	    <input type="hidden" name="to_chair" value="<?php Tools::printHTML(Tools::readPost('to_chair')); ?>" />
	    <input type="hidden" name="to_reviewer" value="<?php Tools::printHTML(Tools::readPost('to_reviewer')); ?>" />
	    <input type="hidden" name="to_observer" value="<?php Tools::printHTML(Tools::readPost('to_observer')); ?>" />
	    <input type="hidden" name="to_committee_list" value="<?php Tools::printHTML(Tools::readPost('to_committee_list')); ?>" />
	    <input type="hidden" name="to_authors_list" value="<?php Tools::printHTML(Tools::readPost('to_authors_list')); ?>" />
	    <input type="hidden" name="to_accept" value="<?php Tools::printHTML(Tools::readPost('to_accept')); ?>" />
	    <input type="hidden" name="to_maybe_accept" value="<?php Tools::printHTML(Tools::readPost('to_maybe_accept')); ?>" />
	    <input type="hidden" name="to_discuss" value="<?php Tools::printHTML(Tools::readPost('to_discuss')); ?>" />
	    <input type="hidden" name="to_maybe_reject" value="<?php Tools::printHTML(Tools::readPost('to_maybe_reject')); ?>" />
	    <input type="hidden" name="to_reject" value="<?php Tools::printHTML(Tools::readPost('to_reject')); ?>" />
	    <input type="hidden" name="to_void" value="<?php Tools::printHTML(Tools::readPost('to_void')); ?>" />
	    <input type="hidden" name="to_custom1" value="<?php Tools::printHTML(Tools::readPost('to_custom1')); ?>" />
	    <input type="hidden" name="to_custom2" value="<?php Tools::printHTML(Tools::readPost('to_custom2')); ?>" />
	    <input type="hidden" name="to_custom3" value="<?php Tools::printHTML(Tools::readPost('to_custom3')); ?>" />
	    <input type="hidden" name="to_custom4" value="<?php Tools::printHTML(Tools::readPost('to_custom4')); ?>" />
	    <input type="hidden" name="to_custom5" value="<?php Tools::printHTML(Tools::readPost('to_custom5')); ?>" />
	    <input type="hidden" name="to_custom6" value="<?php Tools::printHTML(Tools::readPost('to_custom6')); ?>" />
	    <input type="hidden" name="to_custom7" value="<?php Tools::printHTML(Tools::readPost('to_custom7')); ?>" />
	    <input type="hidden" name="uncheck" value="<?php Tools::printHTML(Tools::readPost('uncheck')); ?>" />
	    <input type="hidden" name="init_from_post" value="yes" />
	    <input type="submit" class="buttonLink bigButton" value="Trở về/Sửa mail" />
	  </form>
	</td>
	<td style="width: 50%;">
	  <form action="mailing_result.php" method="post">
	    <input type="hidden" name="subject" value="<?php Tools::printHTML(Tools::readPost('subject')); ?>" />
	    <input type="hidden" name="body" value="<?php Tools::printHTML(Tools::readPost('body')); ?>" />
	    <input type="hidden" name="warning" value="<?php Tools::printHTML($warning); ?>" />
	    <input type="hidden" name="to_committee" value="<?php Tools::printHTML(Tools::readPost('to_committee')); ?>" />
	    <input type="hidden" name="to_authors" value="<?php Tools::printHTML(Tools::readPost('to_authors')); ?>" />
	    <input type="hidden" name="to_chair" value="<?php Tools::printHTML(Tools::readPost('to_chair')); ?>" />
	    <input type="hidden" name="to_reviewer" value="<?php Tools::printHTML(Tools::readPost('to_reviewer')); ?>" />
	    <input type="hidden" name="to_observer" value="<?php Tools::printHTML(Tools::readPost('to_observer')); ?>" />
	    <input type="hidden" name="to_committee_list" value="<?php Tools::printHTML(Tools::readPost('to_committee_list')); ?>" />
	    <input type="hidden" name="to_authors_list" value="<?php Tools::printHTML(Tools::readPost('to_authors_list')); ?>" />
	    <input type="hidden" name="to_accept" value="<?php Tools::printHTML(Tools::readPost('to_accept')); ?>" />
	    <input type="hidden" name="to_maybe_accept" value="<?php Tools::printHTML(Tools::readPost('to_maybe_accept')); ?>" />
	    <input type="hidden" name="to_discuss" value="<?php Tools::printHTML(Tools::readPost('to_discuss')); ?>" />
	    <input type="hidden" name="to_maybe_reject" value="<?php Tools::printHTML(Tools::readPost('to_maybe_reject')); ?>" />
	    <input type="hidden" name="to_reject" value="<?php Tools::printHTML(Tools::readPost('to_reject')); ?>" />
	    <input type="hidden" name="to_void" value="<?php Tools::printHTML(Tools::readPost('to_void')); ?>" />
	    <input type="hidden" name="to_custom1" value="<?php Tools::printHTML(Tools::readPost('to_custom1')); ?>" />
	    <input type="hidden" name="to_custom2" value="<?php Tools::printHTML(Tools::readPost('to_custom2')); ?>" />
	    <input type="hidden" name="to_custom3" value="<?php Tools::printHTML(Tools::readPost('to_custom3')); ?>" />
	    <input type="hidden" name="to_custom4" value="<?php Tools::printHTML(Tools::readPost('to_custom4')); ?>" />
	    <input type="hidden" name="to_custom5" value="<?php Tools::printHTML(Tools::readPost('to_custom5')); ?>" />
	    <input type="hidden" name="to_custom6" value="<?php Tools::printHTML(Tools::readPost('to_custom6')); ?>" />
	    <input type="hidden" name="to_custom7" value="<?php Tools::printHTML(Tools::readPost('to_custom7')); ?>" />
	    <input type="hidden" name="uncheck" value="<?php Tools::printHTML(Tools::readPost('uncheck')); ?>" />
	    <input type="submit" class="buttonLink bigButton" value="Gửi mail" />
	</td>
      </tr>
    </table>
    </center>

    <center>
    <table><tr class="topAlign"><td>
    <?php if(count($articleRecipients) == 0) print('<div class="profileLeft">'); else print('<div>'); ?>
    <center>
    <h2>Người nhận trong số các ban thành viên<!--Recipients among the Committee Members--></h2>
    <table>
      <?php 
      if(Tools::readPost('to_committee') != 'group') {
        foreach($reviewerRecipients as $reviewer) {
	  print('<tr><td><input class="noBorder" type="checkbox" ' . $uncheck . ' id="rev'.$reviewer->getReviewerNumber().'" name="r'.$reviewer->getReviewerNumber().'" value="yes" /></td>');
	  print('<td><label for="rev'.$reviewer->getReviewerNumber().'">' . htmlentities($reviewer->getEmail(), ENT_COMPAT | ENT_HTML401, 'UTF-8') . '</label></td>');
	  print('<td><label for="rev'.$reviewer->getReviewerNumber().'">' . htmlentities($reviewer->getFullName(), ENT_COMPAT | ENT_HTML401, 'UTF-8') . '</label></td></tr>');
        }
      } else {
        if(Tools::readPost('to_chair') == 'yes') {
          print('<tr><td colspan="3"><em>Ban tổ chức'//Chair(s)
            .'</em></td></tr>');
	  foreach($reviewerRecipients as $reviewer) {
	    if($reviewer->getGroup() == Reviewer::$CHAIR_GROUP) {
	      print('<tr><td><input class="noBorder" type="checkbox" ' . $uncheck . ' id="rev'.$reviewer->getReviewerNumber().'" name="r'.$reviewer->getReviewerNumber().'" value="yes" /></td>');
	      print('<td><label for="rev'.$reviewer->getReviewerNumber().'">' . htmlentities($reviewer->getEmail(), ENT_COMPAT | ENT_HTML401, 'UTF-8') . '</label></td>');
	      print('<td><label for="rev'.$reviewer->getReviewerNumber().'">' . htmlentities($reviewer->getFullName(), ENT_COMPAT | ENT_HTML401, 'UTF-8') . '</label></td></tr>');
	    }
	  }
	}
        if(Tools::readPost('to_reviewer') == 'yes') {
          print('<tr><td colspan="3"><em>Người đánh giá'//Reviewer(s)
            .'</em></td></tr>');
	  foreach($reviewerRecipients as $reviewer) {
	    if($reviewer->getGroup() == Reviewer::$REVIEWER_GROUP) {
	      print('<tr><td><input class="noBorder" type="checkbox" ' . $uncheck . ' id="rev'.$reviewer->getReviewerNumber().'" name="r'.$reviewer->getReviewerNumber().'" value="yes" /></td>');
	      print('<td><label for="rev'.$reviewer->getReviewerNumber().'">' . htmlentities($reviewer->getEmail(), ENT_COMPAT | ENT_HTML401, 'UTF-8') . '</label></td>');
	      print('<td><label for="rev'.$reviewer->getReviewerNumber().'">' . htmlentities($reviewer->getFullName(), ENT_COMPAT | ENT_HTML401, 'UTF-8') . '</label></td></tr>');
	    }
	  }
	}
        if(Tools::readPost('to_observer') == 'yes') {
          print('<tr><td colspan="3"><em>Người quan sát'//Neutral Observer(s)
            .'</em></td></tr>');
	  foreach($reviewerRecipients as $reviewer) {
	    if($reviewer->getGroup() == Reviewer::$OBSERVER_GROUP) {
	      print('<tr><td><input class="noBorder" type="checkbox" ' . $uncheck . ' id="rev'.$reviewer->getReviewerNumber().'" name="r'.$reviewer->getReviewerNumber().'" value="yes" /></td>');
	      print('<td><label for="rev'.$reviewer->getReviewerNumber().'">' . htmlentities($reviewer->getEmail(), ENT_COMPAT | ENT_HTML401, 'UTF-8') . '</label></td>');
	      print('<td><label for="rev'.$reviewer->getReviewerNumber().'">' . htmlentities($reviewer->getFullName(), ENT_COMPAT | ENT_HTML401, 'UTF-8') . '</label></td></tr>');
	    }
	  }
	}
      }
      ?>
    </table>
    </center></div></td>
    <td>
    <?php if(count($articleRecipients) != 0) print('<div class="profileRight">'); else print('<div>'); ?>
    <center>
    <h2>Người nhận trong số các tác giả<!--Recipients among the Contact Authors--></h2>
    <table>
      <?php 
      if(Tools::readPost('to_authors') != 'group') {
        foreach($articleRecipients as $article) {
	  print('<tr><td><input class="noBorder" type="checkbox" ' . $uncheck . ' id="art'.$article->getArticleNumber().'" name="a'.$article->getArticleNumber().'" value="yes" /></td>');
	  print('<td><label for="art'.$article->getArticleNumber().'">Bài viết&nbsp;' . $article->getArticleNumber() . '&nbsp;</label></td>');
	  print('<td><label for="art'.$article->getArticleNumber().'">' . Tools::HTMLsubstr($article->getContact(),35) . '</label></td>');
	  print('<td><label for="art'.$article->getArticleNumber().'">' . Tools::HTMLsubstr($article->getTitle(),40) . '</label></td></tr>');
        }
      } else {
        if(Tools::readPost('to_accept') == 'yes') {
          print('<tr><td colspan="4"><em>Bài viết đã được chấp nhận'//Accepted Articles
            .'</em></td></tr>');
	  foreach($articleRecipients as $article) {
	    if($article->getAcceptance() == Article::$ACCEPT) {
	      print('<tr><td><input class="noBorder" type="checkbox" ' . $uncheck . ' id="art'.$article->getArticleNumber().'" name="a'.$article->getArticleNumber().'" value="yes" /></td>');
	      print('<td><label for="art'.$article->getArticleNumber().'">Bài viết&nbsp;' . $article->getArticleNumber() . '&nbsp;</label></td>');
	      print('<td><label for="art'.$article->getArticleNumber().'">' . Tools::HTMLsubstr($article->getContact(),35) . '&nbsp;</label></td>');
	      print('<td><label for="art'.$article->getArticleNumber().'">' . Tools::HTMLsubstr($article->getTitle(),40) . '</label></td></tr>');
	    }
	  }
	}
        if(Tools::readPost('to_maybe_accept') == 'yes') {
          print('<tr><td colspan="4"><em>Bài viết có thể chấp nhận'//Maybe Accepted Articles
            .'</em></td></tr>');
	  foreach($articleRecipients as $article) {
	    if($article->getAcceptance() == Article::$MAYBE_ACCEPT) {
	      print('<tr><td><input class="noBorder" type="checkbox" ' . $uncheck . ' id="art'.$article->getArticleNumber().'" name="a'.$article->getArticleNumber().'" value="yes" /></td>');
	      print('<td><label for="art'.$article->getArticleNumber().'">Bài viết&nbsp;' . $article->getArticleNumber() . '&nbsp;</label></td>');
	      print('<td><label for="art'.$article->getArticleNumber().'">' . Tools::HTMLsubstr($article->getContact(),35) . '</label></td>');
	      print('<td><label for="art'.$article->getArticleNumber().'">' . Tools::HTMLsubstr($article->getTitle(),40) . '</label></td></tr>');
	    }
	  }
	}
        if(Tools::readPost('to_discuss') == 'yes') {
          print('<tr><td colspan="4"><em>Bài viết đang trong thảo luận'//Articles in Discussion
            .'</em></td></tr>');
	  foreach($articleRecipients as $article) {
	    if($article->getAcceptance() == Article::$DISCUSS) {
	      print('<tr><td><input class="noBorder" type="checkbox" ' . $uncheck . ' id="art'.$article->getArticleNumber().'" name="a'.$article->getArticleNumber().'" value="yes" /></td>');
	      print('<td><label for="art'.$article->getArticleNumber().'">Bài viết&nbsp;' . $article->getArticleNumber() . '&nbsp;</label></td>');
	      print('<td><label for="art'.$article->getArticleNumber().'">' . Tools::HTMLsubstr($article->getContact(),35) . '</label></td>');
	      print('<td><label for="art'.$article->getArticleNumber().'">' . Tools::HTMLsubstr($article->getTitle(),40) . '</label></td></tr>');
	    }
	  }
	}
        if(Tools::readPost('to_maybe_reject') == 'yes') {
          print('<tr><td colspan="4"><em>Bài viết có thể bị từ chối'//Maybe Rejected Articles
            .'</em></td></tr>');
	  foreach($articleRecipients as $article) {
	    if($article->getAcceptance() == Article::$MAYBE_REJECT) {
	      print('<tr><td><input class="noBorder" type="checkbox" ' . $uncheck . ' id="art'.$article->getArticleNumber().'" name="a'.$article->getArticleNumber().'" value="yes" /></td>');
	      print('<td><label for="art'.$article->getArticleNumber().'">Bài viết&nbsp;' . $article->getArticleNumber() . '&nbsp;</label></td>');
	      print('<td><label for="art'.$article->getArticleNumber().'">' . Tools::HTMLsubstr($article->getContact(),35) . '</label></td>');
	      print('<td><label for="art'.$article->getArticleNumber().'">' . Tools::HTMLsubstr($article->getTitle(),40) . '</label></td></tr>');
	    }
	  }
	}
        if(Tools::readPost('to_reject') == 'yes') {
          print('<tr><td colspan="4"><em>Bài víêt bị từ chối'//Rejected Articles
            .'</em></td></tr>');
	  foreach($articleRecipients as $article) {
	    if($article->getAcceptance() == Article::$REJECT) {
	      print('<tr><td><input class="noBorder" type="checkbox" ' . $uncheck . ' id="art'.$article->getArticleNumber().'" name="a'.$article->getArticleNumber().'" value="yes" /></td>');
	      print('<td><label for="art'.$article->getArticleNumber().'">Bài viết&nbsp;' . $article->getArticleNumber() . '&nbsp;</label></td>');
	      print('<td><label for="art'.$article->getArticleNumber().'">' . Tools::HTMLsubstr($article->getContact(),35) . '</label></td>');
	      print('<td><label for="art'.$article->getArticleNumber().'">' . Tools::HTMLsubstr($article->getTitle(),40) . '</label></td></tr>');
	    }
	  }
	}
        if(Tools::readPost('to_void') == 'yes') {
          print('<tr><td colspan="4"><em>Bài viết không có ý kiến'//Void Articles
            .'</em></td></tr>');
	  foreach($articleRecipients as $article) {
	    if($article->getAcceptance() == Article::$VOID) {
	      print('<tr><td><input class="noBorder" type="checkbox" ' . $uncheck . ' id="art'.$article->getArticleNumber().'" name="a'.$article->getArticleNumber().'" value="yes" /></td>');
	      print('<td><label for="art'.$article->getArticleNumber().'">Bài viết&nbsp;' . $article->getArticleNumber() . '&nbsp;</label></td>');
	      print('<td><label for="art'.$article->getArticleNumber().'">' . Tools::HTMLsubstr($article->getContact(),35) . '</label></td>');
	      print('<td><label for="art'.$article->getArticleNumber().'">' . Tools::HTMLsubstr($article->getTitle(),40) . '</label></td></tr>');
	    }
	  }
	}
        if(Tools::readPost('to_custom1') == 'yes') {
          print('<tr><td colspan="4"><em>"' . Tools::getCustomAccept1() . '" Bài viết</em></td></tr>');
	  foreach($articleRecipients as $article) {
	    if($article->getAcceptance() == Article::$CUSTOM1) {
	      print('<tr><td><input class="noBorder" type="checkbox" ' . $uncheck . ' id="art'.$article->getArticleNumber().'" name="a'.$article->getArticleNumber().'" value="yes" /></td>');
	      print('<td><label for="art'.$article->getArticleNumber().'">Bài viết&nbsp;' . $article->getArticleNumber() . '&nbsp;</label></td>');
	      print('<td><label for="art'.$article->getArticleNumber().'">' . Tools::HTMLsubstr($article->getContact(),35) . '</label></td>');
	      print('<td><label for="art'.$article->getArticleNumber().'">' . Tools::HTMLsubstr($article->getTitle(),40) . '</label></td></tr>');
	    }
	  }
	}
        if(Tools::readPost('to_custom2') == 'yes') {
          print('<tr><td colspan="4"><em>"' . Tools::getCustomAccept2() . '" Bài viết</em></td></tr>');
	  foreach($articleRecipients as $article) {
	    if($article->getAcceptance() == Article::$CUSTOM2) {
	      print('<tr><td><input class="noBorder" type="checkbox" ' . $uncheck . ' id="art'.$article->getArticleNumber().'" name="a'.$article->getArticleNumber().'" value="yes" /></td>');
	      print('<td><label for="art'.$article->getArticleNumber().'">Bài viết&nbsp;' . $article->getArticleNumber() . '&nbsp;</label></td>');
	      print('<td><label for="art'.$article->getArticleNumber().'">' . Tools::HTMLsubstr($article->getContact(),35) . '</label></td>');
	      print('<td><label for="art'.$article->getArticleNumber().'">' . Tools::HTMLsubstr($article->getTitle(),40) . '</label></td></tr>');
	    }
	  }
	}
        if(Tools::readPost('to_custom3') == 'yes') {
          print('<tr><td colspan="4"><em>"' . Tools::getCustomAccept3() . '" Bài viết</em></td></tr>');
	  foreach($articleRecipients as $article) {
	    if($article->getAcceptance() == Article::$CUSTOM3) {
	      print('<tr><td><input class="noBorder" type="checkbox" ' . $uncheck . ' id="art'.$article->getArticleNumber().'" name="a'.$article->getArticleNumber().'" value="yes" /></td>');
	      print('<td><label for="art'.$article->getArticleNumber().'">Bài viết&nbsp;' . $article->getArticleNumber() . '&nbsp;</label></td>');
	      print('<td><label for="art'.$article->getArticleNumber().'">' . Tools::HTMLsubstr($article->getContact(),35) . '</label></td>');
	      print('<td><label for="art'.$article->getArticleNumber().'">' . Tools::HTMLsubstr($article->getTitle(),40) . '</label></td></tr>');
	    }
	  }
	}
        if(Tools::readPost('to_custom4') == 'yes') {
          print('<tr><td colspan="4"><em>"' . Tools::getCustomAccept4() . '" Bài viết</em></td></tr>');
	  foreach($articleRecipients as $article) {
	    if($article->getAcceptance() == Article::$CUSTOM4) {
	      print('<tr><td><input class="noBorder" type="checkbox" ' . $uncheck . ' id="art'.$article->getArticleNumber().'" name="a'.$article->getArticleNumber().'" value="yes" /></td>');
	      print('<td><label for="art'.$article->getArticleNumber().'">Bài viết&nbsp;' . $article->getArticleNumber() . '&nbsp;</label></td>');
	      print('<td><label for="art'.$article->getArticleNumber().'">' . Tools::HTMLsubstr($article->getContact(),35) . '</label></td>');
	      print('<td><label for="art'.$article->getArticleNumber().'">' . Tools::HTMLsubstr($article->getTitle(),40) . '</label></td></tr>');
	    }
	  }
	}
        if(Tools::readPost('to_custom5') == 'yes') {
          print('<tr><td colspan="4"><em>"' . Tools::getCustomAccept5() . '" Bài viết</em></td></tr>');
	  foreach($articleRecipients as $article) {
	    if($article->getAcceptance() == Article::$CUSTOM5) {
	      print('<tr><td><input class="noBorder" type="checkbox" ' . $uncheck . ' id="art'.$article->getArticleNumber().'" name="a'.$article->getArticleNumber().'" value="yes" /></td>');
	      print('<td><label for="art'.$article->getArticleNumber().'">Bài viết&nbsp;' . $article->getArticleNumber() . '&nbsp;</label></td>');
	      print('<td><label for="art'.$article->getArticleNumber().'">' . Tools::HTMLsubstr($article->getContact(),35) . '</label></td>');
	      print('<td><label for="art'.$article->getArticleNumber().'">' . Tools::HTMLsubstr($article->getTitle(),40) . '</label></td></tr>');
	    }
	  }
	}
        if(Tools::readPost('to_custom6') == 'yes') {
          print('<tr><td colspan="4"><em>"' . Tools::getCustomAccept6() . '" Bài viết</em></td></tr>');
	  foreach($articleRecipients as $article) {
	    if($article->getAcceptance() == Article::$CUSTOM6) {
	      print('<tr><td><input class="noBorder" type="checkbox" ' . $uncheck . ' id="art'.$article->getArticleNumber().'" name="a'.$article->getArticleNumber().'" value="yes" /></td>');
	      print('<td><label for="art'.$article->getArticleNumber().'">Bài viết&nbsp;' . $article->getArticleNumber() . '&nbsp;</label></td>');
	      print('<td><label for="art'.$article->getArticleNumber().'">' . Tools::HTMLsubstr($article->getContact(),35) . '</label></td>');
	      print('<td><label for="art'.$article->getArticleNumber().'">' . Tools::HTMLsubstr($article->getTitle(),40) . '</label></td></tr>');
	    }
	  }
	}
        if(Tools::readPost('to_custom7') == 'yes') {
          print('<tr><td colspan="4"><em>"' . Tools::getCustomAccept7() . '" Bài viết</em></td></tr>');
	  foreach($articleRecipients as $article) {
	    if($article->getAcceptance() == Article::$CUSTOM7) {
	      print('<tr><td><input class="noBorder" type="checkbox" ' . $uncheck . ' id="art'.$article->getArticleNumber().'" name="a'.$article->getArticleNumber().'" value="yes" /></td>');
	      print('<td><label for="art'.$article->getArticleNumber().'">Bài viết&nbsp;' . $article->getArticleNumber() . '&nbsp;</label></td>');
	      print('<td><label for="art'.$article->getArticleNumber().'">' . Tools::HTMLsubstr($article->getContact(),35) . '</label></td>');
	      print('<td><label for="art'.$article->getArticleNumber().'">' . Tools::HTMLsubstr($article->getTitle(),40) . '</label></td></tr>');
	    }
	  }
	}
      }
      ?>
    </table>
    </center></div></td></tr></table>
    </center>
    </form>

  <?php }

?>



<?php }?>

<?php include('footer.php'); ?>
