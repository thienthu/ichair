<?php 

/*
 * Copyright (C) 2006, 2007 Thomas Baigneres, Matthieu Finiasz
 *
 * This file is part of iChair.
 *
 * iChair is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 * 
 * iChair is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

?><?php 
$page_title = "Thiết Lập Mật Khẩu";//"Generate and E-mail Password";
include '../utils/tools.php';
include "header.php";

$reviewer = Reviewer::getByReviewerNumber(Tools::readPost('reviewerNumber'));

if(!is_null($reviewer)) {

  if($reviewer->generateAndEmailPassword()) {
    Log::logGeneratePassword($reviewer);
    ?>
    <div class="OKmessage">
      Mật khẩu của người dùng<!--The mail containing the password for user--> <?php Tools::printHTML($reviewer->getLogin()); ?> đã được gửi đến email <!--has been successfully sent to--> <?php Tools::printHTML($reviewer->getEmail()); ?> thành công.
    </div> 
    <form action="manage_users.php" method="post">
      <div class="floatRight">
        <input type="submit" class="buttonLink bigButton" value="Đồng ý" />
      </div>
    </form>
    <?php 
    }
    else {
    ?>
    <div class="ERRmessage">
      Mật khẩu của người dùng<!--The mail containing the password for user--> <?php 
        
      Tools::printHTML($reviewer->getLogin()); ?> không thể gửi đên email <!--could not be sent to--> <?php Tools::printHTML($reviewer->getEmail()); 
 ?>.
    </div> 
    <form action="manage_users.php" method="post">
      <div class="floatRight">
        <input type="submit" class="buttonLink bigButton" value="Đồng ý" />
      </div>
    </form>
    <?php    
    }
}
?>
</body>
</html>
