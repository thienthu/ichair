<?php 

/*
 * Copyright (C) 2006, 2007 Thomas Baigneres, Matthieu Finiasz
 *
 * This file is part of iChair.
 *
 * iChair is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 * 
 * iChair is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

?><?php 
include("../utils/tools.php");

$currentReviewer = Reviewer::getReviewerByLogin(); 
$reviewerNumber = $currentReviewer->getReviewerNumber();

if (!is_null($currentReviewer)) {
  if (Tools::useZipReviews()) {
    $zippath = Tools::getConfig('server/zipPath') . "zip";
    $tmpdir;
    do {
      $tmpdir = "/tmp/".rand()."ZZZ".$currentReviewer->getReviewerNumber();
    } while (file_exists($tmpdir));
    mkdir($tmpdir);
    $indexFile = fopen($tmpdir."/index.html", "w");
    fwrite($indexFile, '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-type" content="text/html; charset=utf-8" />');
    fwrite($indexFile,'<title>'.htmlentities(Tools::getConfig('conference/name'), ENT_COMPAT | ENT_HTML401, 'utf-8').' - Your reviews</title>
<link href="zip.css" rel="stylesheet" type="text/css" />
</head>
<body>');
    fwrite($indexFile,'<h1>'.htmlentities(Tools::getConfig('conference/name'), ENT_COMPAT | ENT_HTML401, 'utf-8').' - Your reviews</h1>');
    if(Tools::hasReviewGuidelinesHTML("..") || Tools::hasReviewGuidelinesPDF("..") || Tools::hasReviewGuidelinesTXT("..")) {
      fwrite($indexFile, '<center><table><tr>');
      if(Tools::hasReviewGuidelinesHTML("..")) {
	copy("guidelines/guidelines.html", $tmpdir."/guidelines.html");
	fwrite($indexFile, '<td><form action="guidelines.html" target="_blank" method="post"><input type="submit" class="buttonLink bigButton" value="Review Guidelines (HTML)" /></form></td>');
      } 
      if(Tools::hasReviewGuidelinesPDF("..")) {
	copy("guidelines/guidelines.pdf", $tmpdir."/guidelines.pdf");
	fwrite($indexFile, '<td><form action="guidelines.pdf" target="_blank" method="post"><input type="submit" class="buttonLink bigButton" value="Review Guidelines (PDF)" /></form></td>');
      } 
      if(Tools::hasReviewGuidelinesTXT("..")) {
	copy("guidelines/guidelines.txt", $tmpdir."/guidelines.txt");
	fwrite($indexFile, '<td><form action="guidelines.txt" target="_blank" method="post"><input type="submit" class="buttonLink bigButton" value="Review Guidelines (TXT)" /></form></td>');
      } 
      fwrite($indexFile, '</tr></table></center>');
    }

    $articles = Article::getAllArticles();
    $articleNumbers = $currentReviewer->getArticlesByStatus(Assignement::$ASSIGNED);
    $allcategories;   
    if (Tools::useCategory()) {
      $allcategories = Tools::getAllCategories();
    }      

    foreach($articleNumbers as $articleNumber) {
      $article = $articles[$articleNumber];
      copy($article->getFolder()."".$article->getFile(), $tmpdir."/".$article->getFile());
      $assignement = Assignement::getByNumbers($articleNumber, $reviewerNumber);
      $file;
      $isATempFile = false;
      if(file_exists($article->getFolder() . $assignement->getXMLFileName())) {
	$file = $article->getFolder() . $assignement->getXMLFileName();
      } else {
	$reviewDocument = Tools::createXMLReview(false,
						 $article->getArticleNumber(),
						 $article->getTitle(), $article->getAuthors(),
						 $currentReviewer->getFullName(), "",
						 "", "", "", "", "", "", 
						 "", "", "");
	$reviewDocument->formatOutput = true;
	$reviewDocument->encoding = "utf-8";
	$file = tmpfile();
	$isATempFile = true;
	$reviewDocument->save("" . $file);
      }      
      copy("" . $file, $tmpdir.sprintf("/review%04d.xml", $article->getArticleNumber()));
      if ($isATempFile) {
        unlink("" . $file);
      }

      fwrite($indexFile,'
<div class="paperBox">
  <div class="paperBoxTitle">
    <div class="paperBoxNumber">Bài viết '//Article&nbsp;' 
    . $articleNumber . '</div>');
      $count=2;
      if(Tools::useCategory()) { 
	fwrite($indexFile,'Thể loại: '//Category: '
   . htmlentities($allcategories[$article->getCategory()], ENT_COMPAT | ENT_HTML401, 'utf-8') . '<br/>');
	$count--;
      }
      if(Tools::useKeywords()) { 
	fwrite($indexFile,'Từ khoá(Keyword): '//Keywords: '
   . htmlentities($article->getKeywords(), ENT_COMPAT | ENT_HTML401, 'utf-8') . '<br/>');
	$count--;
      }
      for (;$count>0; $count--){
	fwrite($indexFile,'&nbsp;<br/>');
      }
      if (!Tools::authorsAreAnonymous() && Tools::useCountry()) {
        fwrite($indexFile,'
  </div>
  <div class="paperBoxDetails">'  . Tools::printContinentDivs($article->getCountryArray(), False) . $article->printCustomCheckboxes(false, false) . '<div class="versionTitle">Title: '. htmlentities($article->getTitle(), ENT_COMPAT | ENT_HTML401, 'utf-8') .'</div>');
      } else {
        fwrite($indexFile,'
  </div>
  <div class="paperBoxDetails">
    <div><div><div>'. $article->printCustomCheckboxes(false, false) . '<div class="versionTitle">Tiêu đề: '//Title: '
    . htmlentities($article->getTitle(), ENT_COMPAT | ENT_HTML401, 'utf-8') .'</div>');
      }
      if(!Tools::authorsAreAnonymous()) {
	fwrite($indexFile,'<div class="versionAuthors">Tác giả: '//Authors: '
    .htmlentities($article->getAuthors(), ENT_COMPAT | ENT_HTML401, 'utf-8').'</div>');
	if(Tools::useAffiliations()) {
	  fwrite($indexFile, 'Tổ chức: ' //Affiliations: '
     . htmlentities($article->getAffiliations(), ENT_COMPAT | ENT_HTML401, 'utf-8').'<br/>');
	}
	if(Tools::useCountry()) {
	  fwrite($indexFile, 'Quốc gia: '//Countries: ' 
      . htmlentities($article->getCountry(), ENT_COMPAT | ENT_HTML401, 'utf-8').'<br/>');
	}
      }
      if(Tools::useAbstract()) {
	fwrite($indexFile,'Tóm tắc: '//Abstract:
    .'<div class="versionAbstract">'.nl2br(htmlentities($article->getAbstract(), ENT_COMPAT | ENT_HTML401, 'utf-8')).'</div>');
      } 
      fwrite($indexFile,'</div></div></div></div><div class="floatRight"><form action="'.Tools::getConfig('server/location').'/review/review_article.php" method="post" target="_blank"><input type="hidden" name="articleNumber" value="'.$articleNumber.'"/><input type="submit" class="buttonLink" value="Online Review"/> </form> </div>');
      fwrite($indexFile,'<div class="floatRight"><form action="'.$article->getFile().'" method="post"><input type="submit" class="buttonLink" value="Mở tập tin"/> </form> </div>');
      fwrite($indexFile,'<div class="floatRight"><form action="'.sprintf("review%04d.xml", $article->getArticleNumber()).'" method="post"><input type="submit" class="buttonLink" value="Mở đánh giá XML"/> </form> </div>');
      fwrite($indexFile,'<div class="clear"></div></div>');
    }

    fwrite($indexFile, '</body></html>');
    fclose($indexFile);

    copy("../css/zip_".Tools::getConfig("miscellaneous/reviewSkin").".css",$tmpdir."/zip.css");

    header('Content-type: application/zip');
    header('Content-Disposition: attachment; filename="assigned_' . $currentReviewer->getLogin() . '.zip"');
    passthru('cd '.$tmpdir.'; zip -q -r1  - *');
    
    exec("rm -r ".$tmpdir);
  }
}


?>
