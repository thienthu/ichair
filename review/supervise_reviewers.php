<?php 

/*
 * Copyright (C) 2006, 2007 Thomas Baigneres, Matthieu Finiasz
 *
 * This file is part of iChair.
 *
 * iChair is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 * 
 * iChair is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

?><?php $page_title="Phân Quyền";//'Reviewers Access Privileges';
include '../utils/tools.php';
include 'header.php';

if($currentReviewerGroup == Reviewer::$CHAIR_GROUP) {

  $reviewers = Reviewer::getAllActiveReviewers();
  $allArticles = Article::getAllArticles();
  $observers = Reviewer::getAllByGroup(Reviewer::$OBSERVER_GROUP);
  $allcategories;
  if (Tools::useCategory()) {
    $allcategories = Tools::getAllCategories();
  }

  if (count($_POST) != 0) {
    foreach($reviewers as $reviewer) {
      $reviewerNumber = $reviewer->getReviewerNumber();
      if (($_POST['allow'.$reviewerNumber]) && ($_POST['allow'.$reviewerNumber] != $reviewer->getAllowedToViewOtherReviews())) {
        $reviewer->setAllowedToViewOtherReviews($_POST['allow'.$reviewerNumber]);
      }
    }
    foreach($observers as $reviewer) {
      $reviewerNumber = $reviewer->getReviewerNumber();
      if (($_POST['allow'.$reviewerNumber]) && ($_POST['allow'.$reviewerNumber] != $reviewer->getAllowedToViewOtherReviews())) {
        $reviewer->setAllowedToViewOtherReviews($_POST['allow'.$reviewerNumber]);
      }
    }
   ?>
  <div class="OKmessage">Cập nhật quyền truy cập của người đánh giá thành công.<!--The reviewers access privileges have been updated succesfully.-->
  </div>  
<?php }
  ?>
<script>
function checkAll(theBox, rExp){
  var elem = theBox.form.elements;
  for(var i=0;i<elem.length;i++){
    if(elem[i].type=="radio" && elem[i].id.match(rExp)){
       elem[i].checked = true;
    }
  }
}
</script>
<form action="supervise_reviewers.php" method="post">
  <center>
    <table class="dottedTable"> 
      <tr>
	<th class="topRow">&nbsp;</th>
        <th class="topRow leftAlign">Người đánh giá<!--Reviewer--></th>
        <th class="topRow">Đánh giá <br /> hoàn thành/Tổng số<br /> đánh giá<!--Completed / Total<br />Reviews--></th>
        <th class="topRow">Quyền truy cập<br />không thể xem<!--Access to<br />no reviews--></th>
        <th class="topRow">Quyền truy cập<br />đánh giá cấp 1<!--Access to reviews<br />of unassigned articles--></th>
        <th class="topRow">Quyền truy cập<br />đánh giá cấp 1<!--Access to reviews of<br />unassigned &ấms; reviewed articles--></th>
        <th class="topRow">Quyền truy cập<br />có thể xem<!--Access to<br />all reviews--></th>
      </tr>
      <tr>
        <td class="topRow"></td>
        <td class="topRow"></td>
        <td class="topRow"></td>
        <td class="topRow"><label class="buttonLink" onClick='javascipt:checkAll(this,"none");' />Chọn tất cả <!--Check all--></label></td>
        <td class="topRow"><label class="buttonLink" onClick='javascipt:checkAll(this,"unassigned");' />Chọn tất cả <!--Check all--></label></td>
        <td class="topRow"><label class="buttonLink" onClick='javascipt:checkAll(this,"finished");' />Chọn tất cả <!--Check all--></label></td>
        <td class="topRow"><label class="buttonLink" onClick='javascipt:checkAll(this,"all");' />Chọn tất cả <!--Check all--></label></td>
      </tr>
      <?php 
     foreach($reviewers as $reviewer) {
	$reviewerNumber = $reviewer->getReviewerNumber();
	$completedAssignedArticles = $reviewer->getAssignedArticlesByStatus(Assignement::$COMPLETED);
	$inProgressAssignedArticles = $reviewer->getAssignedArticlesByStatus(Assignement::$INPROGRESS);
	$notStartedAssignedArticles = $reviewer->getAssignedArticlesByStatus(Assignement::$VOID);
        $completedAuthorizedArticles = $reviewer->getAuthorizedArticlesByStatus(Assignement::$COMPLETED);
	$inProgressAuthorizedArticles = $reviewer->getAuthorizedArticlesByStatus(Assignement::$INPROGRESS);
	$finished = false;
	if (count($inProgressAssignedArticles) + count($notStartedAssignedArticles) == 0) {
	  $finished = true;
        }
       ?>
         <tr>
	   <td>
	     <?php if (count($completedAssignedArticles) + count($inProgressAssignedArticles) + count($notStartedAssignedArticles) + count($completedAuthorizedArticles) + count($inProgressAuthorizedArticles) == 0) {
		  print('<div class="buttonLink">Chưa có đánh giá'//No Review
        .'</div>');
		} else {?>
	     <div class="popUp">
	       <a href="view_reviews.php?reviewerNumber=<?php print($reviewerNumber); ?>" target="_blank">
	         Đã có đánh giá<!--Reviews-->
	       </a>
	       <div class="hidden">
		 <div class="paperBox">
		   <div class="paperBoxDetails">
	       <?php 
		if (count($completedAssignedArticles) + count($inProgressAssignedArticles) + count($notStartedAssignedArticles) != 0) {
		  print("Danh sách bài viết phân công:"//List of Assigned Articles:
        ."<br />");
		  print('<table class="articlesListTable">' . "\n");
		  print('<tr>' . "\n");
		  print('<td>Số và Tiêu để'//Number and Title
                    .'</td>');
		  print('<td>Tác giả'//Authors
                    .'</td>');
		  if(Tools::useCategory()) {
		    print('<td>Thể loại'//Category
                      .'</td>');
    		  }
		  print('</tr>' . "\n");
		  foreach($completedAssignedArticles as $articleNumber) {
		    $article = $allArticles[$articleNumber];
		    if(!is_null($article)) {
		      print('<tr>' . "\n");
		      $title = $article->getTitle();
		      print('<td><span class="completedReview">' . $article->getArticleNumber() . "</span>: " . Tools::HTMLsubstr($article->getTitle(),38) . '</td>');
		      print('<td>' . Tools::HTMLsubstr($article->getAuthors(),25) . '</td>');
		      if(Tools::useCategory()) {
		        print('<td>' . htmlentities($allcategories[$article->getCategory()], ENT_COMPAT | ENT_HTML401, 'UTF-8') . '</td>');
		      }
		      print('</tr>' . "\n");
		    }
		  }
                  foreach($inProgressAssignedArticles as $articleNumber) {
		    $article = $allArticles[$articleNumber];
                    if(!is_null($article)) {
                      print('<tr>' . "\n");
                      $title = $article->getTitle();
                      print('<td><span class="inProgressReview">' . $article->getArticleNumber() . "</span>: " . Tools::HTMLsubstr($article->getTitle(),38) . '</td>');
                      print('<td>' . Tools::HTMLsubstr($article->getAuthors(),25) . '</td>');
                      if(Tools::useCategory()) {
                        print('<td>' . htmlentities($allcategories[$article->getCategory()], ENT_COMPAT | ENT_HTML401, 'UTF-8') . '</td>');
                      }
                      print('</tr>' . "\n");
                    }
                  }
                  foreach($notStartedAssignedArticles as $articleNumber) {
		    $article = $allArticles[$articleNumber];
                    if(!is_null($article)) {
                      print('<tr>' . "\n");
                      $title = $article->getTitle();
                      print('<td><span class="voidReview">' . $article->getArticleNumber() . "</span>: " . Tools::HTMLsubstr($article->getTitle(),38) . '</td>');
                      print('<td>' . Tools::HTMLsubstr($article->getAuthors(),25) . '</td>');
                      if(Tools::useCategory()) {
                        print('<td>' . htmlentities($allcategories[$article->getCategory()], ENT_COMPAT | ENT_HTML401, 'UTF-8') . '</td>');
                      }
                      print('</tr>' . "\n");
                    }
                  }
		  print('</table>');
		}
                if (count($completedAuthorizedArticles) + count($inProgressAuthorizedArticles) != 0) {
                  print("Danh sách bài viết khác:"//List of Other Articles:
                    ."<br />");
                  print('<table class="articlesListTable">' . "\n");
                  print('<tr>' . "\n");
                  print('<td>Số và Tiêu để'//Number and Title
                    .'</td>');
                  print('<td>Tác giả'//Authors
                    .'</td>');
                  if(Tools::useCategory()) {
                    print('<td>Thể loại'//Category
                      .'</td>');
                  }
                  print('</tr>' . "\n");
                  foreach($completedAuthorizedArticles as $articleNumber) {
		    $article = $allArticles[$articleNumber];
                    if(!is_null($article)) {
                      print('<tr>' . "\n");
                      $title = $article->getTitle();
                      print('<td><span class="completedReview">' . $article->getArticleNumber() . "</span>: " . Tools::HTMLsubstr($article->getTitle(),38) . '</td>');
                      print('<td>' . Tools::HTMLsubstr($article->getAuthors(),25) . '</td>');
                      if(Tools::useCategory()) {
                        print('<td>' . htmlentities($allcategories[$article->getCategory()], ENT_COMPAT | ENT_HTML401, 'UTF-8') . '</td>');
                      }
                      print('</tr>' . "\n");
                    }
                  }
                  foreach($inProgressAuthorizedArticles as $articleNumber) {
		    $article = $allArticles[$articleNumber];
                    if(!is_null($article)) {
                      print('<tr>' . "\n");
                      $title = $article->getTitle();
                      print('<td><span class="inProgressReview">' . $article->getArticleNumber() . "</span>: " . Tools::HTMLsubstr($article->getTitle(),38) . '</td>');
                      print('<td>' . Tools::HTMLsubstr($article->getAuthors(),25) . '</td>');
                      if(Tools::useCategory()) {
                        print('<td>' . htmlentities($allcategories[$article->getCategory()], ENT_COMPAT | ENT_HTML401, 'UTF-8') . '</td>');
                      }
                      print('</tr>' . "\n");
                    }
                  }
                  print('</table>');
                }
               ?>
		 </div>
	       </div>
             </div>
	     <?php } ?>
	   </td>
	   <td class="leftAlign"><?php Tools::printHTML($reviewer->getFullName()); ?></td>
	   <td>
	     <div class="bigNumber">
		<span class="<?php if($finished) print("completedReview"); else print("voidReview");?>" >
	     <?php print(count($completedAssignedArticles)); ?>/<?php print(count($completedAssignedArticles) + count($inProgressAssignedArticles) + count($notStartedAssignedArticles)); ?></span><?php if (count($completedAuthorizedArticles) != 0) print('<span class="inProgressReview">+'.count($completedAuthorizedArticles).'</span>'); ?>
	     </div>
	   </td>
	   <?php 
	     $access = $reviewer->getAllowedToViewOtherReviews();
	     print('<td><input class="noBorder" type="radio" id="none' . $reviewerNumber . '" name="allow' . $reviewerNumber . '" value="' . Reviewer::$ALLOWED_NONE . '"');
	     if($access == Reviewer::$ALLOWED_NONE) print(' checked="checked"');
	     print(' /></td>');
	     print('<td><input class="noBorder" type="radio" id="unassigned' . $reviewerNumber . '" name="allow' . $reviewerNumber . '" value="' . Reviewer::$ALLOWED_UNASSIGNED . '"');
	     if($access == Reviewer::$ALLOWED_UNASSIGNED) print(' checked="checked"');
	     print(' /></td>');
	     print('<td><input class="noBorder" type="radio" id="finished' . $reviewerNumber . '" name="allow' . $reviewerNumber . '" value="' . Reviewer::$ALLOWED_FINISHED . '"');
	     if($access == Reviewer::$ALLOWED_FINISHED) print(' checked="checked"');
	     print(' /></td>');
	     print('<td><input class="noBorder" type="radio" id="all' . $reviewerNumber . '" name="allow' . $reviewerNumber . '" value="' . Reviewer::$ALLOWED_ALL . '"');
	     if($access == Reviewer::$ALLOWED_ALL) print(' checked="checked"');
	     print(' /></td>');
	   ?>
	 </tr>
       <?php 
     }
     foreach($observers as $observer) {
       $reviewerNumber = $observer->getReviewerNumber();
       print('<tr><td>&nbsp;</td><td class="leftAlign">');
       Tools::printHTML($observer->getFullName());
       print('</td><td><div class="bigNumber">-</div></td>');
       $access = $observer->getAllowedToViewOtherReviews();
       print('<td><input class="noBorder" type="radio" id="none' . $reviewerNumber . '" name="allow' . $reviewerNumber . '" value="' . Reviewer::$ALLOWED_NONE . '"');
       if($access == Reviewer::$ALLOWED_NONE) print(' checked="checked"');
       print(' /></td>');
       print('<td></td><td></td>');
       print('<td><input class="noBorder" type="radio" id="all' . $reviewerNumber . '" name="allow' . $reviewerNumber . '" value="' . Reviewer::$ALLOWED_ALL . '"');
       if($access == Reviewer::$ALLOWED_ALL) print(' checked="checked"');
       print(' /></td>');
       print('</tr>');
     }
      ?>
    </table>
    <input type="submit" class="buttonLink bigButton" name="update" value="Cập nhật quyền truy cập" />
  </center>
</form>
<table>
<tr><td style="color:#F00; font-size:16px; font-weight:bold ">* Chú thích:</td></tr>
  <tr>
    <td><strong>- Quyền truy cập đánh giá cấp 1:</strong> có quyền truy cập đánh giá bài viết chưa được phân công.</td>
  </tr>
  <tr>
    <td><strong>- Quyền truy cập đánh giá cấp 2:</strong> có quyền truy cập đánh giá bài viết chưa được phân công và bài viết đã được đánh giá.</td>
  </tr>
</table>
<div class="clear bottomSpacer"></div>

<?php }?>

<?php include('footer.php'); ?>
