<?php 

/*
 * Copyright (C) 2006, 2007 Thomas Baigneres, Matthieu Finiasz
 *
 * This file is part of iChair.
 *
 * iChair is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 * 
 * iChair is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

?><?php 
include '../utils/tools.php';

$currentReviewer = Reviewer::getReviewerByLogin(); 

if((!is_null($currentReviewer))) {
  header('Content-type: text/plain');
  header('Content-Disposition: attachment; filename="abstracts_' . $currentReviewer->getLogin() . '.txt"');
  $articleNumbers = $currentReviewer->getNotBlockedArticles();
  $allArticles = Article::getAllArticles();
  $allcategories;
  if (Tools::useCategory()) {
    $allcategories = Tools::getAllCategories();
  }
  foreach($articleNumbers as $articleNumber) {
    $article = $allArticles[$articleNumber];
    /* Output the list in txt format */
    print("Bài viết "//Article " 
      . $articleNumber . "\n");
    print("Tiêu đề: "//Title: " 
      . $article->getTitle() . "\n");
    if(!Tools::authorsAreAnonymous()) {
      print("Tác giả: "//Authors: " 
        . $article->getAuthors() . "\n");
    }
    if(Tools::useAffiliations() && (!Tools::authorsAreAnonymous())) {
      print("Tổ chức: "//Affiliations: "
       . $article->getAffiliations() . "\n");
    }
    if(Tools::useCountry() && (!Tools::authorsAreAnonymous())) {
      print("Quốc gia: "//Countries: "
       . $article->getCountry() . "\n");
    }
    if(Tools::useCategory()) {
      print("Thể loại: "//Category: " 
        . $allcategories[$article->getCategory()] . "\n");
    }
    if(Tools::useKeywords()) {
      print("Từ khoá: "//Keywords: " 
        . $article->getKeywords() . "\n");
    }
    $ccs = array();
    if (Tools::useCustomCheck1() && !Tools::chaironlyCustomCheck1() && $article->getIsCustomCheck1()) {
      $ccs[] = Tools::getCustomCheck1();
    }
    if (Tools::useCustomCheck2() && !Tools::chaironlyCustomCheck2() && $article->getIsCustomCheck2()) {
      $ccs[] = Tools::getCustomCheck2();
    }
    if (Tools::useCustomCheck3() && !Tools::chaironlyCustomCheck3() && $article->getIsCustomCheck3()) {
      $ccs[] = Tools::getCustomCheck3();
    }
    if (count($ccs) > 0) {
      print("Thông tin bổ sung: "//Additional info: " 
        . implode($ccs, ", ") . "\n");
    }
    print("Tóm tắc:\n"//Abstract:\n" 
      . wordwrap($article->getAbstract()) . "\n");
    print("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  }
}

?>
