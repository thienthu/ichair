<?php 

/*
 * Copyright (C) 2006, 2007 Thomas Baigneres, Matthieu Finiasz
 *
 * This file is part of iChair.
 *
 * iChair is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 * 
 * iChair is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

?><?php $page_title="Xem Tra Bài Viết Gửi";//'Examine Submissions';
include '../utils/tools.php';
include 'header.php';

if($currentReviewerGroup == Reviewer::$CHAIR_GROUP) {

if((Tools::readPost('redisplay') == "Show All Submissions") && ($currentReviewer->sortIsRecentOnly())){
  $currentReviewer->setSortKeyString($currentReviewer->getSortKey(), $currentReviewer->sortIsReversed(), $currentReviewer->sortIsExtended(), $currentReviewer->sortIsRestricted(), false, $currentReviewer->sortUseSnapshot());
} else if ((Tools::readPost('redisplay') == "Only Show New Submissions/Revisions") && (!$currentReviewer->sortIsRecentOnly())) {
  $currentReviewer->setSortKeyString($currentReviewer->getSortKey(), $currentReviewer->sortIsReversed(), $currentReviewer->sortIsExtended(), $currentReviewer->sortIsRestricted(), true, $currentReviewer->sortUseSnapshot());
}

print('<center>');
Submission::printSummaryTable();
print('</center>');


if(Tools::readPost('set_visit_date') != "") {
  $date = Tools::readPost('date');
  if($currentReviewer->setLastVisitDate($date)) {
    print('<div class="OKmessage">Thiết lập ngày truy cập cuối cùng là ngày hiện tại.'//Your last visit date has been successfully set to the current time.
      .'</div>');
  }
}


?>

<div class="floatRight rightAlign">
  Ngày truy cập cuối cùng:<!--Last visit:-->
  <em>
  <?php 
    if(Tools::use12HourFormat()) {
      print(date("d/m/Y - h:i&#160;a", $currentReviewer->getLastVisitDate()));
    } else {	      
      print(date("d/m/Y - H:i", $currentReviewer->getLastVisitDate()));
    }
  ?>
  </em>
  <form action="examine.php" method="post">
    <input type="hidden" name="date" value="<?php print(date("U"));?>" />
    <input type="submit" class="buttonLink" name="set_visit_date" value="Thiết lập ngày truy cập cuối cùng là ngày hiện tại"/>
  </form>
</div>

<form action="examine.php" method="post">
<?php if ($currentReviewer->sortIsRecentOnly()) { ?>
  <input type="submit" class="buttonLink bigButton" name="redisplay" value="Hiện tất cả bài viết"/>
<?php } else { ?>
  <input type="submit" class="buttonLink bigButton" name="redisplay" value="Chỉ hiện bài viết mới gửi/sửa"/>
<?php } ?>
</form>
<div class="clear"></div>

<?php 
$submissions = Submission::getAllSubmissions();
for ($i = count($submissions) - 1 ; $i>=0; $i--){
  $submissionNumber = $submissions[$i]->getSubmissionNumber();
  $lastVersion = $submissions[$i]->getLastVersion();
  if ((!$currentReviewer->sortIsRecentOnly()) || ($lastVersion->getDate() >= $currentReviewer->getLastVisitDate())) {
  print('<div class="paperBox" id="nbr' . $submissionNumber . '"><div class="paperBoxTitle">');
  if ($lastVersion->getDate() >= $currentReviewer->getLastVisitDate()) {
    print('<div class="paperBoxNumber"><div class="maybeRejectArticle">Bài gửi ' //Submission 
      .  $submissionNumber . '</div></div>');
  } else {
    print('<div class="paperBoxNumber">Bài gửi '//Submission '
     .  $submissionNumber . '</div>');
  }
?>

<div class="floatRight">
  <form action="examine_versions.php" method="post">
    <input type="hidden" name="submissionNumber" value="<?php print($submissionNumber); ?>" />
    <input type="submit" class="buttonLink" value="Xem tất cả các phiên bản" />
  </form>
</div>

<?php  
  $submissions[$i]->printInfo();
  print("\n</div>\n");
  if ($submissions[$i]->getIsWithdrawn()) {
    print("<div class=\"paperBoxDetailsWithdrawn\">\n");
  } else {
    if ($submissions[$i]->getIsCommitteeMember()) {
      print("<div class=\"paperBoxDetailsCommittee\">\n");
    } else {
      print("<div class=\"paperBoxDetails\">\n");
    }
  }
  if (Tools::useCountry()) {
    Tools::printContinentDivs($lastVersion->getCountryArray());
  } else {
    print('<div><div><div>');
  }
  $submissions[$i]->printCustomCheckboxes(true);
  $lastVersion->printLong();
  $lastVersion->printShort();
  print("<br />Ban tổ chức bình luận:"//Chair comments:
  	."<br />\n");
  $submissions[$i]->printAllChairComments();
  $submissions[$i]->printJavascriptWarnings();
  $submissions[$i]->printMD5collisions();
  print('</div></div></div></div>');
  $submissions[$i]->printDownloadLink();
  $submissions[$i]->printEditChairCommentsLink();
  if ($submissions[$i]->getIsCommitteeMember()) {
    $submissions[$i]->printNotCommitteeLink();
  } else {
    $submissions[$i]->printCommitteeLink();
  }
  if ($submissions[$i]->getIsWithdrawn()) {
    $submissions[$i]->printUnwithdrawLink();
  } else {
    $submissions[$i]->printWithdrawLink();
  }
  $submissions[$i]->printTrashLink();
  print('<div class="clear"></div>');
  print('</div>');
}
}
}  
?>

<?php include('footer.php'); ?>
