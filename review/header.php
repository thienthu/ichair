<?php 

/*
 * Copyright (C) 2006, 2007 Thomas Baigneres, Matthieu Finiasz
 *
 * This file is part of iChair.
 *
 * iChair is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 * 
 * iChair is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

?><?php 
header("Content-type: text/html; charset=UTF-8");
 date_default_timezone_set('asia/saigon');
?>
<!DOCTYPE html >
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-type" content="text/html; charset=UTF-8" />
<title><?php print(htmlentities(Tools::getConfig("conference/name"), ENT_COMPAT | ENT_HTML401, 'UTF-8')." - ".$page_title); ?></title>
<link href="../css/review_<?php print(Tools::getConfig("miscellaneous/reviewSkin"));?>.css" rel="stylesheet" type="text/css" media="screen" />
<link href="../css/print_<?php print(Tools::getConfig("miscellaneous/reviewSkin"));?>.css" rel="stylesheet" type="text/css" media="print" />
<link rel="SHORTCUT ICON" type="image/png" href="../images/icon_review.png" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="viewport" content="width=device-width">
<link rel="stylesheet" href="../css/bootstrap.css" media="screen">
<link rel="stylesheet" href="../css/bootswatch.min.css">
</head>
<body>
<?php 

/* Before the reviewer database can be accessed, the admin has to configure it properly. */
/* The path to the reviews must exist and so shall the config.xml file */
if(!file_exists('../utils/config.xml') || !file_exists(Tools::getConfig('server/reviewsPath'))) {
    ?>
    <div class="ERRmessage">
Đường dẫn đến người đánh giá chưa được cấu hình. Vui lòng sử dụng chức năng quản trị để thiết lập cấu hình.    
<!--The path to the reviewers database does not seem
    to be configured yet. Please use the admin functionalities of this
    software to configure iChair according to your own needs. For this problem, 
    the <em>Reviews Path</em> should be configured properly.--></div>
    <?php 
    return false;
}

$currentReviewerGroup = Reviewer::getGroupByLogin(); 
$currentReviewer = Reviewer::getReviewerByLogin(); 
if(!is_null($currentReviewer)) {

$currentReviewer->autoUpdateVisitDate();

/* boolean used to know if there are some review guidelines files*/
$revHTML = Tools::hasReviewGuidelinesHTML("..");
$revPDF  = Tools::hasReviewGuidelinesPDF("..");
$revTXT  = Tools::hasReviewGuidelinesTXT("..");

?>

<div class="row">
  <div class="col-xs-0 col-sm-0 col-md-1 col-lg-1">  </div>
  <!-- div main col-->
  <div class="col-xs-12 col-sm-12 col-md-10 col-lg-10">
                <!-- Bắt đầu banner -->
      <div class="col-lg-12" style="margin: -44px 0px -15px 0px">
          <div class="panel panel-default">
            <div class="panel-body" style="padding: 4px !important;">
                <img src="../images/banner.png" style="max-width: 100%;">
            </div>
          </div>
      </div>
      <!-- Kết thúc banner -->

      <!-- Bắt đầu content div-->
      <div class="col-lg-12">
        <div class="row">
          <!-- Bắt đầu sidebar-->
          <div class=" col-md-3 col-lg-3" style="padding-right:5px" >
            <div class="panel-default">
              <div class="panel-body" style="padding:0;">
                      <div class="navigate" style="width:100%;">

                        <?php if($currentReviewerGroup == Reviewer::$CHAIR_GROUP) { ?>
                        <div class="navigateTitle"><a href="index_chair.php">Menu ban tổ chức<!--Chair Menu-->
                      </a></div>
                        <div class="navigateLinks tinymargins">
                          <p><a href="index_chair.php">Trang chủ<!--Chairs Main Page--></a></p>
                          <p><a href="mailing.php">Gửi mail<!--Mass Mailing--></a></p>
                          <p><a href="meeting_edit.php">Thông báo đến ủy ban<!--Messages to Committee--></a></p>
                          <p><a href="configuration.php">Chọn giai đoạn<!--Select iChair Phase---></a></p>
                          <p><a href="export_list.php">Xuất danh sách bài viết<!--Export Article List--></a></p>
                          <p><a href="stats.php">Thống kê hội thảo<!--Conference Statistics--></a></p>
                        </div>
                        <div class="navigateLinks">
                          <?php if((!Tools::restrictChairMenu()) || (Tools::getCurrentPhase() == Tools::$SUBMISSION)) { ?>
                            <p><a href="examine.php">Bài gửi hiện tại<!--Current Submissions--></a></p>
                            <p><a href="transfer.php">Tải bài gửi lên<!--Load Submissions for Review--></a></p>
                          <?php } ?>
                          <?php if((!Tools::restrictChairMenu()) || (Tools::getCurrentPhase() != Tools::$DECISION)) { ?>
                            <p><a href="assign_reviewers_list.php">Phân công người đánh giá<!--Assign Reviewers to Articles--></a></p>
                            <p><a href="assign_articles_list.php">Phân công bài đánh giá<!--Assign Articles to Reviewers--></a></p>
                          <?php } ?>
                          <?php if((!Tools::restrictChairMenu()) || (Tools::getCurrentPhase() == Tools::$ASSIGNATION)) { ?>
                            <p><a href="automatic_assignation.php">Tự động Phân công<!--Automatic Assignment--></a></p>
                          <?php } ?>
                          <?php if((!Tools::restrictChairMenu()) || (Tools::getCurrentPhase() == Tools::$REVIEW)) { ?>
                            <p><a href="supervise_reviewers.php">Quyền người đánh giá<!--Reviewers Access Privileges--></a></p>
                          <?php } ?>
                          <?php if((!Tools::restrictChairMenu()) || (Tools::getCurrentPhase() == Tools::$REVIEW) || (Tools::getCurrentPhase() == Tools::$DECISION)) { ?>
                            <p><a href="accept_and_reject.php">Chấp nhận<!--Accept--> &amp; Từ chối<!--Reject--></a></p>
                          <?php } ?>
                        </div>
                        <?php } ?>

                        <div class="navigateTitle"><a href="index.php">Menu người đánh giá<!--Reviewer Menu--></a></div>
                          <div class="navigateLinks">
                            <p><a href="index.php">Trang chủ<!--Main Page--></a></p>

                            <?php if($revHTML) { ?>
                              <p class="guidelines"><a class="guidelines" href="guidelines/guidelines.html" target="_blank">Hướng dẫn đánh giá<!--Review Guidelines--></a>
                              <?php if($revPDF) { ?>
                                <a class="noblock" href="guidelines/guidelines.pdf" target="_blank">[pdf]</a>
                              <?php } 
                                 if($revTXT) { ?>
                                <a class="noblock" href="guidelines/guidelines.txt" target="_blank">[text]</a>
                              <?php } ?>
                              </p>
                            <?php } else if($revPDF) { ?>
                              <p class="guidelines"><a class="guidelines" href="guidelines/guidelines.pdf" target="_blank">Hướng dẫn đánh giá<!--Review Guidelines--></a>
                              <?php if($revTXT) { ?>
                                <a class="noblock" href="guidelines/guidelines.txt" target="_blank">[text]</a>
                              <?php } ?>
                              </p>
                            <?php } else if($revTXT) { ?>
                              <p><a href="guidelines/guidelines.txt" target="_blank">Hướng dẫn đánh giá<!--Review Guidelines--></a></p>
                            <?php } ?>

                            <p><a href="meeting_show.php">Thông báo từ ban tổ chức<!--Messages from Chair--></a></p>
                            <?php if(Tools::getCurrentPhase() != Tools::$DECISION) {?>
                              <p><a href="profile.php">Hồ sơ cá nhân<!--Personal Profile--></a></p>
                            <?php } ?>
                            <?php if(Tools::getCurrentPhase() == Tools::$ELECTION) { ?>
                              <p><a href="affinity_list.php">Bài viết ưa thích<!--Preferred Articles--></a></p>
                            <?php } ?>
                            <?php if((Tools::getCurrentPhase() == Tools::$REVIEW) && ($currentReviewerGroup != Reviewer::$OBSERVER_GROUP)) { ?>
                              <p><a href="review.php">Đánh giá<!--Your Reviews--></a></p>
                            <?php } ?>
                            <?php if(((Tools::getCurrentPhase() == Tools::$REVIEW) || (Tools::getCurrentPhase() == Tools::$DECISION)) && ($currentReviewer->isAllowedToViewOtherReviews())) { ?>
                                <p><a href="discuss.php">Đánh giá<!--Reviews--> &amp; Thảo luận<!--Discussions--></a></p>
                            <?php } ?>
                            <p class="topLine"><a href="about.php">Giới thiệu iChair<!--About iChair--></a></p>
                          </div>

                        <div class="navigateTitle"><a href="http://www.time.gov/timezone.cgi?UTC/s/0/java" target="_new">Thời gian hiện tại<!--Current Time--></a></div>
                        <div class="navigateTimes">
                          <div class="timeTitleTop">Thời gian GMT+7<!--UTC Time--></div>
                          <?php if(Tools::use12HourFormat()) { ?>
                            <div class="time"><?php print(date("d/m/Y - h:i&#160;a")); ?></div>
                          <?php } else {?>
                            <div class="time"><?php print(date("d/m/Y - H:i")); ?></div>      
                          <?php } ?>
                        </div>

                        <?php if(Tools::getCurrentPhase() == Tools::$ELECTION) {?>
                        <div class="navigateTitle">Thời gian hết hạn chọn bào viết ưa thích<!--Preferred Articles Deadline--></div>
                        <div class="navigateTimes">
                          <div class="timeTitleTop">Thời gian GMT+7<!--UTC Time--></div>
                          <div class="time">
                             <?php 
                               $U = Tools::getUFromDateAndTime(Tools::getConfig("review/preferredDeadlineDate"), Tools::getConfig("review/preferredDeadlineTime"));
                               if(Tools::use12HourFormat()) {
                            print(date("d/m/Y - h:i&#160;a", $U)); 
                         } else {
                            print(date("d/m/Y - H:i", $U)); 
                         }
                             ?>
                          </div>
                          <div class="timeTitle">Thời gian còn lại<!--Time left--></div>
                          <div class="time">
                          <?php 
                            $timeLeft = $U - date("U");
                            if($timeLeft > 0) {
                              print(Tools::stringTimeLeft($timeLeft));
                            } else {
                              print("<div class=\"timeUp\">Thời gian đã hết!"//Time is up!
                                ."</div>");
                            }
                          ?>
                          </div>
                        </div>
                        <?php } ?>


                        <?php if((Tools::getCurrentPhase() == Tools::$REVIEW) && (!Tools::hideTimeBox())) {?>
                        <div class="navigateTitle">Thời gian hết hạn đánh giá<!--Review Deadline--></div>
                        <div class="navigateTimes">
                          <div class="timeTitleTop">Thời gian GMT+7<!--UTC Time--></div>
                          <div class="time">
                            <?php 
                              $U = Tools::getUFromDateAndTime(Tools::getConfig("review/deadlineDate"), Tools::getConfig("review/deadlineTime"));
                              if(Tools::use12HourFormat()) {
                            print(date("d/m/Y - h:i&#160;a", $U)); 
                              } else {
                            print(date("d/m/Y - H:i", $U)); 
                        }
                            ?>
                          </div>
                          <div class="timeTitle">Thời gian còn lại<!--Time left--></div>
                          <div class="time">
                          <?php 
                            $timeLeft = $U - date("U");
                            if($timeLeft > 0) {
                              print(Tools::stringTimeLeft($timeLeft));
                            } else {
                              print("<div class=\"timeUp\">Time is up!</div>");
                            }
                          ?>
                          </div>
                        </div>
                        <?php } ?>

                        <?php if(($currentReviewerGroup == Reviewer::$CHAIR_GROUP) && (Tools::getCurrentPhase() == Tools::$SUBMISSION)) { ?>
                        <div class="navigateTitle">Tắt hệ thống<!--Server Shutdown--></div>
                        <div class="navigateTimes">
                          <div class="timeTitleTop">Thời gian GMT+7<!--UTC Time--></div>
                          <div class="time">
                            <?php 
                              $U = Tools::getUFromDateAndTime(Tools::getConfig("server/shutdownDate"), Tools::getConfig("server/shutdownTime"));
                              if(Tools::use12HourFormat()) {
                            print(date("d/m/Y - h:i&#160;a", $U)); 
                              } else {
                            print(date("d/m/Y - H:i", $U)); 
                        }
                            ?>
                          </div>
                          <div class="timeTitle">Thời gian còn lại<!--Time left--></div>
                          <div class="time">
                          <?php 
                            $timeLeft = $U - date("U");
                            if($timeLeft > 0) {
                              print(Tools::stringTimeLeft($timeLeft));
                            } else {
                              print("<div class=\"timeUp\">Time is up!</div>");
                            }
                          ?>
                          </div>
                        </div>
                        <?php } ?>

                      </div>
                

            </div>
          </div>
      </div>
      <!-- Kết thúc sidebar -->
        <!-- Bắt đầu main content -->
      <div class="col-md-9 col-lg-9" style="padding-left:5px" >
          <div class="panel panel-default">
            <div class="panel-body" style="padding: 4px !important; font-size:11px !important">          
              <h1><?php print(htmlentities(Tools::getConfig("conference/name"), ENT_COMPAT | ENT_HTML401, 'UTF-8')." - " . htmlentities($currentReviewer->getFullName()) . " - ".$page_title); ?></h1>
              <?php } ?>


