<?php 

/*
 * Copyright (C) 2006, 2007 Thomas Baigneres, Matthieu Finiasz
 *
 * This file is part of iChair.
 *
 * iChair is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 * 
 * iChair is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

?><?php 
include('../utils/tools.php');

$articleNumber;
if(Tools::readPost('action') == "any_xml") {
  
  if ($_FILES['file']['name'] == "") {
    $articleNumber = '???';
  } else if ($_FILES['file']['error'] != UPLOAD_ERR_OK) {
    $articleNumber = '???';
  } else {
    preg_match('/<articleNumber>([0-9]+)<\/articleNumber>/', file_get_contents($_FILES['file']['tmp_name']), $bozo);
    $articleNumber = $bozo[1];
  }

} else {
  $articleNumber = Tools::readPost('articleNumber');
}


$page_title='Đánh Giá Bài Viết '//Review Article '
.htmlentities($articleNumber, ENT_COMPAT | ENT_HTML401, 'UTF-8');
include('header.php');

if((!is_null($currentReviewer)) && ($currentReviewerGroup != Reviewer::$OBSERVER_GROUP) && (Tools::getCurrentPhase() == Tools::$REVIEW)) {

  if($articleNumber == "???") {
    print('<div class="ERRmessage">Không thể đọc số bài viết từ tập tin XML.'//Could not read the article number from your XML file.
      .'</div>');
  }

  $article = Article::getByArticleNumber($articleNumber);
  $assignement = Assignement::getByNumbers($articleNumber, $currentReviewer->getReviewerNumber());
  $reviewStatus = $assignement->getReviewStatus();

  if(!is_null($assignement) && $assignement->getAssignementStatus() != Assignement::$BLOCKED) {

    /* variables that may be initialized by post or xml file */
    $subreviewer = "";
    $overallGrade = "";
    $confidenceLevel = "";
    $technicalQuality = "";
    $editorialQuality = "";
    $suitability = "";
    $bestPaper = "";
    $toProgramCommittee = "";
    $toAuthors = "";
    $personalNotes = "";
    $contentInitialized = false;
    $status = "";
    $statusChanged = false;

    $article->printDetailsBoxWithDownload($currentReviewer); ?>
    
    <center>
      <input type="submit" class="buttonLink bigButton" value="Đóng" onclick="javascript:window.close()" />
    </center>

    <?php 
    /* Display the messages from the chair concerning this article */ 

    $specificMessages = Meeting::getNonTrashedAuthorizedArticleMessages($articleNumber);
    foreach($specificMessages as $message) { ?>
      <br />
      <center>
        <table>
	  <tr>
	    <td>
	      <div class="paperBoxTitle" style="margin: 0;">
	        <div class="floatRight">Ngày:<!--Date:--> <?php print(date("d/m/Y h:i", $message->getDate())); ?></div>
	        Thông báo từ ban tổ chức
          <!--Message from the Chair-->
		<br />
	      </div>
	    </td>
	  </tr>
	  <tr>
	    <td><div class="messageBox"><?php Tools::printHTMLbr($message->getContent()); ?></div></td>
	  </tr>
	</table>
      </center>
      <?php 
    } 
    ?>


    <h2>Tập tin đánh giá XML<!--XML Review File--></h2> 

    <center>
    <table class="xmlTable">
      <tr>
        <td>Tải xuống tập tin đánh giá:<!--Dowload Review File:--></td>
        <td>Tải lên tập tin đánh giá:<!--Upload Review File:--></td>
      </tr>
      <tr>
        <td>
	  <form action="download_xml.php" method="post">
	  <input type="hidden" name="articleNumber" value="<?php print($articleNumber); ?>" />
	  <input type="submit" class="buttonLink" value="Tải tập tin XML xuống" />
	  </form>
	</td>
	<td>
        <form action="review_article.php" method="post" enctype="multipart/form-data">
       <input type="hidden" name="articleNumber" value="<?php print($articleNumber); ?>" />
	  <input type="submit" class="buttonLink" value="Tải tập tin XML lên" />
	  <br />
          <input name="file" type="file" size="40" />
          <input type="hidden" name="action" value="xml" />
          </form>
	</td>
      </tr>
    </table>
    </center>



<?php 

/**************************/
/* Update the review file */
/**************************/

if(Tools::readPost('action') == "manual" && Tools::readPost('reformat_text') != 'Làm lại') {

  $revisePreviousXMLFile = false;
  $logDiff = "";
  $recomputeAverage = false;

  /******************************************************************************************************/
  /* If there was a previous xml file, save all its regions to test the differences with the new values */
  /******************************************************************************************************/

  if(file_exists($article->getFolder() . $assignement->getXMLFileName())) {

    $revisePreviousXMLFile = true;

    $reviewDocument = new DOMDocument('1.0');
    $reviewDocument->load($article->getFolder() . $assignement->getXMLFileName());
    $domxPath = new DOMXpath($reviewDocument);
  
    $domNode = $domxPath->query("/xml/subreviewer")->item(0);
    if ($domNode->firstChild) { $oldSubreviewer = utf8_decode($domNode->firstChild->nodeValue); } else { $oldSubreviewer = null; } 
    $domNode = $domxPath->query("/xml/overallGrade")->item(0);
    if ($domNode->firstChild) { $oldOverallGrade = utf8_decode($domNode->firstChild->nodeValue); } else { $oldOverallGrade = null; }
    $domNode = $domxPath->query("/xml/confidenceLevel")->item(0);
    if ($domNode->firstChild) { $oldConfidenceLevel = utf8_decode($domNode->firstChild->nodeValue); } else { $oldConfidenceLevel = null; }
    $domNode = $domxPath->query("/xml/technicalQuality")->item(0);
    if ($domNode->firstChild) { $oldTechnicalQuality = utf8_decode($domNode->firstChild->nodeValue); } else { $oldTechnicalQuality = null; }
    $domNode = $domxPath->query("/xml/editorialQuality")->item(0);
    if ($domNode->firstChild) { $oldEditorialQuality = utf8_decode($domNode->firstChild->nodeValue); } else { $oldEditorialQuality = null; }
    $domNode = $domxPath->query("/xml/suitability")->item(0);
    if ($domNode->firstChild) { $oldSuitability = utf8_decode($domNode->firstChild->nodeValue); } else { $oldSuitability = null; }
    $domNode = $domxPath->query("/xml/bestPaper")->item(0);
    if ($domNode->firstChild) { $oldBestPaper = utf8_decode($domNode->firstChild->nodeValue); } else { $oldBestPaper = null; }
    $domNode = $domxPath->query("/xml/toProgramCommittee")->item(0);
    if ($domNode->firstChild) { $oldToProgramCommittee = utf8_decode($domNode->firstChild->nodeValue); } else { $oldToProgramCommittee = null; }
    $domNode = $domxPath->query("/xml/toAuthors")->item(0);
    if ($domNode->firstChild) { $oldToAuthors = utf8_decode($domNode->firstChild->nodeValue); } else { $oldToAuthors = null; }
    $domNode = $domxPath->query("/xml/personalNotes")->item(0);
    if ($domNode->firstChild) { $oldPersonalNotes = utf8_decode($domNode->firstChild->nodeValue); } else { $oldPersonalNotes = null; }
  
  }

  /* If the information is submitted through the html form... */

  $subreviewer = Tools::readPost('subreviewer');
  $overallGrade = explode(" ", Tools::readPost('overallGrade'));
  $overallGrade = $overallGrade[0];
  $confidenceLevel = explode(" ", Tools::readPost('confidenceLevel'));
  $confidenceLevel = $confidenceLevel[0];
  $technicalQuality = explode(" ", Tools::readPost('technicalQuality'));
  $technicalQuality = $technicalQuality[0];
  $editorialQuality = explode(" ", Tools::readPost('editorialQuality'));
  $editorialQuality = $editorialQuality[0];
  $suitability = explode(" ", Tools::readPost('suitability'));
  $suitability = $suitability[0];
  $bestPaper = explode(" ", Tools::readPost('bestPaper'));
  $bestPaper = $bestPaper[0];
  $toProgramCommittee = trim(Tools::readPost('toProgramCommittee'));
  $toAuthors = trim(Tools::readPost('toAuthors'));
  $personalNotes = trim(Tools::readPost('personalNotes'));
  
  $contentInitialized = true;
  
  /* Log the differences and possibly recompute average */

  if($revisePreviousXMLFile) {

    if($oldSubreviewer != $subreviewer) {
      $logDiff .= "<Subreviewer> ";
    }

    if($oldOverallGrade != $overallGrade) {
      $logDiff .= "<Overall Grade> ";
      $recomputeAverage = true;
    }

    if(Tools::useConfidenceLevel() && ($oldConfidenceLevel != $confidenceLevel)) {
      $logDiff .= "<Confidence Level> ";
      $recomputeAverage = true;      
    }
    if(Tools::useTechnicalQuality() && ($oldTechnicalQuality != $technicalQuality)) {
      $logDiff .= "<Technical Level> ";
    }
    if(Tools::useEditorialQuality() && ($oldEditorialQuality != $editorialQuality)) {
      $logDiff .= "<Editorial Quality> ";
    }
    if(Tools::useSuitability() && ($oldSuitability != $suitability)) {
      $logDiff .= "<Suitability> ";      
    }
    if(Tools::useBestPaper() && ($oldBestPaper != $bestPaper)) {
      $logDiff .= "<Best Paper> ";
    }

    if($oldToProgramCommittee != $toProgramCommittee) {
      $logDiff .= "<To Program Committee> ";
    }
    if($oldToAuthors != $toAuthors) {
      $logDiff .= "<To Authors> ";
    }

    /* recompute average when change between inProgress <-> Completed */
    if (Tools::readPost('review_status') != $reviewStatus) {
      $recomputeAverage = true;
      $statusChanged = true;
      $reviewStatus = Tools::readPost('review_status');
    }

  } else {
    $recomputeAverage = true;
  }



  /* create the xml file */
  
  $reviewDocument = Tools::createXMLReview(true,
					   $articleNumber, 
					   $article->getTitle(), $article->getAuthors(),
					   $currentReviewer->getFullName(), $subreviewer,
					   $overallGrade, $confidenceLevel, $technicalQuality, $editorialQuality, $suitability, $bestPaper,
					   $toProgramCommittee, $toAuthors, $personalNotes);
  
  /* Save the XML to the right file  */
    
  $file = $article->getFolder() . $assignement->getXMLFileName();
  
  if (file_exists($file)) {
    /* we backup it in the archive directory */
    $reviewDocument2 = new DOMDocument('1.0');
    $reviewDocument2->load($file);
    
    $domxPath = new DOMXpath($reviewDocument2);
    $domNode = $domxPath->query("/xml/date")->item(0);
    $oldReviewNewFileName = $article->getFolder() . "archive/" . sprintf("review%04d-", $currentReviewer->getReviewerNumber()) . utf8_decode($domNode->firstChild->nodeValue) .".xml";
    rename($file, $oldReviewNewFileName);
  }
  
  $reviewDocument->save($file);


  /* Test if the file we have just written can be read again ! */

  $testReviewDocument = new DOMDocument('1.0');
  if (!@$testReviewDocument->load($file)) {

    unlink($file);
    if (file_exists($oldReviewNewFileName)) {
      rename($oldReviewNewFileName, $file);
    }
    print('<div class="ERRmessage">');
    print('Không thể lưu đánh giá hợp lệ. Có thể là văn bản tập tin đánh giá có chứa một số ký tự đặc biệt không  phép trong mã UTF-8. 
       Vui lòng kiểm tra trình duyệt của bạn được cấu hình để hiển thị trang này trong mã UTF-8 (latin 1) và kiểm tra văn bản tập tin đánh giá đã một lần nữa.'
/*We were unable to save your review properly. 
      Maybe the text you entered contains some special characters which are not allowed in UTF-8 coding. 
      Please verify that your browser is configured to display this page in UTF-8 encoding (latin 1) and check the text you entered again.'*/
      );
    print('</div>');
    print('<div class="ERRmessage">');
    print('Đánh giá không lưu đuợc. Nếu rời khỏi trang này bây giờ thì nhưng thay đổi sẽ bị mất.'//Your review was NOT saved. If you leave this page now, your changes will be lost.'
      );
    print('</div>');

  } else {

    /* Update review status */
  
    if (Tools::readPost('review_status') == Assignement::$INPROGRESS) {

      $assignement->setReviewStatus(Assignement::$INPROGRESS);
      if($statusChanged) {
        Log::logReview($assignement, $currentReviewer, "review marked as in progress"); 
        $article->setLastModificationDate();
      }
      print('<div class="OKmessage">');    
      print('Đánh giá đã được cập nhật thành công. Đánh giá này vẫn được đánh dấu là <em> trong tiến trình </ em> và nguời đánh giá khác không thể nhìn thấy nó. 
         Hãy nhớ rằng bạn sẽ phải đánh dấu nó như <em> hoàn thành </ em> trước khi nó có thể được đưa vào tài khoản.'
        /*Your review has been updated successfully. 
        It is still marked as <em>in progress</em> and remains invisible to other reviewers. 
        Remember that you will have to mark it as <em>completed</em> before it can be taken into account.'*/
        );
      print('</div>');
  
    } else if (Tools::readPost('review_status') == Assignement::$COMPLETED){

      $assignement->setReviewStatus(Assignement::$COMPLETED);
      if ((!$revisePreviousXMLFile) || $statusChanged) {
        Log::logReview($assignement, $currentReviewer, "new review");
        $article->setLastModificationDate();      
      } else if($logDiff != "") {
        Log::logReview($assignement, $currentReviewer, $logDiff);
        $article->setLastModificationDate();
      } 
      print('<div class="OKmessage">');    
      print('Cập nhật đánh giá thành công.'//Your review has been updated successfully.'
        );
      print('</div>');
    }

    /* recompute average if necessary */
    if($recomputeAverage) {
      $article->computeAverage();
    }
  }

} else if((Tools::readPost('action') == "xml") || (Tools::readPost('action') == "any_xml")) {

  /* If the information is submitted through a xml file... */
  
  if ($_FILES['file']['name'] == "") {
    $status .= "Vui lòng cung cấp tập tin đánh giá\n";//Please provide the file corresponding to your review.\n";
  } else if ($_FILES['file']['error'] != UPLOAD_ERR_OK) {
    $status .= "Lỗi xảy trong quá trình nhận tập tin đánh giá\n";//An error occured when receiving your review file.\n";
  } else {
    
    $reviewDocument = new DOMDocument('1.0');
    if (!@$reviewDocument->load($_FILES['file']['tmp_name'])) {
      $status .= "Tập tin tải lên không phải là tập tin định dạng XML. Hãy chắc rằng các thẻ đã đuợc mở và đóng hợp lệ.\n";
      //The file you uploaded is not a valid XML file. Be sure that all the tags are opened and closed properly.\n";
    } else {
      if (!@$reviewDocument->schemaValidate("../utils/review.xsd")){
	$status .= "Tập tin tải lên không có cấu trúc XML dự kiến ​​(có thể bạn thay đổi một số tên thẻ). Tuy nhiên nó đã được xử lý bình thường. Hãy kiểm tra xem tất cả các mục đã được điền đúng \n";
  //The file you uploaded doesn't have the expected XML structure (maybe you changed some tag names). It has however been processed normally. Please check that all the fields have been filled properly.\n";
      }
      
      $domxPath = new DOMXpath($reviewDocument);
      
      $domNode = $domxPath->query("/xml/articleNumber")->item(0);
      if (!$domNode->firstChild) {
	$status .= "Đánh giá tải lên không chứa bất kỳ bài viết nào. Do đó không thể tải đánh giá lên\n";
  //The review you uploaded does not contain any aticle number. It cannot be uploaded without one.\n";
      } else if (!preg_match("/^0*". $articleNumber."$/", trim(utf8_decode($domNode->firstChild->nodeValue)))) {
	$status .= "Đánh giá tải lên tương ứng với bài viết số \""
  //The review you uploaded corresponds to the article number \""
   .htmlentities(utf8_decode($domNode->firstChild->nodeValue), ENT_COMPAT | ENT_HTML401, 'UTF-8') 
."\". Đánh giá không thể tải lên cho bài viết " . $articleNumber . ".\n";
   //."\". It cannot be uploaded for article " . $articleNumber . ".\n";
      } else {
	$contentInitialized = true;
	
	$domNode = $domxPath->query("/xml/articleTitle")->item(0);
        $artitle = ($domNode->firstChild) ? utf8_decode($domNode->firstChild->nodeValue) : "";
	if ($article->getTitle() != $artitle) {
	  $status .= "Cảnh báo: Tiêu đề bài viết \""
    //Warning: the article title \""
     . htmlentities($artitle, ENT_COMPAT | ENT_HTML401, 'UTF-8') ."\" trong đánh giá không trùng khớp với tiêu đề của bài viết " //."\" included in your review does not match that of article "
      . $articleNumber . " Bạn có chắc muốn tải lên?\n";
      //". Are you sure you uploaded the right review?\n";
	}
	
	Log::logUploadXML($assignement, $currentReviewer);
	
	$domNode = $domxPath->query("/xml/subreviewer")->item(0);
        if ($domNode->firstChild) {
	  $subreviewer = utf8_decode($domNode->firstChild->nodeValue);
        }
	$domNode = $domxPath->query("/xml/overallGrade")->item(0);
        if ($domNode->firstChild) {
	  $overallGrade = trim(utf8_decode($domNode->firstChild->nodeValue));
        }
	$status .= Tools::checkGrade($overallGrade, Tools::getConfig('review/overallGradeMin'), Tools::getConfig('review/overallGradeMax'), "overall grade");
	if (Tools::useConfidenceLevel()){
	  $domNode = $domxPath->query("/xml/confidenceLevel")->item(0);
          if ($domNode->firstChild) {
	    $confidenceLevel = trim(utf8_decode($domNode->firstChild->nodeValue));
          }
	  $status .= Tools::checkGrade($confidenceLevel, Tools::getConfig('review/confidenceLevelMin'), Tools::getConfig('review/confidenceLevelMax'), "confidence level");
	}
	if (Tools::useTechnicalQuality()) {
	  $domNode = $domxPath->query("/xml/technicalQuality")->item(0);
          if ($domNode->firstChild) {
	    $technicalQuality = trim(utf8_decode($domNode->firstChild->nodeValue));
          }
	  $status .= Tools::checkGrade($technicalQuality, Tools::getConfig('review/technicalQualityMin'), Tools::getConfig('review/technicalQualityMax'), "technical level");
	}
	if (Tools::useEditorialQuality()) {
	  $domNode = $domxPath->query("/xml/editorialQuality")->item(0);
          if ($domNode->firstChild) {
	    $editorialQuality = trim(utf8_decode($domNode->firstChild->nodeValue));
          }
	  $status .= Tools::checkGrade($editorialQuality, Tools::getConfig('review/editorialQualityMin'), Tools::getConfig('review/editorialQualityMax'), "editorial quality");
	}
	if (Tools::useSuitability()) {
	  $domNode = $domxPath->query("/xml/suitability")->item(0);
          if ($domNode->firstChild) {
	    $suitability = trim(utf8_decode($domNode->firstChild->nodeValue));
          }
	  $status .= Tools::checkGrade($suitability, Tools::getConfig('review/suitabilityMin'), Tools::getConfig('review/suitabilityMax'), "suitability");
	}
	if (Tools::useBestPaper()){
	  $domNode = $domxPath->query("/xml/bestPaper")->item(0);
          if ($domNode->firstChild) {
	    $bestPaper = trim(utf8_decode($domNode->firstChild->nodeValue));
          }
	  $status .= Tools::checkGrade($bestPaper, Tools::getConfig('review/bestPaperMin'), Tools::getConfig('review/bestPaperMax'), "best paper");
	}
	$domNode = $domxPath->query("/xml/toProgramCommittee")->item(0);
        if ($domNode->firstChild) {
	  $toProgramCommittee = trim(utf8_decode($domNode->firstChild->nodeValue));
        }
	$domNode = $domxPath->query("/xml/toAuthors")->item(0);
        if ($domNode->firstChild) {
	  $toAuthors = trim(utf8_decode($domNode->firstChild->nodeValue));
        }
	$domNode = $domxPath->query("/xml/personalNotes")->item(0);
        if ($domNode->firstChild) {
	  $personalNotes = trim(utf8_decode($domNode->firstChild->nodeValue));
        }
      }
    }
  }
  
  if ($status != "") {
    print('<div class="ERRmessage">');
    Tools::printHTMLbr($status);
    print('</div>');
  }

  if ($contentInitialized) {
    print('<div class="OKmessage">Tập tin tải lên đã điền vào mẫu bên dưới. Thay đổi này sẽ chỉ có hiệu lực khi bạn nhấn nút "Cập nhật đánh giá" ở phía dưới. Hãy kiểm tra xem tất cả mọi thứ là chính xác và không quên để đánh dấu việc đánh giá hoàn thành khi nó được hoàn tất.'
//The file you uploaded has been used to fill in the form below. Changes will only be effective once you press the "Update Review" button at the bottom. Please check that everything is correct and don\'t forget to mark the review as completed when it is finished.
.'.</div>');    
  }
} 


/* If the user is trying to reformat the text in the comments boxes... */
/* overwrite the values read from the xml by the posted values and apply the reformat functions to the comments. */
if(Tools::readPost('reformat_text') == 'Làm lại') {

  $subreviewer = Tools::readPost('subreviewer');
  $overallGrade = explode(" ", Tools::readPost('overallGrade'));
  $overallGrade = $overallGrade[0];
  $confidenceLevel = explode(" ", Tools::readPost('confidenceLevel'));
  $confidenceLevel = $confidenceLevel[0];
  $technicalQuality = explode(" ", Tools::readPost('technicalQuality'));
  $technicalQuality = $technicalQuality[0];
  $editorialQuality = explode(" ", Tools::readPost('editorialQuality'));
  $editorialQuality = $editorialQuality[0];
  $suitability = explode(" ", Tools::readPost('suitability'));
  $suitability = $suitability[0];
  $bestPaper = explode(" ", Tools::readPost('bestPaper'));
  $bestPaper = $bestPaper[0];
  $toProgramCommittee = Tools::reformatText(trim(Tools::readPost('toProgramCommittee')));
  $toAuthors = Tools::reformatText(trim(Tools::readPost('toAuthors')));
  $personalNotes = Tools::readPost('personalNotes');

  $reviewStatus = Tools::readPost('review_status');

  $contentInitialized = true;

  if(Tools::useToProgramCommittee() && Tools::useToAuthors()) {
    print('<div class="OKmessage"> <em>Bình luận từ Ủy ban Chương trình</em> và <em>Bình luận từ tác giả</em> đã được định dạng lại. Thay đổi này sẽ chỉ có hiệu lực khi bạn nhấm nút "Cập nhật giá"ở phía dưới.'
    //The <em>Comments to the Program Committee</em> and the <em>Comments to the Authors</em> have been reformated. Changes will only be effective once you press the "Update Review" button at the bottom.
      .'</div>');
  } else if(Tools::useToProgramCommittee()) {
    print('<div class="OKmessage"> <em>Bình luận từ Ủy ban Chương trình</em> đã được định dạng lại. Thay đổi này sẽ chỉ có hiệu lực khi bạn nhấm nút "Cập nhật giá"ở phía dưới.'
      //The <em>Comments to the Program Committee</em> have been reformated. Changes will only be effective once you press the "Update Review" button at the bottom.
      .'</div>');
  } else if(Tools::useToAuthors()) {
    print('<div class="OKmessage"><em>Bình luận từ tác giả </ em> đã được định dạng lại. Thay đổi này sẽ chỉ có hiệu lực khi bạn nhấm nút "Cập nhật giá"ở phía dưới '
      //The <em>Comments to the Authors</em> have been reformated. Changes will only be effective once you press the "Update Review" button at the bottom.
      .'</div>');
  } 

}

if (!$contentInitialized){
  if(file_exists($article->getFolder() . $assignement->getXMLFileName())) {

    /*************************************************************/
    /* we parse the file to initialise the form fields correctly */
    /*************************************************************/

    $reviewDocument = new DOMDocument('1.0');
    $reviewDocument->load($article->getFolder() . $assignement->getXMLFileName());
    $domxPath = new DOMXpath($reviewDocument);
  
    $domNode = $domxPath->query("/xml/subreviewer")->item(0);
    if ($domNode->firstChild) { $subreviewer = utf8_decode($domNode->firstChild->nodeValue); }
    $domNode = $domxPath->query("/xml/overallGrade")->item(0);
    if ($domNode->firstChild) { $overallGrade = utf8_decode($domNode->firstChild->nodeValue); }
    $domNode = $domxPath->query("/xml/confidenceLevel")->item(0);
    if ($domNode->firstChild) { $confidenceLevel = utf8_decode($domNode->firstChild->nodeValue); }
    $domNode = $domxPath->query("/xml/technicalQuality")->item(0);
    if ($domNode->firstChild) { $technicalQuality = utf8_decode($domNode->firstChild->nodeValue); }
    $domNode = $domxPath->query("/xml/editorialQuality")->item(0);
    if ($domNode->firstChild) { $editorialQuality = utf8_decode($domNode->firstChild->nodeValue); }
    $domNode = $domxPath->query("/xml/suitability")->item(0);
    if ($domNode->firstChild) { $suitability = utf8_decode($domNode->firstChild->nodeValue); }
    $domNode = $domxPath->query("/xml/bestPaper")->item(0);
    if ($domNode->firstChild) { $bestPaper = utf8_decode($domNode->firstChild->nodeValue); }
    $domNode = $domxPath->query("/xml/toProgramCommittee")->item(0);
    if ($domNode->firstChild) { $toProgramCommittee = utf8_decode($domNode->firstChild->nodeValue); }
    $domNode = $domxPath->query("/xml/toAuthors")->item(0);
    if ($domNode->firstChild) { $toAuthors = utf8_decode($domNode->firstChild->nodeValue); }
    $domNode = $domxPath->query("/xml/personalNotes")->item(0);
    if ($domNode->firstChild) { $personalNotes = utf8_decode($domNode->firstChild->nodeValue); }
  
  }
}






?>

<h2>Nội Dung Đánh Giá<!--Review Contents--></h2>

<center>
<form action="review_article.php" method="post">
  <input type="hidden" name="action" value="manual" />
  <input type="hidden" name="articleNumber" value="<?php print($articleNumber); ?>" />
  <table>
    <tr>
      <td>Người đánh giá:<!--Reviewer:--></td>
      <td><?php Tools::printHTML($currentReviewer->getFullName());?></td>
    </tr>
    <tr>
      <td>Nguời hỗ trợ: <!-- Subreviewer --></td>
      <td>
        <input type="text" name="subreviewer" size="50" value="<?php Tools::printHTML($subreviewer); ?>" />
      </td>
    </tr>
    <tr>
      <td><!-- Overall Grade -->Đánh giá chung:</td>
      <td>
        <?php Tools::printSelectGrade("overallGrade",Tools::getConfig('review/overallGradeMin'),Tools::getConfig('review/overallGradeMax'),$overallGrade); ?>
      </td>
      <?php $numberOfFields = 1;
         if (Tools::useConfidenceLevel()) {
	   $numberOfFields++;
	 }
         if (Tools::useTechnicalQuality()) {
	   $numberOfFields++;
	 } 
         if (Tools::useEditorialQuality()) {
	   $numberOfFields++;
	 }
         if (Tools::useSuitability()) {
	   $numberOfFields++;
	 }
	 if (Tools::useBestPaper()) {
	   $numberOfFields++;
	 }?>
    </tr>
    <?php if(Tools::useConfidenceLevel()) { ?>
    <tr>
      <td><!-- Confidence Level --> Độ tin cậy của nhận xét:</td>
      <td>
        <?php Tools::printSelectGrade("confidenceLevel",Tools::getConfig('review/confidenceLevelMin'),Tools::getConfig('review/confidenceLevelMax'),$confidenceLevel); ?>
      </td>
    </tr>
    <?php } ?>
    <?php if(Tools::useTechnicalQuality()) { ?>
    <tr>
      <td>Độ chuyên môn:</td>
      <td>
        <?php Tools::printSelectGrade("technicalQuality",Tools::getConfig('review/technicalQualityMin'),Tools::getConfig('review/technicalQualityMax'),$technicalQuality); ?>
      </td>
    </tr>
    <?php } ?>
    <?php if(Tools::useEditorialQuality()) { ?>
    <tr>
      <td><!-- Editorial Quality -->Chất luợng biên tập:</td>
      <td>
        <?php Tools::printSelectGrade("editorialQuality",Tools::getConfig('review/editorialQualityMin'),Tools::getConfig('review/editorialQualityMax'),$editorialQuality); ?>
      </td>
    </tr>
    <?php } ?>
    <?php if(Tools::useSuitability()) { ?>
    <tr>
      <td><!-- Suitability -->Mức phù hợp:</td>
      <td>
        <?php Tools::printSelectGrade("suitability",Tools::getConfig('review/suitabilityMin'),Tools::getConfig('review/suitabilityMax'),$suitability); ?>
      </td>
    </tr>
    <?php } ?>
    <?php if(Tools::useBestPaper()) { ?>
    <tr>
      <td><!-- Best Paper -->Bài viết tốt nhất:</td>
      <td>
        <?php Tools::printSelectGrade("bestPaper",Tools::getConfig('review/bestPaperMin'),Tools::getConfig('review/bestPaperMax'),$bestPaper); ?>
      </td>
    </tr>
    <?php } ?>
    <?php if(Tools::useToProgramCommittee()) { ?>
    <tr>
      <td colspan="2">Bình luận gửi đến ban tổ chức:<!--Comments to the Program Committee:--></td>
    </tr><tr>
      <td colspan="2">
        <textarea name="toProgramCommittee" rows="10" cols="80"><?php Tools::printHTML($toProgramCommittee); ?></textarea>
      </td>
    </tr>
    <?php } ?>
    <?php if(Tools::useToAuthors()) { ?>
    <tr>
      <td colspan="2">Bình luận gửi đến tác giả:<!--Comments to the Authors:--></td>
    </tr><tr>
      <td colspan="2">
        <textarea name="toAuthors" rows="30" cols="80"><?php Tools::printHTML($toAuthors); ?></textarea>
      </td>
    </tr>
    <?php } ?>
    <tr>
      <td colspan="2">Ghi chú cá nhân :<!--Personal Notes (only accessible to you):--></td>
    </tr><tr>
      <td colspan="2">
        <textarea name="personalNotes" rows="10" cols="80"><?php Tools::printHTML($personalNotes); ?></textarea>
      </td>
    </tr>
    <tr>  
      <td>Trạng thái đánh giá:<!--Review Status:--></td>
      <td> 
      <table>
      <tr> 
      <td>
      <input class="noBorder" type="radio" name="review_status" id="inProgressRadio"
             value="<?php print(Assignement::$INPROGRESS); ?>" 
             <?php if($reviewStatus == Assignement::$INPROGRESS) { print('checked="checked"');} ?> />
      <label for="inProgressRadio"><!--In Progress (review remains invisible to other reviewers)-->Đang đánh giá (không hiển thị với nguời đánh giá khác)</label>
      </td>
      </tr>
      <tr> 
      <td> 
      <input class="noBorder" type="radio" name="review_status" id="completedRadio"
             value="<?php print(Assignement::$COMPLETED); ?>" 
             <?php if($reviewStatus != Assignement::$INPROGRESS) { print('checked="checked"');} ?> />
      <label for="completedRadio">Hoàn thành<!--Completed--></label>
      </td>
      </tr>
      </table>      
      </td>
    </tr>
  </table>
  <?php if(Tools::useToAuthors() || Tools::useToProgramCommittee()) { ?>
    <input type="submit" class="buttonLink bigButton" value="Làm lại" name="reformat_text" />
  <?php } ?>
  <input type="submit" class="buttonLink bigButton" value="Cập nhật đánh giá" />
</form>
</center>

<?php 
}
}
?>

</body>
</html>
