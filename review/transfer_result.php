<?php 

/*
 * Copyright (C) 2006, 2007 Thomas Baigneres, Matthieu Finiasz
 *
 * This file is part of iChair.
 *
 * iChair is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 * 
 * iChair is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

?><?php $page_title='Tải Bài Viết Lên Cho Đánh Giá';//'Load Submissions for Review';
include '../utils/tools.php'; 
include 'header.php';

set_time_limit(0);
if($currentReviewerGroup == Reviewer::$CHAIR_GROUP) {

  $transferedArticles = Article::getAllArticles();
  $reZip = false;

  foreach($_POST as $postedValue) {
    if(preg_match('/^[0-9]*$/', $postedValue)) {
      $submissionNumber = $postedValue;
      /* Transfer the submission into the article database */
      $article = new Article();
      $article->createFromSubmission(Submission::getBySubmissionNumber($submissionNumber));
      $reZip = true;
      Log::logSubmissionToArticle($article,$currentReviewer);
    }
  }
  Article::computeAverages();

  /* create a zip of all articles and store it in the reviewsPath */

  if ((Tools::useZipReviews()) && $reZip) {
    $allcategories;
    if (Tools::useCategory()) {
      $allcategories = Tools::getAllCategories();
    }
  
    $zippath = Tools::getConfig('server/zipPath') . "zip";
    $tmpdir;
    do {
      $tmpdir = "/tmp/".rand()."ZZZ".$currentReviewer->getReviewerNumber();
    } while (file_exists($tmpdir));
    mkdir($tmpdir);
    $indexFile = fopen($tmpdir."/index.html", "w");
    fwrite($indexFile, '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
                        <html xmlns="http://www.w3.org/1999/xhtml">
                        <head>
                        <meta http-equiv="Content-type" content="text/html; charset=UTF-8" />');
    fwrite($indexFile,'<title>'.htmlentities(Tools::getConfig('conference/name'), ENT_COMPAT | ENT_HTML401, 'UTF-8').' - All the Articles</title>
                       <link href="zip.css" rel="stylesheet" type="text/css" />
                       </head>
                       <body>');
    fwrite($indexFile,'<h1>'.htmlentities(Tools::getConfig('conference/name'), ENT_COMPAT | ENT_HTML401, 'UTF-8').' - All  the Articles</h1>');

    /* Include Review Guidelines*/
    if(Tools::hasReviewGuidelinesHTML("..") || Tools::hasReviewGuidelinesPDF("..") || Tools::hasReviewGuidelinesTXT("..")) {
      fwrite($indexFile, '<center><table><tr>');
      if(Tools::hasReviewGuidelinesHTML("..")) {
	copy("guidelines/guidelines.html", $tmpdir."/guidelines.html");
	fwrite($indexFile, '<td><form action="guidelines.html" target="_blank" method="post"><input type="submit" class="buttonLink bigButton" value="Hướng dẫn đánh giá (HTML)" /></form></td>');
      } 
      if(Tools::hasReviewGuidelinesPDF("..")) {
	copy("guidelines/guidelines.pdf", $tmpdir."/guidelines.pdf");
	fwrite($indexFile, '<td><form action="guidelines.pdf" target="_blank" method="post"><input type="submit" class="buttonLink bigButton" value="Hướng dẫn đánh giá (PDF)" /></form></td>');
      } 
      if(Tools::hasReviewGuidelinesTXT("..")) {
	copy("guidelines/guidelines.txt", $tmpdir."/guidelines.txt");
	fwrite($indexFile, '<td><form action="guidelines.txt" target="_blank" method="post"><input type="submit" class="buttonLink bigButton" value="Hướng dẫn đánh giá (TXT)" /></form></td>');
      } 
      fwrite($indexFile, '</tr></table></center>');
    }


    $articles = Article::getAllArticles();
    foreach($articles as $article) {
      $articleNumber = $article->getArticleNumber();
      copy($article->getFolder()."".$article->getFile(), $tmpdir."/".$article->getFile());
    
      /* For each file, include an xml review file */
      $reviewDocument = Tools::createXMLReview(false,
					       $article->getArticleNumber(),
					       $article->getTitle(), $article->getAuthors(),
					       "", "",
					       "", "", "", "", "", "", 
					       "", "", "");
      $reviewDocument->formatOutput = true;
      $reviewDocument->encoding = "UTF-8";
      $file = tmpfile();
      $reviewDocument->save("" . $file);
      rename("" . $file, $tmpdir.sprintf("/review%04d.xml", $article->getArticleNumber()));

      fwrite($indexFile,'
                        <div class="paperBox">
                        <div class="paperBoxTitle">
                        <div class="paperBoxNumber">Bài vết&nbsp;'//Article&nbsp;'
                         . $articleNumber . '</div>');
      $count=2;
      if(Tools::useCategory()) { 
	fwrite($indexFile,'Thể loại: '//Category: '
   . htmlentities($allcategories[$article->getCategory()], ENT_COMPAT | ENT_HTML401, 'UTF-8') . '<br/>');
	$count--;
      }
      if(Tools::useKeywords()) { 
	fwrite($indexFile,'Từ khoá (Keywords): ' 
    . htmlentities($article->getKeywords(), ENT_COMPAT | ENT_HTML401, 'UTF-8') . '<br/>');
	$count--;
      }
      for (;$count>0; $count--){
	fwrite($indexFile,'&nbsp;<br/>');
      }
      if (!Tools::authorsAreAnonymous() && Tools::useCountry()) {
        fwrite($indexFile,'
                         </div>
                         <div class="paperBoxDetails">' . Tools::printContinentDivs($article->getCountryArray(), False) . $article->printCustomCheckboxes(false, false) . 
                         '<div class="versionTitle">Tiêu đề: '//Title: '
                          . htmlentities($article->getTitle(), ENT_COMPAT | ENT_HTML401, 'UTF-8') .'</div>');
      } else {
        fwrite($indexFile,'
                         </div>
                         <div class="paperBoxDetails"><div><div><div>' . $article->printCustomCheckboxes(false, false) .
                         '<div class="versionTitle">Tiêu đề: '//Title: '
                         . htmlentities($article->getTitle(), ENT_COMPAT | ENT_HTML401, 'UTF-8') .'</div>');
      }
      if(!Tools::authorsAreAnonymous()) {
	fwrite($indexFile,'<div class="versionAuthors">Tác giả: '//Authors: '
    .htmlentities($article->getAuthors(), ENT_COMPAT | ENT_HTML401, 'UTF-8').'</div>');
	if(Tools::useAffiliations()) {
	  fwrite($indexFile, 'Tổ chức: '//Affiliations: '
     . htmlentities($article->getAffiliations(), ENT_COMPAT | ENT_HTML401, 'UTF-8').'<br/>');
	}
	if(Tools::useCountry()) {
	  fwrite($indexFile, 'Tổ chức: '//Countries: '
     . htmlentities($article->getCountry(), ENT_COMPAT | ENT_HTML401, 'UTF-8').'<br/>');
	}
      }
      if(Tools::useAbstract()) {
	fwrite($indexFile,'Abstract:<div class="versionAbstract">'.nl2br(htmlentities($article->getAbstract(), ENT_COMPAT | ENT_HTML401, 'UTF-8')).'</div>');
      } 
      fwrite($indexFile,'</div></div></div></div><div class="floatRight"><form action="'.Tools::getConfig('server/location').'/review/review_article.php" method="post" target="_blank"><input type="hidden" name="articleNumber" value="'.$articleNumber.'"/><input type="submit" class="buttonLink" value="Đánh giá trưc tuyến"/> </form> </div>');
      fwrite($indexFile,'<div class="floatRight"><form action="'.$article->getFile().'" method="post"><input type="submit" class="buttonLink" value="Open File"/> </form> </div>');
      fwrite($indexFile,'<div class="floatRight"><form action="'.sprintf("review%04d.xml", $article->getArticleNumber()).'" method="post"><input type="submit" class="buttonLink" value="Open XML Review"/> </form> </div>');
      fwrite($indexFile,'<div class="clear"></div></div>');
    }
    
    fwrite($indexFile, '</body></html>');
    fclose($indexFile);
    
    copy("../css/zip_".Tools::getConfig("miscellaneous/reviewSkin").".css",$tmpdir."/zip.css");
    exec('cd '.$tmpdir.'; zip -r1 all_articles.zip *', $output, $return_value);
    if ($return_value == 0) {
      copy($tmpdir.'/all_articles.zip', Tools::getConfig('server/reviewsPath').'all_articles.zip');
      unlink($tmpdir.'/all_articles.zip');
      print('<div class="OKmessage">Zip của tất cả bài viết đã tạo thành công.'//A zip of all the articles was created successfully.
        .'</div>');
    } else {
      print('<div class="ERRmessage">Xảy ra lỗi khi tạo zip cho tất cả bài viết.'//An error occured when attempting to create a zip of all the articles.
        .'</div>');
    }
    exec("rm -r ".$tmpdir);
  }
  
  ?>
  <div class="OKmessage">
    Bài gửi được chọn đã tải lên thành công.
    <!--The submissions you selected have successfully been transfered to the article database.<br />
    You can now go select which reviewers are allowed to access them. -->
  </div>
  <form action="index_chair.php" method="post">
  <div class="floatRight">
    <input type="submit" class="buttonLink bigButton" value="Đồng ý" />
  </div>
  </form>
  <?php 

}

?>

<?php include('footer.php'); ?>
