<?php 

/*
 * Copyright (C) 2006, 2007 Thomas Baigneres, Matthieu Finiasz
 *
 * This file is part of iChair.
 *
 * iChair is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 * 
 * iChair is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

?><?php $page_title="Trang chủ";//'Main Page';
include '../utils/tools.php';
include 'header.php';
if($currentReviewerGroup == Reviewer::$CHAIR_GROUP) {
?>

<?php 

if (Tools::getCurrentPhase() == Tools::$SUBMISSION) {

  Submission::printSummaryTable();

} else if ((Tools::getCurrentPhase() == Tools::$REVIEW) || (Tools::getCurrentPhase() == Tools::$DECISION)) {

  Article::printAcceptanceSummaryTable();

}

?>


<h2><a href="mailing.php">Gửi Mail<!--Mass Mailing--></a></h2>

<p>Sử dụng chức năng<!--Use--> <a href="mailing.php">Gửi Mail<!--Mass Mailing--></a> để gưi mail đến thành viên của hội thảo hoặc tác giả.
<!--to send emails to all the members of the program committee (e.g., to tell the program committee members that they can chose their preferences) or to the contact authors (e.g., to tell the authors that their paper has been accepted to the conference).-->
</p>

<h2><a href="meeting_edit.php">Thông Báo<!--Messages to Committee--></a></h2>
<p>Ban tổ chức có thể sửa đổi thông báo đến ban thành viên. 
<!--The Program Chair has the possibility to edit some messages for the committee members. These messages can either be of general order or specific to a particular article. 
If they concern a specific article, they will also be displayed in the <em>Reviews &amp; Discussion</em> page concerning this article.-->
</p>

<h2><a href="configuration.php">Chọn Giai Đoạn<!--Select iChair Phase--></a></h2>

<p>Hệ thống có các giai đoạn sau: 
<em><!-- Submission/Transfer/Authorizations -->Gửi Bài/Tải Bài/Phân Quyền</em>, <em><!-- Preferred
Articles Election -->Bầu chọn bài viết ưa thích</em> , <em>Phân công đánh giá</em>,
<em><!-- Review/Discussion/Decision -->Đánh Giá/Thảo Luận/Quyết Định</em>, hoặc <em>Quyết Định </em>. <b>Hiện tại, hệ thống đang ở giai đoạn
<?php 
if (Tools::getCurrentPhase() == Tools::$SUBMISSION) {
  print('<em>Gửi Bài/Tải Bài/Phân Quyền</em>');
} else if (Tools::getCurrentPhase() == Tools::$ELECTION) {
  print('<em>Bầu chọn bài viết ưa thích</em>');
} else if (Tools::getCurrentPhase() == Tools::$ASSIGNATION) {
  print('<em>Phân công đánh giá</em>');
} else if (Tools::getCurrentPhase() == Tools::$REVIEW) {
  print('<em>Đánh Giá/Thảo Luận/Quyết Định</em>');
} else {
  print('<em>Quyết Định </em>');
}
?></b>. Thay đổi giai đoạn hiện tại của hệ thống <a href="configuration.php">tại đây </a></p>

<h2><a href="export_list.php">Xuất Danh Sách Bài Viết<!--Export Article List--></a></h2>
Với chức năng <a href="export_list.php">Xuất Danh Sách Bài Viết<!--Export Article List--></a>, ban tổ chức có thể tải tất cả danh sách bài gửi/bài viết dưới dạng text.  Một số mục thể được bao gồm / loại trừ và danh sách có thể được sắp xếp theo nhiều cách khác nhau
<!--From the <a href="export_list.php">Export Article List</a> page, one can download a list of all the submissions/articles in text format. 
Several fields can be included/excluded and the list can be sorted in many different ways.-->

<h2><a href="stats.php">Thống Kê Hội Thảo<!--Conference Statistics--></a></h2>

Chức năng <!--The --><a href="stats.php">Thống Kê Hội Thảo<!--Conference Statistics--></a> phép thống kê liên quan đến các bài viết gửi, người đánh giá, và 
subreviewers. 
<!--page gives access to
several statistics concerning the submissions, the reviewers, and the
subreviewers. Some of the statistics depend on the current iChair
phase, like for example the time&amp;date at which accepted articles
have been submitted which is only available during the
<em>Review/Discussion/Decision</em> or the <em>Decision only</em>
phases.-->

<?php if((!Tools::restrictChairMenu()) || (Tools::getCurrentPhase() == Tools::$SUBMISSION)) { ?>

<h2><a href="examine.php">Bài Gửi Hiện Tại<!--Current Submissions--></a></h2>
<p>Sử dụng chức năng <a href="examine.php"> Bài Gửi Hịê Tại </a> để xem tất cả 
các bài viết gửi của hội nghị. Từ đó, ban tổ chức có thể 
huỷ / unwithdraw bài víêt đã gửi, xem chi tiết các phiên bản khác nhau (các phiên bản cập nhật khác nhau được thực hiện bởi tác giả) của bài viết, và 
tải về tập tin tương ứng. Cho mỗi phiên bản, ban tổ chức có thể thêm ý kiến.</p>
<!--<p>Use <a href="examine.php">Current Submissions</a> to have a look at all
the submissions made to the conference. From there, you can
withdraw/unwithdraw the submission, view the details of the different
versions (from different updates made by the authors) of a submissions, and
download the corresponding files. For each version, you can add your own
comments. You may find this feature useful during the submission and
assignement procedures. For instance, you can notice potential conflicts of
interest here.</p>-->

<h2><a href="transfer.php">Tải Bài Viết Lên Đánh Giá<!--Load Submissions for Review--></a></h2>
<p> Khi quá trình gửi bài viết là đã qua, thì quá trình đánh giá 
bắt đầu. Khi cần phải chuyển tất cả các viết được gửi lên cho hội thảo để đánh giá. Sử dụng chức năng <a 
href = thông tin đó "transfer.php"> Tải Bài Viết Lên Đánh Giá</a> để làm điều này. Chức năng này chỉ thực hiện được khi giai đoạn
<em> Gửi / Chuyển / Authorizations</em> hoàn thành. </ p> 

<p> Lưu ý rằng vẫn có thể gửi bài viết mới ngay cả khi bài viết tồn tại, và chuyển bài viết khi quá trình đánh giá bắt đầu. 
Điều này có ích khi một phiên bản mới của bài viết đã gửi có sẵn, khi một phiên bản trước là đã chuyển để đánh giá. Trong trường hợp này, phiên bản mới sẽ 
ghi đè lên cũ. </ p> 

<p> Cũng lưu ý rằng khi hủy bài viết đã gửi thì
không thể chuyển bài víêt lên hội thảo. </ p>
<!--
<p>When the submission process is over, the review process can
start. For this to happen, you need to migrate the database of all the
submissions made to the conference to a database of articles. Use <a
href="transfer.php">Load Submissions for Review</a> to select the
submissions that you want to transfer to the article database so that
they can be reviewed. Tipically, this should be done in
<em>Submission/Transfer/Authorizations</em> phase.</p>

<p>Note that it is still possible to add new submissions even if an
article database exists, and migrate them one by one even if the review
process has already started. This may be useful when a new version of
a submission is available, when a previous version was already
migrated to the article database. In such a case, the new version will
overwrite the old one.</p>

<p>Also note that withdrawn submission
cannot be transfered to the article database.</p>-->

<?php } ?>
<?php if((!Tools::restrictChairMenu()) || (Tools::getCurrentPhase() != Tools::$DECISION)) { ?>


<h2><a href="assign_reviewers_list.php">Phân Công Người Đánh Giá Bài Viết<!--Assign Reviewers to Articles--></a></h2>
<p>
Sử dụng chức năng<a href="assign_reviewers_list.php"> Phân Công Người Đánh Giá Bài Viết<!--Assign Reviewers to Articles--> </a> để lấy danh sách tất cả  bài viết.
 Với mỗi bài viết, ban tổ chức có thể </p>
<ul>
  <li>phân công người đánh giá, có nghĩa là người đánh giá sẽ có quyền đánh giá bài viết được phân công,
  </li>
  <li>cho phép người đánh giá xem bài viết, có nghĩa là người đánh giá có quyền truy cập vào xem bài viết (giống như phân công bài viết) nhưng sẽ không phải đánh giá bài viết,
  </li>
  <li>không cho phép một người đánh giá xem bài viết, nếu không muốn người đánh có thể truy cập xem
  bài viết.
  </li>
</ul>
<!--
<p>
Counter hiển thị số lượng người đánh giá hiện tại được phân công cho mỗi
bài viết. Nếu số người đánh giá phân công
thấp hơn so với số lượng người đánh giá dự kiến ​​, tỷ lệ này được hiển thị màu đỏ. Bạn có thể thay đổi số lượng cần thiết cho một nhận xét đặc biệt
bài viết bằng cách sử dụng các mũi tên bên phải của tỷ lệ.
</ P>

<p>
Cho đến khi <a href="assign_reviewers_list.php"> Assign để phản biện
Bài viết </a> được sử dụng lần đầu tiên, tất cả các nhận xét được ủy quyền
cho tất cả các bài viết. </p>

<p> điều tốt nhất để làm lần đầu tiên sử dụng chức năng <a
href = "assign_reviewers_list.php">Phân Công Người Đánh Giá Bài Viết<!--Assign Reviewers to Articles</a> là
bấm vào <em> Phân Công Người Đánh Giá</em> bài viết đầu tiên, có thể
ngăn chặn một số người đánh giá cụ thể, nhấp chuột vào Lưu Thay đổi và <em> tới
bài viết tiếp theo </ em>. Điều này sẽ lặp đi lặp lại cho đến khi người đánh giá có phù hợp
quyền cho <em> cuối cùng </ em> bài viết của hội thảo. Điều này sẽ
cho phép mỗi người xem có một cái nhìn đầu tiên vào danh sách các vật phẩm
có sẵn cho cô ấy / anh ấy và đưa ra một khả năng <em> / sẵn sàng
xem xét cấp </ em> để mỗi bài viết (điều này được thực hiện thông qua <a
href = "affinity_list.php"> Điều thích </ a>). Lớp này
sẽ giúp người xem khi lựa chọn mà một bài viết cụ thể nên
được gán cho. Sau khi nhận xét đã đưa ra điểm số của họ, chủ tịch
nên sử dụng <a href="assign_reviewers_list.php"> Assign để phản biện
Bài viết </ a> một lần nữa để thực sự <em> gán bài viết </ em> để
nhận xét. </ P>
<!--
<p>
Use <a href="assign_reviewers_list.php">Assign Reviewers to Articles</a> to get a list of
all the articles in the article database. For each article, you can</p>
<ul>
  <li>
  assign a reviewer, meaning that this reviewer will have
  to review the article,
  </li>
  <li>
  authorize a reviewer, meaning that this reviewer will
  have access to the article (just as if this article was assigned to
  him) but won't have to review it,
  </li>
  <li>
  block a reviewer, if you do not want this reviewer to have access
  to the article.
  </li>  
</ul>-->
<p>
Bộ đếm hiện hiển thị số lượng hiện tại phân công đánh giá cho mỗi
bài viết, cùng với số lượng <em>người đánh giá</em> phân công bài viết. Nếu số người phân công đánh giá ít hơn so với số lượng người đánh giá dự kiến ​, tỷ lệ này được hiển thị màu đỏ. Người đánh giá có thể thay đổi số lượng người đánh giá cần thiết cho từng
bài viết bằng cách sử dụng các mũi tên bên phải của tỷ lệ. Ví dụ,
bài báo được viết bởi thành viên của ủy ban hội thảo có thể cần nhiều hơn người đánh giá. Lưu ý rằng số lượng mặc định người đánh giá dự kiến ​​cho mỗi
bài viết có thể được thay đổi bởi người quản trị của hệ thống.
</p>
<p>
Cho đến khi <a href="assign_reviewers_list.php">Phân Công Người Đánh Giá</a> cho sử dụng lần đầu tiên, tất cả người đánh giá được phép truy cập vào tất cả bài viết. </p>
<p>Điều này tốt nhất để làm lần đầu tiên là sử dụng <a
href = "assign_reviewers_list.php"> Phân Công Người Đánh Giá </a> nhấp vào <em> Phân Công Đánh Giá </em> cho bài viết đầu tiên, có thể
ngăn chặn một số người đánh giá cụ thể, nhấp vào  <em> Lưu Thay Đổi và Đi Tới Bài Viết Tiếp Theo</em>. Điều này Lặp đi lặp lại cho đến khi có người đánh giá có quyền phù hợp
cho bài viết <em> cuối cùng </em> của hội nghị. Điều này sẽ
cho phép mỗi người đánh giá có thể đầu tiên nhìn thấy vào danh sách các bài víết
có sẵn cho họ và đưa ra một<em>khả năng/sẳn sàng đánh giá </em> để mỗi bài viết (điều này được thực hiện thông qua <a
href = "affinity_list.php"> Chọn Bài Viết Ưa Thích </a>). Mức độ này sẽ giúp người đánh giá khi lựa chọn một bài viết cụ thể được phân công. Sau khi người đánh giá đã đưa ra mức đánh giá của họ, ban tổ chức
nên sử dụng <a href="assign_reviewers_list.php"> Phân Công Người Đánh Giá </a> một lần nữa để chứng thực <em> Phân Công Bài Viết </em> đến người đánh giá. </p>
<!--
<p>
A counter displays the current number of assigned reviewers for each
article, together with the number of reviewers that <em>should</em> be
assigned for this article. If the number of assigned reviewers is
lower than the expected number of reviewers, the ratio is displayed in
red. You can change the number of reviewers needed for a particular
article by using the arrows on the right of the ratio. For example,
articles written by a member of the program committee might need more
reviewers. Note that the default number of reviewers expected for each
article can be changed by the administrator of the iChair software.
</p>

<p>
Until <a href="assign_reviewers_list.php">Assign Reviewers to
Articles</a> is used for the first time, all the reviewers are authorized
for all the articles.</p>

<p>The best thing to do the first time you use <a
href="assign_reviewers_list.php">Assign Reviewers to Articles</a> is
to click on <em>Assign Reviewers</em> for the first article, possibly
block some specific reviewers, click on <em>Save Changes and Go to
Next Article</em>. Iterate until the reviewers have suitable
permissions for the <em>last</em> article of the conference. This will
allow each reviewer to take a first look at the list of articles which
are available to her/him and to give an <em>ability/willingness to
review grade</em> to each article (this is done through the <a
href="affinity_list.php">Preferred Articles</a> page). This grade
shall help the reviewer when choosing whom a specific article should
be assign to. Once the reviewers have given their grades, the chair
should use <a href="assign_reviewers_list.php">Assign Reviewers to
Articles</a> once again to actually <em>assign articles</em> to
reviewers.  </p>
-->
<h2><a href="assign_articles_list.php">Phân Công Bài Viết Cho Người Đánh Giá<!--Assign Articles to Reviewers--></a></h2>
<p> Sử dụng chức năng<a href="assign_articles_list.php"> Phân Công Bài Viết Cho Người Đánh Giá<!--Assign Articles to Reviewers--> </a> 
để có được một loại chuyển vị của <a href="assign_reviewers_list.php"> Phân Công Bài Viết Cho Người Đánh Giá<!--Assign Articles to Reviewers--> </a>. Trong trường hợp này, bạn sẽ có được một danh sách của tất cả các nhận xét 
(người quan sát), và với mỗi một, bạn có thể có thể lựa chọn 
phân công/phân quyền/chặn bài viết bằng cách nhấp vào <em> Phân Công Bài Viết </em>
nút. Đối với mỗi người xem, một truy cập hiển thị số lượng bài viết đã 
giao cho nhà phê bình này, mà sẽ giúp ghế được <em> công bằng </em> 
trong quá trình chuyển nhượng (ở đây, nó có nghĩa là mỗi người xem nên có 
cùng một số ý kiến ​​được giao). </p> 

<p>Ở dưới cùng của trang, có thể tìm thấy danh sách người quan sát. ban tổ chức 
không thể phân bài viết cho người quan sát, nhưng có thể cho phép hoặc không cho phép người quan sát 
 xem bài viết. 
</p>
<!--
<p>Use <a href="assign_articles_list.php">Assign Articles to Reviewers</a>
to get a kind of transpose of <a href="assign_reviewers_list.php">Assign
Reviewers to Articles</a>. In this case, you get a list of all the reviewers
(an observers), and for each one, you can can choose to
assign/authorized/block articles by clicking on the <em>Assign Articles</em>
button. For each reviewer, a counter displays the number of articles already
assigned to this reviewer, which shall help the chair to be <em>fair</em>
during the assignment process (here, it means that each reviewer should have
the same number of assigned reviews). </p>

<p>At the bottom of the page, you will find a list of observers. You
cannot assign articles to observer, but only authorize or block
articles to them.
</p>
-->
<?php } ?>
<?php if((!Tools::restrictChairMenu()) || (Tools::getCurrentPhase() == Tools::$ASSIGNATION)) { ?>

<h2><a href="automatic_assignation.php">Phân Công Tự Động<!--Automatic Assignment--></a></h2>
<p> Sau khi bài gửi đã được chuyển giao và người đánh giá đã
chọn bài viết ưa thích của họ, ban tổ chức có thể sử dụng chức năng <a 
href = "automatic_assignation.php"> Phân Công Tự Động<!--Automatic Assignment-->  </a> để 
<em> tự động </em> phân công bài viết cho từng người. Đồng thời có 
khả năng sửa đổi phân công thực hiện bởi ban tổ chức và giữ mọi thức
phân hiện có làm cơ sở cho quá trình phân công tự động (có nghĩa là 
rằng sẽ không mất tất cả). Cũng lưu ý rằng các 
Thuật toán là không xác định: chạy nó hai lần sẽ không cho cùng 
kết quả. </p>
<!--
<p>Once the submissions have been transfered and the reviewers have
chosen their prefered articles, you can use the <a
href="automatic_assignation.php">Automatic Assignment</a> page to
<em>automatically</em> assign articles to each of them. You have the
possibility to modify the proposition made by iChair and keep any
existing assignement as a basis for the automated process (meaning
that you won't loose all you have done until now). Note also that the
algorithm is not deterministic: running it twice won't give the same
result.</p>
-->
<?php } ?>
<?php if((!Tools::restrictChairMenu()) || (Tools::getCurrentPhase() == Tools::$REVIEW)) { ?>

<h2><a href="supervise_reviewers.php">Quyền Người Đánh Giá<!--Reviewers Access Privileges--></a></h2>

<p> Committe thành viên chỉ có thể xem các đánh giá ​​từ các thành viên khác nếu 
ban tổ chức cho phép. Điều này đã được thực hiện thông qua chức năng<a 
href = "supervise_reviewers.php"> Quyền Người Đánh Giá<!--Reviewers Access Privileges--> </a> . 
Đồng thời, điều này sẽ cho các người đánh giá lựa chọn truy cập 
các cuộc thảo luận. </p>
<!--<p>Committe members can only see reviews from other members if the
chair allows them to do so. This has to be done through the <a
href="supervise_reviewers.php">Reviewers Access Privileges</a>
page. At the same time, this will grant the selected reviewers access
to the discussions.</p>
-->
<?php } ?>
<?php if((!Tools::restrictChairMenu()) || (Tools::getCurrentPhase() == Tools::$REVIEW) || (Tools::getCurrentPhase() == Tools::$DECISION)) { ?>

<h2><a href="accept_and_reject.php">Chấp Nhận &amp; Từ Chối Bài Viết<!--Accept &amp; Reject Articles--></a></h2>
<p>Sử dụng chức năng <a href="accept_and_reject.php">Chấp Nhận &amp; Từ Chối Bài Viết<!--Accept &amp; Reject Articles--> </a> để
<em>chấp nhận</em>, <em>từ chối</em>, <em>thảo luận</em>... bài viết nào đó.</p>
<!--
<p>Use the <a href="accept_and_reject.php">Accept &amp; Reject
Articles</a> page to set, for each article, whether it should be
<em>accepted</em>, <em>rejected</em>, <em>discussed</em>...</p>
-->
<?php } ?>






<?php }?>

<?php include('footer.php'); ?>
