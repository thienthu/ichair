<?php 

/*
 * Copyright (C) 2006, 2007 Thomas Baigneres, Matthieu Finiasz
 *
 * This file is part of iChair.
 *
 * iChair is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 * 
 * iChair is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

?><?php $page_title="Gửi Mail";//'Mass Mailing';
include '../utils/tools.php';
include 'header.php';
if($currentReviewerGroup == Reviewer::$CHAIR_GROUP) {

  $reviewerRecipients = array();
  $articleRecipients = array();
  $allReviewers = Reviewer::getAllReviewersSparse();
  $allArticles = Article::getAllArticles();

  /* Prepare the list of committee recipients */
  switch(Tools::readPost('to_committee')) {
  case 'all':
    $reviewerRecipients = $allReviewers;
    break;
  case 'list':
    $list = explode(",", Tools::readPost('to_committee_list'));
    foreach($list as $reviewerNumber) {
      if(!is_null($allReviewers[$reviewerNumber])) {
	$reviewerRecipients[$reviewerNumber] = $allReviewers[$reviewerNumber];
      }
    }
    break;
  case 'group':
    if(Tools::readPost('to_chair') == 'yes') {
      foreach($allReviewers as $reviewer) {
	if($reviewer->getGroup() == Reviewer::$CHAIR_GROUP) {
	  $reviewerRecipients[$reviewer->getReviewerNumber()] = $reviewer;
	}
      }
    }
    if(Tools::readPost('to_reviewer') == 'yes') {
      foreach($allReviewers as $reviewer) {
	if($reviewer->getGroup() == Reviewer::$REVIEWER_GROUP) {
	  $reviewerRecipients[$reviewer->getReviewerNumber()] = $reviewer;
	}
      }
    }
    if(Tools::readPost('to_observer') == 'yes') {
      foreach($allReviewers as $reviewer) {
	if($reviewer->getGroup() == Reviewer::$OBSERVER_GROUP) {
	  $reviewerRecipients[$reviewer->getReviewerNumber()] = $reviewer;
	}
      }
    }
    break;
  default:
  }

  /* Prepare the list of author recipients */
  switch(Tools::readPost('to_authors')) {
  case 'all':
    $articleRecipients = $allArticles;
    break;
  case 'list':
    $list = explode(",", Tools::readPost('to_authors_list'));
    foreach($list as $articleNumber) {
      if(!is_null($allArticles[$articleNumber])) {
	$articleRecipients[$articleNumber] = $allArticles[$articleNumber];
      }
    }
    break;
  case 'group':
    if(Tools::readPost('to_accept') == 'yes') {
      foreach($allArticles as $article) {
	if($article->getAcceptance() == Article::$ACCEPT) {
	  $articleRecipients[$article->getArticleNumber()] = $article;
	}
      }
    }
    if(Tools::readPost('to_maybe_accept') == 'yes') {
      foreach($allArticles as $article) {
	if($article->getAcceptance() == Article::$MAYBE_ACCEPT) {
	  $articleRecipients[$article->getArticleNumber()] = $article;
	}
      }
    }
    if(Tools::readPost('to_discuss') == 'yes') {
      foreach($allArticles as $article) {
	if($article->getAcceptance() == Article::$DISCUSS) {
	  $articleRecipients[$article->getArticleNumber()] = $article;
	}
      }
    }
    if(Tools::readPost('to_maybe_reject') == 'yes') {
      foreach($allArticles as $article) {
	if($article->getAcceptance() == Article::$MAYBE_REJECT) {
	  $articleRecipients[$article->getArticleNumber()] = $article;
	}
      }
    }
    if(Tools::readPost('to_reject') == 'yes') {
      foreach($allArticles as $article) {
	if($article->getAcceptance() == Article::$REJECT) {
	  $articleRecipients[$article->getArticleNumber()] = $article;
	}
      }
    }
    if(Tools::readPost('to_void') == 'yes') {
      foreach($allArticles as $article) {
	if($article->getAcceptance() == Article::$VOID) {
	  $articleRecipients[$article->getArticleNumber()] = $article;
	}
      }
    }
    if(Tools::readPost('to_custom1') == 'yes') {
      foreach($allArticles as $article) {
	if($article->getAcceptance() == Article::$CUSTOM1) {
	  $articleRecipients[$article->getArticleNumber()] = $article;
	}
      }
    }
    if(Tools::readPost('to_custom2') == 'yes') {
      foreach($allArticles as $article) {
	if($article->getAcceptance() == Article::$CUSTOM2) {
	  $articleRecipients[$article->getArticleNumber()] = $article;
	}
      }
    }
    if(Tools::readPost('to_custom3') == 'yes') {
      foreach($allArticles as $article) {
	if($article->getAcceptance() == Article::$CUSTOM3) {
	  $articleRecipients[$article->getArticleNumber()] = $article;
	}
      }
    }
    if(Tools::readPost('to_custom4') == 'yes') {
      foreach($allArticles as $article) {
	if($article->getAcceptance() == Article::$CUSTOM4) {
	  $articleRecipients[$article->getArticleNumber()] = $article;
	}
      }
    }
    if(Tools::readPost('to_custom5') == 'yes') {
      foreach($allArticles as $article) {
	if($article->getAcceptance() == Article::$CUSTOM5) {
	  $articleRecipients[$article->getArticleNumber()] = $article;
	}
      }
    }
    if(Tools::readPost('to_custom6') == 'yes') {
      foreach($allArticles as $article) {
	if($article->getAcceptance() == Article::$CUSTOM6) {
	  $articleRecipients[$article->getArticleNumber()] = $article;
	}
      }
    }
    if(Tools::readPost('to_custom7') == 'yes') {
      foreach($allArticles as $article) {
	if($article->getAcceptance() == Article::$CUSTOM7) {
	  $articleRecipients[$article->getArticleNumber()] = $article;
	}
      }
    }
    break;
  default:
  }

  /* Check posted values */

  $warning = Tools::readPost('warning');

  if($warning != "") {
    print('<div class="ERRmessage">Trong khi gửi mail có thể nhận các cảnh báo bên dưới:'//You are sending an email even though you received the following warning:
      .'<br />'.nl2br(htmlentities($warning, ENT_COMPAT | ENT_HTML401, 'UTF-8')).'</div>');
    ?>
    <center>
    <table><tr><td>
    <form action="mailing.php" method="post">
      <input type="hidden" name="subject" value="<?php Tools::printHTML(Tools::readPost('subject')); ?>" />
      <input type="hidden" name="body" value="<?php Tools::printHTML(Tools::readPost('body')); ?>" />
      <input type="hidden" name="to_committee" value="<?php Tools::printHTML(Tools::readPost('to_committee')); ?>" />
      <input type="hidden" name="to_authors" value="<?php Tools::printHTML(Tools::readPost('to_authors')); ?>" />
      <input type="hidden" name="to_chair" value="<?php Tools::printHTML(Tools::readPost('to_chair')); ?>" />
      <input type="hidden" name="to_reviewer" value="<?php Tools::printHTML(Tools::readPost('to_reviewer')); ?>" />
      <input type="hidden" name="to_observer" value="<?php Tools::printHTML(Tools::readPost('to_observer')); ?>" />
      <input type="hidden" name="to_committee_list" value="<?php Tools::printHTML(Tools::readPost('to_committee_list')); ?>" />
      <input type="hidden" name="to_authors_list" value="<?php Tools::printHTML(Tools::readPost('to_authors_list')); ?>" />
      <input type="hidden" name="to_accept" value="<?php Tools::printHTML(Tools::readPost('to_accept')); ?>" />
      <input type="hidden" name="to_maybe_accept" value="<?php Tools::printHTML(Tools::readPost('to_maybe_accept')); ?>" />
      <input type="hidden" name="to_discuss" value="<?php Tools::printHTML(Tools::readPost('to_discuss')); ?>" />
      <input type="hidden" name="to_maybe_reject" value="<?php Tools::printHTML(Tools::readPost('to_maybe_reject')); ?>" />
      <input type="hidden" name="to_reject" value="<?php Tools::printHTML(Tools::readPost('to_reject')); ?>" />
      <input type="hidden" name="to_void" value="<?php Tools::printHTML(Tools::readPost('to_void')); ?>" />
      <input type="hidden" name="to_custom1" value="<?php Tools::printHTML(Tools::readPost('to_custom1')); ?>" />
      <input type="hidden" name="to_custom2" value="<?php Tools::printHTML(Tools::readPost('to_custom2')); ?>" />
      <input type="hidden" name="to_custom3" value="<?php Tools::printHTML(Tools::readPost('to_custom3')); ?>" />
      <input type="hidden" name="to_custom4" value="<?php Tools::printHTML(Tools::readPost('to_custom4')); ?>" />
      <input type="hidden" name="to_custom5" value="<?php Tools::printHTML(Tools::readPost('to_custom5')); ?>" />
      <input type="hidden" name="to_custom6" value="<?php Tools::printHTML(Tools::readPost('to_custom6')); ?>" />
      <input type="hidden" name="to_custom7" value="<?php Tools::printHTML(Tools::readPost('to_custom7')); ?>" />
      <input type="hidden" name="uncheck" value="<?php Tools::printHTML(Tools::readPost('uncheck')); ?>" />
      <input type="hidden" name="init_from_post" value="yes" />
      <input type="submit" class="buttonLink bigButton" value="Go Back/Edit Mail" />
    </form>
    </td><td>
    <form action="mailing_result.php" method="post">
      <input type="hidden" name="subject" value="<?php Tools::printHTML(Tools::readPost('subject')); ?>" />
      <input type="hidden" name="body" value="<?php Tools::printHTML(Tools::readPost('body')); ?>" />
      <input type="hidden" name="to_committee" value="<?php Tools::printHTML(Tools::readPost('to_committee')); ?>" />
      <input type="hidden" name="to_authors" value="<?php Tools::printHTML(Tools::readPost('to_authors')); ?>" />
      <input type="hidden" name="to_chair" value="<?php Tools::printHTML(Tools::readPost('to_chair')); ?>" />
      <input type="hidden" name="to_reviewer" value="<?php Tools::printHTML(Tools::readPost('to_reviewer')); ?>" />
      <input type="hidden" name="to_observer" value="<?php Tools::printHTML(Tools::readPost('to_observer')); ?>" />
      <input type="hidden" name="to_committee_list" value="<?php Tools::printHTML(Tools::readPost('to_committee_list')); ?>" />
      <input type="hidden" name="to_authors_list" value="<?php Tools::printHTML(Tools::readPost('to_authors_list')); ?>" />
      <input type="hidden" name="to_accept" value="<?php Tools::printHTML(Tools::readPost('to_accept')); ?>" />
      <input type="hidden" name="to_maybe_accept" value="<?php Tools::printHTML(Tools::readPost('to_maybe_accept')); ?>" />
      <input type="hidden" name="to_discuss" value="<?php Tools::printHTML(Tools::readPost('to_discuss')); ?>" />
      <input type="hidden" name="to_maybe_reject" value="<?php Tools::printHTML(Tools::readPost('to_maybe_reject')); ?>" />
      <input type="hidden" name="to_reject" value="<?php Tools::printHTML(Tools::readPost('to_reject')); ?>" />
      <input type="hidden" name="to_void" value="<?php Tools::printHTML(Tools::readPost('to_void')); ?>" />
      <input type="hidden" name="to_custom1" value="<?php Tools::printHTML(Tools::readPost('to_custom1')); ?>" />
      <input type="hidden" name="to_custom2" value="<?php Tools::printHTML(Tools::readPost('to_custom2')); ?>" />
      <input type="hidden" name="to_custom3" value="<?php Tools::printHTML(Tools::readPost('to_custom3')); ?>" />
      <input type="hidden" name="to_custom4" value="<?php Tools::printHTML(Tools::readPost('to_custom4')); ?>" />
      <input type="hidden" name="to_custom5" value="<?php Tools::printHTML(Tools::readPost('to_custom5')); ?>" />
      <input type="hidden" name="to_custom6" value="<?php Tools::printHTML(Tools::readPost('to_custom6')); ?>" />
      <input type="hidden" name="to_custom7" value="<?php Tools::printHTML(Tools::readPost('to_custom7')); ?>" />
      <input type="hidden" name="uncheck" value="<?php Tools::printHTML(Tools::readPost('uncheck')); ?>" />
      <?php 
      foreach($reviewerRecipients as $reviewer) {
	if(Tools::readPost('r'.$reviewer->getReviewerNumber()) == 'yes') {
	  print('<input type="hidden" name="r'.$reviewer->getReviewerNumber().'" value="yes" />');
	}
      }
      foreach($articleRecipients as $article) {
	if(Tools::readPost('a'.$article->getArticleNumber()) == 'yes') {
	  print('<input type="hidden" name="a'.$article->getArticleNumber().'" value="yes" />');
	}
      }
      ?>
      <input type="submit" class="buttonLink bigButton" value="Gửi mail" />
    </form>
    </td></tr></table>
    </center>
    <?php 

  } else {
    
    /* In case there is no warning, send mail without confirmation */
    
    $templateToSave = new MailTemplate();
    $templateToSave->createFromPost();
    /* create two tables of all recipients */
    $reviewerList = array();
    foreach($reviewerRecipients as $reviewer) {
      if(Tools::readPost('r'.$reviewer->getReviewerNumber()) == 'yes') {
	$reviewerList[$reviewer->getReviewerNumber()] = $reviewer;
      }
    }
    $articleList = array();
    foreach($articleRecipients as $article) {
      if(Tools::readPost('a'.$article->getArticleNumber()) == 'yes') {
	$articleList[$article->getArticleNumber()] = $article;
      }
    }
    if(count($reviewerList) != count($reviewerRecipients)) {
      $templateToSave->setToCommitteeList($reviewerList);
    }
    if(count($articleList) != count($articleRecipients)) {
      $templateToSave->setToAuthorsList($articleList);
    }
    
    if(!$templateToSave->writeXML()) {
      print('<div class="ERRmessage">Không thể lưu mẫu XML trong lịch sử.'//Could not save XML template in history.
        .'</div>');
    }

    /* Send Mails !!! ******************************************************************************************************************/

    $currentSubject;
    $currentBody;
    $nbrOfSuccReviewers = 0;
    $nbrOfSuccArticles = 0;
    $addressListError = "";

    foreach($reviewerList as $reviewer) {
      if(count($articleRecipients) == 0) {
	$currentSubject = Tools::parseMailToCommittee($templateToSave->getSubject(), $reviewer);
	$currentBody = Tools::parseMailToCommittee($templateToSave->getBody(), $reviewer);
      } else {
	$currentSubject = Tools::parseMail($templateToSave->getSubject());
	$currentBody = Tools::parseMail($templateToSave->getBody());      
      }
      if($reviewer->sendMassMail($currentSubject,$currentBody)) {
	$nbrOfSuccReviewers++;
      } else {
	$addressListError .= '<tr><td>' . htmlentities($reviewer->getEmail(), ENT_COMPAT | ENT_HTML401, 'UTF-8') . '</td><td>' . htmlentities($reviewer->getFullName(), ENT_COMPAT | ENT_HTML401, 'UTF-8') . '</td></tr>';
      }
    }

    foreach($articleList as $article) {
      if(count($reviewerRecipients) == 0) {
	$currentSubject = Tools::parseMailToAuthors($templateToSave->getSubject(), $article);
	$currentBody = Tools::parseMailToAuthors($templateToSave->getBody(), $article);
      } else {
	$currentSubject = Tools::parseMail($templateToSave->getSubject());
	$currentBody = Tools::parseMail($templateToSave->getBody());      
      }
      if($article->sendMail($currentSubject,$currentBody)) {
	$nbrOfSuccArticles++;
      } else {
	$addressListError .= '<tr><td>' . htmlentities($article->getContact(), ENT_COMPAT | ENT_HTML401, 'UTF-8') . '</td><td>Article ' . htmlentities($article->getArticleNumber(), ENT_COMPAT | ENT_HTML401, 'UTF-8') . '</td></tr>';
      }
    }

    
    print('<div class="OKmessage">Email đã được gửi thành công tới người đánh giá: '//Your email was successfully sent to '
      . $nbrOfSuccReviewers .'/' . count($reviewerList).' và tác giả: ' //. ' reviewers and ' 
      . $nbrOfSuccArticles.'/' . count($articleList) .// ' contact authors.
      '</div>');
    Log::logMassMail($currentReviewer);
    if($addressListError != "") {
      print('<div class="ERRmessage"> E-mail không thể gửi đến địa chỉ dưới đây: '//Your e-mail could not be sent to the following addresses:
        .'<br /><center><table>' 
        . $addressListError . '</table></center></div>');
    }

    ?>
    <div class="floatRight">
    <form action="mailing.php" method="post">
      <input type="submit" class="buttonLink bigButton" value="Tiếp tục" />
    </form>
    </div>
    <?php 

  }

}

?>

<?php include('footer.php'); ?>
