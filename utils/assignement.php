<?php 

/*
 * Copyright (C) 2006, 2007 Thomas Baigneres, Matthieu Finiasz
 *
 * This file is part of iChair.
 *
 * iChair is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 * 
 * iChair is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

?><?php 

class Assignement {

  /* Parameters for the automatic assignation */

  public static $MAXGRADE =  100000;
  public static $MINGRADE = -100000;

  /* Constant for the status of a reviewer for the article */

  public static $AUTHORIZED = "authorized";
  public static $BLOCKED    = "blocked";
  public static $ASSIGNED   = "assigned";

  public static $VOID       = "void";
  public static $INPROGRESS = "inProgress";
  public static $COMPLETED  = "completed";

  public static $WANTED = 2;
  public static $NOTWANTED = 0;
  public static $DONTCARE = 1;
  
  public static $COMMENTED = "commented";
  public static $NOTCOMMENTED = "notcommented";

  public static $NOTIFY = "notify";
  public static $DONOTNOTIFY = "donotnotify";
  
  

  /* A couple article x reviewer is a primary key for an assignement */

  private $articleNumber;
  private $reviewerNumber;

  private $assignementStatus; /* can be either "authorized", "blocked", or "assigned" */
  private $reviewStatus;      /* can be either "void" (when the reviewer has not started his review yet), "inProgress", or "completed" */
  private $commentStatus;
  
  private $personalComment;   /* A reviewer can enter a personal comment, not part of the review, that is invisible to the rest of the world */

  private $affinity;          /* The grade given by the reviewer to the article, depending on his will to review it or not */
  private $mailNotification;

  function __construct() {}

  static function createDB() {
    $db = new SQLite3(Tools::getConfig("server/reviewsPath")."assignements.db");
    $db->exec("CREATE TABLE assignements(articleNumber, reviewerNumber, " .
               "assignementStatus, reviewStatus, commentStatus, personalComment, affinity, mailNotification)");
  }

  static function openDB() {
    $dbFile = Tools::getConfig("server/reviewsPath")."assignements.db";
    if(!file_exists($dbFile)) {
      Assignement::createDB();
    }
    return new SQLite3($dbFile);
  }

  function createFromDB($db_row) {
    $this->articleNumber = $db_row['articleNumber'];
    $this->reviewerNumber = $db_row['reviewerNumber'];
    $this->assignementStatus = $db_row['assignementStatus'];
    $this->reviewStatus = $db_row['reviewStatus'];
    $this->commentStatus = $db_row['commentStatus'];
    $this->personalComment = base64_decode($db_row['personalComment']);
    $this->affinity = $db_row['affinity'];
    $this->mailNotification = $db_row['mailNotification'];
  }

  function createFromScratch($articleNumber, $reviewerNumber) {
    $this->articleNumber = $articleNumber;
    $this->reviewerNumber = $reviewerNumber;
    $this->assignementStatus = Assignement::$AUTHORIZED;
    $this->reviewStatus = Assignement::$VOID;
    $this->commentStatus = Assignement::$NOTCOMMENTED;
    $this->personalComment = "";
    $this->affinity = Assignement::$DONTCARE;
    $this->mailNotification = Assignement::$DONOTNOTIFY;
    $this->insertInDB();
  }

  function setAssignementStatus($assignementStatus) {
    $this->assignementStatus = $assignementStatus;
    $this->updateInDB();
  }

  function setReviewStatus($reviewStatus) {
    $this->reviewStatus = $reviewStatus;
    $this->updateInDB();
  }

  function setCommentStatus($commentStatus) {
    $this->commentStatus = $commentStatus;
    $this->updateInDB();
  }

  function setAffinity($affinity) {
    $this->affinity = $affinity;
    $this->updateInDB();
  }

  function insertInDB() {
    $db = Assignement::openDB();
    $db->exec('INSERT INTO assignements VALUES ("' 
	       . $this->articleNumber . '","'
	       . $this->reviewerNumber . '","'
	       . $this->assignementStatus . '","'
	       . $this->reviewStatus . '","'
	       . $this->commentStatus . '","'
	       . base64_encode($this->personalComment) . '","'
	       . $this->affinity . '","'
	       . $this->mailNotification . '")');
  }

  /* This static function deletes all the assignements for a specific reviewer */
  static function deleteAllReviewerAssignements($reviewerNumber) {
    $db = Assignement::openDB();
    $db->exec('DELETE FROM assignements WHERE reviewerNumber = "' . $reviewerNumber . '"');
  }

  static function deleteAllArticleAssignements($articleNumber) {
    $db = Assignement::openDB();
    $db->exec('DELETE FROM assignements WHERE articleNumber = "' . $articleNumber . '"');
  }

  function updateInDB() {
    $db = Assignement::openDB();
    $db->exec("UPDATE assignements SET " 
	       . "assignementStatus=\"" . $this->assignementStatus . "\", "
	       . "reviewStatus=\"" . $this->reviewStatus . "\", "
	       . "commentStatus=\"" . $this->commentStatus . "\", "
	       . "personalComment=\"" . base64_encode($this->personalComment) . "\", "
	       . "affinity=\"" . $this->affinity . "\", "
	       . "mailNotification=\"" . $this->mailNotification . "\" "
	       . "WHERE articleNumber=\"" . $this->articleNumber . "\" AND reviewerNumber=\"" . $this->reviewerNumber . "\"");
  }


  static function getByNumbers($articleNumber, $reviewerNumber) {
    if(!preg_match("/[0-9]*/",$articleNumber) || !preg_match("/[0-9]*/",$reviewerNumber)) {
      return;
    }
    $db = Assignement::openDB();
    $result = $db->querySingle("SELECT * FROM assignements WHERE articleNumber = \"" . $articleNumber . "\" AND reviewerNumber = \"" . $reviewerNumber . "\"", true);
    if (!empty($result)) {
      $bozo = new Assignement();
      $bozo->createFromDB($result);
      return $bozo;
    }
    return;
  }

  static function getByReviewerNumber($reviewerNumber) {
    if(!preg_match("/[0-9]*/",$reviewerNumber)) {
      return;
    }
    $db = Assignement::openDB();
    $result = $db->query('SELECT * FROM assignements WHERE reviewerNumber = "' . $reviewerNumber . '"');
    $assignements = array();
    while($db_row = $result->fetchArray()) {
      $bozo = new Assignement();
      $bozo->createFromDB($db_row);
      $assignements[$bozo->getArticleNumber()] = $bozo;
    }
    return $assignements;
  }

  function isSomeWorkDone() {
    return (($this->assignementStatus == Assignement::$ASSIGNED) || ($this->reviewStatus != Assignement::$VOID) || ($this->commentStatus == Assignement::$COMMENTED));
  }

  function printWorkDone() {
    if($this->assignementStatus == Assignement::$ASSIGNED) {
      if ($this->reviewStatus == Assignement::$COMPLETED) {
	print('<div class="completedReview">Assigned</div>');
      } else if ($this->reviewStatus == Assignement::$INPROGRESS) {
	print('<div class="inProgressReview">Assigned</div>');
      } else {
	print('<div class="voidReview">Assigned</div>');
      } 
    } else if ($this->reviewStatus != Assignement::$VOID) {
      if ($this->reviewStatus == Assignement::$COMPLETED) {
	print('<div class="completedReview">Review</div>');
      } else {
	print('<div class="inProgressReview">Review</div>');
      }      
    } else if ($this->commentStatus == Assignement::$COMMENTED) {
      print('<b>Comment</b>');
    } else {
      print('&nbsp;');
    }
  }


  function getAssignementStatus() {
    return $this->assignementStatus;
  }

  static function isValidAssignementStatus($assignementStatus) {
    return (($assignementStatus == Assignement::$BLOCKED) || ($assignementStatus == Assignement::$ASSIGNED) || ($assignementStatus == Assignement::$AUTHORIZED));
  }

  function getReviewStatus() {
    return $this->reviewStatus;
  }

  function getPersonalComment() {
    return $this->personalComment;
  }

  function getAffinity() {
    return $this->affinity;
  }

  function getArticleNumber() {
    return $this->articleNumber;
  }

  function getReviewerNumber() {
    return $this->reviewerNumber;
  }

  function getXMLFileName() {
    return sprintf("review%04d.xml", $this->reviewerNumber);
  }

  static function getAssignementsByStatus($status, $complementary) {
    $db = Assignement::openDB();
    if($complementary) {
      $result = $db->query('SELECT * FROM assignements WHERE assignementStatus<>"' . $status . '"');
    } else {
      $result = $db->query('SELECT * FROM assignements WHERE assignementStatus="' . $status . '"');
    }
    $i = 0;
    $bozo = array();
    while($db_row = $result->fetchArray()) {
      $bozo[$i] = new Assignement();
      $bozo[$i]->createFromDB($db_row);
      $i++;
    }
    return $bozo;
  }

  static function getAssignementsByReviewerNumber($reviewerNumber) {
    $db = Assignement::openDB();
    $result = $db->query('SELECT * FROM assignements WHERE reviewerNumber = "' . $reviewerNumber . '"');
    $bozo = array();
    while($db_row = $result->fetchArray()) {
      $as = new Assignement();
      $as->createFromDB($db_row);
      $bozo[$as->getArticleNumber()] = $as;
    }
    return $bozo;
  }

  static function getAssignementsByArticleNumber($articleNumber) {
    $db = Assignement::openDB();
    $result = $db->query('SELECT * FROM assignements WHERE articleNumber = "' . $articleNumber . '"');
    $bozo = array();
    while($db_row = $result->fetchArray()) {
      $as = new Assignement();
      $as->createFromDB($db_row);
      $bozo[$as->getReviewerNumber()] = $as;
    }
    return $bozo;
  }

  static private function getNumberOfAssignementsByAffinity($reviewerNumber, $affinity) {
    $db = Assignement::openDB();
    $result = $db->querySingle('SELECT COUNT(*) FROM assignements WHERE reviewerNumber="' . $reviewerNumber . '" AND affinity="' . $affinity . '"');
    return $result;
  }

  /* This methode generates a database of suggested assignements and return an empty string if everything went ok, or an error message if it */
  /* is not the case. */
  
  static function generateSuggestionAssignationTable($keepPrevious) {
    $status = "";
    $path = Tools::getConfig('server/reviewsPath');
    $tmpDbFile = $path . 'tmp_assignements.db';
    if(file_exists($tmpDbFile)) {
      unlink($tmpDbFile);
    }
    $tmpDb = new SQLite3($tmpDbFile);
    $tmpDb->exec('CREATE TABLE assignements(articleNumber, reviewerNumber, grade, assigned, rand)');
    $db = Assignement::openDB();
    $reviewers = Reviewer::getAllActiveReviewers();
    $reviewersAssignedTotal = array();
    $reviewersMaxNumberOfArticles = array();
    $articleNumbers;
    $allArticles = Article::getAllArticles();
    // read all the assignements and put them in a huge array
    $allAssignements;
    $result = $db->query('SELECT * FROM assignements');
    while ($db_row = $result->fetchArray()) {
      $assignement = new Assignement();
      $assignement->createFromDB($db_row);
      $allAssignements[$assignement->getArticleNumber()][$assignement->getReviewerNumber()] = $assignement;
    }

    foreach($reviewers as $reviewer) {
      $reviewerNumber = $reviewer->getReviewerNumber();
      if ($reviewer->getNumberOfArticles() != "") {
	$reviewersMaxNumberOfArticles[$reviewerNumber] = $reviewer->getNumberOfArticles();
      }
      $reviewersAssignedTotal[$reviewerNumber] = 0;
      if($keepPrevious) {
	// inline
	// $articleNumbers = $reviewer->getArticlesByStatus(Assignement::$ASSIGNED);
	$articleNumbers = array();
	$result = $db->query("SELECT articleNumber FROM assignements WHERE " . 
			     "reviewerNumber = \"" . $reviewerNumber . "\" AND " . 
			     "assignementStatus = \"" . Assignement::$ASSIGNED . "\" " . 
			     "ORDER BY articleNumber");
	$i = 0;
	while($db_row = $result->fetchArray()) {
	  $articleNumbers[$i] = $db_row['articleNumber'];
	  $i++;
	}
	// end inline
	
	foreach($articleNumbers as $articleNumber) {
	  /* Assign a min grade to an article allready assigned (trick ;-) )*/
	  $tmpDb->exec('INSERT INTO assignements VALUES ("' . $articleNumber . '","' . $reviewerNumber . '","' . Assignement::$MINGRADE . '","true", "'. rand() .'")');
	  $reviewersAssignedTotal[$reviewerNumber]++;
	}
	// inline
	// $articleNumbers = $reviewer->getArticlesByStatus(Assignement::$AUTHORIZED);
	$articleNumbers = array();
	$result = $db->query("SELECT articleNumber FROM assignements WHERE " . 
			     "reviewerNumber = \"" . $reviewerNumber . "\" AND " . 
			     "assignementStatus = \"" . Assignement::$AUTHORIZED . "\" " . 
			     "ORDER BY articleNumber");
	$i = 0;
	while($db_row = $result->fetchArray()) {
	  $articleNumbers[$i] = $db_row['articleNumber'];
	  $i++;
	}
	// end inline
      }
      else {
	// inline 
	// $articleNumbers = $reviewer->getNotBlockedArticles();
	$articleNumbers = array();
	$i = 0;
	$result = $db->query("SELECT articleNumber FROM assignements WHERE " . 
			     "reviewerNumber = \"" . $reviewerNumber . "\" AND " . 
			     "assignementStatus <> \"" . Assignement::$BLOCKED . "\" " . 
			     "ORDER BY articleNumber");
	while($db_row = $result->fetchArray()) {
	  $articleNumbers[$i] = $db_row['articleNumber'];
	  $i++;
	}
	// end inline
      }
      // inline 
      // $numberOfWantedAssignements = Assignement::getNumberOfAssignementsByAffinity($reviewerNumber, Assignement::$WANTED);
      // $numberOfNotWantedAssignements = Assignement::getNumberOfAssignementsByAffinity($reviewerNumber, Assignement::$NOTWANTED);
      // $numberOfOtherAssignements = Assignement::getNumberOfAssignementsByAffinity($reviewerNumber, Assignement::$DONTCARE);
      $numberOfWantedAssignements = $db->querySingle('SELECT COUNT(*) FROM assignements WHERE reviewerNumber="' . $reviewerNumber . '" AND affinity="' . Assignement::$WANTED . '"');
      $numberOfNotWantedAssignements = $db->querySingle('SELECT COUNT(*) FROM assignements WHERE reviewerNumber="' . $reviewerNumber . '" AND affinity="' . Assignement::$NOTWANTED . '"');
      $numberOfOtherAssignements = $db->querySingle('SELECT COUNT(*) FROM assignements WHERE reviewerNumber="' . $reviewerNumber . '" AND affinity="' . Assignement::$DONTCARE . '"');
      // end inline

      $wantedGrade = 0;
      $notWantedGrade = 0;
      if($numberOfWantedAssignements != 0) {
	$wantedGrade = Assignement::$MAXGRADE / $numberOfWantedAssignements;
      }
      if($numberOfNotWantedAssignements != 0) {
	$notWantedGrade = Assignement::$MINGRADE / $numberOfNotWantedAssignements;
      }
      /* Foreach don't care article, give a grade equal to the number of blocked articles: this way is to prevent deadlocks in case you */
      /* are left with one article needing a last reviewer, and one reviewer which is blocked for that article. With this bonus, such a reviewer */
      /* will surely have all assignations before (!) .*/
      // inline
      // $dontcareGrade = count($reviewer->getArticlesByStatus(Assignement::$BLOCKED));
      $dontcareGrade = $db->querySingle('SELECT COUNT(*) FROM assignements WHERE ' . 
			   'reviewerNumber = "' . $reviewerNumber . '" AND ' . 
			   'assignementStatus = "' . Assignement::$BLOCKED . '"');
      // end inline

      foreach($articleNumbers as $articleNumber) {
	// inline
	// $assignement = Assignement::getByNumbers($articleNumber, $reviewerNumber);
	// $result = $db->query('SELECT * FROM assignements WHERE articleNumber = "' . $articleNumber . '" AND reviewerNumber = "' . $reviewerNumber . '"');
	// $assignement = new Assignement();
	// $assignement->createFromDB($result->current());
	// end inline
	$assignement = $allAssignements[$articleNumber][$reviewerNumber];
	$bonus = 0; 
	if(Tools::useCategory()) {
	  $articleCategory = $allArticles[$articleNumber]->getCategory();
	  if(strpos(' ;'.$reviewer->getPreferedCategory().';',';'.$articleCategory.';')) {
	    $bonus += 10;
	  }
	}
	switch($assignement->getAffinity()) {
	case Assignement::$WANTED:
	  $tmpDb->exec('INSERT INTO assignements VALUES ("' . $articleNumber . '","' . $reviewerNumber . '","' . ($wantedGrade+$bonus) . '","false", "'. rand() .'")');
	  break;
	case Assignement::$NOTWANTED:
	  $tmpDb->exec('INSERT INTO assignements VALUES ("' . $articleNumber . '","' . $reviewerNumber . '","' . ($notWantedGrade+$bonus) . '","false", "'. rand() .'")');
	  break;
	default:
	  $tmpDb->exec('INSERT INTO assignements VALUES ("' . $articleNumber . '","' . $reviewerNumber . '","' . ($dontcareGrade+$bonus) . '","false", "'. rand() .'")');
	  break;
	}
      }
    }

    /* AT THIS POINT ALL ASSIGNEMENTS SHOULD HAVE A GRADE IN OUR TEMPORARY TABLE */

    $scorefile = fopen(Tools::getConfig("server/reviewsPath")."scores.txt", "w");
    
    /* we compute the sum of the maxes of articles assigned per reviewer, then the average number of articles left to assign to other reviewers */
    $averageAssignementsPerReviewer = Article::getTotalNumberOfNeededReviews() / Reviewer::getNumberOfActiveReviewers();
    $totalOfTheMaxes = 0;
    $numberBelowAverage = 0;
    foreach($reviewersMaxNumberOfArticles as $maxRevs){
      if ($maxRevs < $averageAssignementsPerReviewer) {
	$totalOfTheMaxes += $maxRevs;
	$numberBelowAverage++;
      }
    }

    if ($numberBelowAverage == Reviewer::getNumberOfActiveReviewers()) {
      $averageAssignementsPerReviewer = $totalOfTheMaxes;
    } else {
      $averageAssignementsPerReviewer = (Article::getTotalNumberOfNeededReviews() - $totalOfTheMaxes) / (Reviewer::getNumberOfActiveReviewers() - $numberBelowAverage);
    }
    $maxAssignementsPerReviewer = ceil($averageAssignementsPerReviewer*1.1);
    $averageAssignementsPerReviewer = floor($averageAssignementsPerReviewer);
    /* Get all articles and for each one, choose the reviewers: this is the first pass, where each reviewer should received approx. the same nbr of articles */
    $articles = Assignement::getSortedArticleNumbers($tmpDb);
    $revarray = Reviewer::getAllReviewersSparse();
    foreach($articles as $article) {
      $articleNumber = $article->getArticleNumber();
      $result = $tmpDb->querySingle('SELECT COUNT(*) FROM assignements WHERE articleNumber="' . $article->getArticleNumber() . '" AND assigned="true"');
      $numberLeftToAssign = $article->getNumberOfReviewers() - $result;
      $result = $tmpDb->query('SELECT reviewerNumber, grade FROM assignements WHERE articleNumber="' . $article->getArticleNumber() . '" AND assigned="false" ORDER BY grade DESC, rand');
      while($numberLeftToAssign > 0 && ($dbrow = $result->fetchArray())) {
	$reviewerNumber = $dbrow['reviewerNumber'];
	if(($reviewersAssignedTotal[$reviewerNumber] < $averageAssignementsPerReviewer) && (!array_key_exists($reviewerNumber, $reviewersMaxNumberOfArticles) || ($reviewersAssignedTotal[$reviewerNumber] < $reviewersMaxNumberOfArticles[$reviewerNumber]))) {
	  $tmpDb->exec('UPDATE assignements SET assigned="true" WHERE articleNumber="' . $articleNumber . '" AND reviewerNumber="' . $reviewerNumber . '"');
	  fprintf($scorefile, "%d,%s : %d\n", $articleNumber, $revarray[$dbrow['reviewerNumber']]->getLogin(), $dbrow['grade']);
	  $reviewersAssignedTotal[$reviewerNumber]++;
	  $numberLeftToAssign--;
	}
      }
    }
    foreach($articles as $article) {
      $articleNumber = $article->getArticleNumber();
      $result = $tmpDb->querySingle('SELECT COUNT(*) FROM assignements WHERE articleNumber="' . $article->getArticleNumber() . '" AND assigned="true"');
      $numberLeftToAssign = $article->getNumberOfReviewers() - $result;
      $result = $tmpDb->query('SELECT reviewerNumber, grade FROM assignements WHERE articleNumber="' . $article->getArticleNumber() . '" AND assigned="false" ORDER BY grade DESC, rand');
      while($numberLeftToAssign > 0 && ($dbrow = $result->fetchArray())) {
	$reviewerNumber = $dbrow['reviewerNumber'];
	if(($reviewersAssignedTotal[$reviewerNumber] < $maxAssignementsPerReviewer) && (($reviewersMaxNumberOfArticles[$reviewerNumber] == "") || ($reviewersAssignedTotal[$reviewerNumber] < $reviewersMaxNumberOfArticles[$reviewerNumber]))) {
	  $tmpDb->exec('UPDATE assignements SET assigned="true" WHERE articleNumber="' . $articleNumber . '" AND reviewerNumber="' . $reviewerNumber . '"');
	  fprintf($scorefile, "%d,%s : %d\n", $articleNumber, $revarray[$dbrow['reviewerNumber']]->getLogin(), $dbrow['grade']);
	  $reviewersAssignedTotal[$reviewerNumber]++;
	  $numberLeftToAssign--;
	}
      }
      if($numberLeftToAssign > 0) {
	$status .= "Không đủ người đánh giá để phân công đánh giá bài viết "//Could not assign enough reviewers to article " 
  . $articleNumber . ".\n";
      }
    }
    fclose($scorefile);
    /* Check for articles for which all reviewers are blocked */
    $articleNumbers = Article::getAllArticleNumbers();
    foreach ($articleNumbers as $articleNumber) {
      if (Assignement::isAllActiveReviewersBlockedByArticleNumber($articleNumber)) {
	$status .= "Tất cả người đánh giá điều bị chặn truy cập bài viết "
  //All reviewers are blocked for article " 
  . $articleNumber . "!\n";
      }
    }
    return $status;
  }

  /* Returns an array of articles sorted according to their highest temporary assignement grade */

  static function getSortedArticleNumbers($db) {
    $result = $db->query('SELECT DISTINCT articleNumber FROM assignements ORDER BY grade DESC');
    $articles = array();
    $i = 0;
    while($dbrow = $result->fetchArray()) {
      $articles[$i] = Article::getByArticleNumber($dbrow['articleNumber']);
      $i++;
    }
    return $articles;
  }


  /* Once the chair has accepted the assignation suggestions, the temporary table must be turned into an assignement table */

  static function transferTempAssignements() {
    $db = Assignement::openDB();
    $path = Tools::getConfig('server/reviewsPath');
    $dbFile = $path . 'tmp_assignements.db';
    if(!file_exists($dbFile)) {
      return "Could not find temporary database.";
    }
    $tmpDb = new SQLite3($dbFile);
    $result = $tmpDb->query('SELECT * FROM assignements');
    $db->exec('UPDATE assignements SET assignementStatus="'. Assignement::$AUTHORIZED .'" WHERE assignementStatus <> "' . Assignement::$BLOCKED . '"');
    while($dbrow = $result->fetchArray()) {
      if($dbrow['assigned'] == "true") {
	$db->exec('UPDATE assignements SET assignementStatus="' . Assignement::$ASSIGNED . '" WHERE articleNumber="' . $dbrow['articleNumber'] . '" AND reviewerNumber="' . $dbrow['reviewerNumber'] . '"');
      }
    }
    unlink($dbFile);
  }

  static function getByArticleNumberAndReviewStatus($articleNumber, $reviewStatus) {
    $db = Assignement::openDB();
    $result = $db->query('SELECT * FROM assignements WHERE articleNumber="' . $articleNumber . '" AND reviewStatus="'. $reviewStatus .'"');
    $i = 0;
    $bozo = array();
    while($db_row = $result->fetchArray()) {
      $bozo[$i] = new Assignement();
      $bozo[$i]->createFromDB($db_row);
      $i++;
    }
    return $bozo;

  }

  static function getByArticleNumberAndAssignementStatus($articleNumber, $assignementStatus) {
    $db = Assignement::openDB();
    $result = $db->query('SELECT * FROM assignements WHERE articleNumber="' . $articleNumber . '" AND assignementStatus="'. $assignementStatus .'"');
    $i = 0;
    $bozo = array();
    while($db_row = $result->fetchArray()) {
      $bozo[$i] = new Assignement();
      $bozo[$i]->createFromDB($db_row);
      $i++;
    }
    return $bozo;
  }

  static function getNotAssignedNotVoidByArticleNumber($articleNumber) {
    $db = Assignement::openDB();
    $result = $db->query('SELECT * FROM assignements WHERE articleNumber="' . $articleNumber . '" AND reviewStatus<>"'. Assignement::$VOID .'" AND assignementStatus<>"'.Assignement::$ASSIGNED.'"');
    $i = 0;
    $bozo = array();
    while($db_row = $result->fetchArray()) {
      $bozo[$i] = new Assignement();
      $bozo[$i]->createFromDB($db_row);
      $i++;
    }
    return $bozo;
  }

  static function isAllActiveReviewersBlockedByArticleNumber($articleNumber) {
    $allBlocked = true;
    $db = Assignement::openDB();
    $result = $db->query('SELECT reviewerNumber FROM assignements WHERE articleNumber="' . $articleNumber . '" AND assignementStatus<>"'. Assignement::$BLOCKED .'"');
    while(($db_row = $result->fetchArray()) && $allBlocked) {
      $reviewer = Reviewer::getByReviewerNumber($db_row['reviewerNumber']);
      if ($reviewer->getGroup() != Reviewer::$OBSERVER_GROUP) {
	$allBlocked = false;
      }
    }
    return $allBlocked;
  }

  /* longest name for a function ever (?) */

  static function getSortedNotBlockedAssignementsByReviewerNumber($reviewerNumber, $reverse, $showAll, $showPreferred, $sortByReviewStatus) {
    $db = Assignement::openDB();
    $query = 'SELECT * FROM assignements WHERE reviewerNumber="' . $reviewerNumber . '" AND assignementStatus<>"' . Assignement::$BLOCKED . '"';
    if(!$showAll) {
      if (!$showPreferred) {
        $query .= ' AND assignementStatus="' . Assignement::$ASSIGNED . '"';
      } else {
        $query .= ' AND (assignementStatus="' . Assignement::$ASSIGNED . '" OR affinity="'. Assignement::$WANTED . '" OR reviewStatus<>"'. Assignement::$VOID .'")';
      }
    }
    if($sortByReviewStatus) {
      if(!$reverse) {
	$query .= ' ORDER BY reviewStatus,articleNumber';
      } else {
	$query .= ' ORDER BY reviewStatus DESC,articleNumber DESC';
      }
    } else {
      if(!$reverse) {
	$query .= ' ORDER BY articleNumber';
      } else {
	$query .= ' ORDER BY articleNumber DESC';
      }
    }
    $result = $db->query($query);    
    $i = 0;
    $bozo = array();
    while($db_row = $result->fetchArray()) {
      $bozo[$i] = new Assignement();
      $bozo[$i]->createFromDB($db_row);
      $i++;
    }
    return $bozo;
  }
}
