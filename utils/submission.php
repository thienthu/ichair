<?php 

/*
 * Copyright (C) 2006, 2007 Thomas Baigneres, Matthieu Finiasz
 *
 * This file is part of iChair.
 *
 * iChair is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 * 
 * iChair is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

?><?php 
include 'version.php';
//include 'PHPMailer/PHPMailerAutoload.php';

class Submission {

  public $mailer;

  private $submissionNumber;      /* Unique submission number -> used only by reviewers. Submitters shall refer to $id */
  private $id;                    /* Unique submission id -> used only by submitters. Reviewers shall refer to $submissionNumber */
  private $version;               /* Version corresponding to this submission */
  private $isWithdrawn;           /* Set to true when the paper is withdrawn, false otherwise */
  private $isCommitteeMember;
  private $isCustomCheck1;
  private $isCustomCheck2;
  private $isCustomCheck3;
  private $MD5collisions;
  private $folder;                /* Folder containing the different versions of the article */
  public  $status = array();      /* contains boolean "isSuccessfull" (set to true if contruction went well) */
                                  /* and a string "message" (displayed in any case) */
  private $contact;

  private $db;

 function __construct(){
    $this->mailer= new PHPMailer;

    $this->mailer->isSMTP();                                      // Set mailer to use SMTP
    $this->mailer->Host = 'smtp.gmail.com;';  // Specify main and backup SMTP servers
    $this->mailer->SMTPAuth = true;                               // Enable SMTP authentication

    // 
    $this->mailer->Username =  Tools::getConfig("mail/admin");     // SMTP username
    $this->mailer->Password =   Tools::getConfig("mail/passmail_admin");               // SMTP password
    $this->mailer->SMTPSecure =      "tls";      // Enable encryption, 'ssl' also accepted

    //$this->mail->From = 'admin@cit.ctu.edu.vn';
    //$this->mail->FromName = 'Nguyen Gia Hung';
    //$this->mail->addAddress('giahung24@gmail.com', 'Joe User');     // Add a recipient
    //$this->mail->addReplyTo('jean.deanny@gmail.com', 'Information - Please reply to this email');
    //$this->$mail->addCC('cc@example.com');
    //$this->$mail->addBCC('bcc@example.com');
    $this->mailer->CharSet = 'UTF-8';
    $this->mailer->WordWrap = 50;                
  }


  function createFromPOST() {

    /* Before the object is actually created, the arguments must be tested */
    $this->status['isSuccessfull'] = True;
    $this->status['message'] = "";
    $this->checkSubmissionPost();
    $this->checkVersionPost();
    $this->checkPostedFile();

    if ($this->status['isSuccessfull']){

      /* Generate an id for the submission, increase the length of the id in case of collision */
      /* a character of the id is in [0-9A-V] -> 5 bits/character. 6 characters => 30 bits, which should be long enough */
      /* As is should not be possible to a malicious adversary to withdraw some article by "guessing" a valid id, we keep 60 bits => 12 characters */
      do {
	$this->id = Tools::generateIdString(Tools::$ID_LENGTH);
      } while (!is_null(Submission::getByID($this->id)));
      
      $this->contact = trim(Tools::readPost('contact'));
      $this->isWithdrawn = "False";
      if (Tools::readPost('committeeMember') == "yes") {
	$this->isCommitteeMember = "True";
      } else {
	$this->isCommitteeMember = "False";
      }
      if (Tools::readPost('customCheck1') == "yes") {
	$this->isCustomCheck1 = "True";
      } else {
	$this->isCustomCheck1 = "False";
      }
      if (Tools::readPost('customCheck2') == "yes") {
	$this->isCustomCheck2 = "True";
      } else {
	$this->isCustomCheck2 = "False";
      }
      if (Tools::readPost('customCheck3') == "yes") {
	$this->isCustomCheck3 = "True";
      } else {
	$this->isCustomCheck3 = "False";
      }
      $this->MD5collisions = "";
      $db = Submission::openDB();
      
      do {

	$result = $db->querySingle("SELECT submissionNumber FROM submissions ORDER BY submissionNumber DESC");
	if(is_null($result)) {
	  $this->submissionNumber = 1;
	} else {
	  $this->submissionNumber = $result+1;
	}
	
	$this->folder = Submission::makeNewSubmissionFolder();
	
	$success = $db->exec("INSERT INTO submissions VALUES (\"" 
		    . $this->submissionNumber . "\", \"" 
		    . base64_encode($this->id) . "\", \"" 
		    . base64_encode($this->isWithdrawn) . "\", \"" 
		    . base64_encode($this->isCommitteeMember) . "\", \"" 
		    . base64_encode($this->isCustomCheck1) . "\", \"" 
		    . base64_encode($this->isCustomCheck2) . "\", \"" 
		    . base64_encode($this->isCustomCheck3) . "\", \"" 
		    . base64_encode($this->MD5collisions) . "\", \"" 
		    . base64_encode($this->folder) . "\", \"" 
		    . base64_encode($this->contact) . "\")");
      } while(!$success);

      $this->version = new Version();
      $this->version->createFromPOST($this);

      if (Tools::readPost('mailNotification') != "nomail") {
        $this->sendSubmissionMail();
      }
    }
  }

  function reviseFromPOST() {
    /* Before the object is actually created, the arguments must be tested */
    $this->status['isSuccessfull'] = True;
    $this->checkVersionPost();
    if($_FILES['file']['name'] != "") {
      $this->checkPostedFile();
    }

    if ($this->status['isSuccessfull']){
      if (Tools::readPost('committeeMember') == "yes") {
	$this->setCommittee();
      } else {
	$this->setNotCommittee();
      }
      if (Tools::readPost('customCheck1') == "yes") {
	$this->setCustomCheck1();
      } else {
	$this->setNotCustomCheck1();
      }
      if (Tools::readPost('customCheck2') == "yes") {
	$this->setCustomCheck2();
      } else {
	$this->setNotCustomCheck2();
      }
      if (Tools::readPost('customCheck3') == "yes") {
	$this->setCustomCheck3();
      } else {
	$this->setNotCustomCheck3();
      }
      
      $this->version = new Version();
      $this->version->createFromPOST($this);
      if (Tools::readPost('mailNotification') != "nomail") {
        $this->sendRevisionMail();
      }
    }
  }

  function makeNewSubmissionFolder() {
    $folder = sprintf("%04d",$this->submissionNumber)."submission/";
    @mkdir(Tools::getConfig("server/submissionsPath").$folder);
    return $folder;
  }

  function deleteSubmission() {
    /* Remove the submission from the submission database */
    $this->deleteFromDB();
    /* Empty the submission Folder from all the versions */
    $versions = $this->getAllVersions();
    foreach($versions as $version) {
      $version->deleteVersion();
    }
    /* remove the database file */
    unlink($this->getFolder() . "versions.db");
    /* Delete directory if it is empty */
    return @rmdir(Tools::getConfig("server/submissionsPath").$this->folder);    
  }

  static function createDB() {
    $db = new SQLite3(Tools::getConfig("server/submissionsPath")."submissions.db");
    $db->exec("CREATE TABLE submissions(submissionNumber INTEGER PRIMARY KEY, id, isWithdrawn, isCommitteeMember, isCustomCheck1, isCustomCheck2, isCustomCheck3, MD5collisions, folder, contact)");
  }

  static function openDB() {
    $dbFile = Tools::getConfig("server/submissionsPath")."submissions.db";
    if (!file_exists($dbFile)){
      Submission::createDB();
    }
    return new SQLite3($dbFile);
  }

  function deleteFromDB() {
    $db = Submission::openDB();
    $db->exec('DELETE FROM submissions WHERE submissionNumber = "' . $this->submissionNumber . '"');
  }

  function createFromDB($db_row) {
    $this->submissionNumber = $db_row['submissionNumber'];
    $this->id = base64_decode($db_row['id']);
    $this->isWithdrawn = base64_decode($db_row['isWithdrawn']);
    $this->isCommitteeMember = base64_decode($db_row['isCommitteeMember']);
    $this->isCustomCheck1 = base64_decode($db_row['isCustomCheck1']);
    $this->isCustomCheck2 = base64_decode($db_row['isCustomCheck2']);
    $this->isCustomCheck3 = base64_decode($db_row['isCustomCheck3']);
    $this->MD5collisions = base64_decode($db_row['MD5collisions']);
    $this->folder = base64_decode($db_row['folder']);
    $this->contact = base64_decode($db_row['contact']);
  }

  function createDummy() {
    $this->submissionNumber = (rand() % 100) + 1;
    $this->id = Tools::generateIdString(Tools::$ID_LENGTH);
    $this->isWithdrawn = "False";
    $this->isCommitteeMember = "False";
    $this->isCustomCheck1 = "False";
    $this->isCustomCheck2 = "False";
    $this->isCustomCheck3 = "False";
    $this->MD5collisions = "";
    $this->contact = "dummy@submission.com";
    $this->version = new Version();
    $this->version->createDummy();
  }


  function checkSubmissionPost() {
    if(!Tools::isValidEmail(Tools::readPost('contact'))) {
      $this->status['message'] .= "Vui lòng cung cấp địa chỉ tác giả chính xác (một địa chỉ email hoặc nhiều địa chỉ cách nhau bởi dấu phẩy)"
      //Please provide a valid email address for your contact author (either a single email address, or a comma separated list of addresses).
      ."\n";
      $this->status['isSuccessfull'] = False;
    }
    if(Tools::readPost('disclaimer') != "yes") {
      $this->status['message'] .= "Phải đồnng ý với điều khoản gửi bài viết trước khi gửi."
      //You must agree with the submission conditions before you can submit.
      ."\n";
      $this->status['isSuccessfull'] = False;
    }
  }

  function checkVersionPost() {
    if(trim(Tools::readPost('title')) == "") {
      $this->status['message'] .= "Vui lòng cung cấp tiều đề bài viết gửi."
      //Please provide a title for your submission.
      ."\n";
      $this->status['isSuccessfull'] = False;
    } else if(trim(Tools::readPost('title')) != Tools::filterControlChars(trim(Tools::readPost('title')))) {
      $this->status['message'] .= "Tiêu đề của bài viết gửi có chứa ký tự điều khiển không hợp lệ (có thể là chữ ghép như fi hoặc ffi từ pdf sao chép-dán). 
      Xin vui lòng xác minh tính chính xác của tiêu đề của bạn (và các lĩnh vực khác sao chép trực tiếp từ pdf)."
      //The title of your submission contains invalid control characters (probably ligatures like fi or ffi from a pdf copy-paste). Please verify the correctness of your title (and of other fields copied directly from a pdf).
      ."\n";
      $this->status['isSuccessfull'] = False;
    }
    foreach($_POST['authors'] as $i => $val) {
      if ((trim(Tools::readPost('affiliations', $i)) == "") && (Tools::readPost('country', $i) == "") && (trim($val) == "")) {
        continue;
      }
      if(trim($val) == "") {
        $this->status['message'] .= "Vui lòng cung cấp tên tác giả"
        //Please provide the name of author " 
        . ($i+1) . ".\n";
        $this->status['isSuccessfull'] = False;
      }
      if(Tools::useAffiliations() && (trim(Tools::readPost('affiliations', $i)) == "")) {
        $this->status['message'] .= "Vui lòng cung cấp tổ chức của tác giả"
        //Please provide the affiliation of author " 
        . ($i+1) . ".\n";
        $this->status['isSuccessfull'] = False;
      }
      if(Tools::useCountry() && (Tools::readPost('country', $i) == "")) {
        $this->status['message'] .= "Vui lòng chọn quốc gia"
        //Please select the country of author " 
        . ($i+1) . ".\n";
        $this->status['isSuccessfull'] = False;
      }
    }
    if (trim(implode($_POST['authors'])) == "") {
      $this->status['message'] .= "Vui lòng nhập ít nhất một tác gỉa của bài viết gửi."
      //Please enter at least one author for your submission.
      ."\n";
      $this->status['isSuccessfull'] = False;
    }
    if(Tools::useAbstract() && (trim(Tools::readPost('abstract')) == "")) {
      $this->status['message'] .= "Vui lòng cung cấp tóm tắc của bài viết."
      //Please provide an abstract for your submission.
      ."\n";
      $this->status['isSuccessfull'] = False;
    }
    if(Tools::useCategory() && (Tools::readPost('category') == "")) {
      $this->status['message'] .= "Vui lòng chọn chủ đề bài viết."
      //Please select a topic for your submission.
      ."\n";
      $this->status['isSuccessfull'] = False;
    }
    if(Tools::useKeywords() && (trim(Tools::readPost('keywords')) == "")) {
      $this->status['message'] .= "Vui lòng cung cấp một vài từ khóa có liên quan đển bài viết gửi."//Please provide a few relevant keywords for your submission.
      ."\n";
      $this->status['isSuccessfull'] = False;
    }                                          
  }

  function checkPostedFile() {
    if($_FILES['file']['name'] == "") {
      $this->status['message'] .= "Vui lòng cung cấp các tập tin tương ứng với bài viết gửi."//Please provide the file corresponding to your submission.
      ."\n";
      $this->status['isSuccessfull'] = False;
    } else if($_FILES['file']['error'] != UPLOAD_ERR_OK) {
      if ($_FILES['file']['error'] == UPLOAD_ERR_INI_SIZE) {
	$this->status['message'] .= "Tập tin tải lên vượt quá kích thước "
//  The file you uploaded is larger than the maximum allowed file size of "
  .ini_get('upload_max_filesize')." cho phép.";
      } else {
	$this->status['message'] .= "Lỗi khi nhận tập tin.Tập tin vượt quá kích thước cho phép."
  //An error occured when receiving your file. Maybe it exceeds the maximum file size allowed.
  ."\n";
      }
      $this->status['isSuccessfull'] = False;
    } else {
      $filetype=Tools::testFileType();
      if (!$filetype['valid']) {
        $this->status['message'] .= "Tập tin tải lên có định dạng"//The header of the file you uploaded indicates it is of "
        .$filetype['type'].//" format. 
        "Chỉ cho phép định dạng PostScript và PDF."//Only PostScript and PDF files with valid headers are allowed.
        ."\n";
        $this->status['isSuccessfull'] = False;
      } 
    }   
    
  }

  /* Returns a submission instance, given its id */

  static function getByID($id) {
    $db = Submission::openDB();
    $result = $db->querySingle("SELECT * FROM submissions WHERE id = \"" . base64_encode($id) . "\"", true);
    if (empty($result)) {
      return;
    } else {
      $bozo = new Submission();
      $bozo->createFromDB($result);
      return $bozo;
    }
  }

  /* Returns a submission instance, given its number */

  static function getBySubmissionNumber($submissionNumber) {
    if(!preg_match("/[0-9]*/",$submissionNumber)) {
      return;
    }
    $db = Submission::openDB();
    $result = $db->querySingle("SELECT * FROM submissions WHERE submissionNumber = \"" . $submissionNumber . "\"", true);
    if (empty($result)) {
      return;
    } else {
      $bozo = new Submission();
      $bozo->createFromDB($result);
      return $bozo;
    }    
  }

  function getLastVersionFileType() {
    $version = $this->getLastVersion();
    $file = $version->getFile();
    if(preg_match("/\.[pP][sS]$/", $file)) {
      return("ps");
    } else if(preg_match("/\.[pP][dD][fF]$/", $file)) {
      return("pdf");
    } 
    return("unknown");
  }

  function getFolder() {
    return Tools::getConfig("server/submissionsPath").$this->folder;
  }

  function getAllVersions() {
    return Version::getAllVersions($this);
  }

  function printInfo() {
    print("Mã:"
//ID:
."&nbsp;".htmlentities($this->id, ENT_COMPAT | ENT_HTML401, 'UTF-8')."<br />");
    print("Liên hệ tác giả: "
//Contact&nbsp;author:&nbsp;"
.htmlentities($this->contact, ENT_COMPAT | ENT_HTML401, 'UTF-8')."<br />");
  }

  function getIsWithdrawn() {
    if($this->isWithdrawn == "False") {
      return false;
    }
    return true;
  }

  function getIsCommitteeMember() {
    if($this->isCommitteeMember == "False") {
      return false;
    }
    return true;
  }

  function getIsCustomCheck1() {
    if($this->isCustomCheck1 == "False") {
      return false;
    }
    return true;
  }

  function getIsCustomCheck2() {
    if($this->isCustomCheck2 == "False") {
      return false;
    }
    return true;
  }

  function getIsCustomCheck3() {
    if($this->isCustomCheck3 == "False") {
      return false;
    }
    return true;
  }

  static function getAllSubmissions() {
    $submissions=array();
    $i=0;
    $db = Submission::openDB();
    $result = $db->query("SELECT * FROM submissions");
    while ($db_row = $result->fetchArray()){
      $ns = new Submission();
      $ns->createFromDB($db_row);
      $submissions[$i] = $ns;
      $i++;
    }
    return $submissions;
  }

  static function getAllNotWithdrawnSubmissions() {
    $submissions=array();
    $db = Submission::openDB();
    $result = $db->query("SELECT * FROM submissions WHERE isWithdrawn=\"" . base64_encode("False") . "\"");
    while ($db_row = $result->fetchArray()){
      $ns = new Submission();
      $ns->createFromDB($db_row);
      $submissions[$ns->getSubmissionNumber()] = $ns;
    }
    return $submissions;
  }

  static function getAllWithdrawnSubmissions() {
    $submissions=array();
    $db = Submission::openDB();
    $result = $db->query('SELECT * FROM submissions WHERE isWithdrawn="' . base64_encode("True") . '"');
    while ($db_row = $result->fetchArray()){
      $ns = new Submission();
      $ns->createFromDB($db_row);
      $submissions[$ns->getSubmissionNumber()] = $ns;
    }
    return $submissions;
  }

  function getId() {
    return $this->id;
  }

  function printLastVersion() {
    $version = $this->getLastVersion();
    $version->printLong();
    $version->printShort();
  }

  function printLastVersionBr() {
    $version = $this->getLastVersion();
    $version->printLongBr();
    $version->printShort();
  }

  function printCustomCheckboxes($forchair = false, $print = true) {
    $out = '<div class="customchecks">';
    if (Tools::useCustomCheck1() && ($forchair || !Tools::chaironlyCustomCheck1()) && $this->getIsCustomCheck1()) {
      $out .= '<div>' . htmlentities(Tools::getCustomCheck1(), ENT_COMPAT | ENT_HTML401, 'UTF-8') . '</div>';
    }
    if (Tools::useCustomCheck2() && ($forchair || !Tools::chaironlyCustomCheck2()) && $this->getIsCustomCheck2()) {
      $out .= '<div>' . htmlentities(Tools::getCustomCheck2(), ENT_COMPAT | ENT_HTML401, 'UTF-8') . '</div>';
    }
    if (Tools::useCustomCheck3() && ($forchair || !Tools::chaironlyCustomCheck3()) && $this->getIsCustomCheck3()) {
      $out .= '<div>' . htmlentities(Tools::getCustomCheck3(), ENT_COMPAT | ENT_HTML401, 'UTF-8') . '</div>';
    }
    $out .= '</div>';
    if ($print) {
      print($out);
    } else {
      return $out;
    }
  }

  function getLastVersion() {
    return Version::getLastVersion($this);
  }
  
  function getLastVersionNumber() {
    return Version::getLastVersionNumber($this);
  }
        

  function getContact() {
    return $this->contact;
  }

  function setContact($email) {
    $this->contact = $email;
    $this->db = Submission::openDB();
    $this->db->exec("UPDATE submissions SET contact=\"" . base64_encode($this->contact) . "\" WHERE submissionNumber=\"" . $this->submissionNumber . "\"");    
  }

  function getVersion() {
    return $this->version;
  }

  /* A typical string looks like #1;2#1>3-1:4-2,3;3>1-4 */
  /* meaning that versions 1 and 2 of this submission contain Javascript */
  /* and that version 1 has the same md5 as submission 3 version 1 and submission 4 versions 2 and 3 */
  /* and that version 3 has the same md5 as submission 1 version 4 */

  function getMD5collisions() {
    return preg_replace("/^#.*#/","",$this->MD5collisions);
  }

  function getJavascriptWarnings() {
    if(preg_match("/#/", $this->MD5collisions)) {
      return str_replace("#", "", preg_replace("/#[^#]*$/","",$this->MD5collisions));
    }
    return "";
  }

  function addMD5collision($collisionString) {
    if ($this->getMD5collisions() != ""){
      $this->MD5collisions .= ";";
    }
    $this->MD5collisions .= $collisionString;
    $this->db = Submission::openDB();
    $this->db->exec("UPDATE submissions SET MD5collisions=\"" . base64_encode($this->MD5collisions) . "\" WHERE submissionNumber=\"" . $this->submissionNumber . "\"");    
  }

  function addJavascriptWarning($versionNumber) {
    $bozo = $this->getJavascriptWarnings();
    if ($bozo != ""){
      $bozo .= ";";
    }
    $bozo .= $versionNumber;
    $this->MD5collisions = "#" . $bozo . "#" . $this->getMD5collisions();
    $this->db = Submission::openDB();
    $this->db->exec("UPDATE submissions SET MD5collisions=\"" . base64_encode($this->MD5collisions) . "\" WHERE submissionNumber=\"" . $this->submissionNumber . "\"");    
  }

  function printMD5collisions() {
    if($this->getMD5collisions() != "") {
      print('<div class="ERRmessage"> Mã MD5 có một vài sự trùng lặp (tập tin của bài gửi giống nhau):'//There are some MD5 collisions:
        .'<br />');
      $topCollisions = explode(";",$this->getMD5collisions());
      foreach ($topCollisions as $topCollision) {
	$subs = explode(">",$topCollision);
	print('Phiên bản '//Version&nbsp;'
    .$subs[0].
    ' trùng lặp với: '//collides with: '
    );
	$listOfCols = "";
	$cols = explode(":", $subs[1]);
	foreach ($cols as $col) {
	  $verss = explode("-",$col);
	  if ($listOfCols != "") {
	    $listOfCols .= ", ";
	  }
	  $listOfCols .= 'Bài gửi: '//Submission&nbsp;'
    .$verss[0].' (phiên bản';//version';
	  $vcols = explode(",",$verss[1]);
	  if (count($vcols) > 1) {
	    $listOfCols .= 's';
	  }
	  $listOfVers = " ";
	  foreach ($vcols as $vcol) {
	    if ($listOfVers != " ") {
	      $listOfVers .= ", ";
	    }
	    $listOfVers .= $vcol;
	  }
	  $listOfCols .= $listOfVers.")";
	}
	print($listOfCols."<br/>");
      }
      print('</div>');
    }
  }

  function printJavascriptWarnings() {
    if($this->getJavascriptWarnings() != "") {
      $s = "Javascript has been detected in this submission:<br />";
      $lastVersionNumber = $this->getLastVersionNumber();
      $jsInLastVersion = false;
      $topJS = explode(";",$this->getJavascriptWarnings());
      foreach ($topJS as $versionNumber) {
	$s .= "Version " . $versionNumber . " contains Javascript.<br />";
	$jsInLastVersion |= ($lastVersionNumber == $versionNumber);
      }
      if($jsInLastVersion) {
	print('<div class="ERRmessage">');
      } else {
	print('<div class="OKmessage">');
      }
      print($s);
      if(!$jsInLastVersion) {
	print("The last submitted version no longer contains Javascript.<br />");
      }
      print('</div>');
    }
  }

  function setCommittee() {
    $this->isCommitteeMember = "True";
    $this->db = Submission::openDB();
    $this->db->exec("UPDATE submissions SET isCommitteeMember=\"" . base64_encode($this->isCommitteeMember) . "\" WHERE submissionNumber=\"" . $this->submissionNumber . "\"");    
  }

  function setNotCommittee() {
    $this->isCommitteeMember = "False";
    $this->db = Submission::openDB();
    $this->db->exec("UPDATE submissions SET isCommitteeMember=\"" . base64_encode($this->isCommitteeMember) . "\" WHERE submissionNumber=\"" . $this->submissionNumber . "\"");
  }

  function setCustomCheck1() {
    $this->isCustomCheck1 = "True";
    $this->db = Submission::openDB();
    $this->db->exec("UPDATE submissions SET isCustomCheck1=\"" . base64_encode($this->isCustomCheck1) . "\" WHERE submissionNumber=\"" . $this->submissionNumber . "\"");    
  }

  function setNotCustomCheck1() {
    $this->isCustomCheck1 = "False";
    $this->db = Submission::openDB();
    $this->db->exec("UPDATE submissions SET isCustomCheck1=\"" . base64_encode($this->isCustomCheck1) . "\" WHERE submissionNumber=\"" . $this->submissionNumber . "\"");
  }

  function setCustomCheck2() {
    $this->isCustomCheck2 = "True";
    $this->db = Submission::openDB();
    $this->db->exec("UPDATE submissions SET isCustomCheck2=\"" . base64_encode($this->isCustomCheck2) . "\" WHERE submissionNumber=\"" . $this->submissionNumber . "\"");    
  }

  function setNotCustomCheck2() {
    $this->isCustomCheck2 = "False";
    $this->db = Submission::openDB();
    $this->db->exec("UPDATE submissions SET isCustomCheck2=\"" . base64_encode($this->isCustomCheck2) . "\" WHERE submissionNumber=\"" . $this->submissionNumber . "\"");
  }

  function setCustomCheck3() {
    $this->isCustomCheck3 = "True";
    $this->db = Submission::openDB();
    $this->db->exec("UPDATE submissions SET isCustomCheck3=\"" . base64_encode($this->isCustomCheck3) . "\" WHERE submissionNumber=\"" . $this->submissionNumber . "\"");    
  }

  function setNotCustomCheck3() {
    $this->isCustomCheck3 = "False";
    $this->db = Submission::openDB();
    $this->db->exec("UPDATE submissions SET isCustomCheck3=\"" . base64_encode($this->isCustomCheck3) . "\" WHERE submissionNumber=\"" . $this->submissionNumber . "\"");
  }

  function setWithdrawn() {
    $this->isWithdrawn = "True";
    $this->db = Submission::openDB();
    $this->db->exec("UPDATE submissions SET isWithdrawn=\"" . base64_encode($this->isWithdrawn) . "\" WHERE submissionNumber=\"" . $this->submissionNumber . "\"");    
  }

  function setNotWithdrawn() {
    $this->isWithdrawn = "False";
    $this->db = Submission::openDB();
    $this->db->exec("UPDATE submissions SET isWithdrawn=\"" . base64_encode($this->isWithdrawn) . "\" WHERE submissionNumber=\"" . $this->submissionNumber . "\"");
  }

  function sendSubmissionMail() {
    $this->version = Version::getLastVersion($this);
    $message = Tools::getConfig("mail/submissionMessage");
    $subject = Tools::getConfig("mail/submissionSubject");
    $message = Tools::parseMailToSubmiters($message, $this, "S");
    $subject = Tools::parseMailToSubmiters($subject, $this, "S");
    $this->sendMail($subject, $message);
  }
    
  function sendRevisionMail() {
    $this->version = Version::getLastVersion($this);
    $message = Tools::getConfig("mail/revisionMessage");
    $subject = Tools::getConfig("mail/revisionSubject");
    $message = Tools::parseMailToSubmiters($message, $this, "R");
    $subject = Tools::parseMailToSubmiters($subject, $this, "R");
    $this->sendMail($subject, $message);
  }

  function sendWithdrawalMail() {
    $this->version = Version::getLastVersion($this);
    $message = Tools::getConfig("mail/withdrawMessage");
    $subject = Tools::getConfig("mail/withdrawSubject");
    $message = Tools::parseMailToSubmiters($message, $this, "W");
    $subject = Tools::parseMailToSubmiters($subject, $this, "W");
    $this->sendMail($subject, $message);
  }

  function sendMail($subject, $message) {
    $headers = "From: " . Tools::getConfig("mail/authorsFrom") . "\r\n" .
      "Reply-To: " . Tools::getConfig("mail/authorsFrom") . "\r\n" .
      "Bcc: " . Tools::getConfig("mail/admin");
    if(Tools::useGlobalBCC()) {
      $headers .= "," . Tools::getConfig("mail/globalBCC") . "\r\n";
    } else {
      $headers .= "\r\n";
    }
    $headers .= 'X-Mailer: PHP/' . phpversion();
    // if (!mail($this->getContact(), $subject, $message, $headers, "-f ".Tools::getConfig("mail/authorsFrom"))) {
    //   Log::logMailError($this->getContact(), $subject, $message, $headers);
    // }
    $this->mailer->From = Tools::getConfig("mail/authorsFrom");
    $this->mailer->FromName =  Tools::getConfig("conference/name");
    $this->mailer->addReplyTo(Tools::getConfig("mail/authorsFrom") );
    $this->mailer->addBCC(Tools::getConfig("mail/admin"));
    if(Tools::useGlobalBCC()) {
      $this->mailer->addBCC(Tools::getConfig("mail/globalBCC"));
    } 

    $this->mailer->Subject = $subject;
    $this->mailer->Body    = $message;

    // split contact
    $contact_arr = explode(",", $this->getContact());
    foreach ($contact_arr as $contact_email) {
      $this->mailer->addAddress($contact_email);  
    }
        
    if (!$this->mailer->send()) {
       Log::logMailError($this->getContact(), $subject, $message, $headers);
    }
  }

  function getAllChairComments() {
    $versions = $this->getAllVersions();
    $lastVersion = $this->getLastVersion();
    $comments="";
    for ($i = count($versions) - 1; $i > 0; $i--) {
      if($versions[$i]->getChairComments() != "") {
	$comments .= "\nPhiên bản "//Version "
   . $versions[$i]->getVersionNumber() .": ".$versions[$i]->getChairComments();
      }
    }

    $noPreviousComment = $comments == "";

    if ($lastVersion->getChairComments() == "") {
      $comments = "Không có."//None." 
      . $comments;
    } else {
      $comments = $lastVersion->getChairComments() . $comments;
    }
    if (!$noPreviousComment) {
      $comments = "Phiên bản  "//Version " 
      . $lastVersion->getVersionNumber() . ": " . $comments;
    }
    return $comments;
  }

  function printAllChairComments() {
    $comments = $this->getAllChairComments();
    print("<div class=\"chairComments\">\n");
    Tools::printHTMLbr($comments);
    print("\n</div>\n");
  }

  function getSubmissionNumber() {
    return $this->submissionNumber;
  }

  function printDownloadLink() {
    $this->getLastVersion()->printDownloadLink();
  }

  function printEditChairCommentsLink() {
    $this->getLastVersion()->printEditChairCommentsLink('submissions');
  }

  function printUnwithdrawLink() {
    print("<div class=\"floatRight\">");
    print("<form action=\"unwithdraw_submission.php\" method=\"post\">\n");
    print("<input type=\"hidden\" name=\"submissionNumber\" value=\"". $this->getSubmissionNumber() . "\" />\n");
    print("<input type=\"submit\" class=\"buttonLink\" value=\"Không hủy\" />\n");
    print("</form>\n");
    print("</div>\n");
  }

  function printWithdrawLink() {
    print('<div class="floatRight">');
    print('<form action="withdraw_submission.php" method="post">');
    print('<input type="hidden" name="submissionNumber" value="'. $this->getSubmissionNumber() . '" />');
    print('<input type="submit" class="buttonLink" value="Hủy" />');
    print('</form>');
    print('</div>');
  }

  function printNotCommitteeLink() {
    print("<div class=\"floatRight\">");
    print("<form action=\"uncommittee_submission.php\" method=\"post\">\n");
    print("<input type=\"hidden\" name=\"submissionNumber\" value=\"". $this->getSubmissionNumber() . "\" />\n");
    print("<input type=\"submit\" class=\"buttonLink\" value=\"Không thuộc ủy ban\" />\n");
    print("</form>\n");
    print("</div>\n");
  }

  function printCommitteeLink() {
    print('<div class="floatRight">');
    print('<form action="committee_submission.php" method="post">');
    print('<input type="hidden" name="submissionNumber" value="'. $this->getSubmissionNumber() . '" />');
    print('<input type="submit" class="buttonLink" value="Thuộc ủy ban" />');
    print('</form>');
    print('</div>');
  }

  function printTrashLink() {
    print('<div class="floatRight">');
    print('<form action="delete_submission.php" method="post">');
    print('<input type="hidden" name="submissionNumber" value="'. $this->getSubmissionNumber() . '" />');
    print('<input type="submit" class="buttonLink" value="Xóa" />');
    print('</form>');
    print('</div>');
  }

  static function printSummaryTable() {
    $submissions = Submission::getAllSubmissions();
    $totals = array();
    $totals['submissions'] = 0;
    $totals['notWithdrawn'] = 0;
    $totals['withdrawn'] = 0;
    $totals['committee'] = 0;
    $totals['customcheck1'] = 0;
    $totals['customcheck2'] = 0;
    $totals['customcheck3'] = 0;
    foreach($submissions as $submission) {
      $totals['submissions']++;
      if ($submission->getIsWithdrawn()) {
	$totals['withdrawn']++;
      } else {
	$totals['notWithdrawn']++;
	if ($submission->getIsCommitteeMember()) {
	  $totals['committee']++;
	}
	if ($submission->getIsCustomCheck1()) {
	  $totals['customcheck1']++;
	}
	if ($submission->getIsCustomCheck2()) {
	  $totals['customcheck2']++;
	}
	if ($submission->getIsCustomCheck3()) {
	  $totals['customcheck3']++;
	}
      }
    }
    print('<table class="dottedTable"><tr>');
    print('<th class="topRow" colspan="4">Tóm tắt bài viết gửi hiện tại'//Summary of current submissions
      .'</th>');
    print('</tr><tr>');
    print('<th class="center">Bài viết đã gửi'//Submissions
      .'</th>');
    print('<th class="center">Bài viết đã hủy'//Withdrawn
      .'</th>');
    print('<th class="center">Bài viết không hủy<!--Not Withdrawn--></th>');
    print('<th class="center">From Committee</th>');
    if (Tools::useCustomCheck1()) {
      print('<th class="center">' . htmlentities(Tools::getCustomCheck1(), ENT_COMPAT | ENT_HTML401, 'UTF-8') . '</th>');
    }
    if (Tools::useCustomCheck2()) {
      print('<th class="center">' . htmlentities(Tools::getCustomCheck2(), ENT_COMPAT | ENT_HTML401, 'UTF-8') . '</th>');
    }
    if (Tools::useCustomCheck3()) {
      print('<th class="center">' . htmlentities(Tools::getCustomCheck3(), ENT_COMPAT | ENT_HTML401, 'UTF-8') . '</th>');
    }
    print('</tr><tr>');
    print('<td class="bigNumber center">' . $totals['submissions'] . '</td>');
    print('<td class="bigNumber voidReview center">' . $totals['withdrawn'] . '</td>');
    print('<td class="bigNumber completedReview center">' . $totals['notWithdrawn'] . '</td>');
    print('<td class="bigNumber inProgressReview center">' . $totals['committee'] . '</td>');
    if (Tools::useCustomCheck1()) {
      print('<td class="bigNumber customAccept1 center">' . $totals['customcheck1'] . '</td>');
    }
    if (Tools::useCustomCheck2()) {
      print('<td class="bigNumber customAccept1 center">' . $totals['customcheck2'] . '</td>');
    }
    if (Tools::useCustomCheck3()) {
      print('<td class="bigNumber customAccept1 center">' . $totals['customcheck3'] . '</td>');
    }
    print('</tr></table>');
  }

}

?>
