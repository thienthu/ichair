<?php 

/*
 * Copyright (C) 2006, 2007 Thomas Baigneres, Matthieu Finiasz
 *
 * This file is part of iChair.
 *
 * iChair is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 * 
 * iChair is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

?><?php include("../utils/tools.php");?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
  <title>Preview Test</title>
  <link href="../css/admin_<?php print(Tools::getConfig("miscellaneous/adminSkin"));?>.css" rel="stylesheet" type="text/css" />
</head>
<body class="nomargin">

<?php if ($_GET['format']=="ps") { ?>
<h1>Test Postscript Preview</h1>
<?php } else { ?>
<h1>Test PDF Preview</h1>
<?php } ?>
<div class="paperBoxDetails">
<center>
<?php 
print('<img src="test_preview.php?format='.$_GET['format'].'" />');
?>
</center>
</div>
<div class="ERRmessage">If you see a &quot;No Preview&quot; image above, an error occured, please refer to the iChair documentation to see the possible reasons. If you cannot solve this problem we recommend deactivating the previews.</div>

<form>
<center>
<input type="submit" class="buttonLink bigButton" value="Đóng" onClick="javascript:window.close();"/>
</center>
</form>

</body>
</html>
