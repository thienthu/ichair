<?php 

/*
 * Copyright (C) 2006, 2007 Thomas Baigneres, Matthieu Finiasz
 *
 * This file is part of iChair.
 *
 * iChair is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 * 
 * iChair is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

?><?php 
$page_title = "Thay Đổi Mật Khẩu";//"Change Password";
include '../utils/tools.php';
include "header.php";

$status = "";
$oldHash = $currentReviewer->getPasswordHash();
$oldSalt = "";
if(preg_match("/^[$]1[$]/", $oldHash)) {
  $pos = strpos(substr($oldHash,3),"$");
  $oldSalt = substr($oldHash,0,$pos+4);
} else {
  $oldSalt = substr($oldHash,0,2);
}


if($oldHash != crypt(Tools::readPost('old'), $oldSalt)) {
  $status .= "Mật khẩu cũ không đúng."
  //Incorrect old password.
  ."\n";
} else if(Tools::readPost('new1') != Tools::readPost('new2')) {
  $status .= "Nhập lại mật khẩu không trùng khớp."
  //The two new passwords do not match.
  ."\n";  
} else if(!preg_match("/.{4}/",Tools::readPost('new1'))) {
  $status .= "Mật khẩu quá ngắn. Mật khẩu phải ít nhất 4 ký tự!"
  //Your new password is too short. It should be at least 4 characters long!
  ."\n";
}

if($status != "") {
  print("<div class=\"ERRmessage\">" . $status . "</div>");

?>
  <form action="profile.php" method="post">
  <div class="floatRight">
  <input type="submit" class="buttonLink bigButton" value="Trở về" />
  </div>
  </form>
<?php 
  
} else {

  $newSalt = base64_encode(Tools::readPost('new1'));
  $newHash = crypt(Tools::readPost('new1'),$newSalt);
  
  $currentReviewer->setPasswordHash($newHash);
  $currentReviewer->updatePasswordHashInDB();
  Log::logPassword($currentReviewer);

  ?>

  <div class="OKmessage">Cập nhật mật khẩu thành công. Mật khẩu đã có hiệu lực.
 <!-- Your password was updated successfully. This new password is effective immediately.-->
  </div>
  
  <form action="index.php" method="post">
  <div class="floatRight">
  <input type="submit" class="buttonLink bigButton" value="Tiếp tục" />
  </div>
  </form>

<?php 

}

?>

<?php include('footer.php'); ?>
