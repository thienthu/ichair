<?php 

/*
 * Copyright (C) 2006, 2007 Thomas Baigneres, Matthieu Finiasz
 *
 * This file is part of iChair.
 *
 * iChair is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 * 
 * iChair is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

?><?php 
if(file_exists("header_custom.php")) {
  include("header_custom.php");
} else {
header("Content-type: text/html; charset=utf-8");
?>
 <!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
  <title><?php print(htmlentities(Tools::getConfig("conference/name"), ENT_COMPAT | ENT_HTML401, 'utf-8')." - ".$page_title); ?></title>
  <link href="css/main_<?php print(Tools::getConfig("miscellaneous/submissionSkin"));?>.css" rel="stylesheet" type="text/css" media="screen" />
  <link href="css/print_<?php print(Tools::getConfig("miscellaneous/submissionSkin"));?>.css" rel="stylesheet" type="text/css" media="print" />
  <link rel="SHORTCUT ICON" type="image/png" href="images/icon.png" />
  <script type="text/javascript" src="js/jquery.min.js"></script>
  <script type="text/javascript" src="js/authors.js"></script>  
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="viewport" content="width=device-width">
  <link rel="stylesheet" href="css/bootstrap.css" media="screen">
  <link rel="stylesheet" href="css/bootswatch.min.css">
</head>



<body>

<?php 

$cfpHTML = Tools::hasCallForPaperHTML(".");
$cfpPDF  = Tools::hasCallForPaperPDF(".");
$cfpTXT  = Tools::hasCallForPaperTXT(".");

$subHTML = Tools::hasSubmissionGuidelinesHTML(".");
$subPDF  = Tools::hasSubmissionGuidelinesPDF(".");
$subTXT  = Tools::hasSubmissionGuidelinesTXT(".");

?>
<div class="row">
  <div class="col-xs-0 col-sm-0 col-md-1 col-lg-1">  </div>
  <!-- div main col-->
  <div class="col-xs-12 col-sm-12 col-md-10 col-lg-10">
                <!-- Bắt đầu banner -->
      <div class="col-lg-12" style="margin: -44px 0px -15px 0px">
          <div class="panel panel-default">
            <div class="panel-body" style="padding: 4px !important;">
                <img src="images/banner.png" style="max-width: 100%;">
            </div>
          </div>
      </div>
      <!-- Kết thúc banner -->

      <!-- Bắt đầu content div-->
      <div class="col-lg-12">
        <div class="row">
          <!-- Bắt đầu sidebar-->
          <div class=" col-md-3 col-lg-3" style="padding-right:5px"  >
            <div class="panel-default">
              <div class="panel-body" style="padding:0;">
                <div class="navigate"  style="width:100%;">

                  <div class="navigateTitle">
                    <a href="<?php print(Tools::getConfig("conference/site")) ?>"><?php Tools::printHTML(Tools::getConfig("conference/name")) ?></a>
                  </div>
                  <div class="navigateLinks">
                    <p><a href="index.php">Trang chủ <!--Main Page --></a></p>

                    <?php if($cfpHTML) { ?>
                      <p class="guidelines"><a class="guidelines" href="cfp/cfp.html" target="_blank">Thông báo (Call for Papers)</a>
                      <?php if($cfpPDF) { ?>
                        <a class="noblock" href="cfp/cfp.pdf" target="_blank">[pdf]</a>
                      <?php } 
                         if($cfpTXT) { ?>
                        <a class="noblock" href="cfp/cfp.txt" target="_blank">[text]</a>
                      <?php } ?>
                      </p>
                    <?php } else if($cfpPDF) { ?>
                      <p class="guidelines"><a class="guidelines" href="cfp/cfp.pdf" target="_blank">Thông báo (Call for Papers)</a>
                      <?php if($cfpTXT) { ?>
                        <a class="noblock" href="cfp/cfp.txt" target="_blank">[text]</a>
                      <?php } ?>
                      </p>
                    <?php } else if($cfpTXT) { ?>
                      <p><a href="cfp/cfp.txt" target="_blank">Thông báo (Call for Papers)</a></p>
                    <?php } ?>

                    <?php if($subHTML) { ?>
                      <p class="guidelines"><a class="guidelines" href="guidelines/guidelines.html" target="_blank">Hướng dẫn<!--Guidelines--></a>
                      <?php if($subPDF) { ?>
                        <a class="noblock" href="guidelines/guidelines.pdf" target="_blank">[pdf]</a>
                      <?php } 
                         if($subTXT) { ?>
                        <a class="noblock" href="guidelines/guidelines.txt" target="_blank">[text]</a>
                      <?php } ?>
                      </p>
                    <?php } else if($subPDF) { ?>
                      <p class="guidelines"><a class="guidelines" href="guidelines/guidelines.pdf" target="_blank">Hướng dẫn<!--Guidelines--></a>
                      <?php if($subTXT) { ?>
                        <a class="noblock" href="guidelines/guidelines.txt" target="_blank">[text]</a>
                      <?php } ?>
                      </p>
                    <?php } else if($subTXT) { ?>
                      <p><a href="guidelines/guidelines.txt" target="_blank">Hướng dẫn<!--Guidelines--></a></p>
                    <?php } ?>

                    <p class="topLine"><a href="submit.php">Gửi bài<!--Submission Form--></a></p>
                    <p><a href="revise.php">Sửa bài gửi<!--Revision Form--></a></p>
                    <p><a href="withdraw.php">Hủy bài gửi<!--Withdrawal Form--></a></p>
                    <p class="topLine"><a href="about.php">Giới thiệu ichair<!--About iChair--></a></p>
                  </div>

                  <div class="navigateTitle">
                    <a href="http://www.time.gov/timezone.cgi?UTC/s/0/java" target="_new">Thời gian hiện tại<!--Current Time--></a>
                  </div>
                  <div class="navigateTimes">
                    <div class="timeTitleTop">Thời gian GMT+7<!--UTC Time--></div>
                    <?php if(Tools::use12HourFormat()) { 
                       date_default_timezone_set('asia/saigon');?>
                      <div class="time"><?php print(date("d/m/Y - h:i&#160;a")); ?></div>
                    <?php } else {?>
                      <div class="time"><?php print(date("d/m/Y - H:i")); ?></div>      
                    <?php } ?>
                  </div>

                  <div class="navigateTitle">
                    Thời gian hết hạn gửi<!--Submission deadline-->
                  </div>
                  <div class="navigateTimes">
                    <div class="timeTitleTop">Thời gian GMT+7<!--UTC Time--></div>
                    <div class="time">
                      <?php 
                         $U = Tools::getUFromDateAndTime(Tools::getConfig("server/deadlineDate"), Tools::getConfig("server/deadlineTime"));
                    if(Tools::use12HourFormat()) {
                      print(date("d/m/Y - h:i&#160;a", $U)); 
                    } else {
                      print(date("d/m/Y - H:i", $U)); 
                    }
                      ?>
                    </div>
                    <div class="timeTitle">Thời gian còn lại<!--Time left--></div>
                    <div class="time">
                    <?php 
                    $timeLeft = $U - date("U");
                    if($timeLeft > 0) {
                      print(Tools::stringTimeLeft($timeLeft));
                    } else {
                      print("<div class=\"timeUp\">Time is up!</div>");
                    }
                    ?>
                    </div>
                  </div>

                  <div class="navigateImages">
                  <?php 
                  Tools::printLogos();
                  ?>
                  </div>

                </div>

            </div>
          </div>
      </div>
      <!-- Kết thúc sidebar -->
        <!-- Bắt đầu main content -->
      <div class="col-md-9 col-lg-9" style="padding-left:5px" >
          <div class="panel panel-default">
            <div class="panel-body" style="padding: 4px !important;">
              <h1><?php print(htmlentities(Tools::getConfig("conference/name"), ENT_COMPAT | ENT_HTML401, 'ISO-8859-1')." - ".$page_title); ?></h1>
              <?php 
              }
              ?>
