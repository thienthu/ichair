<?php
require "PHPMailerAutoload.php";
class JeanMailer{
	public $mail; 
	function __construct(){
		$this->mail= new PHPMailer;
		$this->mail->isSMTP();                                      // Set mailer to use SMTP
		$this->mail->Host = 'smtp.gmail.com;';  // Specify main and backup SMTP servers
		$this->mail->SMTPAuth = true;                               // Enable SMTP authentication

		// 
		$this->mail->Username =  Tools::getConfig("mail/admin");     // SMTP username
		$this->mail->Password =   Tools::getConfig("mail/passmail_admin");               // SMTP password
		$this->mail->SMTPSecure =      "tls";      // Enable encryption, 'ssl' also accepted

		//$this->mail->From = 'admin@cit.ctu.edu.vn';
		//$this->mail->FromName = 'Nguyen Gia Hung';
		//$this->mail->addAddress('giahung24@gmail.com', 'Joe User');     // Add a recipient
		//$this->mail->addReplyTo('jean.deanny@gmail.com', 'Information - Please reply to this email');
		//$this->$mail->addCC('cc@example.com');
		//$this->$mail->addBCC('bcc@example.com');

		$this->mail->WordWrap = 50;                                 // Set word wrap to 50 characters
		//$this->$mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
		//$this->$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name
		//$this->$mail->isHTML(true);                                  // Set email format to HTML

		//$this->mail->Subject = 'Here is the subject';
		//$this->mail->Body    = 'This is the HTML message body <b>in bold!</b>';
		//$this->mail->AltBody = 'This is the body in plain text for non-HTML mail clients';
	}

}



/*
if(!$this->$mail->send()) {
    echo 'Message could not be sent.';
    echo 'Mailer Error: ' . $this->$mail->ErrorInfo;
} else {
    echo 'Message has been sent';
}
*/