<?php 

/*
 * Copyright (C) 2006, 2007 Thomas Baigneres, Matthieu Finiasz
 *
 * This file is part of iChair.
 *
 * iChair is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 * 
 * iChair is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

?><?php 
class Version {

  private $versionNumber;
  private $title;
  private $authors;
  private $file;
  private $md5;
  private $date;
  private $affiliations;
  private $country;
  private $abstract;
  private $category;
  private $keywords;
  private $chairComments;
  private $submission;

  function __construct(){}

  function createFromPOST($submission) {

    /* if the database does not exist (i.e., in case of a new submission), create it */
    if (!file_exists($submission->getFolder()."versions.db")){
      Version::createDB($submission);
    }

    /* initialize instance's variables */
    $this->submission = $submission;
    $this->title = Tools::filterControlChars(trim(Tools::readPost('title')));

    $this->authors = "";
    $this->country = "";
    $this->affiliations = "";
    foreach($_POST['authors'] as $i => $val) {
      if (trim($val) != "") {
        $this->authors .= "Š" . Tools::filterControlChars(trim(Tools::readPost('authors', $i)));
        $this->affiliations .= "Š" . Tools::filterControlChars(trim(Tools::readPost('affiliations', $i)));
        $this->country .= "Š" . trim(Tools::readPost('country', $i));        
      }
    }
    $this->authors = substr($this->authors, 1);
    $this->affiliations = substr($this->affiliations, 1);
    $this->country = substr($this->country, 1);

    $this->versionNumber = Version::getNewVersionNumber($submission);
    $this->file = Version::makeNewVersionFile();
    if(Tools::getConfig("submission/usePreview")) {
      $this->makePreview();
    }
    $this->md5 = md5_file($submission->getFolder().$this->file);
    $this->date = time();
    $this->abstract = Tools::filterControlChars(trim(Tools::readPost('abstract')));
    $this->category = Tools::readPost('category');
    $this->keywords = Tools::filterControlChars(trim(Tools::readPost('keywords')));
    $this->chairComments = "";

    $this->insertInDB();

    /* check for md5 collisions */
    $collisionString = $this->getMD5collisions();
    if ($collisionString != "") {
      $this->submission->addMD5collision($this->versionNumber.">".$collisionString);
    }
    
    /* look for Javascript */
    if($this->containsJavascript()) {
      $this->submission->addJavascriptWarning($this->getVersionNumber());
    }
    

  }

  /* If a new file was uploaded, this function puts it in the right place. In case no file was submitted (eg. during *
   * a revision) the latest version is simply duplicated                                                             */

  private function makeNewVersionFile() {
    if($_FILES['file']['name'] != "") {
      $file = sprintf("%02d",$this->versionNumber).$_FILES['file']['name'];
      move_uploaded_file($_FILES['file']['tmp_name'],$this->submission->getFolder().$file);
    } else {
      $version = Version::getLastVersion($this->submission);
      $file = sprintf("%02d",$this->versionNumber).substr($version->getFile(),2);
      copy($this->submission->getFolder().$version->getFile(), $this->submission->getFolder().$file);
    }
    return $file;
  }

  private function makePreview() {
    $preview_file=$this->file.".preview.jpg";
    $returnVal;
    if (preg_match("/\.ps$/",$this->file)) {
      exec("psselect -p1-1 ".escapeshellarg($this->submission->getFolder().$this->file)."| gs -q -dTextAlphaBits=4 -dGraphicsAlphaBits=2 -sDEVICE=jpeg -dJPEGQ=75 -dBATCH -dNOPAUSE -dSAFER -r60 -sOutputFile=/tmp/".escapeshellarg($preview_file)." -", $output, $returnVal);
    } else {
      exec("gs -q -dTextAlphaBits=4 -dGraphicsAlphaBits=2 -sDEVICE=jpeg -dJPEGQ=75 -dLastPage=1 -dBATCH -dNOPAUSE -dSAFER -r60 -sOutputFile=/tmp/".escapeshellarg($preview_file)." ".escapeshellarg($this->submission->getFolder().$this->file), $output, $returnVal);
    }
    if ($returnVal == 0) {
      copy("/tmp/".$preview_file, $this->submission->getFolder().$preview_file);
      unlink("/tmp/".$preview_file);
    } else {
      if (file_exists("/tmp/".$preview_file)) {
        unlink("/tmp/".$preview_file);
      }
    }
  }

  private static function createDB($submission) {
    $db = new SQLite3($submission->getFolder()."versions.db");
    $db->exec("CREATE TABLE versions(versionNumber INTEGER PRIMARY KEY, title, authors, md5, file, date, affiliations, country, abstract, category, keywords, chairComments)");
  }

  static function openDB($submission) {
    $dbFile = $submission->getFolder() . "versions.db";
    if (!file_exists($dbFile)) {
      Version::createDB($submission);
    }
    return new SQLite3($dbFile);
  }

  private function createFromDB($db_row,$submission){
    $this->submission = $submission;
    $this->versionNumber = $db_row['versionNumber'];
    $this->file = base64_decode($db_row['file']);
    $this->title = base64_decode($db_row['title']);
    $this->authors = base64_decode($db_row['authors']);
    $this->md5 = base64_decode($db_row['md5']);
    $this->date = base64_decode($db_row['date']);
    $this->affiliations = base64_decode($db_row['affiliations']);
    $this->country = base64_decode($db_row['country']);
    $this->abstract = base64_decode($db_row['abstract']);
    $this->category = base64_decode($db_row['category']);
    $this->keywords = base64_decode($db_row['keywords']);
    $this->chairComments = base64_decode($db_row['chairComments']);
  }

  function createDummy(){
    $this->versionNumber = (rand() % 10) + 1;
    $this->file = "dummy_article.pdf";
    $this->title = "Dummy Title";
    $this->authors = "Matthieu and Thomas";
    $this->md5 = "01234567890123456789012345678901";
    $this->date = Tools::getUFromDateAndTime("03/08/2005","08:46");
    $this->affiliations = "Dummy Affiliation";
    $this->country = "EU_FR";
    $this->abstract = "Dummy Abstract";
    $this->category = "Dummy Category";
    $this->keywords = "Dummy Keywords";
    $this->chairComments = "";
  }

  public function getMD5(){
    return $this->md5;
  }

  static function getAllVersions($submission){
    $versions=array();
    $i=0;
    $db = Version::openDB($submission);
    $result = $db->query("SELECT * FROM versions");
    while ($db_row = $result->fetchArray()){
      $nv = new Version();
      $nv->createFromDB($db_row, $submission);
      $versions[$nv->getVersionNumber()] = $nv;
      $i++;
    }
    return $versions;
  }

  static function getLastVersion($submission) {
    $db = Version::openDB($submission);
    $result = $db->querySingle("SELECT * FROM versions ORDER BY versionNumber DESC", true);
    $lv = new Version();
    $lv->createFromDB($result, $submission);
    return $lv;
  }

  static function getLastVersionNumber($submission) {
    $db = Version::openDB($submission);
    $result = $db->querySingle("SELECT versionNumber FROM versions ORDER BY versionNumber DESC");
    return $result;
  }

  function getTitle(){
    return $this->title;
  }

  function getAuthors(){
    return implode(' and ', explode('Š', $this->authors));
  }

  function getAuthorsArray(){
    return explode('Š', $this->authors);
  }

  function getCategory(){
    return $this->category;
  }
        
  function getKeywords(){
    return $this->keywords;
  }
                 
  function getAffiliations(){
    return implode(' and ', explode('Š', $this->affiliations));
  }

  function getAffiliationsArray(){
    return explode('Š', $this->affiliations);
  }

  function getCountry(){
    $countries = explode('Š', $this->country);
    $ret = "";
    foreach($countries as $country) {
      $ret .= ' and ' . Tools::$COUNTRY_LIST[$country];
    }
    return substr($ret, 5);
  }

  function getCountryArray(){
    return explode('Š', $this->country);
  }

  function getPreviewFile(){
    if (file_exists($this->submission->getFolder().$this->file.".preview.jpg")) {
    	return $this->submission->getFolder().$this->file.".preview.jpg";
    } else {
    	return;
    }
  }

  function getVersionNumber() {
    return $this->versionNumber;
  }

  function getAbstract(){
    return $this->abstract;
  }

  function getFile() {
    return $this->file;
  }

  function getDate() {
    return $this->date;
  }

  function getChairComments() {
    return $this->chairComments;
  }

  function printShort() {
    print("Thời gian gửi: "//"Date: "
.date('r',$this->date)."<br />");
    print("Mã MD5: $this->md5");
  }

  function printLong() {
    print("<div class=\"versionTitle\">Tiêu đề: "/*Title: "*/. htmlentities($this->title, ENT_COMPAT | ENT_HTML401, 'utf-8') ."</div>\n");
    print("<div class=\"versionAuthors\"> Tác giả: "/*Authors: "*/.htmlentities($this->getAuthors(), ENT_COMPAT | ENT_HTML401, 'utf-8')."</div>\n");
    if(Tools::useAffiliations()) {
      print("Tổ chức: "/*Affiliations: "*/. htmlentities($this->getAffiliations(), ENT_COMPAT | ENT_HTML401, 'utf-8') ."<br />\n");
    }
    if(Tools::useCountry()) {
      print("Quốc gia: "/*Countries: " */. htmlentities($this->getCountry(), ENT_COMPAT | ENT_HTML401, 'utf-8') ."<br />\n");
    }
    if(Tools::useAbstract()) {
      print('<div class="versionAbstract">Tóm tắt: '/*abstract: ' */. htmlentities($this->abstract, ENT_COMPAT | ENT_HTML401, 'utf-8') . '</div>');
    }
    if(Tools::useCategory()) {
      print("Thể loại: "
//Category: "
.htmlentities(Tools::getCategoryNameById($this->category), ENT_COMPAT | ENT_HTML401, 'utf-8')."<br />\n");
    }
    if(Tools::useKeywords()) {
      print("Từ khóa: "
//Keywords: "
.htmlentities($this->keywords, ENT_COMPAT | ENT_HTML401, 'utf-8')."<br />\n");
    }
    print("Tập tin: "
//File: "
.htmlentities($this->file, ENT_COMPAT | ENT_HTML401, 'utf-8')."<br />\n");
  }

  function printLongBr() {
    print("<div class=\"versionTitle\">Tiêu đề: "/*Title: "*/. htmlentities($this->title, ENT_COMPAT | ENT_HTML401, 'utf-8')."</div>\n");
    print("<div class=\"versionAuthors\">Tác giả: "/*Authors: "*/.htmlentities($this->getAuthors(), ENT_COMPAT | ENT_HTML401, 'utf-8')."</div>\n");
    if(Tools::useAffiliations()) {
      print("Tổ chức: "/*Affiliations: "*/.htmlentities($this->getAffiliations(), ENT_COMPAT | ENT_HTML401, 'utf-8')."<br />\n");
    }
    if(Tools::useCountry()) {
      print("Quốc gia: "/*Countries: " */. htmlentities($this->getCountry(), ENT_COMPAT | ENT_HTML401, 'utf-8') ."<br />\n");
    }
    if(Tools::useAbstract()) {
      print("Tóm tắc: "./*Abstract:*/"<br />\n<div class=\"versionAbstract\">".nl2br(htmlentities($this->abstract, ENT_COMPAT | ENT_HTML401, 'utf-8'))."</div>\n");
    }
    if(Tools::useCategory()) {
      print("Thể loại: "/*Category: "*/.htmlentities(Tools::getCategoryNameById($this->category), ENT_COMPAT | ENT_HTML401, 'utf-8')."<br />\n");
    }
    if(Tools::useKeywords()) {
      print("Từ khóa: "
//Keywords: "
.htmlentities($this->keywords, ENT_COMPAT | ENT_HTML401, 'utf-8')."<br />\n");
    }
    print("Tập tin: "
//File: "
.htmlentities($this->file, ENT_COMPAT | ENT_HTML401, 'utf-8')."<br />\n");
  }


  /* This function inserts the current version object in the database */
  function insertInDB() {
    $db = Version::openDB($this->submission);
    $db->exec("INSERT INTO versions VALUES (\"" 
	       . $this->versionNumber . "\", \"" 
	       . base64_encode($this->title) . "\", \"" 
	       . base64_encode($this->authors) . "\", \"" 
	       . base64_encode($this->md5) . "\", \"" 
	       . base64_encode($this->file) . "\", \"" 
	       . base64_encode($this->date) . "\", \"" 
	       . base64_encode($this->affiliations) . "\", \"" 
	       . base64_encode($this->country) . "\", \"" 
	       . base64_encode($this->abstract) . "\", \"" 
	       . base64_encode($this->category) . "\", \"" 
	       . base64_encode($this->keywords) . "\", \"" 
	       . base64_encode($this->chairComments) . "\")");
  }

  function deleteFromDB() {
    $db = Version::openDB($this->submission);
    $db->exec('DELETE FROM versions WHERE versionNumber = "' . $this->versionNumber . '"');
  }


  /*
   * This function is called when a new version has to be inserted
   * in the database. It returns the version number it should have.
   */

  static function getNewVersionNumber($submission) {
    $db = Version::openDB($submission);
    $result = $db->querySingle("SELECT versionNumber FROM versions ORDER BY versionNumber DESC");
    if(is_null($result)) {
      return 1;
    }
    return $result + 1;
  }

  /*
   * Displays the link to the file downloader page
   * This is used in the admin pages only
   */

  function printDownloadLink() {
    print("<div class=\"floatRight\">");
    print("<form action=\"download_file.php\" method=\"post\">\n");
    print("<input type=\"hidden\" name=\"submissionNumber\" value=\"". $this->submission->getSubmissionNumber() . "\" />\n");
    print("<input type=\"hidden\" name=\"versionNumber\" value=\"". $this->getVersionNumber() . "\" />\n");
    print("<input type=\"submit\" class=\"buttonLink\" value=\"Download File\" />\n");
    print("</form>\n");
    print("</div>\n");
  }

  function printChairComments() {
    print('<div class="chairComments">');
    $comments = $this->getChairComments();
    if($comments == "") {
      print('<en>None.</em>');
    } else {
      Tools::printHTMLbr($comments);
    }
    print('</div>');
  }

  function printEditChairCommentsLink($from) {
    print("<div class=\"floatRight\">");
    print("<form action=\"edit_chair_comments.php\" method=\"post\">\n");
    print("<input type=\"hidden\" name=\"from\" value=\"". $from . "\" />\n");
    print("<input type=\"hidden\" name=\"submissionNumber\" value=\"". $this->submission->getSubmissionNumber() . "\" />\n");
    print("<input type=\"hidden\" name=\"versionNumber\" value=\"". $this->getVersionNumber() . "\" />\n");
    print("<input type=\"submit\" class=\"buttonLink\" value=\"Edit Comments\" />\n");
    print("</form>\n");
    print("</div>\n");
  }

  function setChairComments($chairComments) {
    $this->chairComments = trim($chairComments);
    $db = Version::openDB($this->submission);
    $db->exec("UPDATE versions SET chairComments=\"" . base64_encode($this->chairComments) . "\" WHERE versionNumber=\"" . $this->versionNumber . "\"");    
  }

  function deleteVersion() {
    /* Delete the version from the database */
    $this->deleteFromDB();
    /* Delete the version file if it exists */
    if(file_exists("" . $this->submission->getFolder() . $this->file)) {
      unlink("" . $this->submission->getFolder() . $this->file);
    }
    /* Delete de .jpg file if there is one */
    $jpgFile = $this->file . ".preview.jpg";
    if(file_exists("" . $this->submission->getFolder() . $jpgFile)) {
      unlink("" . $this->submission->getFolder() . $jpgFile);
    }
  }


  /* return a string coding all the collisions with previously submitted md5 sums */
  function getMD5collisions() {
    $collisionString = "";
    $submissions = Submission::getAllSubmissions();
    foreach ($submissions as $submission) {
      if ($submission->getSubmissionNumber() != $this->submission->getSubmissionNumber()) {
	$db = Version::openDB($submission);
	$result = $db->query('SELECT versionNumber FROM versions WHERE md5="'.base64_encode($this->md5).'"');
	$versionsString = "";
	while ($db_row = $result->fetchArray()) {
	  if ($versionsString != "") {
	    $versionsString .= ",";
	  }
	  $versionsString .= $db_row['versionNumber'];
	}
	if ($versionsString != "" ) {
	  if ($collisionString != "") {
	    $collisionString .= ":";
	  }
	  $collisionString .= $submission->getSubmissionNumber() . "-" . $versionsString;
	}
      }
    }
    return $collisionString;
  }
  

  /* This function tests if there is Javascript in the version file */

  function containsJavascript() {
    $string = file_get_contents($this->submission->getFolder() . $this->file);
    if(preg_match('/<<\s*\/JS\s*\(([^)]+(\\\\\)[^)]*)*)\)\s*\/S\s*\/Javascript\s*>>/i',strtr($string,"\n\r", "  "))) {
      return true;
    }
    return preg_match('/<<\s*\/S\s*\/Javascript\s*\/JS\s*\(([^)]+(\\\\\)[^)]*)*)\)\s*>>/i',strtr($string,"\n\r", "  "));
  }
  

}

?>
