<?php 

/*
 * Copyright (C) 2006, 2007 Thomas Baigneres, Matthieu Finiasz
 *
 * This file is part of iChair.
 *
 * iChair is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 * 
 * iChair is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

?><?php 
include '../utils/tools.php';
$page_title='Trong Quá Trình Thảo Luận Bài Viết '//Discussion on Article '
.htmlentities(Tools::readPost('articleNumber'), ENT_COMPAT | ENT_HTML401, 'UTF-8').htmlentities($_GET['articleNumber'], ENT_COMPAT | ENT_HTML401, 'UTF-8');
include 'header.php';

if(($currentReviewerGroup != null) && ($currentReviewer->isAllowedToViewOtherReviews())  && ((Tools::getCurrentPhase() == Tools::$DECISION) || (Tools::getCurrentPhase() == Tools::$REVIEW))) {

  $articleNumber = $_GET['articleNumber'];
  $article = Article::getByArticleNumber($articleNumber);
  if (!is_null($article)) {

    $assignement = Assignement::getByNumbers($articleNumber, $currentReviewer->getReviewerNumber());

    /* Check that the reviewer is not blocked for this article. Authorize chair (that can access this page from the "Message to Commitee" page. */
    if((($assignement->getAssignementStatus() != Assignement::$BLOCKED) && $currentReviewer->isAllowedToViewOtherReviewsForArticle($article))|| ($currentReviewerGroup == Reviewer::$CHAIR_GROUP)) {


      /* check if some new comment has been posted. Add the comment to the database.  */
      
      $newComment = Tools::Readpost('newComment');
      if($newComment != "") {
	Discussion::addComment($articleNumber, $currentReviewer->getReviewerNumber(), $newComment);
	Log::logDiscussion($articleNumber, $currentReviewer);
	$article->setLastModificationDate();
	$assignement->setCommentStatus(Assignement::$COMMENTED);
	print('<div class="OKmessage">Thêm bình luận thành công.'//Your comment has been added successfully.
		.'</div>');
      }
  
      ?>

      <div class="paperBox" id="nbr<?php print($articleNumber); ?>">
        <div class="paperBoxTitle">
	  <div class="paperBoxNumber">Article&nbsp;<?php print($articleNumber) ?></div>
	  <div class="floatRight">
	    <?php 
	    $acceptance = $article->getAcceptance();
            if($acceptance == Article::$ACCEPT) {
	      print('<span class="acceptArticle">Chấp nhận '//Accept 
	      	.'</span>');
	      $grade = $article->roundAverageOverallGrade(2);
	      if ($grade != "-") {
		print('<span class="bigNumber acceptArticle">' . $grade . '</span>');
	      }
	    } else if($acceptance == Article::$MAYBE_ACCEPT) {
	      print('<span class="maybeAcceptArticle">Có thể chấp nhận '//Maybe Accept 
	      	.'</span>');
	      $grade = $article->roundAverageOverallGrade(2);
	      if ($grade != "-") {
		print('<span class="bigNumber maybeAcceptArticle">' . $grade . '</span>');
	      }
	    } else if($acceptance == Article::$DISCUSS) {
	      print('<span class="discussArticle">Thảo luận ' //Discuss 
	      	.'</span>');
	      $grade = $article->roundAverageOverallGrade(2);
	      if ($grade != "-") {
		print('<span class="bigNumber discussArticle">' . $grade . '</span>');
	      }
	    } else if($acceptance == Article::$MAYBE_REJECT) {
	      print('<span class="maybeRejectArticle">Có thể từ chối '//Maybe Reject 
	      	.'</span>');
	      $grade = $article->roundAverageOverallGrade(2);
	      if ($grade != "-") {
		print('<span class="bigNumber maybeRejectArticle">' . $grade . '</span>');
	      }
	    } else if($acceptance == Article::$REJECT) {
	      print('<span class="rejectArticle">Từ chối '//Reject 
	      	.'</span>');
	      $grade = $article->roundAverageOverallGrade(2);
	      if ($grade != "-") {
		print('<span class="bigNumber rejectArticle">' . $grade . '</span>');
	      }
	    } else if($acceptance == Article::$CUSTOM1) {
	      print('<span class="customAccept1">' . Tools::getCustomAccept1() . ' </span>');
	      $grade = $article->roundAverageOverallGrade(2);
	      if ($grade != "-") {
		print('<span class="bigNumber customAccept1">' . $grade . '</span>');
	      }
	    } else if($acceptance == Article::$CUSTOM2) {
	      print('<span class="customAccept2">' . Tools::getCustomAccept2() . ' </span>');
	      $grade = $article->roundAverageOverallGrade(2);
	      if ($grade != "-") {
		print('<span class="bigNumber customAccept2">' . $grade . '</span>');
	      }
	    } else if($acceptance == Article::$CUSTOM3) {
	      print('<span class="customAccept3">' . Tools::getCustomAccept3() . ' </span>');
	      $grade = $article->roundAverageOverallGrade(2);
	      if ($grade != "-") {
		print('<span class="bigNumber customAccept3">' . $grade . '</span>');
	      }
	    } else if($acceptance == Article::$CUSTOM4) {
	      print('<span class="customAccept4">' . Tools::getCustomAccept4() . ' </span>');
	      $grade = $article->roundAverageOverallGrade(2);
	      if ($grade != "-") {
		print('<span class="bigNumber customAccept4">' . $grade . '</span>');
	      }
	    } else if($acceptance == Article::$CUSTOM5) {
	      print('<span class="customAccept5">' . Tools::getCustomAccept5() . ' </span>');
	      $grade = $article->roundAverageOverallGrade(2);
	      if ($grade != "-") {
		print('<span class="bigNumber customAccept5">' . $grade . '</span>');
	      }
	    } else if($acceptance == Article::$CUSTOM6) {
	      print('<span class="customAccept6">' . Tools::getCustomAccept6() . ' </span>');
	      $grade = $article->roundAverageOverallGrade(2);
	      if ($grade != "-") {
		print('<span class="bigNumber customAccept6">' . $grade . '</span>');
	      }
	    } else if($acceptance == Article::$CUSTOM7) {
	      print('<span class="customAccept7">' . Tools::getCustomAccept7() . ' </span>');
	      $grade = $article->roundAverageOverallGrade(2);
	      if ($grade != "-") {
		print('<span class="bigNumber customAccept7">' . $grade . '</span>');
	      }
	    } else {
	      $grade = $article->roundAverageOverallGrade(2);
	      if ($grade != "-") {
		print('<span class="bigNumber">' . $grade . '</span>');
	      }
	    }

	    ?>
	  </div>
	  <?php $article->printTitleAndOther(); ?>
        </div>
 
        <?php 
	$reviewReader = new ReviewReader();
	$reviewReader->createFromXMLFile($assignement);
	print('<div class="paperBoxDetails">');
	$article->printCustomCheckboxes();
        $assignements  = Assignement::getByArticleNumberAndReviewStatus($articleNumber, Assignement::$COMPLETED);
        if(count($assignements) != 0) {
          ?>
          <center>
          <table class="smallCentered">
            <tr>
              <?php ReviewReader::printReviewRowHead(); ?> 
            </tr>
            <?php 
            foreach($assignements as $assignementTmp) {
              $reviewerNumber = $assignementTmp->getReviewerNumber();
              $reviewer = Reviewer::getByReviewerNumber($reviewerNumber);
              $reviewReaderTmp = new ReviewReader();
              $reviewReaderTmp->createFromXMLFile($assignementTmp);
              ?>  
              <tr>
                <?php $reviewReaderTmp->printReviewRow(false); ?>     
              </tr>
            <?php } ?>
          </table></center><br/><?php 
        }

	if (trim($reviewReader->getPersonalNotes()) != "") {
	  print('Ghi chú cá nhận:'//Personal Notes:
	  	.'<div class="chairComments">');
	  Tools::printHTMLbr($reviewReader->getPersonalNotes());
	  print('</div>');
	}

	?>

	Cập nhật lần cuối:<!--Last Updates:-->
	<div class="versionAbstract"><?php $article->printLastModifications(7); ?></div>
      </div>

      <div class="floatRight">
        <?php $article->printDownloadArticleLink();?>
      </div>
      <?php if (($currentReviewerGroup != Reviewer::$OBSERVER_GROUP) && (Tools::getCurrentPhase() != Tools::$DECISION)){?>
        <div class="floatRight">
          <form action="review_article.php" method="post">
	    <input type="hidden" name="articleNumber" value="<?php print($articleNumber); ?>" />
	    <input type="submit" class="buttonLink" value="Đánh giá bài víêt" />
	  </form>
	</div>
      <?php } ?>
      <div class="clear"></div>
    </div>

    <center>
      <input type="submit" class="buttonLink bigButton" value="Đóng" onclick="javascript:window.close()" />
    </center>


    <?php 
    /* Display the messages from the chair concerning this article */ 

    $specificMessages = Meeting::getNonTrashedAuthorizedArticleMessages($articleNumber);
    foreach($specificMessages as $message) { ?>
      <br />
      <center>
        <table>
	  <tr>
	    <td>
	      <div class="paperBoxTitle" style="margin: 0;">
	        <div class="floatRight">Ngày<!--Date:--> <?php print(date("d/M/Y h:i a", $message->getDate())); ?></div>
	        Thông báo từ ban tổ chức<!--Message from the Chair-->
		<br />
	      </div>
	    </td>
	  </tr>
	  <tr>
	    <td><div class="messageBox"><?php Tools::printHTMLbr($message->getContent()); ?></div></td>
	  </tr>
	</table>
      </center>
      <?php 
    } 


    ?>

    <h2>Thảo luận<!--Discussion--></h2>

    <?php 

    $result = Discussion::getQueryResultByArticleNumber($articleNumber);

    $firstNotSeenAlreadySet = false;
    while($dbrow = $result->fetchArray()) {
      $firstNotSeenAlreadySet = Discussion::printDiscussionElement($dbrow, $firstNotSeenAlreadySet, $currentReviewer->getLastVisitDate());
    }

    ?>

    <br />
    <?php 
    if(Tools::getCurrentPhase() != Tools::$DECISION) {?>
      <center>
        <form action="discuss_article.php?articleNumber=<?php print($articleNumber); ?>#firstNotSeen" method="post">
	  <table>
	    <tr>
	      <td><textarea name="newComment" rows="10" cols="80"></textarea></td>
	    </tr>
	  <tr>
	    <td class="center"><input type="submit" class="buttonLink bigButton" value="Đăng bình luận mới" /></td>
	  </tr>
	</table>
      </form>
    </center>
    <?php 
    } 
    ?>

  <h2>Đánh giá<!--Reviews--></h2>

  <div class="paperBox">
    <div class="paperBoxDetails">

      <?php /* Display a list of affected reviewers */ ?>
      Người đánh giá đã phân công:<!--Assigned Reviewers:-->
      <div class="nameList">
        <?php 
	$affectedReviewerNumbers = $article->getAssignedReviewers();
        if (count($affectedReviewerNumbers) == 0) {
	  print('<em>>Không có.'//None.
        	.'</em>');
	}
        foreach($affectedReviewerNumbers as $affectedReviewerNumber) {
	  $reviewer = Reviewer::getByReviewerNumber($affectedReviewerNumber);
	  Tools::printHTMLbr($reviewer->getFullName() . '(' . $reviewer->getEmail() . ")\n");
	} ?>	  
      </div>

      <?php /* Display the list of all the completed reviews concerning this article    */
        /* $assignements allready contains completed reviews, no need to fetch them again. */?>
      Đánh giá hoàn thành:<!--Completed Reviews:-->
      <div class="nameList">
      <?php if(count($assignements) == 0) {
        print('<em>Không có.'//None.
        	.'</em>');
      } else {
	foreach($assignements as $assignement) {
	  $reviewerNumber = $assignement->getReviewerNumber();
	  $reviewer = Reviewer::getByReviewerNumber($reviewerNumber);
	  Tools::printHTMLbr($reviewer->getFullName().'(' . $reviewer->getEmail() . ")\n"); 
	}
      } ?>
      </div>
      <?php /* End of paperBoxDetails */?>
    </div>
  </div>
  <?php 
  /* $assignements allready contains completed reviews, no need to fetch them again. */
  foreach($assignements as $assignement) {
    $reviewerNumber = $assignement->getReviewerNumber();
    $reviewer = Reviewer::getByReviewerNumber($reviewerNumber);
    $reviewReader = new ReviewReader();
    $reviewReader->createFromXMLFile($assignement);
    $reviewReader->printDetailsBox();
  }

  ?>

<?php }}}
else{
	echo("Error");
	} ?>
<?php include('footer.php'); ?>
