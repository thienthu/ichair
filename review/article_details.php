<?php 

/*
 * Copyright (C) 2006, 2007 Thomas Baigneres, Matthieu Finiasz
 *
 * This file is part of iChair.
 *
 * iChair is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 * 
 * iChair is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

?><?php $page_title='Chi Tiết Bài Viết';//Article Details';
include '../utils/tools.php';
include 'header_reduced.php';

if (!is_null($currentReviewer)){
$articleNumber = $_GET['articleNumber'];

/* Check that the reviewer is allowed to see the review for this paper */

$assignement = Assignement::getByNumbers($articleNumber, $currentReviewer->getReviewerNumber());

if((!is_null($assignement)) and ($assignement->getAssignementStatus() != Assignement::$BLOCKED)) {


$article = Article::getByArticleNumber($articleNumber);

/* if the article number was indeed an article, $article is not null */
if(!is_null($article)) {
  $article->printDetailsBox();
}

?>

<center>
<input type="submit" class="buttonLink bigButton" value="Đóng" onclick="javascript:window.close()" />
</center>

<?php }} ?>

<?php include('footer.php'); ?>
