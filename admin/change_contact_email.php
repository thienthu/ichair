<?php 

/*
 * Copyright (C) 2006, 2007 Thomas Baigneres, Matthieu Finiasz
 *
 * This file is part of iChair.
 *
 * iChair is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 * 
 * iChair is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

?><?php $page_title='Thay Đổi E-mail Liên Hệ';//'Change Contact e-mail';
include '../utils/tools.php';
include 'header.php';

/* Check if the admin has a password */
Tools::adminHasNoPassword();

/* Check if the admin has properly configured iChair */

$status = Tools::getAdminConfigStatus();
if($status != "") {
  print('<div class="ERRmessage">' . $status . '</div>');
  return false;
}

/* If we reach this point the config is ok */

?>

Vui lòng nhập mã bài gửi<!--Please type in the ID of the paper-->.

<form action="change_contact_email.php" method="post">  
<center> Mã<!--ID-->:&nbsp;<input class="id" name="id" type="text" size="35" /><br />
  <input type="submit" class="buttonLink bigButton" value="Xem" />
</center>
</form>

<?php 

/* Check the POST */

$id = Tools::readPost('id');
$submission = null;
if (Tools::isAnId($id)) {
  $submission = Submission::getByID($id);
}
if (is_null($submission)) {
  if($id != "") {
  ?>
  <div class="ERRmessage">
     Không tìm thấy bài gửi mã<!--We could not find any submission matching ID--> <i><?php Tools::printHTML(Tools::readPost('id'));?></i>. &nbsp;Vui lòng nhập lại<!--in our database. Please make sure you typed it correctly-->.
  </div>
  <?php } ?>
  </body>
  </html>
  <?php 
  return;
}									       

if(Tools::readPost('new_email') != "") {
  if(Tools::isValidEmail(Tools::readPost('new_email'))) {
    $oldemail = $submission->getContact();
    $submission->setContact(Tools::readPost('new_email'));
    $article = Article::getByArticleNumber($submission->getSubmissionNumber());
    if (!is_null($article)) {
      $article->setContact(Tools::readPost('new_email'));
    }
    Log::logAdminContact($submission, $oldemail);
    print('<div class="OKmessage">Cập nhật đỉa chỉ email thành công'.
//Contact e-mail succesfully updated.
'</div>');
  } else {
    print('<div class="ERRmessage">'.htmlentities(Tools::readPost('new_email'), ENT_COMPAT | ENT_HTML401, 'utf-8').' là địa chỉ email không hợp lệ.'
      //is not a valid e-mail.'
      .'</div>');
  }
}


$lastVersion = $submission->getLastVersion();

print('<div class="paperBox"><div class="paperBoxTitle">');
print('<div class="paperBoxNumber">Bài Gửi '//Submission ' 
  .  $submission->getSubmissionNumber() . '</div>Mã: '//ID:
  .'&nbsp;'.$id.'<br />&nbsp;</div>');
if ($submission->getIsWithdrawn()) {
  print("<div class=\"paperBoxDetailsWithdrawn\">\n");
} else {
  if ($submission->getIsCommitteeMember()) {
    print("<div class=\"paperBoxDetailsCommittee\">\n");
  } else {
    print("<div class=\"paperBoxDetails\">\n");
  }
}
$lastVersion->printLong();
$lastVersion->printShort();
?>
<center>
<form action="change_contact_email.php" method="post">
  <input type="hidden" name="id" value="<?php print($id); ?>" />
  <table>
    <tr>
      <td>
        Email cũ<!--Old Contact e-mail-->:
      </td>
      <td>
        <input type="text" name="old_email" size="50" value="<?php Tools::printHTML($submission->getContact()); ?>" readonly="readonly" />
      </td>
    </tr>
    <tr>
      <td>
        Email mới<!--New Contact e-mail-->:
      </td>
      <td>
        <input type="text" name="new_email" size="50" value="" />
      </td>
    </tr>
  </table>
  <input type="submit" class="buttonLink bigButton" value="Cập nhật" />
</form>
</center>
<?php 
print('</div></div>');


?>



</body>
</html>
