<?php 

/*
 * Copyright (C) 2006, 2007 Thomas Baigneres, Matthieu Finiasz
 *
 * This file is part of iChair.
 *
 * iChair is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 * 
 * iChair is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

?><?php 
$page_title = "Edit User";
include '../utils/tools.php';
include "header.php";

$reviewer = Reviewer::getByReviewerNumber(Tools::readPost('reviewerNumber'));
$reviewer->updateFromPost();
if($reviewer->getStatus() != "") {
?>
Lỗi sửa tài khoản người dùng. Xem chi tiết thông báo lỗi bên dưới.
<!--There was an error with your modifications. See the error message bellow for more details.-->
<div class="ERRmessage">
<?php Tools::printHTMLbr($reviewer->getStatus()); ?>
</div>
<form action="manage_users_edit.php" method="post">
  <input name="reviewerNumber" type="hidden" value="<?php Tools::printHTML(Tools::readPost('reviewerNumber')); ?>" />
  <input name="login" type="hidden" value="<?php Tools::printHTML(Tools::readPost('login')); ?>"  />
  <input name="fullName" type="hidden" value="<?php Tools::printHTML(Tools::readPost('fullName')); ?>"  />
  <input name="email" type="hidden" value="<?php Tools::printHTML(Tools::readPost('email')); ?>"  />
  <input name="group" type="hidden" value="<?php Tools::printHTML(Tools::readPost('group')); ?>" />
 <div class="floatRight">
  <input type="submit" class="buttonLink bigButton" value="Trở về" />
 </div>
</form>
<?php 
} else {
Log::logEditUser($reviewer);
Reviewer::writeHtpasswdFile(); 
?>
<div class="OKmessage">
<!--The user-->Cập nhât tài khoản người dùng <?php Tools::printHTML(Tools::readPost('login')); ?> <!--has been successfully updated in the database.-->thành công. 
</div> 
<form action="manage_users.php" method="post">
  <div class="floatRight">
    <input type="submit" class="buttonLink bigButton" value="Đồng ý" />
  </div>
</form>

<?php 
}
?>

</body>
</html>
