<?php 

/*
 * Copyright (C) 2006, 2007 Thomas Baigneres, Matthieu Finiasz
 *
 * This file is part of iChair.
 *
 * iChair is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 * 
 * iChair is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

?><?php $page_title="Xem Lại Mail";//'Mail Preview';
include('../utils/tools.php');
include('header_reduced.php');


$mailType = $_GET['mail'];

$exampleSubject;
$exampleBody;

if(($mailType == "submission") || ($mailType == "revision") || ($mailType == "withdrawal")) {

  $submission = new Submission();
  $submission->createDummy();

  $type = "";

  if($mailType == "submission") {
    $exampleSubject = Tools::getConfig("mail/submissionSubject");
    $exampleMessage = Tools::getConfig("mail/submissionMessage");
    $type = "S";
  } else if($mailType == "revision") {
    $exampleSubject = Tools::getConfig("mail/revisionSubject");
    $exampleMessage = Tools::getConfig("mail/revisionMessage");
    $type = "R";
  } else {
    $exampleSubject = Tools::getConfig("mail/withdrawSubject");
    $exampleMessage = Tools::getConfig("mail/withdrawMessage");
  }

  $exampleSubject = Tools::parseMailToSubmiters($exampleSubject, $submission, $type);
  $exampleMessage = Tools::parseMailToSubmiters($exampleMessage, $submission, $type);

} else if($mailType == "password") {

  $reviewer = new Reviewer();
  $reviewer->createDummy();
  $password = md5(time() . getmypid() . rand());
  $password = substr($password,4,16);
  $exampleSubject = Tools::getConfig("mail/passwordSubject");
  $exampleMessage = Tools::getConfig("mail/passwordMessage");
  $exampleSubject = Tools::parseMailToCommittee($exampleSubject, $reviewer);
  $exampleMessage = Tools::parseMailForPassword($exampleMessage, $reviewer, $password);



} else if(($mailType == "accept") ||($mailType == "reject") ||($mailType == "reports")) {

  $article = new Article();
  $article->createDummy();

  if($mailType == "accept"){
    $exampleSubject = Tools::getConfig("mail/massMailAcceptanceSubject");
    $exampleMessage = Tools::getConfig("mail/massMailAcceptanceMessage");
  } else if($mailType == "reject") {
    $exampleSubject = Tools::getConfig("mail/massMailRejectionSubject");
    $exampleMessage = Tools::getConfig("mail/massMailRejectionMessage");
  } else {
    $exampleSubject = Tools::getConfig("mail/massMailReportsSubject");
    $exampleMessage = Tools::getConfig("mail/massMailReportsMessage");
  }

  $exampleSubject = Tools::parseMailToAuthors($exampleSubject, $article);
  $exampleMessage = Tools::parseMailToAuthors($exampleMessage, $article);

} else if(($mailType == "preference") || ($mailType == "review") || ($mailType == "discuss") || ($mailType == "timeIsUp")) {

  $reviewer = new Reviewer();
  $reviewer->createDummy();

  if($mailType == "preference") {
    $exampleSubject = Tools::getConfig("mail/massMailPreferencesSubject");
    $exampleMessage = Tools::getConfig("mail/massMailPreferencesMessage");
  } else if($mailType == "review") {
    $exampleSubject = Tools::getConfig("mail/massMailReviewSubject");
    $exampleMessage = Tools::getConfig("mail/massMailReviewMessage");
  } else if($mailType == "discuss") {
    $exampleSubject = Tools::getConfig("mail/massMailDiscussSubject");
    $exampleMessage = Tools::getConfig("mail/massMailDiscussMessage");
  } else {
    $exampleSubject = Tools::getConfig("mail/massMailTimeIsUpSubject");
    $exampleMessage = Tools::getConfig("mail/massMailTimeIsUpMessage");
  }

  $exampleSubject = Tools::parseMailToCommittee($exampleSubject, $reviewer);
  $exampleMessage = Tools::parseMailToCommittee($exampleMessage, $reviewer);

}
?>

<form>
<center>
<table><tr><td>Chủ đề<!--Subject-->:</td></tr>
<tr><td><input type="text" size="50" readonly="readonly" value="<?php Tools::printHTML($exampleSubject);?>" /></td></tr>
<tr><td>Nội dung<!--Message Body-->:</td></tr>
<tr><td><textarea readonly="readonly" cols="80" rows="30"><?php Tools::printHTML($exampleMessage);?></textarea></td></tr></table>

<input type="submit" class="buttonLink bigButton" value="Đóng" onClick="javascript:window.close();"/>
</center>
</form>

<center>
  <h3>Accepted Tags</h3>
  <table>
    <?php 
    switch($mailType) {
      case "submission":
      case "revision":
      case "withdrawal":
        Tools::printMailToSubmitersTags();
        break;
      case "accept":
      case "reject":
      case "reports":
        Tools::printMailToAuthorsTags();
        break;
      case "password":
        Tools::printMailForPasswordTags(); 
      default:
        Tools::printMailToCommitteeTags(); 
    }
    Tools::printMailTags();
    ?>
  </table>
</center>


<?php include('w3c.php'); ?>
</body>
</html>
