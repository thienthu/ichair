<?php 

/*
 * Copyright (C) 2006, 2007 Thomas Baigneres, Matthieu Finiasz
 *
 * This file is part of iChair.
 *
 * iChair is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 * 
 * iChair is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

?><?php 

class MailTemplate {

  public static $CATEGORY_AUTHORS = "authors";
  public static $CATEGORY_COMMITTEE = "committee";
  
  public static $TO_ALL = "all";
  public static $TO_NONE = "none";
  public static $TO_LIST = "list";
  public static $TO_GROUP = "group";

  public static $ACCEPT='accept';
  public static $MAYBE_ACCEPT='maybe_accept';
  public static $DISCUSS='discuss';
  public static $MAYBE_REJECT='maybe_reject';
  public static $REJECT='reject';
  public static $VOID='void';
  public static $CUSTOM1='custom1';
  public static $CUSTOM2='custom2';
  public static $CUSTOM3='custom3';
  public static $CUSTOM4='custom4';
  public static $CUSTOM5='custom5';
  public static $CUSTOM6='custom6';
  public static $CUSTOM7='custom7';

  public static $CHAIR = "chair";
  public static $REVIEWER = "reviewer";
  public static $OBSERVER = "observer";

  public static $LISTS_THRESHOLD = 6;

  private $initialized = false;

  private $category;
  private $title;
  private $description;

  private $uncheck;

  private $toCommittee;
  private $toCommitteeList;
  private $toCommitteeGroup;

  private $toAuthors;
  private $toAuthorsList;
  private $toAuthorsGroup;

  private $subject;
  private $body;

  private $xmlFileName;
  private $xmlFilePath;

  private $date;

  function __construct(){}

  function createFromXML($file, $path) {

    if(file_exists($path . $file)) {

      $initialized = true;

      $mailTemplateDocument = new DOMDocument('1.0');
      $mailTemplateDocument->load($path . $file);
      $domxPath = new DOMXpath($mailTemplateDocument);
      
      $domNode = $domxPath->query("/xml/category")->item(0);
      if ($domNode->firstChild) {
      $this->category = utf8_decode(trim($domNode->firstChild->nodeValue));
      if(($this->category != MailTemplate::$CATEGORY_COMMITTEE) && ($this->category != MailTemplate::$CATEGORY_AUTHORS)) {
	$this->category = "";
      }
      }

      $domNode = $domxPath->query("/xml/title")->item(0);
      if ($domNode->firstChild) {
      $this->title = utf8_decode(trim($domNode->firstChild->nodeValue));
      }
  
      $domNode = $domxPath->query("/xml/description")->item(0);
      if ($domNode->firstChild) {
      $this->description = utf8_decode(trim($domNode->firstChild->nodeValue));
      }

      $domNode = $domxPath->query("/xml/uncheck")->item(0);
      if ($domNode->firstChild) {
        $this->uncheck = utf8_decode(trim($domNode->firstChild->nodeValue));
        if($this->uncheck != "yes") {
  	  $this->uncheck = "";
        }
      }

      $domNode = $domxPath->query("/xml/toCommittee")->item(0);
      if ($domNode->firstChild) {
       $this->toCommittee = utf8_decode(trim($domNode->firstChild->nodeValue));
        if(($this->toCommittee != MailTemplate::$TO_ALL) && ($this->toCommittee != MailTemplate::$TO_LIST) && ($this->toCommittee != MailTemplate::$TO_GROUP)) {
	  $this->toCommittee = MailTemplate::$TO_NONE;
        }
      }
      
      $domNode = $domxPath->query("/xml/toCommitteeList")->item(0);
      if ($domNode->firstChild) {
      $this->toCommitteeList = utf8_decode(trim($domNode->firstChild->nodeValue));
      if(!preg_match("/^[0-9]+(,[0-9]+)*$/", $this->toCommitteeList)) {
	$this->toCommitteeList = "";
      }
      }

      $domNode = $domxPath->query("/xml/toCommitteeGroup")->item(0);
      if ($domNode->firstChild) {
      $this->toCommitteeGroup = utf8_decode(trim($domNode->firstChild->nodeValue));
      if(!preg_match("/^(chair|reviewer|observer)(,(chair|reviewer|observer))*$/", $this->toCommitteeGroup)) {
	$this->toCommitteeGroup = "";
      }
      }

      $domNode = $domxPath->query("/xml/toAuthors")->item(0);
      if ($domNode->firstChild) {
      $this->toAuthors = utf8_decode(trim($domNode->firstChild->nodeValue));
      if(($this->toAuthors != MailTemplate::$TO_ALL) && ($this->toAuthors != MailTemplate::$TO_LIST) && ($this->toAuthors != MailTemplate::$TO_GROUP)) {
	$this->toAuthors = MailTemplate::$TO_NONE;
      }
      }
      
      $domNode = $domxPath->query("/xml/toAuthorsList")->item(0);
      if ($domNode->firstChild) {
      $this->toAuthorsList = utf8_decode(trim($domNode->firstChild->nodeValue));
      if(!preg_match("/^[0-9]+(,[0-9]+)*$/", $this->toAuthorsList)) {
	$this->toAuthorsList = "";
      }
      }
      
      $domNode = $domxPath->query("/xml/toAuthorsGroup")->item(0);
      if ($domNode && $domNode->firstChild) {
      $this->toAuthorsGroup = utf8_decode(trim($domNode->firstChild->nodeValue));
      if(!preg_match("/^(accept|maybe_accept|discuss|maybe_reject|reject|void|custom1|custom2|custom3|custom4|custom5|custom6|custom7)(,(accept|maybe_accept|discuss|maybe_reject|reject|void|custom1|custom2|custom3|custom4|custom5|custom6|custom7))*$/", $this->toAuthorsGroup)) {
	$this->toAuthorsGroup = "";
      }
      }
      
      $domNode = $domxPath->query("/xml/subject")->item(0);
      if ($domNode->firstChild) {
      $this->subject = utf8_decode(trim($domNode->firstChild->nodeValue));
      }

      $domNode = $domxPath->query("/xml/body")->item(0);
      if ($domNode->firstChild) {
      $this->body = utf8_decode(trim($domNode->firstChild->nodeValue));
      }

      $domNode = $domxPath->query("/xml/date")->item(0);
      if ($domNode->firstChild) {
      $this->date = utf8_decode(trim($domNode->firstChild->nodeValue));
      }
      
      $this->xmlFileName = $file;
      $this->xmlFilePath = $path;
      
    } 

  }

  function createFromPOST() {

    $this->initialized = true;
    
    $this->uncheck = Tools::readPost('uncheck');
    if($this->uncheck != "yes") {
      $this->uncheck = "";
    }
    
    $this->toCommittee = Tools::readPost('to_committee');
    if(($this->toCommittee != MailTemplate::$TO_ALL) && ($this->toCommittee != MailTemplate::$TO_LIST) && ($this->toCommittee != MailTemplate::$TO_GROUP)) {
      $this->toCommittee = MailTemplate::$TO_NONE;
    }

    $this->toCommitteeList = Tools::readPost('to_committee_list');
    if(!preg_match("/^[0-9]+(,[0-9]+)*$/", $this->toCommitteeList)) {
      $this->toCommitteeList = "";
    }

    $committeeGroup = "";
    if(Tools::readPost('to_chair') == 'yes') $committeeGroup .= ',chair';
    if(Tools::readPost('to_reviewer') == 'yes') $committeeGroup .= ',reviewer';
    if(Tools::readPost('to_observer') == 'yes') $committeeGroup .= ',observer';
    $this->toCommitteeGroup = substr($committeeGroup, 1);
    
    $this->toAuthors = Tools::readPost('to_authors');
    if(($this->toAuthors != MailTemplate::$TO_ALL) && ($this->toAuthors != MailTemplate::$TO_LIST) && ($this->toAuthors != MailTemplate::$TO_GROUP)) {
      $this->toAuthors = MailTemplate::$TO_NONE;
    }
    
    $this->toAuthorsList = Tools::readPost('to_authors_list');
    if(!preg_match("/^[0-9]+(,[0-9]+)*$/", $this->toAuthorsList)) {
      $this->toAuthorsList = "";
    }

    $authorsGroup = "";
    if(Tools::readPost('to_accept') == 'yes') $authorsGroup .= ',accept';
    if(Tools::readPost('to_maybe_accept') == 'yes') $authorsGroup .= ',maybe_accept';
    if(Tools::readPost('to_discuss') == 'yes') $authorsGroup .= ',discuss';
    if(Tools::readPost('to_maybe_reject') == 'yes') $authorsGroup .= ',maybe_reject';
    if(Tools::readPost('to_reject') == 'yes') $authorsGroup .= ',reject';
    if(Tools::readPost('to_void') == 'yes') $authorsGroup .= ',void';
    if(Tools::readPost('to_custom1') == 'yes') $authorsGroup .= ',custom1';
    if(Tools::readPost('to_custom2') == 'yes') $authorsGroup .= ',custom2';
    if(Tools::readPost('to_custom3') == 'yes') $authorsGroup .= ',custom3';
    if(Tools::readPost('to_custom4') == 'yes') $authorsGroup .= ',custom4';
    if(Tools::readPost('to_custom5') == 'yes') $authorsGroup .= ',custom5';
    if(Tools::readPost('to_custom6') == 'yes') $authorsGroup .= ',custom6';
    if(Tools::readPost('to_custom7') == 'yes') $authorsGroup .= ',custom7';
    $this->toAuthorsGroup = substr($authorsGroup, 1);

    
    $this->subject = trim(Tools::readPost('subject'));
    $this->body = trim(Tools::readPost('body'));
    
  }

  function isInitialized() {
    return $this->initialized;
  }

  function getCategory() {
    return $this->category;
  }

  function getTitle() {
    return $this->title;
  }

  function getDescription() {
    return $this->description;
  }

  function isUnchecked() {
    return ($this->uncheck == "yes");
  }

  function getToCommittee() {
    return $this->toCommittee;
  }

  function getToCommitteeList() {
    return $this->toCommitteeList;
  }

  function setToCommitteeList($reviewers) {
    if(count($reviewers) < MailTemplate::$LISTS_THRESHOLD) {
      $str = "";
      foreach($reviewers as $reviewer) {
	$str .= "," . $reviewer->getReviewerNumber();
      }
      $this->toCommitteeList = substr($str, 1);
    }
  }

  function getToCommitteeGroup() {
    return $this->toCommitteeGroup;
  }

  function getToAuthors() {
    return $this->toAuthors;
  }

  function getToAuthorsList() {
    return $this->toAuthorsList;
  }

  function setToAuthorsList($articles) {
    if(count($articles) < MailTemplate::$LISTS_THRESHOLD) {
      $str = "";
      foreach($articles as $article) {
	$str .= "," . $article->getArticleNumber();
      }
      $this->toAuthorsList = substr($str, 1);
    }
  }

  function getToAuthorsGroup() {
    return $this->toAuthorsGroup;
  }

  function getSubject() {
    return $this->subject;
  }

  function getBody() {
    return $this->body;
  }

  function getXMLFileName() {
    return $this->xmlFileName;
  }

  function getXMLFilePath() {
    return $this->xmlFilePath;
  }

  function getDate() {
    return $this->date;
  }

  function writeXML() {

    if(!file_exists(Tools::getConfig('server/logPath') . 'mailing/')) {
      mkdir(Tools::getConfig('server/logPath') . 'mailing/');
    } 
  
    $xmlDoc = new DOMDocument();
    $xmlDoc->formatOutput = true;
    $xmlDoc->encoding = "utf-8";
    
    $xml = $xmlDoc->createElement('xml'); 
    $xml = $xmlDoc->appendChild($xml);
    
    $date = $xmlDoc->createElement('date');
    $date = $xml->appendChild($date);
    $currentDate = date('U');
    $value = $xmlDoc->createTextNode($currentDate);
    $date->appendChild($value);

    $category = $xmlDoc->createElement('category');
    $category = $xml->appendChild($category);
    
    $title = $xmlDoc->createElement('title');
    $title = $xml->appendChild($title);
    $value = $xmlDoc->createTextNode(utf8_encode(date("d/m/Y H:i", $currentDate) . ' - ' . $this->subject));
    $title->appendChild($value);

    $description = $xmlDoc->createElement('description');
    $description = $xml->appendChild($description);

    $uncheck = $xmlDoc->createElement('uncheck');
    $uncheck = $xml->appendChild($uncheck);
    if($this->isUnchecked()) {
      $value = $xmlDoc->createTextNode('yes');
      $uncheck->appendChild($value);
    }
    
    $toCommittee = $xmlDoc->createElement('toCommittee');
    $toCommittee = $xml->appendChild($toCommittee);
    $value = $xmlDoc->createTextNode($this->toCommittee);
    $toCommittee->appendChild($value);
    
    $toCommitteeList = $xmlDoc->createElement('toCommitteeList');
    $toCommitteeList = $xml->appendChild($toCommitteeList);
    $value = $xmlDoc->createTextNode($this->toCommitteeList);
    $toCommitteeList->appendChild($value);

    $toCommitteeGroup = $xmlDoc->createElement('toCommitteeGroup');
    $toCommitteeGroup = $xml->appendChild($toCommitteeGroup);
    $value = $xmlDoc->createTextNode($this->toCommitteeGroup);
    $toCommitteeGroup->appendChild($value);
    
    $toAuthors = $xmlDoc->createElement('toAuthors');
    $toAuthors = $xml->appendChild($toAuthors);
    $value = $xmlDoc->createTextNode($this->toAuthors);
    $toAuthors->appendChild($value);
    
    $toAuthorsList = $xmlDoc->createElement('toAuthorsList');
    $toAuthorsList = $xml->appendChild($toAuthorsList);
    $value = $xmlDoc->createTextNode($this->toAuthorsList);
    $toAuthorsList->appendChild($value);
    
    $toAuthorsGroup = $xmlDoc->createElement('toAuthorsGroup');
    $toAuthorsGroup = $xml->appendChild($toAuthorsGroup);
    $value = $xmlDoc->createTextNode($this->toAuthorsGroup);
    $toAuthorsGroup->appendChild($value);

    $subject = $xmlDoc->createElement('subject');
    $subject = $xml->appendChild($subject);
    $value = $xmlDoc->createTextNode(utf8_encode($this->subject));
    $subject->appendChild($value);

    $body = $xmlDoc->createElement('body');
    $body = $xml->appendChild($body);
    $value = $xmlDoc->createTextNode(utf8_encode($this->body));
    $body->appendChild($value);

    return $xmlDoc->save(Tools::getConfig('server/logPath') . 'mailing/' . $currentDate . '.xml');
  
  }

}


?>
