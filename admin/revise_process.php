<?php 

/*
 * Copyright (C) 2006, 2007 Thomas Baigneres, Matthieu Finiasz
 *
 * This file is part of iChair.
 *
 * iChair is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 * 
 * iChair is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

include('../utils/tools.php');

$relative_url = "admin/revise_result.php";

/* Create a new object version */

$submission = new Submission();
$submission = Submission::getByID(Tools::readPost('id'));
$submission->reviseFromPOST();

if ($submission->status['isSuccessfull']) {
    
    Log::logAdminRevision($submission);
    $relative_url .= '?id=' . $submission->getId();
    header('Location: '.Tools::getConfig('server/location') . "/" . $relative_url);
    
    
} else { 
    
    include 'header.php';

    ?>

    <p>Lỗi sửa bài viết. Xem chi tiết lỗi bên dưới.
<!--There was an error with your revision. See the error message
    bellow for more details.--></p>

    <div class="ERRmessage">
      <?php Tools::printHTMLbr($submission->status['message']); ?>
    </div>

    <form action="revise_form.php" method="post" enctype="multipart/form-data">
      <input name="id" type="hidden" value="<?php Tools::printHTML($submission->getId()); ?>"  />
      <div class="floatRight">
	<input type="submit" class="buttonLink bigButton" value="Trở về" />
      </div>
    </form>
    <?php 
}

?>
</body>
</html>
