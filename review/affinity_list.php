<?php 

/*
 * Copyright (C) 2006, 2007 Thomas Baigneres, Matthieu Finiasz
 *
 * This file is part of iChair.
 *
 * iChair is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 * 
 * iChair is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

?><?php $page_title='Bài Viết Ưa Thích';//Preferred Articles';
include '../utils/tools.php';
include 'header.php';

if(!is_null($currentReviewer)) {

  if(Tools::getCurrentPhase() != Tools::$ELECTION) {
    print('<div class="OKmessage">Chức năng này không thể truy cập được trong giai đoạn hiện tại này.'//This page cannot be accessed during the current phase.
      .'</div>');
  } else {

    /* Update the number of articles displayed for the current reviewer */
    
    $numberOfArticlesPerPage = Tools::readPost('numberOfArticlesPerPage');
    if(preg_match("/^[1-9][0-9]*$/", $numberOfArticlesPerPage)) {
      $currentReviewer->setNumberOfArticlesPerPage($numberOfArticlesPerPage);
    }
    
    $numberOfArticlesPerPage = $currentReviewer->getNumberOfArticlesPerPage();
    
    $articleToDisplay = 1;
    $bozo = explode(" ",Tools::readPost('page'));
    if (preg_match("/^[0-9]+$/", $bozo[0])) {
      // a page button was clicked
      $articleToDisplay = $bozo[0];
    } else if (preg_match("/[0-9]+/", Tools::readPost('articleToDisplay'))) {
      $articleToDisplay = Tools::readPost('articleToDisplay');
    }

    $allCategories;
    $categoryToDisplay;
    if(Tools::useCategory()) {
      $allCategories = Tools::getAllCategories();
      $categoryToDisplay = key($allCategories);
      if(Tools::readPost('category') != '') {
	$categoryToDisplay = Tools::getCategoryIdByHtmlName(Tools::readPost('category'));
      } else if(Tools::readPost('categoryToDisplay') != '') {
	$categoryToDisplay = Tools::readPost('categoryToDisplay');
      } 
    }

    /* check posted sorting options */
    
    $sortKey = Tools::readPost('sortKey');
    switch($sortKey) {
    case 'articleNumber':
      $currentReviewer->setSortKeyStringForPreferences(Reviewer::$SORT_BY_ARTICLE_NUMBER_PREF);
      break;
    case 'category':
      $currentReviewer->setSortKeyStringForPreferences(Reviewer::$SORT_BY_CATEGORY);
      break;
    default:
      switch($currentReviewer->getSortKeyPreferred()) {
      case Reviewer::$SORT_BY_CATEGORY:
	$sortKey = 'category';
	break;
      default:
	$sortKey = 'articleNumber';
      }
    }

  /* Display a form allowing to change the number of article displayed on the same page */
  ?>

  <form action="affinity_list.php" method="post">

     <input type="hidden" name="articleToDisplay" value="<?php print($articleToDisplay);?>"/>
     <input type="hidden" name="categoryToDisplay" value="<?php Tools::printHTML($categoryToDisplay);?>"/>

    <div class="floatRight">
      <table><tr>
        <td><div class="versionAuthors">Số bài viết hiện trong mỗi trang:<!--Number of articles displayed per page:-->&nbsp;</div></td>
        <td><input type="text" name="numberOfArticlesPerPage" size="3" class="largeInput" value="<?php print($currentReviewer->getNumberOfArticlesPerPage()); ?>" /></td>
	<td><input type="submit" class="buttonLink" value="Đóng" /></td>
       </tr></table>
    </div>
    <div class="clear"></div>
  <?php 

  /* Display a banner allowing to choose how the page should be sorted */

  ?>
  <?php if(Tools::useCategory()) { ?>
  <div class="paperBoxDetails">
      <center>
      <input type="radio" class="noBorder" name="sortKey" id="sortKeyArticleNumber" value="articleNumber" <?php if($sortKey == 'articleNumber') { ?>checked="checked" <?php } ?>/>
      <label for="sortKeyArticleNumber">Sắp xếp theo số bài viết<!--Sort by Article Number-->
      </label>	        
      <input type="radio" class="noBorder" name="sortKey" id="sortKeyCategory" value="category" <?php if($sortKey == 'category') { ?>checked="checked" <?php } ?>/>
      <label for="sortKeyCategory">Sắp xếp theo thể loại<!--Sort by Category--></label>	        
      <br />
      <input type="submit" class="buttonLink bigButton" name="updateView" value="Xem" />
    </center>
  </div>
  <?php } 

  /* Get all not blocked articles */

  $articleNumbers = $currentReviewer->getNotBlockedArticles();


  /* Display the buttons */

  if($sortKey == 'category') {
    
    $i = 0;
    print('<center><table>');
    foreach($allCategories as $catid => $category) {
      if($i % 3 == 0) print('<tr>');
      if($catid == $categoryToDisplay) {
	print('<td style="text-align: center;"><input type="submit" class="buttonLink currentButton" name="category" value="'.htmlentities($category, ENT_COMPAT | ENT_HTML401, 'utf-8').'"></td>');
      } else {
	print('<td style="text-align: center;"><input type="submit" class="buttonLink" name="category" value="'.htmlentities($category, ENT_COMPAT | ENT_HTML401, 'utf-8').'"></td>');
      }
      if($i % 3 == 2) print('</tr>');
      $i++;
    }
    if($i % 3 != 0) print('</tr>');
    print('</table></center>');
    
  } else {
										
    for($btNbr=0; $btNbr<ceil(count($articleNumbers) / $numberOfArticlesPerPage); $btNbr++) {
      /* Display a form for button number $btNbr */
      $startNbr = $articleNumbers[$btNbr * $numberOfArticlesPerPage];
      $stopNbr = $articleNumbers[min(($btNbr+1) * $numberOfArticlesPerPage-1,count($articleNumbers)-1)];
	?>
	<div class="floatLeft">
	   <?php if($startNbr != $stopNbr) { ?>
 	   <?php if(($startNbr <= $articleToDisplay) && ($stopNbr >= $articleToDisplay)) { ?>
          <input type="submit" class="buttonLink currentButton" name="page" value="<?php print($startNbr . ' - ' . $stopNbr); ?>">
        <?php } else {?>
          <input type="submit" class="buttonLink" name="page" value="<?php print($startNbr . ' - ' . $stopNbr); ?>">
        <?php } ?>
      <?php } else { ?>
        <?php if($startNbr == $articleToDisplay) { ?>
          <input type="submit" class="buttonLink currentButton" name="page" value="<?php print($startNbr); ?>">
        <?php } else {?>
          <input type="submit" class="buttonLink" name="page" value="<?php print($startNbr); ?>">
        <?php } ?>
      <?php } ?>
    </div>
    <?php 
    }
  }

  print('<div class="clear"></div>');

  /* Process the $_POST variable if there is one */
  if ($currentReviewerGroup != Reviewer::$OBSERVER_GROUP) {

    $affinitiesSet = false;
    if(count($_POST) != 0) {
      foreach($articleNumbers as $articleNumber) {
	if(Tools::readPost($articleNumber . "") != "") {
	  $assignement = Assignement::getByNumbers($articleNumber, $currentReviewer->getReviewerNumber());
	  if(!is_null($assignement)) {
	    $affinityModified = (($assignement->getAffinity()) != Tools::readPost($articleNumber . ""));
	    if ($affinityModified) {
	      $assignement->setAffinity(Tools::readPost($articleNumber . ""));
	    }
	    $affinitiesSet |= $affinityModified;
	  }
	}
      }
      if($affinitiesSet) {
	Log::logAffinityUpdate($currentReviewer);
	print('<br /><div class="OKmessage">Bài viết ưa thích đã được cập nhật thành công.'//Your preferences have been updated successfully.
    .'</div>');
      }
    }
  }

  /* Check if the reviewer is not too early, i.e., if he is not blocked for all articles. In such a case, display a small error message */

  if(count($articleNumbers) == 0) {

    print('<div class="OKmessage">Không thể truy cập bất kỳ bài viết nào'//You do not have access to any article.
      .'</div>');

  } else {

  /* Display a list of the articles that the current reviewer can see = authorized + assigned articles */
    $totals = $currentReviewer->getAffinityTotals();

  ?>
  <center>
  <table class="dottedTable">
    <tr>
      <th class="topRow" colspan="3">&nbsp;</th>
      <th  class="topRow" colspan="3">Khả năng/Săn sàng để đánh giá<!--Ability/Willingness to Review--></th>
    </tr>
    <tr>
      <th class="topRow"> Số <br /> bài viết <!--Article<br />Number-->
      </th>
      <th class="topRow">&nbsp;</th>
      <th class="topRow leftAlign">
        <table class="leftAlign">
	  <tr>
	    <th class="topRow leftAlign">Tiêu đề<!--Title-->
      </th>
	  </tr><tr>
	    <th class="topRow leftAlign"><em><?php print(Article::getAvailableFieldTitle());?></em></th>
	  </tr>
	</table>
      </th>
      <th class="topRow"><div class="bigNumber voidReview"><?php print($totals[Assignement::$NOTWANTED]);?></div></th>
      <th class="topRow"><div class="bigNumber inProgressReview"><?php print($totals[Assignement::$DONTCARE]);?></div></th>
      <th class="topRow"><div class="bigNumber completedReview"><?php print($totals[Assignement::$WANTED]);?></div></th>
    </tr>
  <?php 

  $articleNumbers = "";
  if($sortKey == 'category') {
    $articleNumbers = $currentReviewer->getNotBlockedArticlesByCategory($categoryToDisplay);
  } else {
    $articleNumbers = $currentReviewer->getRangeOfNotBlockedArticles($articleToDisplay);
  }

  foreach($articleNumbers as $articleNumber) {
    $article = Article::getByArticleNumber($articleNumber);
    $assignement = Assignement::getByNumbers($articleNumber, $currentReviewer->getReviewerNumber());
    $affinity = 0;
    if(!is_null($assignement)) {
      $affinity = $assignement->getAffinity();
    }
    ?>
    <tr>
      <td><div class="bigNumber">&nbsp;<?php print($articleNumber); ?>&nbsp;</div></td>
      <td>
        <div class="popUp">
	  <a href="article_details.php?articleNumber=<?php print($articleNumber); ?>" target="_blank">
	    Chi tiết<!--Details-->
	  </a>
	  <div class="hidden"><?php $article->printDetailsBoxPopup(); ?></div>
	</div>
      </td>
      <td class="leftAlign">
        <table class="leftAlign">
	  <tr>
	    <td class="leftAlign"><?php Tools::printHTMLsubstr($article->getTitle(),40); ?></td>
	  </tr><tr>
	    <td class="leftAlign"><?php 
              if (Article::getAvailableFieldTitle() != ""){
		print('<em>');
		Tools::printHTMLsubstr(Article::getAvailableFieldTitle().': '.$article->getAvailableField(),40);
		print('</em>');
	      } else {
		print('<br/>');
	      } ?></td>
	  </tr>
	</table>
      </td>
      <td>
        <input class="noBorder" type="radio" id="n<?php print($articleNumber); ?>" name="<?php print($articleNumber); ?>" value="0" <?php if($affinity == 0) {print('checked="checked"');}?> />
	<label for="n<?php print($articleNumber); ?>">Không thể<!--Not able--></label>
      </td>
      <td>
        <input class="noBorder" type="radio" id="dc<?php print($articleNumber); ?>" name="<?php print($articleNumber); ?>" value="1" <?php if($affinity == 1) {print('checked="checked"');}?> />
	<label for="dc<?php print($articleNumber); ?>">Không thể / Không sẵn sàng<!--Able / Not Willing--></label>
      </td>
      <td>
        <input class="noBorder" type="radio" id="w<?php print($articleNumber); ?>" name="<?php print($articleNumber); ?>" value="2" <?php if($affinity == 2) {print('checked="checked"');}?> />
	<label for="w<?php print($articleNumber); ?>">Sẵn sàng<!--Willing--></label>
      </td>
    </tr>
    <?php 
  }
  ?>
  </table>
  <input type="submit" class="buttonLink bigButton" value="Cập nhât ưa thích" />
  </center>
  </form>
  <div class="bottomSpacer"></div>
<?php }}}  ?>

<?php include('footer.php'); ?>
