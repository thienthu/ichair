<?php 

/*
 * Copyright (C) 2006, 2007 Thomas Baigneres, Matthieu Finiasz
 *
 * This file is part of iChair.
 *
 * iChair is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 * 
 * iChair is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

?><?php 
include '../utils/tools.php';

$tmpfile = tmpfile();
$returnval;
$command="";

if ($_GET['format'] == "ps") {
  $command = "psselect -p1-1 ../utils/test.ps| gs -q -dTextAlphaBits=4 -dGraphicsAlphaBits=2 -sDEVICE=jpeg -dJPEGQ=75 -dBATCH -dNOPAUSE -dSAFER -r60 -sOutputFile=".escapeshellarg("" . $tmpfile)." -";
} else {
  $command = "gs -q -dTextAlphaBits=4 -dGraphicsAlphaBits=2 -sDEVICE=jpeg -dJPEGQ=75 -dLastPage=1 -dBATCH -dNOPAUSE -dSAFER -r60 -sOutputFile=".escapeshellarg("" . $tmpfile)." ../utils/test.pdf";
}

exec($command, $output, $returnVal);

if ($returnVal == 0) {
  header("Content-type: image/jpeg");
  @readfile("" . $tmpfile);
} else {
  header("Content-type: image/gif");
  @readfile("../images/nopreview.gif");
}
unlink("" . $tmpfile);
?>
