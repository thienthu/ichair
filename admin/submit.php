<?php 

/*
 * Copyright (C) 2006, 2007 Thomas Baigneres, Matthieu Finiasz
 *
 * This file is part of iChair.
 *
 * iChair is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 * 
 * iChair is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

?><?php $page_title='Gửi Bài Viết';//'Submission Form';
include("../utils/tools.php");
include("header.php");

/* Check if the admin has a password */
Tools::adminHasNoPassword();

/* Check if the admin has properly configured iChair */

$status = Tools::getAdminConfigStatus();
if($status != "") {
  print('<div class="ERRmessage">' . $status . '</div>');
  return false;
}

/* If we reach this point the config is ok */

?>

<p>Chức năng này chỉ để gửi bài mới<!--Use this form for new submissions only-->. Để chỉnh sửa bài gửi sử dụng chức năng có sẳn <!--A--><a href="revise.php">Sửa bài gửi<!--Revision Form-->
</a> <!--is available to modify a previous submission-->.</p>

<form action="submit_process.php" method="post" enctype="multipart/form-data">
  <h2>Thông Tin Bài Gửi<!--Submission Information--></h2>
  Tiêu đề<!--Title-->:            <br /><input name="title" type="text" size="80" value="<?php Tools::printHTML(Tools::readPost('title'));?>"/><br />
  <div id="authorsDiv">
  Tác giả<!--Authors--> (gồm tất cả tác giả tham gia bài viết<!--include ALL authors to appear in the final version-->):<a id="addAuthor" class="buttonLink" href="#">Thêm tác giả<!--Add an author--></a><br/>
  <?php
    if (array_key_exists('authors', $_POST)) {
      $posted_authors = $_POST['authors'];
    } else {
      $posted_authors = array('', '');
    }
    foreach($posted_authors as $j => $val) {
  ?>
  <div class="authorBox"><div class="authorBoxWidth">
  <a class="floatRight removeAuthor" href="#">Xóa bỏ tác giả<!--Remove author--></a>
  Họ tên<!--Full name-->:<br /><input name="authors[]" type="text" size="35"  value="<?php Tools::printHTML(Tools::readPost('authors', $j));?>"/>
  <?php if(Tools::useAffiliations()) {?>
    <br/>Tổ chức<!--Affiliation-->: <br /><input name="affiliations[]" type="text" size="35"  value="<?php Tools::printHTML(Tools::readPost('affiliations', $j));?>"/>
  <?php }?>
  <?php if(Tools::useCountry()) {?>
    <br/>Quốc gia<!--Country-->: <br /><?php Tools::printCountrySelect(Tools::readPost('country', $j)); ?>
  <?php } ?>
  </div></div>
  <?php } ?>
  </div>
  <div class="clear"></div>
  <table><tr><td>
  <input type="radio" class="noBorder" name="committeeMember" id="notCommittee" value="no"<?php 
    if (Tools::readPost('committeeMember') != "yes") {
      print(' checked="checked"');
    }
  ?> /></td><td><label for="notCommittee">Không có tác giả nào là thành viên của ban tổ chức hội thảo<!--None of the authors above is a member of the--> <?php Tools::printHTML(Tools::getConfig('conference/name'));?> <!--program committee-->.</label></td></tr><tr><td>
  <input type="radio" class="noBorder" name="committeeMember" id="yesCommittee" value="yes"<?php 
    if (Tools::readPost('committeeMember') == "yes") {
      print(' checked="checked"');
    }
  ?> /></td><td><label for="yesCommittee">Tác giả của bài viết thuộc ban tổ chức hội thảo<!--This paper involves a member of the--> <?php Tools::printHTML(Tools::getConfig('conference/name'));?> <!--program committee-->.</label></td></tr></table><br/>
  <?php if(Tools::useCustomCheck1()) {?>
  <table><tr><td>
    <input type="checkbox" class="noBorder" name="customCheck1" id="customCheck1" value="yes"<?php
      if (Tools::readPost('customCheck1') == "yes") {
        print(' checked="checked"');
      }
  ?> /></td><td><label for="customCheck1"><?php Tools::printHTML(Tools::getCustomCheck1(true)); ?></label>
  </td></tr></table><br />
  <?php }?>
  <?php if(Tools::useCustomCheck2()) {?>
  <table><tr><td>
    <input type="checkbox" class="noBorder" name="customCheck2" id="customCheck2" value="yes"<?php
      if (Tools::readPost('customCheck2') == "yes") {
        print(' checked="checked"');
      }
  ?> /></td><td><label for="customCheck2"><?php Tools::printHTML(Tools::getCustomCheck2(true)); ?></label>
  </td></tr></table><br />
  <?php }?>
  <?php if(Tools::useCustomCheck3()) {?>
  <table><tr><td>
    <input type="checkbox" class="noBorder" name="customCheck3" id="customCheck3" value="yes"<?php
      if (Tools::readPost('customCheck3') == "yes") {
        print(' checked="checked"');
      }
  ?> /></td><td><label for="customCheck3"><?php Tools::printHTML(Tools::getCustomCheck3(true)); ?></label>
  </td></tr></table><br />
  <?php }?>
 
Địa chỉ email tác giả<!--Email address of contact author-->
(Địa chỉ email phải chính xác vì nó sẽ không thể sửa đổi<!--be sure to type it correctly as you will never be able to modify it-->):<br /><input name="contact" type="text" size="80"  value="<?php Tools::printHTML(Tools::readPost('contact'));?>"/><br />
  <?php if(Tools::useAbstract()) {?>
    Tóm tắc<!--Abstract-->:<br />
    <textarea name="abstract" cols="80" rows="25"><?php Tools::printHTML(Tools::readPost('abstract')); ?></textarea><br />
  <?php }?>
  <?php if(Tools::useCategory() || Tools::useKeywords()) {?>
    <h2>Chủ Đề<!--Topic--></h2>
  <?php }?>
  <?php if(Tools::useCategory()) {?>
    Vui lòng chọn chủ đề phù hợp nhất từ danh sách bên dưới<!--Please select the most suitable topic from the following list-->:<br />
    <?php Tools::printCategories(Tools::readPost('category'),'category'); ?>
    <br />
  <?php }?>
  <?php if(Tools::useKeywords()) {?>
    Từ khoa (Keywords) (Chuổi giới hạn 80 ký tự<!--the size of this field is limited to 80 characters-->):<br />
    <input name="keywords" type="text" size="80" maxlength="80" value="<?php Tools::printHTML(Tools::readPost('keywords'));?>"/><br />
  <?php }?>
  <h2>Tập Tin<!--File--></h2>  
  Tải tập tin  lên<!--Upload your file--> (Giới hạn<!--limited to--> <?php print(ini_get('upload_max_filesize'));?>): <br /><input name="file" type="file" size="80" /><br />
  <input type="hidden" name="action" value="submit" />
  <input type="hidden" name="disclaimer" value="yes" />
  <input type="checkbox" class="noBorder" name="mailNotification" id="mailNotification" value="nomail" />
  <label for="mailNotification">Không gửi email thông báo cho tác giả.<!--Do NOT send a notification email to the contact author.--></label>
  <center>
  <input type="submit" class="buttonLink bigButton" value="Gửi bài viết" />
  </center>
</form>

</body>
</html>
