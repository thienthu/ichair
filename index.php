<?php 

/*
 * Copyright (C) 2006, 2007 Thomas Baigneres, Matthieu Finiasz
 *
 * This file is part of iChair.
 *
 * iChair is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 * 
 * iChair is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

?><?php $page_title='HƯỚNG DẪN';//Instructions;
include 'utils/tools.php';
if(!Tools::isConfigured()) {return;}
include 'header.php';

if(file_exists("utils/custom_index_content.part")) {
  readfile("utils/custom_index_content.part");
} else {

?>



<?php if($subHTML) { ?>
  <h2><a href="guidelines/guidelines.html" target="_blank">
Hướng dẫn gửi bài <!--Submission Guidelines -->
</a></h2>     

  <p> Danh sách các câu trả lời có sẳn gần nhất<!--A list of answers to the most frequently asked questions is available in
  the --><a href="guidelines/guidelines.html" target="_blank"> Hướng dẫn gửi bài <!--Submission Guidelines --></a> <!--page-->

  <?php if($subPDF && $subTXT) { ?>
    (also available in <a href="guidelines/guidelines.pdf" target="_blank">[pdf]</a> and <a href="guidelines/guidelines.txt" target="_blank">[text]</a>)
  <?php } else if($subPDF) { ?>
    (also available in <a href="guidelines/guidelines.pdf" target="_blank">[pdf]</a>)
  <?php } else if($subTXT) { ?>
    (also available in <a href="guidelines/guidelines.txt" target="_blank">[text]</a>)
  <?php } ?>
. Đọc kỹ hướng dẫn trước khi gửi bài.
  <!--. Be sure to read them before submitting your article. --></p>

<?php } else if($subPDF) { ?>

  <h2><a href="guidelines/guidelines.pdf" target="_blank"> Hướng dẫn gửi bài <!--Submission Guidelines --></a></h2>     

  <p>Danh sách các câu trả lời có sẳn gần nhất<!--A list of answers to the most frequently asked questions is available in
  the -->
 <a href="guidelines/guidelines.pdf" target="_blank"> Hướng dẫn gửi bài <!--Submission Guidelines --></a><!-- page-->

  <?php if($subTXT) { ?>
    (also available in <a href="guidelines/guidelines.txt" target="_blank">[text]</a>)
  <?php } ?>

  Đọc kỹ hướng dẫn trước khi gửi bài.
  <!--. Be sure to read them before submitting your article. --></p>

<?php } else if($subTXT) { ?>

  <h2><a href="guidelines/guidelines.txt" target="_blank">Hướng dẫn gửi bài <!--Submission Guidelines --></a></h2>     

  <p>Danh sách các câu trả lời có sẳn gần nhất<!--A list of answers to the most frequently asked questions is available in
  the --><a href="guidelines/guidelines.txt" target="_blank">Hướng dẫn gửi bài <!--Submission Guidelines --><</a><!-- page-->. 
   Đọc kỹ hướng dẫn trước khi gửi bài.
  <!--. Be sure to read them before submitting your article. --><</p>

<?php } ?>

<h2><a href="submit.php">Gửi Bài Viết Mới<!--Submitting a New File--></a></h2>

<p> Sử dụng chức năng <!--Use the --><a href="submit.php"> gửi bài <!--Submission Form--></a> để gửi bài mới. <!--to submit a new article.-->
Vui lòng cung cấp chính xác địa chỉ email bởi vị địa chỉ sẽ không thể thay đổi sau khi gửi bài. Bài gửi phải dưới dạng PostScript hoặc PDF. Ngoài ra dưới bất kỳ dưới dạng khác đều bị hệ thống từ chối. 
<!--Be sure to provide a valid email address for the contact author as it will
not be possible to change it afterwards. Your submission must be in
PostScript or PDF. Any other format will be rejected by the server.--></p>


<p>Vui lòng xem kỹ <!--Please have a look at the --> 

<?php if($cfpHTML) { ?>
  <a href="cfp/cfp.html" target="_blank">thông báo (Call for Papers)</a>
  <?php if($cfpPDF && $cfpTXT) { ?>
    (also available in <a href="cfp/cfp.pdf" target="_blank">[pdf]</a> and <a href="cfp/cfp.txt" target="_blank">[text]</a>)
  <?php } else if($cfpPDF) { ?>
    (also available in <a href="cfp/cfp.pdf" target="_blank">[pdf]</a>)
  <?php } else if($cfpTXT) { ?>
    (also available in <a href="cfp/cfp.txt" target="_blank">[text]</a>)
  <?php } ?>
<?php } else if($cfpPDF) { ?>
  <a href="cfp/cfp.pdf" target="_blank">thông báo (Call for Papers)</a>
  <?php if($cfpTXT) { ?>
    (also available in <a href="cfp/cfp.txt" target="_blank">[text]</a>)
  <?php } ?>
<?php } else if($cfpTXT) { ?>
  <a href="cfp/cfp.txt" target="_blank">thông báo (Call for Papers)</a>
<?php } else { ?>
 thông báo (Call for Papers)
<?php } ?>

và <!--and at the--> 

<?php if($subHTML) { ?>
  <a href="guidelines/guidelines.html" target="_blank">hướng dẫn gửi bài<!--Submission Guidelines--></a>
  <?php if($subPDF && $subTXT) { ?>
    (also available in <a href="guidelines/guidelines.pdf" target="_blank">[pdf]</a> and <a href="guidelines/guidelines.txt" target="_blank">[text]</a>)
  <?php } else if($subPDF) { ?>
    (also available in <a href="guidelines/guidelines.pdf" target="_blank">[pdf]</a>)
  <?php } else if($subTXT) { ?>
    (also available in <a href="guidelines/guidelines.txt" target="_blank">[text]</a>)
  <?php } ?>
<?php } else if($subPDF) { ?>
  <a href="guidelines/guidelines.pdf" target="_blank">hướng dẫn gửi bài<!--Submission Guidelines--></a>
  <?php if($subTXT) { ?>
    (also available in <a href="guidelines/guidelines.txt" target="_blank">[text]</a>)
  <?php } ?>
<?php } else if($subTXT) { ?>
  <a href="guidelines/guidelines.txt" target="_blank">hướng dẫn gửi bài<!--Submission Guidelines--></a>
<?php } else { ?>
  hướng dẫn gửi bài<!--Submission Guidelines-->
<?php } ?>
trước khi gửi.
<!--before submitting.--></p>

<h2><a href="revise.php">Sửa Lại Bài Đã Gửi<!--Modifying a Previous Submission--></a></h2>

<p>Bài viết có thể xem lại và sửa bất cứ khi nào với chức năng <!--At any time you can modify your submission with the --><a
href="revise.php">Xem lại bài viết<!--Revision Form--></a>. Khi sử dụng chức năng này cần cung cấp mã được cấp trong lần nộp bài đầu tiên và mã này cũng được gửi qua email. Bài gửi có thể sửa trươc thời gian hết giạn gửi bài. <!--You will need to provide the unique
ID which was given to you upon the first submission and was also
emailed to the contact author. You can do as many revisions as you want
before the submission deadline.--></p>

<h2><a href="withdraw.php">Hủy bài gửi<!--Withdrawing a Submission--></a></h2>

<p>Bài viết đã được gửi có thể hủy gửi với chức năng <!--You have the possibility to withdraw a submission from the conference
with the --><a href="withdraw.php">Hủy bài gửi<!--Withdrawal Form--></a>. Đối với việc hủy bài gửi, Cần cung cấp mã được cấp trong lần nộp bài đầu tiên và  mã này cũng được gửi qua email. <!--As for the revisions,
you will need to provide the unique ID which was given to you upon
the first submission and was also emailed to the contact author. Note that
once a paper has been withdrawn it will not be possible to
&quot;un-withdraw&quot; it: you will need to submit it again.-->Lưu ý: khi đã hủy bài gửi thì không thể phục hồi được chỉ có thể gưi lại bài viết mới.</p>

<center>
<table>
  <tr>
    <td>
      <img src="images/uarrow.gif" alt="" />
    </td>
    <td>
      <img src="images/rarrow.gif" alt=""  />
    </td>
  </tr>
  <tr>
    <td>
      <img src="images/larrow.gif" alt=""  />
    </td>
    <td>
      <img src="images/cross.gif" alt=""  />
    </td>
  </tr>
</table>
</center>

<?php } ?>
<?php include("w3c.php"); ?>
<?php include("footer.php"); ?>
