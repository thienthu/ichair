<?php 

/*
 * Copyright (C) 2006, 2007 Thomas Baigneres, Matthieu Finiasz
 *
 * This file is part of iChair.
 *
 * iChair is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 * 
 * iChair is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

?><?php 
$page_title = "Thiết lập mật khẩu";//"Generate and E-mail Password";
include '../utils/tools.php';
include "header.php";

$reviewer = Reviewer::getByReviewerNumber(Tools::readPost('reviewerNumber'));
if(!is_null($reviewer)) {
  if($reviewer->isPasswordSet()) {
    print("<div class=\"ERRmessage\">Tài khoản" 
	  . htmlentities($reviewer->getLogin(), ENT_COMPAT | ENT_HTML401, 'ISO-8859-1') .
	  " đã có mật khẩu.\nKhi thiết lập mật khẩu mới sẽ được ghi đè lên mậy khẩu cũ.</div>");
  }
  else {
    print("<div class=\"OKmessage\">Tài khoản " 
	  . htmlentities($reviewer->getLogin(), ENT_COMPAT | ENT_HTML401, 'ISO-8859-1') .
	  " chưa thiết lập mật khẩu.</div>");
  }
  ?>
  <center>
  <table>
  <tr>
  <td>
  <form action="manage_users.php" method="post">
    <div class="floatRight">
    <input type="submit" class="buttonLink bigButton" value="Trở về" />
    </div>
  </form>
  </td>
  <td>
  <form action="manage_users_password_result.php" method="post">
    <input name="reviewerNumber" type="hidden" value="<?php Tools::printHTML(Tools::readPost('reviewerNumber')); ?>" />
    <div class="floatRight">
    <input type="submit" class="buttonLink bigButton" value="Thiết lập mẩu khẩu" />
    </div>
  </form>
  </td>
  </tr>
  </table>
  </center>

<?php 
}
?>

</body>
</html>
