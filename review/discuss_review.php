<?php 

/*
 * Copyright (C) 2006, 2007 Thomas Baigneres, Matthieu Finiasz
 *
 * This file is part of iChair.
 *
 * iChair is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 * 
 * iChair is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

?><?php $page_title='Chi Tiết ';//Review Details';
include '../utils/tools.php';
include 'header_reduced.php';

$articleNumber = Tools::readPost('articleNumber');
$reviewerNumber = Tools::readPost('reviewerNumber');

/* Check that the reviewer is allowed to see this paper and that it is allowed to see reviews */

$assignement = Assignement::getByNumbers($articleNumber, $reviewerNumber);

if (!is_null($assignement)) {
  $isAllowedToViewOtherReviews = $currentReviewer->isAllowedToViewOtherReviewsForArticle(Article::getByArticleNumber($articleNumber));
  if (($assignement->getReviewStatus() == Assignement::$COMPLETED) and ($assignement->getAssignementStatus() != Assignement::$BLOCKED) and $isAllowedToViewOtherReviews) {
    $reviewReader = new ReviewReader();
    $reviewReader->createFromXMLFile($assignement);
    $reviewReader->printDetailsBox();
  }
}


?>

<center>
<input type="submit" class="buttonLink bigButton" value="Close this Window" onclick="javascript:window.close()" />
</center>



<?php include('footer.php'); ?>
